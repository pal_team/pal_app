﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreeScript
struct  TreeScript_t1745883499  : public MonoBehaviour_t1158329972
{
public:
	// System.String TreeScript::title
	String_t* ___title_2;
	// System.String TreeScript::content
	String_t* ___content_3;
	// UnityEngine.Canvas TreeScript::panelCanvas
	Canvas_t209405766 * ___panelCanvas_4;
	// UnityEngine.UI.Text TreeScript::editTitle
	Text_t356221433 * ___editTitle_5;
	// UnityEngine.UI.Text TreeScript::editContent
	Text_t356221433 * ___editContent_6;
	// System.Boolean TreeScript::panelOpen
	bool ___panelOpen_7;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(TreeScript_t1745883499, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}

	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(TreeScript_t1745883499, ___content_3)); }
	inline String_t* get_content_3() const { return ___content_3; }
	inline String_t** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(String_t* value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier(&___content_3, value);
	}

	inline static int32_t get_offset_of_panelCanvas_4() { return static_cast<int32_t>(offsetof(TreeScript_t1745883499, ___panelCanvas_4)); }
	inline Canvas_t209405766 * get_panelCanvas_4() const { return ___panelCanvas_4; }
	inline Canvas_t209405766 ** get_address_of_panelCanvas_4() { return &___panelCanvas_4; }
	inline void set_panelCanvas_4(Canvas_t209405766 * value)
	{
		___panelCanvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___panelCanvas_4, value);
	}

	inline static int32_t get_offset_of_editTitle_5() { return static_cast<int32_t>(offsetof(TreeScript_t1745883499, ___editTitle_5)); }
	inline Text_t356221433 * get_editTitle_5() const { return ___editTitle_5; }
	inline Text_t356221433 ** get_address_of_editTitle_5() { return &___editTitle_5; }
	inline void set_editTitle_5(Text_t356221433 * value)
	{
		___editTitle_5 = value;
		Il2CppCodeGenWriteBarrier(&___editTitle_5, value);
	}

	inline static int32_t get_offset_of_editContent_6() { return static_cast<int32_t>(offsetof(TreeScript_t1745883499, ___editContent_6)); }
	inline Text_t356221433 * get_editContent_6() const { return ___editContent_6; }
	inline Text_t356221433 ** get_address_of_editContent_6() { return &___editContent_6; }
	inline void set_editContent_6(Text_t356221433 * value)
	{
		___editContent_6 = value;
		Il2CppCodeGenWriteBarrier(&___editContent_6, value);
	}

	inline static int32_t get_offset_of_panelOpen_7() { return static_cast<int32_t>(offsetof(TreeScript_t1745883499, ___panelOpen_7)); }
	inline bool get_panelOpen_7() const { return ___panelOpen_7; }
	inline bool* get_address_of_panelOpen_7() { return &___panelOpen_7; }
	inline void set_panelOpen_7(bool value)
	{
		___panelOpen_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
