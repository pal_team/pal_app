﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Generate/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3787076049;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Generate/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3868338480 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Generate/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m4239949672 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Generate/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1363090004 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Generate/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1353901516 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Generate/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m4023550243 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Generate/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2142271433 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
