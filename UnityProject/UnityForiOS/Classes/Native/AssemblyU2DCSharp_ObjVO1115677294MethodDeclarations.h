﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ObjVO
struct ObjVO_t1115677294;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ObjVO::.ctor()
extern "C"  void ObjVO__ctor_m4169190289 (ObjVO_t1115677294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjVO::setTitle(System.String)
extern "C"  void ObjVO_setTitle_m3418548101 (ObjVO_t1115677294 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjVO::setContent(System.String)
extern "C"  void ObjVO_setContent_m4129050350 (ObjVO_t1115677294 * __this, String_t* ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ObjVO::getTitle()
extern "C"  String_t* ObjVO_getTitle_m627832088 (ObjVO_t1115677294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ObjVO::getContent()
extern "C"  String_t* ObjVO_getContent_m1391192527 (ObjVO_t1115677294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
