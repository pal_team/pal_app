﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// Info
struct Info_t2064034856;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjGenerator
struct  ObjGenerator_t2287658554  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ObjGenerator::prefab
	GameObject_t1756533147 * ___prefab_2;
	// System.String ObjGenerator::title
	String_t* ___title_3;
	// System.String ObjGenerator::content
	String_t* ___content_4;
	// UnityEngine.WWW ObjGenerator::itemsData
	WWW_t2919945039 * ___itemsData_5;
	// System.Boolean ObjGenerator::isRunning
	bool ___isRunning_6;
	// Info ObjGenerator::info
	Info_t2064034856 * ___info_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ObjGenerator::obj
	List_1_t1125654279 * ___obj_8;

public:
	inline static int32_t get_offset_of_prefab_2() { return static_cast<int32_t>(offsetof(ObjGenerator_t2287658554, ___prefab_2)); }
	inline GameObject_t1756533147 * get_prefab_2() const { return ___prefab_2; }
	inline GameObject_t1756533147 ** get_address_of_prefab_2() { return &___prefab_2; }
	inline void set_prefab_2(GameObject_t1756533147 * value)
	{
		___prefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_2, value);
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(ObjGenerator_t2287658554, ___title_3)); }
	inline String_t* get_title_3() const { return ___title_3; }
	inline String_t** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(String_t* value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier(&___title_3, value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(ObjGenerator_t2287658554, ___content_4)); }
	inline String_t* get_content_4() const { return ___content_4; }
	inline String_t** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(String_t* value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier(&___content_4, value);
	}

	inline static int32_t get_offset_of_itemsData_5() { return static_cast<int32_t>(offsetof(ObjGenerator_t2287658554, ___itemsData_5)); }
	inline WWW_t2919945039 * get_itemsData_5() const { return ___itemsData_5; }
	inline WWW_t2919945039 ** get_address_of_itemsData_5() { return &___itemsData_5; }
	inline void set_itemsData_5(WWW_t2919945039 * value)
	{
		___itemsData_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemsData_5, value);
	}

	inline static int32_t get_offset_of_isRunning_6() { return static_cast<int32_t>(offsetof(ObjGenerator_t2287658554, ___isRunning_6)); }
	inline bool get_isRunning_6() const { return ___isRunning_6; }
	inline bool* get_address_of_isRunning_6() { return &___isRunning_6; }
	inline void set_isRunning_6(bool value)
	{
		___isRunning_6 = value;
	}

	inline static int32_t get_offset_of_info_7() { return static_cast<int32_t>(offsetof(ObjGenerator_t2287658554, ___info_7)); }
	inline Info_t2064034856 * get_info_7() const { return ___info_7; }
	inline Info_t2064034856 ** get_address_of_info_7() { return &___info_7; }
	inline void set_info_7(Info_t2064034856 * value)
	{
		___info_7 = value;
		Il2CppCodeGenWriteBarrier(&___info_7, value);
	}

	inline static int32_t get_offset_of_obj_8() { return static_cast<int32_t>(offsetof(ObjGenerator_t2287658554, ___obj_8)); }
	inline List_1_t1125654279 * get_obj_8() const { return ___obj_8; }
	inline List_1_t1125654279 ** get_address_of_obj_8() { return &___obj_8; }
	inline void set_obj_8(List_1_t1125654279 * value)
	{
		___obj_8 = value;
		Il2CppCodeGenWriteBarrier(&___obj_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
