﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GyroCamera
struct GyroCamera_t3228729828;

#include "codegen/il2cpp-codegen.h"

// System.Void GyroCamera::.ctor()
extern "C"  void GyroCamera__ctor_m1396902201 (GyroCamera_t3228729828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCamera::Start()
extern "C"  void GyroCamera_Start_m396900657 (GyroCamera_t3228729828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCamera::Update()
extern "C"  void GyroCamera_Update_m1156475090 (GyroCamera_t3228729828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCamera::ResetGyroRotation()
extern "C"  void GyroCamera_ResetGyroRotation_m4143923043 (GyroCamera_t3228729828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
