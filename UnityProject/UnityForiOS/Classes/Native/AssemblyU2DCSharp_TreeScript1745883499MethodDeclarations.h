﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TreeScript
struct TreeScript_t1745883499;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TreeScript::.ctor()
extern "C"  void TreeScript__ctor_m3481356894 (TreeScript_t1745883499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeScript::OnMouseDown()
extern "C"  void TreeScript_OnMouseDown_m1610342334 (TreeScript_t1745883499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeScript::setTitle(System.String)
extern "C"  void TreeScript_setTitle_m3285513596 (TreeScript_t1745883499 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeScript::setContent(System.String)
extern "C"  void TreeScript_setContent_m3995843423 (TreeScript_t1745883499 * __this, String_t* ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TreeScript::getTitle()
extern "C"  String_t* TreeScript_getTitle_m226220833 (TreeScript_t1745883499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TreeScript::getContent()
extern "C"  String_t* TreeScript_getContent_m2641054676 (TreeScript_t1745883499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
