﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// TreeGenerator
struct TreeGenerator_t1945456447;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TreeVO
struct TreeVO_t3057640605;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DUnityScript_TreeGenerator1945456447.h"
#include "AssemblyU2DUnityScript_TreeGenerator1945456447MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices1910041954MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DUnityScript_TreeVO3057640605.h"
#include "AssemblyU2DUnityScript_TreeVO3057640605MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"

// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TreeGenerator::.ctor()
extern "C"  void TreeGenerator__ctor_m984034103 (TreeGenerator_t1945456447 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TreeGenerator::Start()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t TreeGenerator_Start_m864030355_MetadataUsageId;
extern "C"  void TreeGenerator_Start_m864030355 (TreeGenerator_t1945456447 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TreeGenerator_Start_m864030355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0017;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_1);
		VirtActionInvoker1< Il2CppObject * >::Invoke(6 /* System.Void TreeGenerator::GenerateTree(System.Object) */, __this, L_2);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0017:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TreeGenerator::Update()
extern "C"  void TreeGenerator_Update_m1366838998 (TreeGenerator_t1945456447 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TreeGenerator::GenerateTree(System.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3967547434;
extern const uint32_t TreeGenerator_GenerateTree_m3116463024_MetadataUsageId;
extern "C"  void TreeGenerator_GenerateTree_m3116463024 (TreeGenerator_t1945456447 * __this, Il2CppObject * ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TreeGenerator_GenerateTree_m3116463024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t1756533147 * V_3 = NULL;
	GameObject_t1756533147 * V_4 = NULL;
	{
		float L_0 = Random_Range_m2884721203(NULL /*static, unused*/, (-10.0f), (10.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Random_Range_m2884721203(NULL /*static, unused*/, (-10.0f), (10.0f), /*hidden argument*/NULL);
		V_1 = L_1;
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		float L_4 = V_0;
		float L_5 = V_1;
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, L_4, (((float)((float)0))), L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		GameObject_t1756533147 * L_8 = __this->get_treeObj_2();
		V_3 = L_8;
		GameObject_t1756533147 * L_9 = V_3;
		Vector3_t2243707580  L_10 = V_2;
		Quaternion_t4030073918  L_11 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_12 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_4 = L_12;
		GameObject_t1756533147 * L_13 = V_4;
		Il2CppObject * L_14 = ___i0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_15 = RuntimeServices_op_Addition_m3403021532(NULL /*static, unused*/, _stringLiteral3967547434, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Object_set_name_m4157836998(L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TreeGenerator::Main()
extern "C"  void TreeGenerator_Main_m456520686 (TreeGenerator_t1945456447 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TreeVO::.ctor()
extern "C"  void TreeVO__ctor_m3544254977 (TreeVO_t3057640605 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TreeVO::OnMouseDown()
extern "C"  void TreeVO_OnMouseDown_m1568564863 (TreeVO_t3057640605 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_panelOpen_3();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		__this->set_panelOpen_3((bool)1);
		Canvas_t209405766 * L_1 = __this->get_panelCanvas_2();
		NullCheck(L_1);
		Behaviour_set_enabled_m1796096907(L_1, (bool)1, /*hidden argument*/NULL);
		goto IL_0036;
	}

IL_0023:
	{
		__this->set_panelOpen_3((bool)0);
		Canvas_t209405766 * L_2 = __this->get_panelCanvas_2();
		NullCheck(L_2);
		Behaviour_set_enabled_m1796096907(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void TreeVO::Main()
extern "C"  void TreeVO_Main_m4235660574 (TreeVO_t3057640605 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
