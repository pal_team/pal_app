﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Transform
struct Transform_t3275118058;
// Info
struct Info_t2064034856;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateNote
struct  CreateNote_t925731540  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CreateNote::prefab
	GameObject_t1756533147 * ___prefab_2;
	// System.Boolean CreateNote::isRunning
	bool ___isRunning_3;
	// UnityEngine.Canvas CreateNote::writeForm
	Canvas_t209405766 * ___writeForm_4;
	// UnityEngine.UI.Text CreateNote::title
	Text_t356221433 * ___title_5;
	// UnityEngine.UI.Text CreateNote::content
	Text_t356221433 * ___content_6;
	// UnityEngine.Transform CreateNote::worlObj
	Transform_t3275118058 * ___worlObj_7;
	// Info CreateNote::info
	Info_t2064034856 * ___info_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CreateNote::obj
	List_1_t1125654279 * ___obj_9;

public:
	inline static int32_t get_offset_of_prefab_2() { return static_cast<int32_t>(offsetof(CreateNote_t925731540, ___prefab_2)); }
	inline GameObject_t1756533147 * get_prefab_2() const { return ___prefab_2; }
	inline GameObject_t1756533147 ** get_address_of_prefab_2() { return &___prefab_2; }
	inline void set_prefab_2(GameObject_t1756533147 * value)
	{
		___prefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_2, value);
	}

	inline static int32_t get_offset_of_isRunning_3() { return static_cast<int32_t>(offsetof(CreateNote_t925731540, ___isRunning_3)); }
	inline bool get_isRunning_3() const { return ___isRunning_3; }
	inline bool* get_address_of_isRunning_3() { return &___isRunning_3; }
	inline void set_isRunning_3(bool value)
	{
		___isRunning_3 = value;
	}

	inline static int32_t get_offset_of_writeForm_4() { return static_cast<int32_t>(offsetof(CreateNote_t925731540, ___writeForm_4)); }
	inline Canvas_t209405766 * get_writeForm_4() const { return ___writeForm_4; }
	inline Canvas_t209405766 ** get_address_of_writeForm_4() { return &___writeForm_4; }
	inline void set_writeForm_4(Canvas_t209405766 * value)
	{
		___writeForm_4 = value;
		Il2CppCodeGenWriteBarrier(&___writeForm_4, value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(CreateNote_t925731540, ___title_5)); }
	inline Text_t356221433 * get_title_5() const { return ___title_5; }
	inline Text_t356221433 ** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(Text_t356221433 * value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier(&___title_5, value);
	}

	inline static int32_t get_offset_of_content_6() { return static_cast<int32_t>(offsetof(CreateNote_t925731540, ___content_6)); }
	inline Text_t356221433 * get_content_6() const { return ___content_6; }
	inline Text_t356221433 ** get_address_of_content_6() { return &___content_6; }
	inline void set_content_6(Text_t356221433 * value)
	{
		___content_6 = value;
		Il2CppCodeGenWriteBarrier(&___content_6, value);
	}

	inline static int32_t get_offset_of_worlObj_7() { return static_cast<int32_t>(offsetof(CreateNote_t925731540, ___worlObj_7)); }
	inline Transform_t3275118058 * get_worlObj_7() const { return ___worlObj_7; }
	inline Transform_t3275118058 ** get_address_of_worlObj_7() { return &___worlObj_7; }
	inline void set_worlObj_7(Transform_t3275118058 * value)
	{
		___worlObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___worlObj_7, value);
	}

	inline static int32_t get_offset_of_info_8() { return static_cast<int32_t>(offsetof(CreateNote_t925731540, ___info_8)); }
	inline Info_t2064034856 * get_info_8() const { return ___info_8; }
	inline Info_t2064034856 ** get_address_of_info_8() { return &___info_8; }
	inline void set_info_8(Info_t2064034856 * value)
	{
		___info_8 = value;
		Il2CppCodeGenWriteBarrier(&___info_8, value);
	}

	inline static int32_t get_offset_of_obj_9() { return static_cast<int32_t>(offsetof(CreateNote_t925731540, ___obj_9)); }
	inline List_1_t1125654279 * get_obj_9() const { return ___obj_9; }
	inline List_1_t1125654279 ** get_address_of_obj_9() { return &___obj_9; }
	inline void set_obj_9(List_1_t1125654279 * value)
	{
		___obj_9 = value;
		Il2CppCodeGenWriteBarrier(&___obj_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
