﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_JSONObject1971882247.h"
#include "AssemblyU2DCSharp_JSONObject_Type1314578890.h"
#include "AssemblyU2DCSharp_JSONObject_AddJSONContents3850664647.h"
#include "AssemblyU2DCSharp_JSONObject_FieldNotFound865402053.h"
#include "AssemblyU2DCSharp_JSONObject_GetFieldResponse1259369279.h"
#include "AssemblyU2DCSharp_JSONObject_U3CBakeAsyncU3Ec__Ite1149809410.h"
#include "AssemblyU2DCSharp_JSONObject_U3CPrintAsyncU3Ec__Ite716304657.h"
#include "AssemblyU2DCSharp_JSONObject_U3CStringifyAsyncU3Ec4037879552.h"
#include "AssemblyU2DCSharp_JSONTemplates3006274921.h"
#include "AssemblyU2DCSharp_ClickTreeEvent925145888.h"
#include "AssemblyU2DCSharp_Generate664530809.h"
#include "AssemblyU2DCSharp_Generate_U3CStartU3Ec__Iterator03787076049.h"
#include "AssemblyU2DCSharp_ThrowNote1571054592.h"
#include "AssemblyU2DCSharp_TreeScript1745883499.h"
#include "AssemblyU2DCSharp_CameraAsBackground75902795.h"
#include "AssemblyU2DCSharp_GyroCamera3228729828.h"
#include "AssemblyU2DCSharp_Info2064034856.h"
#include "AssemblyU2DCSharp_ReadPanelController2513243910.h"
#include "AssemblyU2DCSharp_CreateNote925731540.h"
#include "AssemblyU2DCSharp_CreateNote_U3CcreateNoteU3Ec__It3242378972.h"
#include "AssemblyU2DCSharp_ObjGenerator2287658554.h"
#include "AssemblyU2DCSharp_ObjGenerator_U3CStartU3Ec__Itera2867974660.h"
#include "AssemblyU2DCSharp_ObjVO1115677294.h"
#include "AssemblyU2DCSharp_OnClickEvent1249826957.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_TreeGenerator1945456447.h"
#include "AssemblyU2DUnityScript_TreeVO3057640605.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1702[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (JSONObject_t1971882247), -1, sizeof(JSONObject_t1971882247_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1705[16] = 
{
	0,
	0,
	0,
	0,
	0,
	JSONObject_t1971882247_StaticFields::get_offset_of_WHITESPACE_5(),
	JSONObject_t1971882247::get_offset_of_type_6(),
	JSONObject_t1971882247::get_offset_of_list_7(),
	JSONObject_t1971882247::get_offset_of_keys_8(),
	JSONObject_t1971882247::get_offset_of_str_9(),
	JSONObject_t1971882247::get_offset_of_n_10(),
	JSONObject_t1971882247::get_offset_of_useInt_11(),
	JSONObject_t1971882247::get_offset_of_i_12(),
	JSONObject_t1971882247::get_offset_of_b_13(),
	0,
	JSONObject_t1971882247_StaticFields::get_offset_of_printWatch_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (Type_t1314578890)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1706[8] = 
{
	Type_t1314578890::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (AddJSONContents_t3850664647), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (FieldNotFound_t865402053), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (GetFieldResponse_t1259369279), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (U3CBakeAsyncU3Ec__Iterator0_t1149809410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[6] = 
{
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24locvar0_0(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U3CsU3E__0_1(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24this_2(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24current_3(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24disposing_4(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (U3CPrintAsyncU3Ec__Iterator1_t716304657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[9] = 
{
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U3CbuilderU3E__0_0(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_pretty_1(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24locvar0_2(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U3CeU3E__1_3(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24locvar1_4(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24this_5(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24current_6(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24disposing_7(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (U3CStringifyAsyncU3Ec__Iterator2_t4037879552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[19] = 
{
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_depth_0(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar0_1(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_builder_2(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_pretty_3(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CiU3E__0_4(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CkeyU3E__1_5(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CobjU3E__2_6(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar1_7(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CeU3E__3_8(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar2_9(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CiU3E__4_10(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar3_11(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CeU3E__5_12(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar4_13(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24this_14(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24current_15(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24disposing_16(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CU24U3Edepth_17(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24PC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (JSONTemplates_t3006274921), -1, sizeof(JSONTemplates_t3006274921_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1713[1] = 
{
	JSONTemplates_t3006274921_StaticFields::get_offset_of_touched_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (ClickTreeEvent_t925145888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (Generate_t664530809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[7] = 
{
	Generate_t664530809::get_offset_of_prefab_2(),
	Generate_t664530809::get_offset_of_tree_3(),
	Generate_t664530809::get_offset_of_obj_4(),
	Generate_t664530809::get_offset_of_title_5(),
	Generate_t664530809::get_offset_of_content_6(),
	Generate_t664530809::get_offset_of_itemsData_7(),
	Generate_t664530809::get_offset_of_isRunning_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (U3CStartU3Ec__Iterator0_t3787076049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[8] = 
{
	U3CStartU3Ec__Iterator0_t3787076049::get_offset_of_U3Cutf8U3E__0_0(),
	U3CStartU3Ec__Iterator0_t3787076049::get_offset_of_U3Cutf8byteU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3787076049::get_offset_of_U3Cutf8stringU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3787076049::get_offset_of_U3CjsonU3E__3_3(),
	U3CStartU3Ec__Iterator0_t3787076049::get_offset_of_U24this_4(),
	U3CStartU3Ec__Iterator0_t3787076049::get_offset_of_U24current_5(),
	U3CStartU3Ec__Iterator0_t3787076049::get_offset_of_U24disposing_6(),
	U3CStartU3Ec__Iterator0_t3787076049::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (ThrowNote_t1571054592), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (TreeScript_t1745883499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[6] = 
{
	TreeScript_t1745883499::get_offset_of_title_2(),
	TreeScript_t1745883499::get_offset_of_content_3(),
	TreeScript_t1745883499::get_offset_of_panelCanvas_4(),
	TreeScript_t1745883499::get_offset_of_editTitle_5(),
	TreeScript_t1745883499::get_offset_of_editContent_6(),
	TreeScript_t1745883499::get_offset_of_panelOpen_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (CameraAsBackground_t75902795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[3] = 
{
	CameraAsBackground_t75902795::get_offset_of_image_2(),
	CameraAsBackground_t75902795::get_offset_of_cam_3(),
	CameraAsBackground_t75902795::get_offset_of_arf_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (GyroCamera_t3228729828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[5] = 
{
	GyroCamera_t3228729828::get_offset_of_gyro_2(),
	GyroCamera_t3228729828::get_offset_of_gyroSupported_3(),
	GyroCamera_t3228729828::get_offset_of_rotFix_4(),
	GyroCamera_t3228729828::get_offset_of_worldObj_5(),
	GyroCamera_t3228729828::get_offset_of_startY_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (Info_t2064034856), -1, sizeof(Info_t2064034856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1721[5] = 
{
	Info_t2064034856_StaticFields::get_offset_of_instance_0(),
	Info_t2064034856::get_offset_of_objList_1(),
	Info_t2064034856::get_offset_of_UserId_2(),
	Info_t2064034856::get_offset_of_latitude_3(),
	Info_t2064034856::get_offset_of_longitude_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (ReadPanelController_t2513243910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[3] = 
{
	ReadPanelController_t2513243910::get_offset_of_panelCanvas_2(),
	ReadPanelController_t2513243910::get_offset_of_title_3(),
	ReadPanelController_t2513243910::get_offset_of_content_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (CreateNote_t925731540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[8] = 
{
	CreateNote_t925731540::get_offset_of_prefab_2(),
	CreateNote_t925731540::get_offset_of_isRunning_3(),
	CreateNote_t925731540::get_offset_of_writeForm_4(),
	CreateNote_t925731540::get_offset_of_title_5(),
	CreateNote_t925731540::get_offset_of_content_6(),
	CreateNote_t925731540::get_offset_of_worlObj_7(),
	CreateNote_t925731540::get_offset_of_info_8(),
	CreateNote_t925731540::get_offset_of_obj_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (U3CcreateNoteU3Ec__Iterator0_t3242378972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[13] = 
{
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3CformU3E__0_0(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3CheadersU3E__1_1(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3CrawDataU3E__2_2(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3CurlU3E__3_3(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3CwwwU3E__4_4(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3Cutf8U3E__5_5(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3Cutf8byteU3E__6_6(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3Cutf8stringU3E__7_7(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U3CjsonU3E__8_8(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U24this_9(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U24current_10(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U24disposing_11(),
	U3CcreateNoteU3Ec__Iterator0_t3242378972::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (ObjGenerator_t2287658554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[7] = 
{
	ObjGenerator_t2287658554::get_offset_of_prefab_2(),
	ObjGenerator_t2287658554::get_offset_of_title_3(),
	ObjGenerator_t2287658554::get_offset_of_content_4(),
	ObjGenerator_t2287658554::get_offset_of_itemsData_5(),
	ObjGenerator_t2287658554::get_offset_of_isRunning_6(),
	ObjGenerator_t2287658554::get_offset_of_info_7(),
	ObjGenerator_t2287658554::get_offset_of_obj_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (U3CStartU3Ec__Iterator0_t2867974660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[12] = 
{
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U3CformU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U3CheadersU3E__1_1(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U3CrawDataU3E__2_2(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U3CurlU3E__3_3(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U3Cutf8U3E__4_4(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U3Cutf8byteU3E__5_5(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U3Cutf8stringU3E__6_6(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U3CjsonU3E__7_7(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U24this_8(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U24current_9(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U24disposing_10(),
	U3CStartU3Ec__Iterator0_t2867974660::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (ObjVO_t1115677294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[2] = 
{
	ObjVO_t1115677294::get_offset_of_title_2(),
	ObjVO_t1115677294::get_offset_of_content_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (OnClickEvent_t1249826957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[5] = 
{
	OnClickEvent_t1249826957::get_offset_of_panelCanvas_2(),
	OnClickEvent_t1249826957::get_offset_of_panelOpen_3(),
	OnClickEvent_t1249826957::get_offset_of_title_4(),
	OnClickEvent_t1249826957::get_offset_of_content_5(),
	OnClickEvent_t1249826957::get_offset_of_obj_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1729[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (TreeGenerator_t1945456447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[1] = 
{
	TreeGenerator_t1945456447::get_offset_of_treeObj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (TreeVO_t3057640605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[2] = 
{
	TreeVO_t3057640605::get_offset_of_panelCanvas_2(),
	TreeVO_t3057640605::get_offset_of_panelOpen_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
