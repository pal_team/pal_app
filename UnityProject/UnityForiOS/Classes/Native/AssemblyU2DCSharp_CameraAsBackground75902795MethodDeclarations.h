﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraAsBackground
struct CameraAsBackground_t75902795;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraAsBackground::.ctor()
extern "C"  void CameraAsBackground__ctor_m2481726058 (CameraAsBackground_t75902795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAsBackground::Start()
extern "C"  void CameraAsBackground_Start_m2831094006 (CameraAsBackground_t75902795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAsBackground::Update()
extern "C"  void CameraAsBackground_Update_m1267164179 (CameraAsBackground_t75902795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
