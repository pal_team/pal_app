﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1079476942;
// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t3114550109;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraAsBackground
struct  CameraAsBackground_t75902795  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.RawImage CameraAsBackground::image
	RawImage_t2749640213 * ___image_2;
	// UnityEngine.WebCamTexture CameraAsBackground::cam
	WebCamTexture_t1079476942 * ___cam_3;
	// UnityEngine.UI.AspectRatioFitter CameraAsBackground::arf
	AspectRatioFitter_t3114550109 * ___arf_4;

public:
	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(CameraAsBackground_t75902795, ___image_2)); }
	inline RawImage_t2749640213 * get_image_2() const { return ___image_2; }
	inline RawImage_t2749640213 ** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(RawImage_t2749640213 * value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier(&___image_2, value);
	}

	inline static int32_t get_offset_of_cam_3() { return static_cast<int32_t>(offsetof(CameraAsBackground_t75902795, ___cam_3)); }
	inline WebCamTexture_t1079476942 * get_cam_3() const { return ___cam_3; }
	inline WebCamTexture_t1079476942 ** get_address_of_cam_3() { return &___cam_3; }
	inline void set_cam_3(WebCamTexture_t1079476942 * value)
	{
		___cam_3 = value;
		Il2CppCodeGenWriteBarrier(&___cam_3, value);
	}

	inline static int32_t get_offset_of_arf_4() { return static_cast<int32_t>(offsetof(CameraAsBackground_t75902795, ___arf_4)); }
	inline AspectRatioFitter_t3114550109 * get_arf_4() const { return ___arf_4; }
	inline AspectRatioFitter_t3114550109 ** get_address_of_arf_4() { return &___arf_4; }
	inline void set_arf_4(AspectRatioFitter_t3114550109 * value)
	{
		___arf_4 = value;
		Il2CppCodeGenWriteBarrier(&___arf_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
