﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReadPanelController
struct  ReadPanelController_t2513243910  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Canvas ReadPanelController::panelCanvas
	Canvas_t209405766 * ___panelCanvas_2;
	// UnityEngine.UI.Text ReadPanelController::title
	Text_t356221433 * ___title_3;
	// UnityEngine.UI.Text ReadPanelController::content
	Text_t356221433 * ___content_4;

public:
	inline static int32_t get_offset_of_panelCanvas_2() { return static_cast<int32_t>(offsetof(ReadPanelController_t2513243910, ___panelCanvas_2)); }
	inline Canvas_t209405766 * get_panelCanvas_2() const { return ___panelCanvas_2; }
	inline Canvas_t209405766 ** get_address_of_panelCanvas_2() { return &___panelCanvas_2; }
	inline void set_panelCanvas_2(Canvas_t209405766 * value)
	{
		___panelCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___panelCanvas_2, value);
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(ReadPanelController_t2513243910, ___title_3)); }
	inline Text_t356221433 * get_title_3() const { return ___title_3; }
	inline Text_t356221433 ** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(Text_t356221433 * value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier(&___title_3, value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(ReadPanelController_t2513243910, ___content_4)); }
	inline Text_t356221433 * get_content_4() const { return ___content_4; }
	inline Text_t356221433 ** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(Text_t356221433 * value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier(&___content_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
