﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ObjGenerator/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t2867974660;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ObjGenerator/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3369458555 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ObjGenerator/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2711278237 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ObjGenerator/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4243978025 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ObjGenerator/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1485435745 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjGenerator/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3539632498 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjGenerator/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1921155132 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
