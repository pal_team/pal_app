﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TreeGenerator
struct TreeGenerator_t1945456447;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void TreeGenerator::.ctor()
extern "C"  void TreeGenerator__ctor_m984034103 (TreeGenerator_t1945456447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeGenerator::Start()
extern "C"  void TreeGenerator_Start_m864030355 (TreeGenerator_t1945456447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeGenerator::Update()
extern "C"  void TreeGenerator_Update_m1366838998 (TreeGenerator_t1945456447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeGenerator::GenerateTree(System.Object)
extern "C"  void TreeGenerator_GenerateTree_m3116463024 (TreeGenerator_t1945456447 * __this, Il2CppObject * ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeGenerator::Main()
extern "C"  void TreeGenerator_Main_m456520686 (TreeGenerator_t1945456447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
