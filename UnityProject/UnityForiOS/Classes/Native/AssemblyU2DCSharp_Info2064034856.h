﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Info
struct Info_t2064034856;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Info
struct  Info_t2064034856  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Info::objList
	List_1_t1125654279 * ___objList_1;
	// System.String Info::UserId
	String_t* ___UserId_2;
	// System.Single Info::latitude
	float ___latitude_3;
	// System.Single Info::longitude
	float ___longitude_4;

public:
	inline static int32_t get_offset_of_objList_1() { return static_cast<int32_t>(offsetof(Info_t2064034856, ___objList_1)); }
	inline List_1_t1125654279 * get_objList_1() const { return ___objList_1; }
	inline List_1_t1125654279 ** get_address_of_objList_1() { return &___objList_1; }
	inline void set_objList_1(List_1_t1125654279 * value)
	{
		___objList_1 = value;
		Il2CppCodeGenWriteBarrier(&___objList_1, value);
	}

	inline static int32_t get_offset_of_UserId_2() { return static_cast<int32_t>(offsetof(Info_t2064034856, ___UserId_2)); }
	inline String_t* get_UserId_2() const { return ___UserId_2; }
	inline String_t** get_address_of_UserId_2() { return &___UserId_2; }
	inline void set_UserId_2(String_t* value)
	{
		___UserId_2 = value;
		Il2CppCodeGenWriteBarrier(&___UserId_2, value);
	}

	inline static int32_t get_offset_of_latitude_3() { return static_cast<int32_t>(offsetof(Info_t2064034856, ___latitude_3)); }
	inline float get_latitude_3() const { return ___latitude_3; }
	inline float* get_address_of_latitude_3() { return &___latitude_3; }
	inline void set_latitude_3(float value)
	{
		___latitude_3 = value;
	}

	inline static int32_t get_offset_of_longitude_4() { return static_cast<int32_t>(offsetof(Info_t2064034856, ___longitude_4)); }
	inline float get_longitude_4() const { return ___longitude_4; }
	inline float* get_address_of_longitude_4() { return &___longitude_4; }
	inline void set_longitude_4(float value)
	{
		___longitude_4 = value;
	}
};

struct Info_t2064034856_StaticFields
{
public:
	// Info Info::instance
	Info_t2064034856 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(Info_t2064034856_StaticFields, ___instance_0)); }
	inline Info_t2064034856 * get_instance_0() const { return ___instance_0; }
	inline Info_t2064034856 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(Info_t2064034856 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
