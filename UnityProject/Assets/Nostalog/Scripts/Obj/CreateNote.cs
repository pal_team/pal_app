﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;

public class CreateNote : MonoBehaviour {
	
	public GameObject noteModel;
	public GameObject mainCamera;

	private bool isRunning;
//	public Canvas writeForm;
	public Text content;
	public Transform worlObj;

//	private Info info = Info.instance;
	private List<GameObject> obj;
	private const int distance = 8;

	public IEnumerator createNote(){
		obj = Info.instance.getObjList();
		if (getTextForm()) {
			isRunning = true;
			WWWForm form = new WWWForm ();
			form.AddField ("id", Info.instance.getUserId ());
			form.AddField ("lat", Info.instance.getLatitude ().ToString ());
			form.AddField ("lng", Info.instance.getLongitude ().ToString ());
			form.AddField ("content", content.text);
			Dictionary<string, string> headers = form.headers;
			headers ["content-type"] = "application/json";

			byte[] rawData = form.data;

//			string url = "http://13.124.11.80:9000/createNote";
			string url = "http://192.168.1.37:9000/createNote";
			WWW www = new WWW (url, rawData, headers);
			yield return www;



			Encoding utf8 = Encoding.UTF8;
			//		string itemsDataString = itemsData.text;
			byte[] utf8byte = utf8.GetBytes(www.text);
			string utf8string = utf8.GetString(utf8byte);
		
			JSONObject json = new JSONObject (utf8string);

			if(json != null && json.list [0].str.Equals("success")){
				string date = json.list[1].list[0].str;
			

//			if(json != null && json.list [0].str.Equals ("success")){

				float rotY = Camera.main.transform.eulerAngles.y;

				float posX = Mathf.Sin (rotY*Mathf.PI/180) * distance;
				float posZ = Mathf.Cos (rotY*Mathf.PI/180) * distance;
				Vector3 position = new Vector3 (posX, 10, posZ);
				position += transform.position;
				obj.Add(Instantiate(noteModel, position, Random.rotation));

				obj[obj.Count-1].GetComponent<ObjVO> ().setId (Info.instance.getUserId ());
				obj[obj.Count-1].GetComponent<ObjVO> ().setContent (content.text);
				obj[obj.Count-1].GetComponent<ObjVO> ().setIndex (obj.Count - 1);
				obj[obj.Count-1].GetComponent<ObjVO> ().setDate (date);
			}

			isRunning = false;
//			writeForm.enabled = false;
		}

	}
	private bool getTextForm(){
//		if (title.text.Length < 3) {
//			title.text = "Title will be at least 3 letter";
//			return false;
//		}
//		if (content.text.Length < 5) {
//			title.text = "Content will be at least 5 letter";
//			return false;
//		}
		return true;
	}

//	public void toggleBtn(){
//		writeForm.enabled = !writeForm.enabled;
//	}

	public void createBtn(){
		if (!isRunning) {
			StartCoroutine (createNote());
		}
	}
}
