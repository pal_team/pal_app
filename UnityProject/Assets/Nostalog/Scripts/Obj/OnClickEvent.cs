﻿using UnityEngine;
using UnityEngine.UI;
using UITween;
using System.Collections;
using System.Collections.Generic;

public class OnClickEvent : MonoBehaviour {
//	public Canvas readCanvas;

	[SerializeField] private GameObject readCanvas;
	[SerializeField] private GameObject mainButton;
	[SerializeField] private GameObject deleteButton;
	[SerializeField] private GameObject followButton;

//	public bool panelOpen;
	[SerializeField] private Text id;
	[SerializeField] private Text date;
	[SerializeField] private Text content;
	private bool activeFlag = false;
	private ObjVO obj;
	private WWW itemsData;
	private bool isRunning;


	public void Start(){
		obj = gameObject.GetComponent<ObjVO> ();
	}

	public void OnMouseDown(){
//		title = GameObject.FindGameObjectWithTag ("ReadModalTitle").GetComponent<Text>();
//		content = GameObject.FindGameObjectWithTag ("ReadModalContent").GetComponent<Text>();

		id.text = obj.getId();
		date.text = obj.getDate ();
		content.text = obj.getContent ();
		if (id.text != Info.instance.getUserId ()) {
			Debug.Log ("남의 쪽지");
			deleteButton.SetActive (false);
			followButton.SetActive (true);
		} else {
			Debug.Log ("내쪽지");
			deleteButton.SetActive (true);
			followButton.SetActive (false);
		}
//		if (!activeFlag) {
			readCanvas.SetActive (true);
			mainButton.SetActive(false);
//			activeFlag = true;
//		}
//		panelOpen = true;

//		readCanvas.GetComponent = !readCanvas.enabled;
	}
}