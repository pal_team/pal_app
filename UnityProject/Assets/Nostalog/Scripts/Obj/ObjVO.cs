﻿using UnityEngine;
public class ObjVO : MonoBehaviour{
	private string id;
	private string date;
	private string content;
	private int index;

	public void setIndex(int idx){
		this.index = idx;
	}
	public int getIndex(){
		return this.index;
	}
	public void setDate(string date){
		this.date = date;
	}
	public string getDate(){
		return this.date;
	}
	public void setId(string id){
		this.id = id;
	}
	public string getId(){
		return this.id;
	}
	public void setContent(string content){
		this.content = content;
	}
	public string getContent(){
		return this.content;
	}
}
