﻿using UnityEngine;
using UnityEngine.UI;
using UITween;
using System.Collections;
using System.Collections.Generic;


public class ADClickEvent : MonoBehaviour {
	[SerializeField] private GameObject readCanvas;
	[SerializeField] private GameObject mainButton;
	[SerializeField] private Text content;
	[SerializeField] private RawImage adImg;

	private bool activeFlag = false;
	private ObjVO obj;

	private WWW itemsData;
	private bool isRunning;


	public void Start(){
		obj = gameObject.GetComponent<ObjVO> ();
	}

	public void OnMouseDown(){

		if (!isRunning) {
			StartCoroutine (LoadImg ());
		}
		content.text = obj.getContent ();
		readCanvas.SetActive (true);
		mainButton.SetActive(false);
	}


	public IEnumerator LoadImg(){
		isRunning = true;
		string fileUrl = "http://192.168.1.37:9000/adImg/"+obj.getId();
		// adImg위치는 귀차는 관계로 id에 집어넣음.
//		string fileUrl = "http://13.124.11.80:9000/file/ad/"+obj.getId();
		Debug.Log (fileUrl);
		WWW imgLink = new WWW (fileUrl);
		yield return imgLink;
		if (!imgLink.data.Contains ("404")) {
			adImg.texture = imgLink.texture;
		}
		isRunning = false;
	}	
}