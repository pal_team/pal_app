﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using System.Text;
using UnityEngine.UI;

public class ObjGenerator : MonoBehaviour {
	public GameObject noteModel;
	public GameObject adModel;

	private string content;
	private string id;
	private string date;
	private WWW itemsData;
	private bool isRunning;
//	private Info info = Info.instance;
	private List<GameObject> obj;

	// Use this for initialization

	private IEnumerator Start(){
		isRunning = true;

		WWWForm form = new WWWForm ();
		form.AddField ("id", Info.instance.getUserId ());
		form.AddField ("lat", Info.instance.getLatitude ().ToString ());
		form.AddField ("lng", Info.instance.getLongitude ().ToString ());
		Dictionary<string, string> headers = form.headers;
		headers ["content-type"] = "application/json";
		byte[] rawData = form.data;
//		string url = "http://13.124.11.80:9000/searchNote";
		string url = "http://192.168.1.37:9000/searchNote";
		itemsData = new WWW (url, rawData, headers);
		yield return itemsData;

	
		if (obj != null && obj.Count > 0) {
			for (int idx = 0; idx < obj.Count; idx++) {
				Destroy (obj [idx]);
			}
			obj.Clear ();
		}

		Encoding utf8 = Encoding.UTF8;
		//		string itemsDataString = itemsData.text;
		byte[] utf8byte = utf8.GetBytes(itemsData.text);
		string utf8string = utf8.GetString(utf8byte);

		JSONObject json = new JSONObject (utf8string);
		if (json != null && json.list.Count != 0) {
			obj = Info.instance.getObjList ();
			DecodeJson (json);
		} else {
			Debug.Log ("Note doesn't exist here.");
		}
		isRunning = false;
	}
	private void DecodeJson(JSONObject json){
		int num = json.list.Count;
		Debug.Log (json);
		for (int idx = 0; idx < num; idx++) {
			string key = (string)json.keys [idx];
			switch (key) {
			case "note":
				JSONObject note = (JSONObject)json.list [idx];
				int noteCnt = note.list.Count;
				Debug.Log ("Note : " + noteCnt);
				for (int i = 0; i < noteCnt; i++) {
					id = note.list[i].list [0].str;
					date = note.list [i].list [1].str;
					content = note.list [i].list [2].str;
//					Debug.Log ("Id : " + id);
//					Debug.Log ("Content : " + content);
					geterateObj (noteModel, id, date,content);
				}
				break;
			case "ad":
				JSONObject ad = (JSONObject)json.list [idx];
				int adCnt = ad.list.Count;
				Debug.Log ("Ad : " + adCnt);
				for (int i = 0; i < adCnt; i++) {
//					id = ad.list[i].list [0].str;
					date = ad.list [i].list [1].str;
					content = ad.list [i].list [2].str;
					id = ad.list [i].list [3].str;

//					Debug.Log ("AD Id : " + id);
//					Debug.Log ("AD Content : " + content);
					geterateObj (adModel, id, date,content);
				}
				break;
			}
		}
	}
	private void geterateObj(GameObject model, string id, string date, string content){
		int x = Random.Range (-8, 8);
		int z = Random.Range (-8, 8);
		Vector3 position = new Vector3 (x, 10, z);
		position += transform.position;
		if (model.Equals (adModel)) {
			obj.Add (Instantiate (model, position, Quaternion.identity));
		} else {
			obj.Add(Instantiate(model, position, Random.rotation));
		}


//		ObjVO obj = obj [idx].GetComponent<ObjVO> ();
//		obj.setId (id);
//		obj.setContent (content);
		int lastIdx = obj.Count-1;
		ObjVO lastObj = obj [lastIdx].GetComponent<ObjVO> ();
		lastObj.setIndex (lastIdx);
		lastObj.setId (id);
		lastObj.setDate (date);
		lastObj.setContent (content);

//		obj [lastIdx].GetComponent<ObjVO> ().setContent (content);
//		obj [obj.Count-1].GetComponent<ObjVO> ().setId (id);
	}

	public void resetBtn(){
		if (!isRunning) {
			StartCoroutine (Start());
		}
	}
}