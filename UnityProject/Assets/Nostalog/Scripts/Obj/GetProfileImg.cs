﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class GetProfileImg : MonoBehaviour {
	public Text userId;
	public RawImage profile;
	Texture2D img;
	public Texture2D nilImg;
	private WWW itemsData;
	private bool isRunning;

	public IEnumerator LoadImg(){
		isRunning = true;
		string fileUrl = "http://192.168.1.37:9000/userImg/"+userId.text+".png";
//		string fileUrl = "http://13.124.11.80:9000/userImg/"+userId.text+".png";

		Debug.Log (fileUrl);
		WWW imgLink = new WWW (fileUrl);
		yield return imgLink;
		if (!imgLink.data.Contains ("404")) {
			profile.texture = imgLink.texture;
		} else {
			profile.texture = nilImg;	
		}
		isRunning = false;


	}	

	public void OnOpen(){
		
			StartCoroutine (LoadImg ());

	}
}
