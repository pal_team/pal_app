﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;


public class addfollowing : MonoBehaviour {
	[SerializeField] private Text id;


	private WWW itemsData;
	private bool isRunning;


	public void clickEvent()
	{
		Debug.Log ("following click");
		if (!isRunning) {
			StartCoroutine (addFollow (id.text));
		}
	}

	private IEnumerator addFollow(string follow){
		isRunning = true;
		//		Debug.Log (id.text + ", " + date.text);
		WWWForm form = new WWWForm ();
		form.AddField ("id", Info.instance.getUserId ());
		form.AddField ("follow", follow);
		Dictionary<string, string> headers = form.headers;
		headers ["content-type"] = "application/json";
		byte[] rawData = form.data;
//		string url = "http://13.124.11.80:9000/addfollow";
		string url = "http://192.168.1.37:9000/deleteNote";
		itemsData = new WWW (url, rawData, headers);
		yield return itemsData;
		isRunning = false;
	}
}
