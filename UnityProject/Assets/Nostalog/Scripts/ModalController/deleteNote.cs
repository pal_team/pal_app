﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class deleteNote : MonoBehaviour {
	public Text id;
	public Text date;
	private WWW itemsData;
	private bool isRunning;
	private List<GameObject> obj;

	private void Start(){
		obj = Info.instance.getObjList();
	}
	public void clickEvent(){
//		Debug.Log (obj.Count);
		for (int idx = 0; idx < obj.Count; idx++) {
			ObjVO temp = obj [idx].GetComponent<ObjVO> ();
			if (!obj[idx] != null && id.text.Equals (temp.getId()) && date.text.Equals(temp.getDate())) {
//				Debug.Log (idx);

				Destroy (obj [idx]);
				obj.Remove (obj [idx]);
				if (!isRunning) {
					StartCoroutine (delete (id.text, date.text));
				}
				break;
			}
		}
	}

	private IEnumerator delete(string id, string date){
		isRunning = true;
		//		Debug.Log (id.text + ", " + date.text);
		WWWForm form = new WWWForm ();
		form.AddField ("id", id);
		form.AddField ("createDate", date);
		Dictionary<string, string> headers = form.headers;
		headers ["content-type"] = "application/json";
		byte[] rawData = form.data;
//		string url = "http://13.124.11.80:9000/deleteNote";
		string url = "http://192.168.1.37:9000/deleteNote";
		itemsData = new WWW (url, rawData, headers);
		yield return itemsData;
		isRunning = false;
	}
}
