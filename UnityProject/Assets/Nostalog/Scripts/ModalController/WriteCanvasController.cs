﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class WriteCanvasController : MonoBehaviour {
	[SerializeField] private Text time;
	[SerializeField] private Text location;
	// Use this for initialization
	void Start () {
		location.text = Info.instance.getLocation();
	}
	
	// Update is called once per frame
	void Update () {
		time.text = string.Format ("{0:yyyy년 MM월 dd일 hh:mm:ss}", DateTime.Now);
	}
}
