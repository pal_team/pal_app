﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;
[RequireComponent(typeof(Button))]
public class switchImage: MonoBehaviour
{
	[SerializeField] private Text id;
	[SerializeField] private Text date;
	[SerializeField] private Text likes;
	[SerializeField] private Text views;
	[SerializeField] private Image targetImage;
	[SerializeField] private Sprite onSprite;
	[SerializeField] private Sprite offSprite;
	[SerializeField] private bool isOn;
	private WWW itemsData;
	private bool isRunning;

	public bool IsOn { get { return isOn; } set { isOn = value; UpdateValue(); }}
	public event Action<bool> onValueChanged;

	private Button button;

	const string temp = "http://192.168.1.37:9000";
//	const string temp = "http://13.124.11.80:9000";
	string selectUrl = temp + "/selectlike";
	string insertUrl = temp + "/insertlike";
	string deleteUrl = temp + "/deletelike";



	private IEnumerator connectLike(string touchUrl){
		isRunning = true;
//		Debug.Log (id.text + ", " + date.text);
		WWWForm form = new WWWForm ();
		form.AddField ("userId", Info.instance.getUserId ());
		form.AddField ("postId", id.text);
		form.AddField ("createDate", date.text);
		Dictionary<string, string> headers = form.headers;
		headers ["content-type"] = "application/json";
		byte[] rawData = form.data;
		string url = touchUrl;
		itemsData = new WWW (url, rawData, headers);
		yield return itemsData;
		isRunning = false;

		Encoding utf8 = Encoding.UTF8;
		byte[] utf8byte = utf8.GetBytes(itemsData.text);
		string utf8string = utf8.GetString(utf8byte);

		JSONObject json = new JSONObject (utf8string);
//		Debug.Log (json.ToString ());
		if (url.Contains ("select")) {
			if (json != null && json.list [0].str.Equals ("success")) {
				int cnt = (int)json.list [2].n;
				views.text = json.list [3].n.ToString();
//				Debug.Log (cnt);
				if (json.list [1].b) {
					Initialize (true, cnt);
				} else {
					Initialize (false, cnt);
				}
			} else {
//				Debug.Log ("error");
			}
		}else if(url.Contains("insert")){
			if(json !=null && json.list[0].str.Equals("success")){
//				Debug.Log("insert success");
			}
		}

	}
	public void OnOpen(){
		
		if (!isRunning) {
			StartCoroutine (connectLike (selectUrl));
		}
	}

	// to set initial value and skip onValueChanged notification
	public void Initialize(bool value, int cnt)
	{
		likes.text = cnt.ToString();
		isOn = value;
		UpdateValue(isOn);
	}

	// Use this for initialization
	public void Start ()
	{
		button = GetComponent<Button>();
		button.transition = Selectable.Transition.None;
		button.onClick.AddListener(OnClick);
	}

	void OnClick()
	{
		Debug.Log ("Like Click");
		if (!isOn) {
			if (!isRunning) {
				StartCoroutine (connectLike (insertUrl));
				likes.text = (Int32.Parse (likes.text) + 1).ToString ();
			}
		} else {
			if (!isRunning) {
				StartCoroutine (connectLike (deleteUrl));
				likes.text = (Int32.Parse (likes.text) - 1).ToString ();
			}
		}
		isOn = !isOn;
		UpdateValue();
	}

	private void UpdateValue(bool notifySubscribers = true)
	{
		if(notifySubscribers && onValueChanged != null)
			onValueChanged(isOn);

		if (targetImage == null)
			return;

		targetImage.sprite = isOn ? onSprite : offSprite;
	}
}