﻿//Singleton pattern Class
//using UnityEngine;
//using System.Collections.Generic;
//public sealed class Info{
//	private static Info instance;
//
//	private List<GameObject> objList = new List<GameObject>();
//	private string UserId = "user";
//	private float  latitude = 37.49466f;
//	private float  longitude = 127.03765f;
//	//lat = 37.494661
//	//lon = 127.027657
//
//	private Info(){}
//
//	public static Info GetInstance(){
//		if(instance == null){
//			instance = new Info();
//		}
//		return instance;
//	}
//	public List<GameObject> getObjList(){
//		return objList;
//	}
//
//	public float getLatitude(){
//		return this.latitude;
//	}
//	public float getLongitude(){
//		return this.longitude;
//	}
//	public string getUserId(){
//		return this.UserId;
//	}
//	public void setUserId(string id){
//		this.UserId = id;
//	}
//	public void setLatitude(float lat){
//		this.latitude = lat;
//	}
//	public void setLongitude(float lon){
//		this.longitude = lon;
//	}
//}
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
public class Info:MonoBehaviour{
	//Singleton Pattern Generate Code
	public Text userId;
	public static Info instance;

	void Awake(){
		instance = this;
	}



	private List<GameObject> objList = new List<GameObject>();
	private string UserId = "jihoon";
//	private string latitude = "37.49445447462366";
//	private string longitude = "127.02763259410858";

	private string latitude = "37.47445447462366";
	private string longitude = "127.01763259410858";
	private string location = "대한민국 서울특별시 서초구 서초대로 74길";

	public List<GameObject> getObjList(){
		return objList;
	}

	public string getLocation(){
		return this.location;
	}
	public string getLatitude(){
		return this.latitude;
	}
	public string getLongitude(){
		return this.longitude;
	}
	public string getUserId(){
		return this.UserId;
	}
	public void setLocation(string location){
		this.location = location;
	}
	public void setUserId(string id){
		this.UserId = id;
		userId.text = id;
	}
	public void setLatitude(string lat){
		this.latitude = lat;
		Debug.Log ("setLatitude Call : " + this.latitude);
	}
	public void setLongitude(string lon){
		this.longitude = lon;
		Debug.Log ("setLongitude Call : " + this.longitude);
	}
}