﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class reportNote : MonoBehaviour {
	public Text id;
	public Text date;
	private WWW itemsData;
	private bool isRunning;

	public void clickEvent(){
		if (!isRunning) {
			StartCoroutine (report (id.text, date.text));
		}
	}

	private IEnumerator report(string id, string date){
		isRunning = true;
		//		Debug.Log (id.text + ", " + date.text);
		WWWForm form = new WWWForm ();
		form.AddField ("id", id);
		form.AddField ("createDate", date);
		Dictionary<string, string> headers = form.headers;
		headers ["content-type"] = "application/json";
		byte[] rawData = form.data;
//		string url = "http://13.124.11.80:9000/reportNote";
		string url = "http://192.168.1.37:9000/reportNote";
		itemsData = new WWW (url, rawData, headers);
		yield return itemsData;
		isRunning = false;
	}
}