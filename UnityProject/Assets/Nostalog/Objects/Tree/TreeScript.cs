﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreeScript : MonoBehaviour {
	public string title;
	public string content;
	public Canvas panelCanvas;
	public Text editTitle;
	public Text editContent;
	public bool panelOpen;

	private void OnMouseDown(){
		if(!panelOpen){

			editTitle.text = this.title;
			editContent.text = this.content;
		
			panelOpen = true;
			panelCanvas.enabled = true;
		}
		else{
			panelOpen = false;
			panelCanvas.enabled = false;
		}
	}

	public void setTitle (string title){
		this.title = title;
	}
	public void setContent(string content){
		this.content = content;
	}

	public string getTitle(){
		return this.title;
	}
	public string getContent(){
		return this.content;
	}
}
