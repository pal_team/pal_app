﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.UI;

public class Generate : MonoBehaviour {
	public GameObject prefab;
//	public RawImage indicator;

	private GameObject[] tree;
	private List<GameObject> obj;
	private string title;
	private string content;
	private WWW itemsData;
	private bool isRunning;
	// Use this for initialization

	private IEnumerator Start(){
		isRunning = true;
//		indicator.enabled = true;
		itemsData = new WWW ("http://ec2-52-79-99-22.ap-northeast-2.compute.amazonaws.com:9000/searchNote");
		yield return itemsData;
		isRunning = false;
//		if (tree != null) {
//			for (int i = 0; i < tree.Length; i++) {
//				Destroy (tree [i]);
//			}
//			tree = null;
//		}
		if (obj != null) {
			obj.Clear();
			obj = null;
		}
		Encoding utf8 = Encoding.UTF8;
		//		string itemsDataString = itemsData.text;
		byte[] utf8byte = utf8.GetBytes(itemsData.text);
		string utf8string = utf8.GetString(utf8byte);

		JSONObject json = new JSONObject (utf8string);
		if (json != null) {
			int num = json.list.Count;
//			tree = new GameObject[num];
			obj = new List<GameObject>();
			for (int i = 0; i < num; i++) {
				title = json.list [i].list [6].str;
				content = json.list [i].list [7].str;

				print (title + "," + content);
				setPrefab (i, title, content);
			}
//			indicator.enabled = false;
		} else {
			Debug.Log ("No Message");
		}
		
	}
	private void setPrefab(int idx, string title, string content){
		int x = Random.Range (-10, 10);
		int z = Random.Range (-10, 10);
		Vector3 position = new Vector3 (x, -4, z);
		position += transform.position;
		obj.Add(Instantiate(prefab, position, Quaternion.identity));
		TreeScript sc = obj[idx].GetComponent<TreeScript> ();
		sc.setTitle (title);
		sc.setContent (content);
	}
	public void Wapper(){
		if (!isRunning) {
			StartCoroutine (Start());
		}
	}
}