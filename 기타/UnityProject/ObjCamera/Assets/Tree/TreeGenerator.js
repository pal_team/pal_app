﻿#pragma strict

public var treeObj : GameObject;

function Start () {
for(var i =0; i < 10; i++){
GenerateTree();
}
}

function Update () {
}

function GenerateTree(){
	var offsx : float = Random.Range(-10.0,10.0);
	var offsz : float = Random.Range(-10.0,10.0);
	var position : Vector3 = transform.position + Vector3(offsx,0,offsz);
	var prefab : GameObject = treeObj;
	Instantiate(prefab, position,Quaternion.identity);
}
