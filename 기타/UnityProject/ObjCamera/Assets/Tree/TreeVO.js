﻿#pragma strict

var panelCanvas : Canvas;
var panelOpen = false;

function OnMouseDown(){
	if(!panelOpen){
		panelOpen = true;
		panelCanvas.enabled = true;
	}
	else{
		panelOpen = false;
		panelCanvas.enabled = false;
	}
}
