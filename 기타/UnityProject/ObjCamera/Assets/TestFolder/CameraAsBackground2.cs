﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAsBackground2 : MonoBehaviour {
	public Material mt;

	// Use this for initialization
	void Start () {
		WebCamDevice[] device = WebCamTexture.devices;
		WebCamTexture wct = new WebCamTexture ();

		if (device.Length > 0) {
			wct.deviceName = device [0].name;
			wct.Play ();
		}
		mt.mainTexture = wct;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
