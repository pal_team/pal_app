﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickTreeEvent
struct ClickTreeEvent_t925145888;

#include "codegen/il2cpp-codegen.h"

// System.Void ClickTreeEvent::.ctor()
extern "C"  void ClickTreeEvent__ctor_m3578795207 (ClickTreeEvent_t925145888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickTreeEvent::OnMouseDown()
extern "C"  void ClickTreeEvent_OnMouseDown_m3894428433 (ClickTreeEvent_t925145888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
