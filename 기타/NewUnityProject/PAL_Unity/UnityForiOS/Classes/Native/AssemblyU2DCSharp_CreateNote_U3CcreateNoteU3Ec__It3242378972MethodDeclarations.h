﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CreateNote/<createNote>c__Iterator0
struct U3CcreateNoteU3Ec__Iterator0_t3242378972;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CreateNote/<createNote>c__Iterator0::.ctor()
extern "C"  void U3CcreateNoteU3Ec__Iterator0__ctor_m241372123 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CreateNote/<createNote>c__Iterator0::MoveNext()
extern "C"  bool U3CcreateNoteU3Ec__Iterator0_MoveNext_m400128733 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CreateNote/<createNote>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CcreateNoteU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m905848205 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CreateNote/<createNote>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CcreateNoteU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2234605237 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateNote/<createNote>c__Iterator0::Dispose()
extern "C"  void U3CcreateNoteU3Ec__Iterator0_Dispose_m552473446 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateNote/<createNote>c__Iterator0::Reset()
extern "C"  void U3CcreateNoteU3Ec__Iterator0_Reset_m1809002792 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
