﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Gyroscope
struct Gyroscope_t1705362817;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroCamera
struct  GyroCamera_t3228729828  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Gyroscope GyroCamera::gyro
	Gyroscope_t1705362817 * ___gyro_2;
	// System.Boolean GyroCamera::gyroSupported
	bool ___gyroSupported_3;
	// UnityEngine.Quaternion GyroCamera::rotFix
	Quaternion_t4030073918  ___rotFix_4;
	// UnityEngine.Transform GyroCamera::worldObj
	Transform_t3275118058 * ___worldObj_5;
	// System.Single GyroCamera::startY
	float ___startY_6;

public:
	inline static int32_t get_offset_of_gyro_2() { return static_cast<int32_t>(offsetof(GyroCamera_t3228729828, ___gyro_2)); }
	inline Gyroscope_t1705362817 * get_gyro_2() const { return ___gyro_2; }
	inline Gyroscope_t1705362817 ** get_address_of_gyro_2() { return &___gyro_2; }
	inline void set_gyro_2(Gyroscope_t1705362817 * value)
	{
		___gyro_2 = value;
		Il2CppCodeGenWriteBarrier(&___gyro_2, value);
	}

	inline static int32_t get_offset_of_gyroSupported_3() { return static_cast<int32_t>(offsetof(GyroCamera_t3228729828, ___gyroSupported_3)); }
	inline bool get_gyroSupported_3() const { return ___gyroSupported_3; }
	inline bool* get_address_of_gyroSupported_3() { return &___gyroSupported_3; }
	inline void set_gyroSupported_3(bool value)
	{
		___gyroSupported_3 = value;
	}

	inline static int32_t get_offset_of_rotFix_4() { return static_cast<int32_t>(offsetof(GyroCamera_t3228729828, ___rotFix_4)); }
	inline Quaternion_t4030073918  get_rotFix_4() const { return ___rotFix_4; }
	inline Quaternion_t4030073918 * get_address_of_rotFix_4() { return &___rotFix_4; }
	inline void set_rotFix_4(Quaternion_t4030073918  value)
	{
		___rotFix_4 = value;
	}

	inline static int32_t get_offset_of_worldObj_5() { return static_cast<int32_t>(offsetof(GyroCamera_t3228729828, ___worldObj_5)); }
	inline Transform_t3275118058 * get_worldObj_5() const { return ___worldObj_5; }
	inline Transform_t3275118058 ** get_address_of_worldObj_5() { return &___worldObj_5; }
	inline void set_worldObj_5(Transform_t3275118058 * value)
	{
		___worldObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___worldObj_5, value);
	}

	inline static int32_t get_offset_of_startY_6() { return static_cast<int32_t>(offsetof(GyroCamera_t3228729828, ___startY_6)); }
	inline float get_startY_6() const { return ___startY_6; }
	inline float* get_address_of_startY_6() { return &___startY_6; }
	inline void set_startY_6(float value)
	{
		___startY_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
