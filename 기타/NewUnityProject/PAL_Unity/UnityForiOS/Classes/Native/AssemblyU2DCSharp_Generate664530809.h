﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Generate
struct  Generate_t664530809  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Generate::prefab
	GameObject_t1756533147 * ___prefab_2;
	// UnityEngine.GameObject[] Generate::tree
	GameObjectU5BU5D_t3057952154* ___tree_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Generate::obj
	List_1_t1125654279 * ___obj_4;
	// System.String Generate::title
	String_t* ___title_5;
	// System.String Generate::content
	String_t* ___content_6;
	// UnityEngine.WWW Generate::itemsData
	WWW_t2919945039 * ___itemsData_7;
	// System.Boolean Generate::isRunning
	bool ___isRunning_8;

public:
	inline static int32_t get_offset_of_prefab_2() { return static_cast<int32_t>(offsetof(Generate_t664530809, ___prefab_2)); }
	inline GameObject_t1756533147 * get_prefab_2() const { return ___prefab_2; }
	inline GameObject_t1756533147 ** get_address_of_prefab_2() { return &___prefab_2; }
	inline void set_prefab_2(GameObject_t1756533147 * value)
	{
		___prefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_2, value);
	}

	inline static int32_t get_offset_of_tree_3() { return static_cast<int32_t>(offsetof(Generate_t664530809, ___tree_3)); }
	inline GameObjectU5BU5D_t3057952154* get_tree_3() const { return ___tree_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_tree_3() { return &___tree_3; }
	inline void set_tree_3(GameObjectU5BU5D_t3057952154* value)
	{
		___tree_3 = value;
		Il2CppCodeGenWriteBarrier(&___tree_3, value);
	}

	inline static int32_t get_offset_of_obj_4() { return static_cast<int32_t>(offsetof(Generate_t664530809, ___obj_4)); }
	inline List_1_t1125654279 * get_obj_4() const { return ___obj_4; }
	inline List_1_t1125654279 ** get_address_of_obj_4() { return &___obj_4; }
	inline void set_obj_4(List_1_t1125654279 * value)
	{
		___obj_4 = value;
		Il2CppCodeGenWriteBarrier(&___obj_4, value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(Generate_t664530809, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier(&___title_5, value);
	}

	inline static int32_t get_offset_of_content_6() { return static_cast<int32_t>(offsetof(Generate_t664530809, ___content_6)); }
	inline String_t* get_content_6() const { return ___content_6; }
	inline String_t** get_address_of_content_6() { return &___content_6; }
	inline void set_content_6(String_t* value)
	{
		___content_6 = value;
		Il2CppCodeGenWriteBarrier(&___content_6, value);
	}

	inline static int32_t get_offset_of_itemsData_7() { return static_cast<int32_t>(offsetof(Generate_t664530809, ___itemsData_7)); }
	inline WWW_t2919945039 * get_itemsData_7() const { return ___itemsData_7; }
	inline WWW_t2919945039 ** get_address_of_itemsData_7() { return &___itemsData_7; }
	inline void set_itemsData_7(WWW_t2919945039 * value)
	{
		___itemsData_7 = value;
		Il2CppCodeGenWriteBarrier(&___itemsData_7, value);
	}

	inline static int32_t get_offset_of_isRunning_8() { return static_cast<int32_t>(offsetof(Generate_t664530809, ___isRunning_8)); }
	inline bool get_isRunning_8() const { return ___isRunning_8; }
	inline bool* get_address_of_isRunning_8() { return &___isRunning_8; }
	inline void set_isRunning_8(bool value)
	{
		___isRunning_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
