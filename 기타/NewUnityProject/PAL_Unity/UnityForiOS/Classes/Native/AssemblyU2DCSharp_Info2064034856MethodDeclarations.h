﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Info
struct Info_t2064034856;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Info::.ctor()
extern "C"  void Info__ctor_m3149560641 (Info_t2064034856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Info Info::GetInstance()
extern "C"  Info_t2064034856 * Info_GetInstance_m196706287 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.GameObject> Info::getObjList()
extern "C"  List_1_t1125654279 * Info_getObjList_m3675622311 (Info_t2064034856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Info::getLatitude()
extern "C"  float Info_getLatitude_m3586550869 (Info_t2064034856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Info::getLongitude()
extern "C"  float Info_getLongitude_m4220800764 (Info_t2064034856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Info::getUserId()
extern "C"  String_t* Info_getUserId_m1498575430 (Info_t2064034856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Info::setUserId(System.String)
extern "C"  void Info_setUserId_m2955544633 (Info_t2064034856 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Info::setLatitude(System.Single)
extern "C"  void Info_setLatitude_m3302714788 (Info_t2064034856 * __this, float ___lat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Info::setLongitude(System.Single)
extern "C"  void Info_setLongitude_m651487129 (Info_t2064034856 * __this, float ___lon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
