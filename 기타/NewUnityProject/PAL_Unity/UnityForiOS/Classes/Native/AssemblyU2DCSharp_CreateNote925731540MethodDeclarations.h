﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CreateNote
struct CreateNote_t925731540;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void CreateNote::.ctor()
extern "C"  void CreateNote__ctor_m1783892855 (CreateNote_t925731540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CreateNote::createNote()
extern "C"  Il2CppObject * CreateNote_createNote_m324287845 (CreateNote_t925731540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CreateNote::getTextForm()
extern "C"  bool CreateNote_getTextForm_m2844656960 (CreateNote_t925731540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateNote::toggleBtn()
extern "C"  void CreateNote_toggleBtn_m1760753357 (CreateNote_t925731540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateNote::createBtn()
extern "C"  void CreateNote_createBtn_m3541765391 (CreateNote_t925731540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
