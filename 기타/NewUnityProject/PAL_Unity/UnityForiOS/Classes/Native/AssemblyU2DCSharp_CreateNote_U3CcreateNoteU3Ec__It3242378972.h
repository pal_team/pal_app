﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Text.Encoding
struct Encoding_t663144255;
// JSONObject
struct JSONObject_t1971882247;
// CreateNote
struct CreateNote_t925731540;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateNote/<createNote>c__Iterator0
struct  U3CcreateNoteU3Ec__Iterator0_t3242378972  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm CreateNote/<createNote>c__Iterator0::<form>__0
	WWWForm_t3950226929 * ___U3CformU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CreateNote/<createNote>c__Iterator0::<headers>__1
	Dictionary_2_t3943999495 * ___U3CheadersU3E__1_1;
	// System.Byte[] CreateNote/<createNote>c__Iterator0::<rawData>__2
	ByteU5BU5D_t3397334013* ___U3CrawDataU3E__2_2;
	// System.String CreateNote/<createNote>c__Iterator0::<url>__3
	String_t* ___U3CurlU3E__3_3;
	// UnityEngine.WWW CreateNote/<createNote>c__Iterator0::<www>__4
	WWW_t2919945039 * ___U3CwwwU3E__4_4;
	// System.Text.Encoding CreateNote/<createNote>c__Iterator0::<utf8>__5
	Encoding_t663144255 * ___U3Cutf8U3E__5_5;
	// System.Byte[] CreateNote/<createNote>c__Iterator0::<utf8byte>__6
	ByteU5BU5D_t3397334013* ___U3Cutf8byteU3E__6_6;
	// System.String CreateNote/<createNote>c__Iterator0::<utf8string>__7
	String_t* ___U3Cutf8stringU3E__7_7;
	// JSONObject CreateNote/<createNote>c__Iterator0::<json>__8
	JSONObject_t1971882247 * ___U3CjsonU3E__8_8;
	// CreateNote CreateNote/<createNote>c__Iterator0::$this
	CreateNote_t925731540 * ___U24this_9;
	// System.Object CreateNote/<createNote>c__Iterator0::$current
	Il2CppObject * ___U24current_10;
	// System.Boolean CreateNote/<createNote>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 CreateNote/<createNote>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3CformU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t3943999495 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t3943999495 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CrawDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3CrawDataU3E__2_2)); }
	inline ByteU5BU5D_t3397334013* get_U3CrawDataU3E__2_2() const { return ___U3CrawDataU3E__2_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CrawDataU3E__2_2() { return &___U3CrawDataU3E__2_2; }
	inline void set_U3CrawDataU3E__2_2(ByteU5BU5D_t3397334013* value)
	{
		___U3CrawDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrawDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__3_3() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3CurlU3E__3_3)); }
	inline String_t* get_U3CurlU3E__3_3() const { return ___U3CurlU3E__3_3; }
	inline String_t** get_address_of_U3CurlU3E__3_3() { return &___U3CurlU3E__3_3; }
	inline void set_U3CurlU3E__3_3(String_t* value)
	{
		___U3CurlU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_4() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3CwwwU3E__4_4)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__4_4() const { return ___U3CwwwU3E__4_4; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__4_4() { return &___U3CwwwU3E__4_4; }
	inline void set_U3CwwwU3E__4_4(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3Cutf8U3E__5_5() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3Cutf8U3E__5_5)); }
	inline Encoding_t663144255 * get_U3Cutf8U3E__5_5() const { return ___U3Cutf8U3E__5_5; }
	inline Encoding_t663144255 ** get_address_of_U3Cutf8U3E__5_5() { return &___U3Cutf8U3E__5_5; }
	inline void set_U3Cutf8U3E__5_5(Encoding_t663144255 * value)
	{
		___U3Cutf8U3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cutf8U3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3Cutf8byteU3E__6_6() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3Cutf8byteU3E__6_6)); }
	inline ByteU5BU5D_t3397334013* get_U3Cutf8byteU3E__6_6() const { return ___U3Cutf8byteU3E__6_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3Cutf8byteU3E__6_6() { return &___U3Cutf8byteU3E__6_6; }
	inline void set_U3Cutf8byteU3E__6_6(ByteU5BU5D_t3397334013* value)
	{
		___U3Cutf8byteU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cutf8byteU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3Cutf8stringU3E__7_7() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3Cutf8stringU3E__7_7)); }
	inline String_t* get_U3Cutf8stringU3E__7_7() const { return ___U3Cutf8stringU3E__7_7; }
	inline String_t** get_address_of_U3Cutf8stringU3E__7_7() { return &___U3Cutf8stringU3E__7_7; }
	inline void set_U3Cutf8stringU3E__7_7(String_t* value)
	{
		___U3Cutf8stringU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cutf8stringU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__8_8() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U3CjsonU3E__8_8)); }
	inline JSONObject_t1971882247 * get_U3CjsonU3E__8_8() const { return ___U3CjsonU3E__8_8; }
	inline JSONObject_t1971882247 ** get_address_of_U3CjsonU3E__8_8() { return &___U3CjsonU3E__8_8; }
	inline void set_U3CjsonU3E__8_8(JSONObject_t1971882247 * value)
	{
		___U3CjsonU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U24this_9)); }
	inline CreateNote_t925731540 * get_U24this_9() const { return ___U24this_9; }
	inline CreateNote_t925731540 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(CreateNote_t925731540 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_9, value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CcreateNoteU3Ec__Iterator0_t3242378972, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
