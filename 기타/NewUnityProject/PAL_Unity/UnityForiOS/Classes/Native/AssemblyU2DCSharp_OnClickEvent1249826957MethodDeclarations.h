﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnClickEvent
struct OnClickEvent_t1249826957;

#include "codegen/il2cpp-codegen.h"

// System.Void OnClickEvent::.ctor()
extern "C"  void OnClickEvent__ctor_m4243796372 (OnClickEvent_t1249826957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickEvent::Start()
extern "C"  void OnClickEvent_Start_m2260616076 (OnClickEvent_t1249826957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickEvent::OnMouseDown()
extern "C"  void OnClickEvent_OnMouseDown_m1568766256 (OnClickEvent_t1249826957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
