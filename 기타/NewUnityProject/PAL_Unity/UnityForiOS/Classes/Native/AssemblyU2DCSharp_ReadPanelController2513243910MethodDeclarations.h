﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReadPanelController
struct ReadPanelController_t2513243910;

#include "codegen/il2cpp-codegen.h"

// System.Void ReadPanelController::.ctor()
extern "C"  void ReadPanelController__ctor_m282136683 (ReadPanelController_t2513243910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadPanelController::Start()
extern "C"  void ReadPanelController_Start_m3624722871 (ReadPanelController_t2513243910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadPanelController::CloseBtn()
extern "C"  void ReadPanelController_CloseBtn_m2624603461 (ReadPanelController_t2513243910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
