﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Generate
struct Generate_t664530809;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Generate::.ctor()
extern "C"  void Generate__ctor_m609598290 (Generate_t664530809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Generate::Start()
extern "C"  Il2CppObject * Generate_Start_m1411269226 (Generate_t664530809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Generate::setPrefab(System.Int32,System.String,System.String)
extern "C"  void Generate_setPrefab_m2335224753 (Generate_t664530809 * __this, int32_t ___idx0, String_t* ___title1, String_t* ___content2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Generate::Wapper()
extern "C"  void Generate_Wapper_m3130702321 (Generate_t664530809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
