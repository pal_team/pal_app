﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// CameraAsBackground
struct CameraAsBackground_t75902795;
// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t3114550109;
// System.Object
struct Il2CppObject;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// ClickTreeEvent
struct ClickTreeEvent_t925145888;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4251328308;
// System.String
struct String_t;
// CreateNote
struct CreateNote_t925731540;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// CreateNote/<createNote>c__Iterator0
struct U3CcreateNoteU3Ec__Iterator0_t3242378972;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ObjVO
struct ObjVO_t1115677294;
// Generate
struct Generate_t664530809;
// TreeScript
struct TreeScript_t1745883499;
// Generate/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3787076049;
// GyroCamera
struct GyroCamera_t3228729828;
// Info
struct Info_t2064034856;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// JSONObject
struct JSONObject_t1971882247;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.String,JSONObject>
struct Dictionary_2_t3886661509;
// JSONObject/AddJSONContents
struct AddJSONContents_t3850664647;
// JSONObject[]
struct JSONObjectU5BU5D_t2270799614;
// System.Char[]
struct CharU5BU5D_t1328083999;
// JSONObject/FieldNotFound
struct FieldNotFound_t865402053;
// JSONObject/GetFieldResponse
struct GetFieldResponse_t1259369279;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// JSONObject/<BakeAsync>c__Iterator0
struct U3CBakeAsyncU3Ec__Iterator0_t1149809410;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// JSONObject/<PrintAsync>c__Iterator1
struct U3CPrintAsyncU3Ec__Iterator1_t716304657;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;
// JSONObject/<StringifyAsync>c__Iterator2
struct U3CStringifyAsyncU3Ec__Iterator2_t4037879552;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// ObjGenerator
struct ObjGenerator_t2287658554;
// ObjGenerator/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t2867974660;
// OnClickEvent
struct OnClickEvent_t1249826957;
// ReadPanelController
struct ReadPanelController_t2513243910;
// UnityEngine.Canvas
struct Canvas_t209405766;
// ThrowNote
struct ThrowNote_t1571054592;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717MethodDeclarations.h"
#include "AssemblyU2DCSharp_CameraAsBackground75902795.h"
#include "AssemblyU2DCSharp_CameraAsBackground75902795MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture1079476942MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage2749640213MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage2749640213.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_WebCamTexture1079476942.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_ClickTreeEvent925145888.h"
#include "AssemblyU2DCSharp_ClickTreeEvent925145888MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_CreateNote925731540.h"
#include "AssemblyU2DCSharp_CreateNote925731540MethodDeclarations.h"
#include "AssemblyU2DCSharp_Info2064034856MethodDeclarations.h"
#include "AssemblyU2DCSharp_Info2064034856.h"
#include "AssemblyU2DCSharp_CreateNote_U3CcreateNoteU3Ec__It3242378972MethodDeclarations.h"
#include "AssemblyU2DCSharp_CreateNote_U3CcreateNoteU3Ec__It3242378972.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject1971882247MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1341003379MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "AssemblyU2DCSharp_ObjVO1115677294MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "AssemblyU2DCSharp_JSONObject1971882247.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1341003379.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_ObjVO1115677294.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_Generate664530809.h"
#include "AssemblyU2DCSharp_Generate664530809MethodDeclarations.h"
#include "AssemblyU2DCSharp_Generate_U3CStartU3Ec__Iterator03787076049MethodDeclarations.h"
#include "AssemblyU2DCSharp_Generate_U3CStartU3Ec__Iterator03787076049.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "AssemblyU2DCSharp_TreeScript1745883499MethodDeclarations.h"
#include "AssemblyU2DCSharp_TreeScript1745883499.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "AssemblyU2DCSharp_GyroCamera3228729828.h"
#include "AssemblyU2DCSharp_GyroCamera3228729828MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gyroscope1705362817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gyroscope1705362817.h"
#include "AssemblyU2DCSharp_JSONObject_Type1314578890.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3886661509.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3886661509MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21644006731.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En911718915.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En911718915MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21644006731MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_AddJSONContents3850664647.h"
#include "AssemblyU2DCSharp_JSONObject_AddJSONContents3850664647MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_FormatException2948921286.h"
#include "AssemblyU2DCSharp_JSONObject_FieldNotFound865402053.h"
#include "AssemblyU2DCSharp_JSONObject_FieldNotFound865402053MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_GetFieldResponse1259369279.h"
#include "AssemblyU2DCSharp_JSONObject_GetFieldResponse1259369279MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_U3CBakeAsyncU3Ec__Ite1149809410MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_U3CBakeAsyncU3Ec__Ite1149809410.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_JSONObject_U3CPrintAsyncU3Ec__Ite716304657MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_U3CPrintAsyncU3Ec__Ite716304657.h"
#include "AssemblyU2DCSharp_JSONObject_U3CStringifyAsyncU3Ec4037879552MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_U3CStringifyAsyncU3Ec4037879552.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "System_System_Diagnostics_Stopwatch1380178105MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "System_System_Diagnostics_Stopwatch1380178105.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_JSONObject_Type1314578890MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONTemplates3006274921.h"
#include "AssemblyU2DCSharp_JSONTemplates3006274921MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_FieldInfo255040150MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340MethodDeclarations.h"
#include "AssemblyU2DCSharp_ObjGenerator2287658554.h"
#include "AssemblyU2DCSharp_ObjGenerator2287658554MethodDeclarations.h"
#include "AssemblyU2DCSharp_ObjGenerator_U3CStartU3Ec__Itera2867974660MethodDeclarations.h"
#include "AssemblyU2DCSharp_ObjGenerator_U3CStartU3Ec__Itera2867974660.h"
#include "AssemblyU2DCSharp_OnClickEvent1249826957.h"
#include "AssemblyU2DCSharp_OnClickEvent1249826957MethodDeclarations.h"
#include "AssemblyU2DCSharp_ReadPanelController2513243910.h"
#include "AssemblyU2DCSharp_ReadPanelController2513243910MethodDeclarations.h"
#include "AssemblyU2DCSharp_ThrowNote1571054592.h"
#include "AssemblyU2DCSharp_ThrowNote1571054592MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.AspectRatioFitter>()
#define Component_GetComponent_TisAspectRatioFitter_t3114550109_m3848059983(__this, method) ((  AspectRatioFitter_t3114550109 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.RawImage>()
#define Component_GetComponent_TisRawImage_t2749640213_m1817787565(__this, method) ((  RawImage_t2749640213 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.AndroidJavaObject::Get<System.Object>(System.String)
extern "C"  Il2CppObject * AndroidJavaObject_Get_TisIl2CppObject_m3086200777_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, const MethodInfo* method);
#define AndroidJavaObject_Get_TisIl2CppObject_m3086200777(__this, p0, method) ((  Il2CppObject * (*) (AndroidJavaObject_t4251328308 *, String_t*, const MethodInfo*))AndroidJavaObject_Get_TisIl2CppObject_m3086200777_gshared)(__this, p0, method)
// !!0 UnityEngine.AndroidJavaObject::Get<UnityEngine.AndroidJavaObject>(System.String)
#define AndroidJavaObject_Get_TisAndroidJavaObject_t4251328308_m1778010852(__this, p0, method) ((  AndroidJavaObject_t4251328308 * (*) (AndroidJavaObject_t4251328308 *, String_t*, const MethodInfo*))AndroidJavaObject_Get_TisIl2CppObject_m3086200777_gshared)(__this, p0, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ObjVO>()
#define GameObject_GetComponent_TisObjVO_t1115677294_m333022829(__this, method) ((  ObjVO_t1115677294 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<TreeScript>()
#define GameObject_GetComponent_TisTreeScript_t1745883499_m2418258018(__this, method) ((  TreeScript_t1745883499 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Int32 System.Array::IndexOf<System.Char>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisChar_t3454481338_m3847818391_gshared (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* p0, Il2CppChar p1, const MethodInfo* method);
#define Array_IndexOf_TisChar_t3454481338_m3847818391(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CharU5BU5D_t1328083999*, Il2CppChar, const MethodInfo*))Array_IndexOf_TisChar_t3454481338_m3847818391_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t209405766_m195193039(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraAsBackground::.ctor()
extern "C"  void CameraAsBackground__ctor_m2481726058 (CameraAsBackground_t75902795 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraAsBackground::Start()
extern Il2CppClass* WebCamTexture_t1079476942_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAspectRatioFitter_t3114550109_m3848059983_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRawImage_t2749640213_m1817787565_MethodInfo_var;
extern const uint32_t CameraAsBackground_Start_m2831094006_MetadataUsageId;
extern "C"  void CameraAsBackground_Start_m2831094006 (CameraAsBackground_t75902795 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraAsBackground_Start_m2831094006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AspectRatioFitter_t3114550109 * L_0 = Component_GetComponent_TisAspectRatioFitter_t3114550109_m3848059983(__this, /*hidden argument*/Component_GetComponent_TisAspectRatioFitter_t3114550109_m3848059983_MethodInfo_var);
		__this->set_arf_4(L_0);
		RawImage_t2749640213 * L_1 = Component_GetComponent_TisRawImage_t2749640213_m1817787565(__this, /*hidden argument*/Component_GetComponent_TisRawImage_t2749640213_m1817787565_MethodInfo_var);
		__this->set_image_2(L_1);
		int32_t L_2 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_4 = (WebCamTexture_t1079476942 *)il2cpp_codegen_object_new(WebCamTexture_t1079476942_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m3080992457(L_4, L_2, L_3, /*hidden argument*/NULL);
		__this->set_cam_3(L_4);
		RawImage_t2749640213 * L_5 = __this->get_image_2();
		WebCamTexture_t1079476942 * L_6 = __this->get_cam_3();
		NullCheck(L_5);
		RawImage_set_texture_m2400157626(L_5, L_6, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_7 = __this->get_cam_3();
		NullCheck(L_7);
		WebCamTexture_Play_m1997372813(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraAsBackground::Update()
extern "C"  void CameraAsBackground_Update_m1267164179 (CameraAsBackground_t75902795 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		WebCamTexture_t1079476942 * L_0 = __this->get_cam_3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)100))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		WebCamTexture_t1079476942 * L_2 = __this->get_cam_3();
		NullCheck(L_2);
		int32_t L_3 = WebCamTexture_get_videoRotationAngle_m1556283588(L_2, /*hidden argument*/NULL);
		V_0 = (((float)((float)((-L_3)))));
		RawImage_t2749640213 * L_4 = __this->get_image_2();
		NullCheck(L_4);
		RectTransform_t3349966182 * L_5 = Graphic_get_rectTransform_m2697395074(L_4, /*hidden argument*/NULL);
		float L_6 = V_0;
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, (0.0f), (0.0f), L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localEulerAngles_m2927195985(L_5, L_7, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_8 = __this->get_cam_3();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		WebCamTexture_t1079476942 * L_10 = __this->get_cam_3();
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_10);
		V_1 = ((float)((float)(((float)((float)L_9)))/(float)(((float)((float)L_11)))));
		AspectRatioFitter_t3114550109 * L_12 = __this->get_arf_4();
		float L_13 = V_1;
		NullCheck(L_12);
		AspectRatioFitter_set_aspectRatio_m4159324881(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ClickTreeEvent::.ctor()
extern "C"  void ClickTreeEvent__ctor_m3578795207 (ClickTreeEvent_t925145888 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ClickTreeEvent::OnMouseDown()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_Get_TisAndroidJavaObject_t4251328308_m1778010852_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2213576056;
extern Il2CppCodeGenString* _stringLiteral903848959;
extern Il2CppCodeGenString* _stringLiteral1102173553;
extern Il2CppCodeGenString* _stringLiteral2289291713;
extern const uint32_t ClickTreeEvent_OnMouseDown_m3894428433_MetadataUsageId;
extern "C"  void ClickTreeEvent_OnMouseDown_m3894428433 (ClickTreeEvent_t925145888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClickTreeEvent_OnMouseDown_m3894428433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaClass_t2973420583 * V_0 = NULL;
	AndroidJavaObject_t4251328308 * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		AndroidJavaClass_t2973420583 * L_1 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_1, _stringLiteral2213576056, /*hidden argument*/NULL);
		V_0 = L_1;
		AndroidJavaClass_t2973420583 * L_2 = V_0;
		NullCheck(L_2);
		AndroidJavaObject_t4251328308 * L_3 = AndroidJavaObject_Get_TisAndroidJavaObject_t4251328308_m1778010852(L_2, _stringLiteral903848959, /*hidden argument*/AndroidJavaObject_Get_TisAndroidJavaObject_t4251328308_m1778010852_MethodInfo_var);
		V_1 = L_3;
		AndroidJavaObject_t4251328308 * L_4 = V_1;
		ObjectU5BU5D_t3614634134* L_5 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral2289291713);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2289291713);
		NullCheck(L_4);
		AndroidJavaObject_Call_m3681854287(L_4, _stringLiteral1102173553, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateNote::.ctor()
extern "C"  void CreateNote__ctor_m1783892855 (CreateNote_t925731540 * __this, const MethodInfo* method)
{
	{
		Info_t2064034856 * L_0 = Info_GetInstance_m196706287(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_info_8(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CreateNote::createNote()
extern Il2CppClass* U3CcreateNoteU3Ec__Iterator0_t3242378972_il2cpp_TypeInfo_var;
extern const uint32_t CreateNote_createNote_m324287845_MetadataUsageId;
extern "C"  Il2CppObject * CreateNote_createNote_m324287845 (CreateNote_t925731540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CreateNote_createNote_m324287845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CcreateNoteU3Ec__Iterator0_t3242378972 * V_0 = NULL;
	{
		U3CcreateNoteU3Ec__Iterator0_t3242378972 * L_0 = (U3CcreateNoteU3Ec__Iterator0_t3242378972 *)il2cpp_codegen_object_new(U3CcreateNoteU3Ec__Iterator0_t3242378972_il2cpp_TypeInfo_var);
		U3CcreateNoteU3Ec__Iterator0__ctor_m241372123(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CcreateNoteU3Ec__Iterator0_t3242378972 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_9(__this);
		U3CcreateNoteU3Ec__Iterator0_t3242378972 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean CreateNote::getTextForm()
extern "C"  bool CreateNote_getTextForm_m2844656960 (CreateNote_t925731540 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void CreateNote::toggleBtn()
extern "C"  void CreateNote_toggleBtn_m1760753357 (CreateNote_t925731540 * __this, const MethodInfo* method)
{
	{
		Canvas_t209405766 * L_0 = __this->get_writeForm_4();
		Canvas_t209405766 * L_1 = __this->get_writeForm_4();
		NullCheck(L_1);
		bool L_2 = Behaviour_get_enabled_m4079055610(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateNote::createBtn()
extern "C"  void CreateNote_createBtn_m3541765391 (CreateNote_t925731540 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isRunning_3();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = CreateNote_createNote_m324287845(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void CreateNote/<createNote>c__Iterator0::.ctor()
extern "C"  void U3CcreateNoteU3Ec__Iterator0__ctor_m241372123 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CreateNote/<createNote>c__Iterator0::MoveNext()
extern Il2CppClass* WWWForm_t3950226929_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4244870320_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisObjVO_t1115677294_m333022829_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral696029425;
extern Il2CppCodeGenString* _stringLiteral3068682405;
extern Il2CppCodeGenString* _stringLiteral2435225568;
extern Il2CppCodeGenString* _stringLiteral1561970665;
extern Il2CppCodeGenString* _stringLiteral4165124290;
extern Il2CppCodeGenString* _stringLiteral1391431453;
extern Il2CppCodeGenString* _stringLiteral2237594080;
extern Il2CppCodeGenString* _stringLiteral2658865507;
extern const uint32_t U3CcreateNoteU3Ec__Iterator0_MoveNext_m400128733_MetadataUsageId;
extern "C"  bool U3CcreateNoteU3Ec__Iterator0_MoveNext_m400128733 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CcreateNoteU3Ec__Iterator0_MoveNext_m400128733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_U24PC_12();
		V_0 = L_0;
		__this->set_U24PC_12((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_019e;
		}
	}
	{
		goto IL_0329;
	}

IL_0021:
	{
		CreateNote_t925731540 * L_2 = __this->get_U24this_9();
		CreateNote_t925731540 * L_3 = __this->get_U24this_9();
		NullCheck(L_3);
		Info_t2064034856 * L_4 = L_3->get_info_8();
		NullCheck(L_4);
		List_1_t1125654279 * L_5 = Info_getObjList_m3675622311(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_obj_9(L_5);
		CreateNote_t925731540 * L_6 = __this->get_U24this_9();
		NullCheck(L_6);
		bool L_7 = CreateNote_getTextForm_m2844656960(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0322;
		}
	}
	{
		CreateNote_t925731540 * L_8 = __this->get_U24this_9();
		NullCheck(L_8);
		L_8->set_isRunning_3((bool)1);
		WWWForm_t3950226929 * L_9 = (WWWForm_t3950226929 *)il2cpp_codegen_object_new(WWWForm_t3950226929_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2129424870(L_9, /*hidden argument*/NULL);
		__this->set_U3CformU3E__0_0(L_9);
		WWWForm_t3950226929 * L_10 = __this->get_U3CformU3E__0_0();
		CreateNote_t925731540 * L_11 = __this->get_U24this_9();
		NullCheck(L_11);
		Info_t2064034856 * L_12 = L_11->get_info_8();
		NullCheck(L_12);
		String_t* L_13 = Info_getUserId_m1498575430(L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		WWWForm_AddField_m1334606983(L_10, _stringLiteral287061489, L_13, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_14 = __this->get_U3CformU3E__0_0();
		CreateNote_t925731540 * L_15 = __this->get_U24this_9();
		NullCheck(L_15);
		Info_t2064034856 * L_16 = L_15->get_info_8();
		NullCheck(L_16);
		float L_17 = Info_getLatitude_m3586550869(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		String_t* L_18 = Single_ToString_m1813392066((&V_1), /*hidden argument*/NULL);
		NullCheck(L_14);
		WWWForm_AddField_m1334606983(L_14, _stringLiteral696029425, L_18, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_19 = __this->get_U3CformU3E__0_0();
		CreateNote_t925731540 * L_20 = __this->get_U24this_9();
		NullCheck(L_20);
		Info_t2064034856 * L_21 = L_20->get_info_8();
		NullCheck(L_21);
		float L_22 = Info_getLongitude_m4220800764(L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		String_t* L_23 = Single_ToString_m1813392066((&V_2), /*hidden argument*/NULL);
		NullCheck(L_19);
		WWWForm_AddField_m1334606983(L_19, _stringLiteral3068682405, L_23, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_24 = __this->get_U3CformU3E__0_0();
		CreateNote_t925731540 * L_25 = __this->get_U24this_9();
		NullCheck(L_25);
		Text_t356221433 * L_26 = L_25->get_title_5();
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_26);
		NullCheck(L_24);
		WWWForm_AddField_m1334606983(L_24, _stringLiteral2435225568, L_27, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_28 = __this->get_U3CformU3E__0_0();
		CreateNote_t925731540 * L_29 = __this->get_U24this_9();
		NullCheck(L_29);
		Text_t356221433 * L_30 = L_29->get_content_6();
		NullCheck(L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_30);
		NullCheck(L_28);
		WWWForm_AddField_m1334606983(L_28, _stringLiteral1561970665, L_31, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_32 = __this->get_U3CformU3E__0_0();
		NullCheck(L_32);
		Dictionary_2_t3943999495 * L_33 = WWWForm_get_headers_m3744493569(L_32, /*hidden argument*/NULL);
		__this->set_U3CheadersU3E__1_1(L_33);
		Dictionary_2_t3943999495 * L_34 = __this->get_U3CheadersU3E__1_1();
		NullCheck(L_34);
		Dictionary_2_set_Item_m4244870320(L_34, _stringLiteral4165124290, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		WWWForm_t3950226929 * L_35 = __this->get_U3CformU3E__0_0();
		NullCheck(L_35);
		ByteU5BU5D_t3397334013* L_36 = WWWForm_get_data_m1788094649(L_35, /*hidden argument*/NULL);
		__this->set_U3CrawDataU3E__2_2(L_36);
		__this->set_U3CurlU3E__3_3(_stringLiteral2237594080);
		String_t* L_37 = __this->get_U3CurlU3E__3_3();
		ByteU5BU5D_t3397334013* L_38 = __this->get_U3CrawDataU3E__2_2();
		Dictionary_2_t3943999495 * L_39 = __this->get_U3CheadersU3E__1_1();
		WWW_t2919945039 * L_40 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m1577105574(L_40, L_37, L_38, L_39, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__4_4(L_40);
		WWW_t2919945039 * L_41 = __this->get_U3CwwwU3E__4_4();
		__this->set_U24current_10(L_41);
		bool L_42 = __this->get_U24disposing_11();
		if (L_42)
		{
			goto IL_0199;
		}
	}
	{
		__this->set_U24PC_12(1);
	}

IL_0199:
	{
		goto IL_032b;
	}

IL_019e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_43 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3Cutf8U3E__5_5(L_43);
		Encoding_t663144255 * L_44 = __this->get_U3Cutf8U3E__5_5();
		WWW_t2919945039 * L_45 = __this->get_U3CwwwU3E__4_4();
		NullCheck(L_45);
		String_t* L_46 = WWW_get_text_m1558985139(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		ByteU5BU5D_t3397334013* L_47 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_44, L_46);
		__this->set_U3Cutf8byteU3E__6_6(L_47);
		Encoding_t663144255 * L_48 = __this->get_U3Cutf8U3E__5_5();
		ByteU5BU5D_t3397334013* L_49 = __this->get_U3Cutf8byteU3E__6_6();
		NullCheck(L_48);
		String_t* L_50 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_48, L_49);
		__this->set_U3Cutf8stringU3E__7_7(L_50);
		String_t* L_51 = __this->get_U3Cutf8stringU3E__7_7();
		JSONObject_t1971882247 * L_52 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m1387305321(L_52, L_51, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__8_8(L_52);
		JSONObject_t1971882247 * L_53 = __this->get_U3CjsonU3E__8_8();
		if (!L_53)
		{
			goto IL_0305;
		}
	}
	{
		JSONObject_t1971882247 * L_54 = __this->get_U3CjsonU3E__8_8();
		NullCheck(L_54);
		List_1_t1341003379 * L_55 = L_54->get_list_7();
		NullCheck(L_55);
		JSONObject_t1971882247 * L_56 = List_1_get_Item_m429614411(L_55, 0, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_56);
		String_t* L_57 = L_56->get_str_9();
		NullCheck(L_57);
		bool L_58 = String_Equals_m2633592423(L_57, _stringLiteral2658865507, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0305;
		}
	}
	{
		CreateNote_t925731540 * L_59 = __this->get_U24this_9();
		NullCheck(L_59);
		Transform_t3275118058 * L_60 = L_59->get_worlObj_7();
		NullCheck(L_60);
		Vector3_t2243707580  L_61 = Transform_get_eulerAngles_m4066505159(L_60, /*hidden argument*/NULL);
		V_4 = L_61;
		float L_62 = (&V_4)->get_x_1();
		V_3 = L_62;
		float L_63 = V_3;
		Vector3__ctor_m2638739322((&V_5), L_63, (-2.0f), (10.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_64 = V_5;
		CreateNote_t925731540 * L_65 = __this->get_U24this_9();
		NullCheck(L_65);
		Transform_t3275118058 * L_66 = Component_get_transform_m2697483695(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		Vector3_t2243707580  L_67 = Transform_get_position_m1104419803(L_66, /*hidden argument*/NULL);
		Vector3_t2243707580  L_68 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_64, L_67, /*hidden argument*/NULL);
		V_5 = L_68;
		CreateNote_t925731540 * L_69 = __this->get_U24this_9();
		NullCheck(L_69);
		List_1_t1125654279 * L_70 = L_69->get_obj_9();
		CreateNote_t925731540 * L_71 = __this->get_U24this_9();
		NullCheck(L_71);
		GameObject_t1756533147 * L_72 = L_71->get_prefab_2();
		Vector3_t2243707580  L_73 = V_5;
		Quaternion_t4030073918  L_74 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_75 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_72, L_73, L_74, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		NullCheck(L_70);
		List_1_Add_m3441471442(L_70, L_75, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		CreateNote_t925731540 * L_76 = __this->get_U24this_9();
		NullCheck(L_76);
		List_1_t1125654279 * L_77 = L_76->get_obj_9();
		CreateNote_t925731540 * L_78 = __this->get_U24this_9();
		NullCheck(L_78);
		List_1_t1125654279 * L_79 = L_78->get_obj_9();
		NullCheck(L_79);
		int32_t L_80 = List_1_get_Count_m2764296230(L_79, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		NullCheck(L_77);
		GameObject_t1756533147 * L_81 = List_1_get_Item_m939767277(L_77, ((int32_t)((int32_t)L_80-(int32_t)1)), /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_81);
		ObjVO_t1115677294 * L_82 = GameObject_GetComponent_TisObjVO_t1115677294_m333022829(L_81, /*hidden argument*/GameObject_GetComponent_TisObjVO_t1115677294_m333022829_MethodInfo_var);
		CreateNote_t925731540 * L_83 = __this->get_U24this_9();
		NullCheck(L_83);
		Text_t356221433 * L_84 = L_83->get_title_5();
		NullCheck(L_84);
		String_t* L_85 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_84);
		NullCheck(L_82);
		ObjVO_setTitle_m3418548101(L_82, L_85, /*hidden argument*/NULL);
		CreateNote_t925731540 * L_86 = __this->get_U24this_9();
		NullCheck(L_86);
		List_1_t1125654279 * L_87 = L_86->get_obj_9();
		CreateNote_t925731540 * L_88 = __this->get_U24this_9();
		NullCheck(L_88);
		List_1_t1125654279 * L_89 = L_88->get_obj_9();
		NullCheck(L_89);
		int32_t L_90 = List_1_get_Count_m2764296230(L_89, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		NullCheck(L_87);
		GameObject_t1756533147 * L_91 = List_1_get_Item_m939767277(L_87, ((int32_t)((int32_t)L_90-(int32_t)1)), /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_91);
		ObjVO_t1115677294 * L_92 = GameObject_GetComponent_TisObjVO_t1115677294_m333022829(L_91, /*hidden argument*/GameObject_GetComponent_TisObjVO_t1115677294_m333022829_MethodInfo_var);
		CreateNote_t925731540 * L_93 = __this->get_U24this_9();
		NullCheck(L_93);
		Text_t356221433 * L_94 = L_93->get_content_6();
		NullCheck(L_94);
		String_t* L_95 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_94);
		NullCheck(L_92);
		ObjVO_setContent_m4129050350(L_92, L_95, /*hidden argument*/NULL);
	}

IL_0305:
	{
		CreateNote_t925731540 * L_96 = __this->get_U24this_9();
		NullCheck(L_96);
		L_96->set_isRunning_3((bool)0);
		CreateNote_t925731540 * L_97 = __this->get_U24this_9();
		NullCheck(L_97);
		Canvas_t209405766 * L_98 = L_97->get_writeForm_4();
		NullCheck(L_98);
		Behaviour_set_enabled_m1796096907(L_98, (bool)0, /*hidden argument*/NULL);
	}

IL_0322:
	{
		__this->set_U24PC_12((-1));
	}

IL_0329:
	{
		return (bool)0;
	}

IL_032b:
	{
		return (bool)1;
	}
}
// System.Object CreateNote/<createNote>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CcreateNoteU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m905848205 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_10();
		return L_0;
	}
}
// System.Object CreateNote/<createNote>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CcreateNoteU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2234605237 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_10();
		return L_0;
	}
}
// System.Void CreateNote/<createNote>c__Iterator0::Dispose()
extern "C"  void U3CcreateNoteU3Ec__Iterator0_Dispose_m552473446 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_11((bool)1);
		__this->set_U24PC_12((-1));
		return;
	}
}
// System.Void CreateNote/<createNote>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CcreateNoteU3Ec__Iterator0_Reset_m1809002792_MetadataUsageId;
extern "C"  void U3CcreateNoteU3Ec__Iterator0_Reset_m1809002792 (U3CcreateNoteU3Ec__Iterator0_t3242378972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CcreateNoteU3Ec__Iterator0_Reset_m1809002792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Generate::.ctor()
extern "C"  void Generate__ctor_m609598290 (Generate_t664530809 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Generate::Start()
extern Il2CppClass* U3CStartU3Ec__Iterator0_t3787076049_il2cpp_TypeInfo_var;
extern const uint32_t Generate_Start_m1411269226_MetadataUsageId;
extern "C"  Il2CppObject * Generate_Start_m1411269226 (Generate_t664530809 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Generate_Start_m1411269226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartU3Ec__Iterator0_t3787076049 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t3787076049 * L_0 = (U3CStartU3Ec__Iterator0_t3787076049 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t3787076049_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m3868338480(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t3787076049 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_4(__this);
		U3CStartU3Ec__Iterator0_t3787076049 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Generate::setPrefab(System.Int32,System.String,System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTreeScript_t1745883499_m2418258018_MethodInfo_var;
extern const uint32_t Generate_setPrefab_m2335224753_MetadataUsageId;
extern "C"  void Generate_setPrefab_m2335224753 (Generate_t664530809 * __this, int32_t ___idx0, String_t* ___title1, String_t* ___content2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Generate_setPrefab_m2335224753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	TreeScript_t1745883499 * V_3 = NULL;
	{
		int32_t L_0 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)-10), ((int32_t)10), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)-10), ((int32_t)10), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		Vector3__ctor_m2638739322((&V_2), (((float)((float)L_2))), (-4.0f), (((float)((float)L_3))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = V_2;
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		List_1_t1125654279 * L_8 = __this->get_obj_4();
		GameObject_t1756533147 * L_9 = __this->get_prefab_2();
		Vector3_t2243707580  L_10 = V_2;
		Quaternion_t4030073918  L_11 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_12 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		NullCheck(L_8);
		List_1_Add_m3441471442(L_8, L_12, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		List_1_t1125654279 * L_13 = __this->get_obj_4();
		int32_t L_14 = ___idx0;
		NullCheck(L_13);
		GameObject_t1756533147 * L_15 = List_1_get_Item_m939767277(L_13, L_14, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_15);
		TreeScript_t1745883499 * L_16 = GameObject_GetComponent_TisTreeScript_t1745883499_m2418258018(L_15, /*hidden argument*/GameObject_GetComponent_TisTreeScript_t1745883499_m2418258018_MethodInfo_var);
		V_3 = L_16;
		TreeScript_t1745883499 * L_17 = V_3;
		String_t* L_18 = ___title1;
		NullCheck(L_17);
		TreeScript_setTitle_m3285513596(L_17, L_18, /*hidden argument*/NULL);
		TreeScript_t1745883499 * L_19 = V_3;
		String_t* L_20 = ___content2;
		NullCheck(L_19);
		TreeScript_setContent_m3995843423(L_19, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Generate::Wapper()
extern "C"  void Generate_Wapper_m3130702321 (Generate_t664530809 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isRunning_8();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = Generate_Start_m1411269226(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void Generate/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3868338480 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Generate/<Start>c__Iterator0::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m4030601119_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1719915765;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral4048476618;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m4239949672_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m4239949672 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m4239949672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0067;
		}
	}
	{
		goto IL_01ea;
	}

IL_0021:
	{
		Generate_t664530809 * L_2 = __this->get_U24this_4();
		NullCheck(L_2);
		L_2->set_isRunning_8((bool)1);
		Generate_t664530809 * L_3 = __this->get_U24this_4();
		WWW_t2919945039 * L_4 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_4, _stringLiteral1719915765, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_itemsData_7(L_4);
		Generate_t664530809 * L_5 = __this->get_U24this_4();
		NullCheck(L_5);
		WWW_t2919945039 * L_6 = L_5->get_itemsData_7();
		__this->set_U24current_5(L_6);
		bool L_7 = __this->get_U24disposing_6();
		if (L_7)
		{
			goto IL_0062;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0062:
	{
		goto IL_01ec;
	}

IL_0067:
	{
		Generate_t664530809 * L_8 = __this->get_U24this_4();
		NullCheck(L_8);
		L_8->set_isRunning_8((bool)0);
		Generate_t664530809 * L_9 = __this->get_U24this_4();
		NullCheck(L_9);
		List_1_t1125654279 * L_10 = L_9->get_obj_4();
		if (!L_10)
		{
			goto IL_009f;
		}
	}
	{
		Generate_t664530809 * L_11 = __this->get_U24this_4();
		NullCheck(L_11);
		List_1_t1125654279 * L_12 = L_11->get_obj_4();
		NullCheck(L_12);
		List_1_Clear_m4030601119(L_12, /*hidden argument*/List_1_Clear_m4030601119_MethodInfo_var);
		Generate_t664530809 * L_13 = __this->get_U24this_4();
		NullCheck(L_13);
		L_13->set_obj_4((List_1_t1125654279 *)NULL);
	}

IL_009f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_14 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3Cutf8U3E__0_0(L_14);
		Encoding_t663144255 * L_15 = __this->get_U3Cutf8U3E__0_0();
		Generate_t664530809 * L_16 = __this->get_U24this_4();
		NullCheck(L_16);
		WWW_t2919945039 * L_17 = L_16->get_itemsData_7();
		NullCheck(L_17);
		String_t* L_18 = WWW_get_text_m1558985139(L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		ByteU5BU5D_t3397334013* L_19 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_15, L_18);
		__this->set_U3Cutf8byteU3E__1_1(L_19);
		Encoding_t663144255 * L_20 = __this->get_U3Cutf8U3E__0_0();
		ByteU5BU5D_t3397334013* L_21 = __this->get_U3Cutf8byteU3E__1_1();
		NullCheck(L_20);
		String_t* L_22 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_20, L_21);
		__this->set_U3Cutf8stringU3E__2_2(L_22);
		String_t* L_23 = __this->get_U3Cutf8stringU3E__2_2();
		JSONObject_t1971882247 * L_24 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m1387305321(L_24, L_23, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__3_3(L_24);
		JSONObject_t1971882247 * L_25 = __this->get_U3CjsonU3E__3_3();
		if (!L_25)
		{
			goto IL_01d9;
		}
	}
	{
		JSONObject_t1971882247 * L_26 = __this->get_U3CjsonU3E__3_3();
		NullCheck(L_26);
		List_1_t1341003379 * L_27 = L_26->get_list_7();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m3485821544(L_27, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		V_1 = L_28;
		Generate_t664530809 * L_29 = __this->get_U24this_4();
		List_1_t1125654279 * L_30 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_30, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		NullCheck(L_29);
		L_29->set_obj_4(L_30);
		V_2 = 0;
		goto IL_01cd;
	}

IL_012a:
	{
		Generate_t664530809 * L_31 = __this->get_U24this_4();
		JSONObject_t1971882247 * L_32 = __this->get_U3CjsonU3E__3_3();
		NullCheck(L_32);
		List_1_t1341003379 * L_33 = L_32->get_list_7();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		JSONObject_t1971882247 * L_35 = List_1_get_Item_m429614411(L_33, L_34, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_35);
		List_1_t1341003379 * L_36 = L_35->get_list_7();
		NullCheck(L_36);
		JSONObject_t1971882247 * L_37 = List_1_get_Item_m429614411(L_36, 6, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_37);
		String_t* L_38 = L_37->get_str_9();
		NullCheck(L_31);
		L_31->set_title_5(L_38);
		Generate_t664530809 * L_39 = __this->get_U24this_4();
		JSONObject_t1971882247 * L_40 = __this->get_U3CjsonU3E__3_3();
		NullCheck(L_40);
		List_1_t1341003379 * L_41 = L_40->get_list_7();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		JSONObject_t1971882247 * L_43 = List_1_get_Item_m429614411(L_41, L_42, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_43);
		List_1_t1341003379 * L_44 = L_43->get_list_7();
		NullCheck(L_44);
		JSONObject_t1971882247 * L_45 = List_1_get_Item_m429614411(L_44, 7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_45);
		String_t* L_46 = L_45->get_str_9();
		NullCheck(L_39);
		L_39->set_content_6(L_46);
		Generate_t664530809 * L_47 = __this->get_U24this_4();
		NullCheck(L_47);
		String_t* L_48 = L_47->get_title_5();
		Generate_t664530809 * L_49 = __this->get_U24this_4();
		NullCheck(L_49);
		String_t* L_50 = L_49->get_content_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = String_Concat_m612901809(NULL /*static, unused*/, L_48, _stringLiteral372029314, L_50, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Generate_t664530809 * L_52 = __this->get_U24this_4();
		int32_t L_53 = V_2;
		Generate_t664530809 * L_54 = __this->get_U24this_4();
		NullCheck(L_54);
		String_t* L_55 = L_54->get_title_5();
		Generate_t664530809 * L_56 = __this->get_U24this_4();
		NullCheck(L_56);
		String_t* L_57 = L_56->get_content_6();
		NullCheck(L_52);
		Generate_setPrefab_m2335224753(L_52, L_53, L_55, L_57, /*hidden argument*/NULL);
		int32_t L_58 = V_2;
		V_2 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_01cd:
	{
		int32_t L_59 = V_2;
		int32_t L_60 = V_1;
		if ((((int32_t)L_59) < ((int32_t)L_60)))
		{
			goto IL_012a;
		}
	}
	{
		goto IL_01e3;
	}

IL_01d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4048476618, /*hidden argument*/NULL);
	}

IL_01e3:
	{
		__this->set_U24PC_7((-1));
	}

IL_01ea:
	{
		return (bool)0;
	}

IL_01ec:
	{
		return (bool)1;
	}
}
// System.Object Generate/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1363090004 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Generate/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1353901516 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void Generate/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m4023550243 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void Generate/<Start>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m2142271433_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2142271433 (U3CStartU3Ec__Iterator0_t3787076049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m2142271433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GyroCamera::.ctor()
extern "C"  void GyroCamera__ctor_m1396902201 (GyroCamera_t3228729828 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GyroCamera::Start()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3752213123;
extern const uint32_t GyroCamera_Start_m396900657_MetadataUsageId;
extern "C"  void GyroCamera_Start_m396900657 (GyroCamera_t3228729828 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroCamera_Start_m396900657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		bool L_0 = SystemInfo_get_supportsGyroscope_m2145709384(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_gyroSupported_3(L_0);
		GameObject_t1756533147 * L_1 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_1, _stringLiteral3752213123, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m2469242620(L_3, L_5, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_parent_m3281327839(L_6, L_8, /*hidden argument*/NULL);
		bool L_9 = __this->get_gyroSupported_3();
		if (!L_9)
		{
			goto IL_009d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Gyroscope_t1705362817 * L_10 = Input_get_gyro_m4027090408(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_gyro_2(L_10);
		Gyroscope_t1705362817 * L_11 = __this->get_gyro_2();
		NullCheck(L_11);
		Gyroscope_set_enabled_m487023127(L_11, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = V_0;
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = GameObject_get_transform_m909382139(L_12, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_14 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (90.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_rotation_m3411284563(L_13, L_14, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Quaternion__ctor_m3196903881(&L_15, (0.0f), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_rotFix_4(L_15);
	}

IL_009d:
	{
		return;
	}
}
// System.Void GyroCamera::Update()
extern "C"  void GyroCamera_Update_m1156475090 (GyroCamera_t3228729828 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_gyroSupported_3();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		float L_1 = __this->get_startY_6();
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		GyroCamera_ResetGyroRotation_m4143923043(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Gyroscope_t1705362817 * L_3 = __this->get_gyro_2();
		NullCheck(L_3);
		Quaternion_t4030073918  L_4 = Gyroscope_get_attitude_m3665233473(L_3, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = __this->get_rotFix_4();
		Quaternion_t4030073918  L_6 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localRotation_m2055111962(L_2, L_6, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void GyroCamera::ResetGyroRotation()
extern "C"  void GyroCamera_ResetGyroRotation_m4143923043 (GyroCamera_t3228729828 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_eulerAngles_m4066505159(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		__this->set_startY_6(L_2);
		Transform_t3275118058 * L_3 = __this->get_worldObj_5();
		float L_4 = __this->get_startY_6();
		Quaternion_t4030073918  L_5 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), L_4, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m3411284563(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Info::.ctor()
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1448002985;
extern const uint32_t Info__ctor_m3149560641_MetadataUsageId;
extern "C"  void Info__ctor_m3149560641 (Info_t2064034856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Info__ctor_m3149560641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set_objList_1(L_0);
		__this->set_UserId_2(_stringLiteral1448002985);
		__this->set_latitude_3((37.49466f));
		__this->set_longitude_4((127.037651f));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Info Info::GetInstance()
extern Il2CppClass* Info_t2064034856_il2cpp_TypeInfo_var;
extern const uint32_t Info_GetInstance_m196706287_MetadataUsageId;
extern "C"  Info_t2064034856 * Info_GetInstance_m196706287 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Info_GetInstance_m196706287_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Info_t2064034856 * L_0 = ((Info_t2064034856_StaticFields*)Info_t2064034856_il2cpp_TypeInfo_var->static_fields)->get_instance_0();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Info_t2064034856 * L_1 = (Info_t2064034856 *)il2cpp_codegen_object_new(Info_t2064034856_il2cpp_TypeInfo_var);
		Info__ctor_m3149560641(L_1, /*hidden argument*/NULL);
		((Info_t2064034856_StaticFields*)Info_t2064034856_il2cpp_TypeInfo_var->static_fields)->set_instance_0(L_1);
	}

IL_0014:
	{
		Info_t2064034856 * L_2 = ((Info_t2064034856_StaticFields*)Info_t2064034856_il2cpp_TypeInfo_var->static_fields)->get_instance_0();
		return L_2;
	}
}
// System.Collections.Generic.List`1<UnityEngine.GameObject> Info::getObjList()
extern "C"  List_1_t1125654279 * Info_getObjList_m3675622311 (Info_t2064034856 * __this, const MethodInfo* method)
{
	{
		List_1_t1125654279 * L_0 = __this->get_objList_1();
		return L_0;
	}
}
// System.Single Info::getLatitude()
extern "C"  float Info_getLatitude_m3586550869 (Info_t2064034856 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_latitude_3();
		return L_0;
	}
}
// System.Single Info::getLongitude()
extern "C"  float Info_getLongitude_m4220800764 (Info_t2064034856 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_longitude_4();
		return L_0;
	}
}
// System.String Info::getUserId()
extern "C"  String_t* Info_getUserId_m1498575430 (Info_t2064034856 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_UserId_2();
		return L_0;
	}
}
// System.Void Info::setUserId(System.String)
extern "C"  void Info_setUserId_m2955544633 (Info_t2064034856 * __this, String_t* ___id0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id0;
		__this->set_UserId_2(L_0);
		return;
	}
}
// System.Void Info::setLatitude(System.Single)
extern "C"  void Info_setLatitude_m3302714788 (Info_t2064034856 * __this, float ___lat0, const MethodInfo* method)
{
	{
		float L_0 = ___lat0;
		__this->set_latitude_3(L_0);
		return;
	}
}
// System.Void Info::setLongitude(System.Single)
extern "C"  void Info_setLongitude_m651487129 (Info_t2064034856 * __this, float ___lon0, const MethodInfo* method)
{
	{
		float L_0 = ___lon0;
		__this->set_longitude_4(L_0);
		return;
	}
}
// System.Void JSONObject::.ctor(JSONObject/Type)
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3984043222_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m4071571216_MetadataUsageId;
extern "C"  void JSONObject__ctor_m4071571216 (JSONObject_t1971882247 * __this, int32_t ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m4071571216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___t0;
		__this->set_type_6(L_0);
		int32_t L_1 = ___t0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___t0;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0030;
		}
	}
	{
		goto IL_004b;
	}

IL_0020:
	{
		List_1_t1341003379 * L_3 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_3, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		__this->set_list_7(L_3);
		goto IL_004b;
	}

IL_0030:
	{
		List_1_t1341003379 * L_4 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_4, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		__this->set_list_7(L_4);
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_8(L_5);
		goto IL_004b;
	}

IL_004b:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(System.Boolean)
extern "C"  void JSONObject__ctor_m840431321 (JSONObject_t1971882247 * __this, bool ___b0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_6(5);
		bool L_0 = ___b0;
		__this->set_b_13(L_0);
		return;
	}
}
// System.Void JSONObject::.ctor(System.Single)
extern "C"  void JSONObject__ctor_m3839245543 (JSONObject_t1971882247 * __this, float ___f0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_6(2);
		float L_0 = ___f0;
		__this->set_n_10(L_0);
		return;
	}
}
// System.Void JSONObject::.ctor(System.Int32)
extern "C"  void JSONObject__ctor_m2801954363 (JSONObject_t1971882247 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_6(2);
		int32_t L_0 = ___i0;
		__this->set_i_12((((int64_t)((int64_t)L_0))));
		__this->set_useInt_11((bool)1);
		int32_t L_1 = ___i0;
		__this->set_n_10((((float)((float)L_1))));
		return;
	}
}
// System.Void JSONObject::.ctor(System.Int64)
extern "C"  void JSONObject__ctor_m3964753740 (JSONObject_t1971882247 * __this, int64_t ___l0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_6(2);
		int64_t L_0 = ___l0;
		__this->set_i_12(L_0);
		__this->set_useInt_11((bool)1);
		int64_t L_1 = ___l0;
		__this->set_n_10((((float)((float)L_1))));
		return;
	}
}
// System.Void JSONObject::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3984043222_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1989408781_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1372024679_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1710042386_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3057035458_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4005245300_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m1919544205_MetadataUsageId;
extern "C"  void JSONObject__ctor_m1919544205 (JSONObject_t1971882247 * __this, Dictionary_2_t3943999495 * ___dic0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m1919544205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1701344717  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t969056901  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_6(3);
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_8(L_0);
		List_1_t1341003379 * L_1 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_1, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		__this->set_list_7(L_1);
		Dictionary_2_t3943999495 * L_2 = ___dic0;
		NullCheck(L_2);
		Enumerator_t969056901  L_3 = Dictionary_2_GetEnumerator_m2895728349(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var);
		V_1 = L_3;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_002f:
		{
			KeyValuePair_2_t1701344717  L_4 = Enumerator_get_Current_m1989408781((&V_1), /*hidden argument*/Enumerator_get_Current_m1989408781_MethodInfo_var);
			V_0 = L_4;
			List_1_t1398341365 * L_5 = __this->get_keys_8();
			String_t* L_6 = KeyValuePair_2_get_Key_m1372024679((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_MethodInfo_var);
			NullCheck(L_5);
			List_1_Add_m4061286785(L_5, L_6, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
			List_1_t1341003379 * L_7 = __this->get_list_7();
			String_t* L_8 = KeyValuePair_2_get_Value_m1710042386((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
			JSONObject_t1971882247 * L_9 = JSONObject_CreateStringObject_m227421806(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			NullCheck(L_7);
			List_1_Add_m3057035458(L_7, L_9, /*hidden argument*/List_1_Add_m3057035458_MethodInfo_var);
		}

IL_0060:
		{
			bool L_10 = Enumerator_MoveNext_m4005245300((&V_1), /*hidden argument*/Enumerator_MoveNext_m4005245300_MethodInfo_var);
			if (L_10)
			{
				goto IL_002f;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m882561911((&V_1), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
		IL2CPP_END_FINALLY(113)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007f:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(System.Collections.Generic.Dictionary`2<System.String,JSONObject>)
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3984043222_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3045170719_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1832947439_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2781186853_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1726882906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3057035458_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1874977526_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m214092148_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m3429744944_MetadataUsageId;
extern "C"  void JSONObject__ctor_m3429744944 (JSONObject_t1971882247 * __this, Dictionary_2_t3886661509 * ___dic0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m3429744944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1644006731  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t911718915  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_6(3);
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_8(L_0);
		List_1_t1341003379 * L_1 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_1, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		__this->set_list_7(L_1);
		Dictionary_2_t3886661509 * L_2 = ___dic0;
		NullCheck(L_2);
		Enumerator_t911718915  L_3 = Dictionary_2_GetEnumerator_m3045170719(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m3045170719_MethodInfo_var);
		V_1 = L_3;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005b;
		}

IL_002f:
		{
			KeyValuePair_2_t1644006731  L_4 = Enumerator_get_Current_m1832947439((&V_1), /*hidden argument*/Enumerator_get_Current_m1832947439_MethodInfo_var);
			V_0 = L_4;
			List_1_t1398341365 * L_5 = __this->get_keys_8();
			String_t* L_6 = KeyValuePair_2_get_Key_m2781186853((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m2781186853_MethodInfo_var);
			NullCheck(L_5);
			List_1_Add_m4061286785(L_5, L_6, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
			List_1_t1341003379 * L_7 = __this->get_list_7();
			JSONObject_t1971882247 * L_8 = KeyValuePair_2_get_Value_m1726882906((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1726882906_MethodInfo_var);
			NullCheck(L_7);
			List_1_Add_m3057035458(L_7, L_8, /*hidden argument*/List_1_Add_m3057035458_MethodInfo_var);
		}

IL_005b:
		{
			bool L_9 = Enumerator_MoveNext_m1874977526((&V_1), /*hidden argument*/Enumerator_MoveNext_m1874977526_MethodInfo_var);
			if (L_9)
			{
				goto IL_002f;
			}
		}

IL_0067:
		{
			IL2CPP_LEAVE(0x7A, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m214092148((&V_1), /*hidden argument*/Enumerator_Dispose_m214092148_MethodInfo_var);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x7A, IL_007a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007a:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(JSONObject/AddJSONContents)
extern "C"  void JSONObject__ctor_m3888149271 (JSONObject_t1971882247 * __this, AddJSONContents_t3850664647 * ___content0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		AddJSONContents_t3850664647 * L_0 = ___content0;
		NullCheck(L_0);
		AddJSONContents_Invoke_m1742311611(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::.ctor(JSONObject[])
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m32801174_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m398818817_MetadataUsageId;
extern "C"  void JSONObject__ctor_m398818817 (JSONObject_t1971882247 * __this, JSONObjectU5BU5D_t2270799614* ___objs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m398818817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_6(4);
		JSONObjectU5BU5D_t2270799614* L_0 = ___objs0;
		List_1_t1341003379 * L_1 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m32801174(L_1, (Il2CppObject*)(Il2CppObject*)L_0, /*hidden argument*/List_1__ctor_m32801174_MethodInfo_var);
		__this->set_list_7(L_1);
		return;
	}
}
// System.Void JSONObject::.ctor()
extern "C"  void JSONObject__ctor_m3429292120 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::.ctor(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  void JSONObject__ctor_m1387305321 (JSONObject_t1971882247 * __this, String_t* ___str0, int32_t ___maxDepth1, bool ___storeExcessLevels2, bool ___strict3, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___str0;
		int32_t L_1 = ___maxDepth1;
		bool L_2 = ___storeExcessLevels2;
		bool L_3 = ___strict3;
		JSONObject_Parse_m547301692(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JSONObject::get_isContainer()
extern "C"  bool JSONObject_get_isContainer_m3175857226 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((((int32_t)L_0) == ((int32_t)4)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = __this->get_type_6();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)3))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Int32 JSONObject::get_Count()
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern const uint32_t JSONObject_get_Count_m2165978410_MetadataUsageId;
extern "C"  int32_t JSONObject_get_Count_m2165978410 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_Count_m2165978410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1341003379 * L_0 = __this->get_list_7();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		List_1_t1341003379 * L_1 = __this->get_list_7();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m3485821544(L_1, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		return L_2;
	}
}
// System.Single JSONObject::get_f()
extern "C"  float JSONObject_get_f_m1158093019 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_n_10();
		return L_0;
	}
}
// JSONObject JSONObject::get_nullJO()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_nullJO_m3951718059_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_get_nullJO_m3951718059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_nullJO_m3951718059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m2024843434(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::get_obj()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_obj_m3201114506_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_get_obj_m3201114506 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_obj_m3201114506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m2024843434(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::get_arr()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_arr_m437956224_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_get_arr_m437956224 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_arr_m437956224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m2024843434(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::StringObject(System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_StringObject_m2324803274_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_StringObject_m2324803274 (Il2CppObject * __this /* static, unused */, String_t* ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_StringObject_m2324803274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_CreateStringObject_m227421806(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::Absorb(JSONObject)
extern const MethodInfo* List_1_AddRange_m3291711728_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1157143463_MethodInfo_var;
extern const uint32_t JSONObject_Absorb_m287377368_MetadataUsageId;
extern "C"  void JSONObject_Absorb_m287377368 (JSONObject_t1971882247 * __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Absorb_m287377368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1341003379 * L_0 = __this->get_list_7();
		JSONObject_t1971882247 * L_1 = ___obj0;
		NullCheck(L_1);
		List_1_t1341003379 * L_2 = L_1->get_list_7();
		NullCheck(L_0);
		List_1_AddRange_m3291711728(L_0, L_2, /*hidden argument*/List_1_AddRange_m3291711728_MethodInfo_var);
		List_1_t1398341365 * L_3 = __this->get_keys_8();
		JSONObject_t1971882247 * L_4 = ___obj0;
		NullCheck(L_4);
		List_1_t1398341365 * L_5 = L_4->get_keys_8();
		NullCheck(L_3);
		List_1_AddRange_m1157143463(L_3, L_5, /*hidden argument*/List_1_AddRange_m1157143463_MethodInfo_var);
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_str_9();
		__this->set_str_9(L_7);
		JSONObject_t1971882247 * L_8 = ___obj0;
		NullCheck(L_8);
		float L_9 = L_8->get_n_10();
		__this->set_n_10(L_9);
		JSONObject_t1971882247 * L_10 = ___obj0;
		NullCheck(L_10);
		bool L_11 = L_10->get_useInt_11();
		__this->set_useInt_11(L_11);
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		int64_t L_13 = L_12->get_i_12();
		__this->set_i_12(L_13);
		JSONObject_t1971882247 * L_14 = ___obj0;
		NullCheck(L_14);
		bool L_15 = L_14->get_b_13();
		__this->set_b_13(L_15);
		JSONObject_t1971882247 * L_16 = ___obj0;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_type_6();
		__this->set_type_6(L_17);
		return;
	}
}
// JSONObject JSONObject::Create()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m134949822_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m134949822 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m134949822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t1971882247 * L_0 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m3429292120(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::Create(JSONObject/Type)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3984043222_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t JSONObject_Create_m2024843434_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m2024843434 (Il2CppObject * __this /* static, unused */, int32_t ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m2024843434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		int32_t L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_type_6(L_2);
		int32_t L_3 = ___t0;
		if ((((int32_t)L_3) == ((int32_t)4)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_4 = ___t0;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_0030;
		}
	}
	{
		goto IL_004b;
	}

IL_0020:
	{
		JSONObject_t1971882247 * L_5 = V_0;
		List_1_t1341003379 * L_6 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_6, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		NullCheck(L_5);
		L_5->set_list_7(L_6);
		goto IL_004b;
	}

IL_0030:
	{
		JSONObject_t1971882247 * L_7 = V_0;
		List_1_t1341003379 * L_8 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_8, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		NullCheck(L_7);
		L_7->set_list_7(L_8);
		JSONObject_t1971882247 * L_9 = V_0;
		List_1_t1398341365 * L_10 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_10, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		NullCheck(L_9);
		L_9->set_keys_8(L_10);
		goto IL_004b;
	}

IL_004b:
	{
		JSONObject_t1971882247 * L_11 = V_0;
		return L_11;
	}
}
// JSONObject JSONObject::Create(System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m322727955_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m322727955 (Il2CppObject * __this /* static, unused */, bool ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m322727955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(5);
		JSONObject_t1971882247 * L_2 = V_0;
		bool L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_b_13(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.Single)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m4234548109_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m4234548109 (Il2CppObject * __this /* static, unused */, float ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m4234548109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(2);
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_n_10(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m2780992853_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m2780992853 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m2780992853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(2);
		JSONObject_t1971882247 * L_2 = V_0;
		int32_t L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_n_10((((float)((float)L_3))));
		JSONObject_t1971882247 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_useInt_11((bool)1);
		JSONObject_t1971882247 * L_5 = V_0;
		int32_t L_6 = ___val0;
		NullCheck(L_5);
		L_5->set_i_12((((int64_t)((int64_t)L_6))));
		JSONObject_t1971882247 * L_7 = V_0;
		return L_7;
	}
}
// JSONObject JSONObject::Create(System.Int64)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m1974423762_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m1974423762 (Il2CppObject * __this /* static, unused */, int64_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m1974423762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(2);
		JSONObject_t1971882247 * L_2 = V_0;
		int64_t L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_n_10((((float)((float)L_3))));
		JSONObject_t1971882247 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_useInt_11((bool)1);
		JSONObject_t1971882247 * L_5 = V_0;
		int64_t L_6 = ___val0;
		NullCheck(L_5);
		L_5->set_i_12(L_6);
		JSONObject_t1971882247 * L_7 = V_0;
		return L_7;
	}
}
// JSONObject JSONObject::CreateStringObject(System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_CreateStringObject_m227421806_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_CreateStringObject_m227421806 (Il2CppObject * __this /* static, unused */, String_t* ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_CreateStringObject_m227421806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(1);
		JSONObject_t1971882247 * L_2 = V_0;
		String_t* L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_str_9(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::CreateBakedObject(System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_CreateBakedObject_m1414246618_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_CreateBakedObject_m1414246618 (Il2CppObject * __this /* static, unused */, String_t* ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_CreateBakedObject_m1414246618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(6);
		JSONObject_t1971882247 * L_2 = V_0;
		String_t* L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_str_9(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.String,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m1582109667_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m1582109667 (Il2CppObject * __this /* static, unused */, String_t* ___val0, int32_t ___maxDepth1, bool ___storeExcessLevels2, bool ___strict3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m1582109667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		String_t* L_2 = ___val0;
		int32_t L_3 = ___maxDepth1;
		bool L_4 = ___storeExcessLevels2;
		bool L_5 = ___strict3;
		NullCheck(L_1);
		JSONObject_Parse_m547301692(L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_6 = V_0;
		return L_6;
	}
}
// JSONObject JSONObject::Create(JSONObject/AddJSONContents)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m2296526109_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m2296526109 (Il2CppObject * __this /* static, unused */, AddJSONContents_t3850664647 * ___content0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m2296526109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		AddJSONContents_t3850664647 * L_1 = ___content0;
		JSONObject_t1971882247 * L_2 = V_0;
		NullCheck(L_1);
		AddJSONContents_Invoke_m1742311611(L_1, L_2, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_3 = V_0;
		return L_3;
	}
}
// JSONObject JSONObject::Create(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3984043222_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1989408781_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1372024679_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1710042386_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3057035458_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4005245300_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern const uint32_t JSONObject_Create_m2408207271_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m2408207271 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3943999495 * ___dic0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m2408207271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	KeyValuePair_2_t1701344717  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t969056901  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(3);
		JSONObject_t1971882247 * L_2 = V_0;
		List_1_t1398341365 * L_3 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_3, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		NullCheck(L_2);
		L_2->set_keys_8(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		List_1_t1341003379 * L_5 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_5, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		NullCheck(L_4);
		L_4->set_list_7(L_5);
		Dictionary_2_t3943999495 * L_6 = ___dic0;
		NullCheck(L_6);
		Enumerator_t969056901  L_7 = Dictionary_2_GetEnumerator_m2895728349(L_6, /*hidden argument*/Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var);
		V_2 = L_7;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_002f:
		{
			KeyValuePair_2_t1701344717  L_8 = Enumerator_get_Current_m1989408781((&V_2), /*hidden argument*/Enumerator_get_Current_m1989408781_MethodInfo_var);
			V_1 = L_8;
			JSONObject_t1971882247 * L_9 = V_0;
			NullCheck(L_9);
			List_1_t1398341365 * L_10 = L_9->get_keys_8();
			String_t* L_11 = KeyValuePair_2_get_Key_m1372024679((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_MethodInfo_var);
			NullCheck(L_10);
			List_1_Add_m4061286785(L_10, L_11, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
			JSONObject_t1971882247 * L_12 = V_0;
			NullCheck(L_12);
			List_1_t1341003379 * L_13 = L_12->get_list_7();
			String_t* L_14 = KeyValuePair_2_get_Value_m1710042386((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
			JSONObject_t1971882247 * L_15 = JSONObject_CreateStringObject_m227421806(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			NullCheck(L_13);
			List_1_Add_m3057035458(L_13, L_15, /*hidden argument*/List_1_Add_m3057035458_MethodInfo_var);
		}

IL_0060:
		{
			bool L_16 = Enumerator_MoveNext_m4005245300((&V_2), /*hidden argument*/Enumerator_MoveNext_m4005245300_MethodInfo_var);
			if (L_16)
			{
				goto IL_002f;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m882561911((&V_2), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
		IL2CPP_END_FINALLY(113)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007f:
	{
		JSONObject_t1971882247 * L_17 = V_0;
		return L_17;
	}
}
// System.Void JSONObject::Parse(System.String,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t2948921286_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3984043222_MethodInfo_var;
extern const MethodInfo* Array_IndexOf_TisChar_t3454481338_m3847818391_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3057035458_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral916993783;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral1751346954;
extern Il2CppCodeGenString* _stringLiteral376316188;
extern Il2CppCodeGenString* _stringLiteral452738781;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral150668183;
extern const uint32_t JSONObject_Parse_m547301692_MetadataUsageId;
extern "C"  void JSONObject_Parse_m547301692 (JSONObject_t1971882247 * __this, String_t* ___str0, int32_t ___maxDepth1, bool ___storeExcessLevels2, bool ___strict3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Parse_m547301692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	String_t* V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B67_0 = NULL;
	List_1_t1341003379 * G_B67_1 = NULL;
	String_t* G_B66_0 = NULL;
	List_1_t1341003379 * G_B66_1 = NULL;
	int32_t G_B68_0 = 0;
	String_t* G_B68_1 = NULL;
	List_1_t1341003379 * G_B68_2 = NULL;
	{
		String_t* L_0 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0400;
		}
	}
	{
		String_t* L_2 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_3 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_5();
		NullCheck(L_2);
		String_t* L_4 = String_Trim_m3982520224(L_2, L_3, /*hidden argument*/NULL);
		___str0 = L_4;
		bool L_5 = ___strict3;
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_6 = ___str0;
		NullCheck(L_6);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_6, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)91))))
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_8 = ___str0;
		NullCheck(L_8);
		Il2CppChar L_9 = String_get_Chars_m4230566705(L_8, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)123))))
		{
			goto IL_004d;
		}
	}
	{
		__this->set_type_6(0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral916993783, /*hidden argument*/NULL);
		return;
	}

IL_004d:
	{
		String_t* L_10 = ___str0;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1606060069(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_03f4;
		}
	}
	{
		String_t* L_12 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_13 = String_Compare_m2851607672(NULL /*static, unused*/, L_12, _stringLiteral3323263070, (bool)1, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_007d;
		}
	}
	{
		__this->set_type_6(5);
		__this->set_b_13((bool)1);
		goto IL_03ef;
	}

IL_007d:
	{
		String_t* L_14 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_15 = String_Compare_m2851607672(NULL /*static, unused*/, L_14, _stringLiteral2609877245, (bool)1, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00a1;
		}
	}
	{
		__this->set_type_6(5);
		__this->set_b_13((bool)0);
		goto IL_03ef;
	}

IL_00a1:
	{
		String_t* L_16 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_17 = String_Compare_m2851607672(NULL /*static, unused*/, L_16, _stringLiteral1743624307, (bool)1, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_type_6(0);
		goto IL_03ef;
	}

IL_00be:
	{
		String_t* L_18 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, _stringLiteral1751346954, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e5;
		}
	}
	{
		__this->set_type_6(2);
		__this->set_n_10((std::numeric_limits<float>::infinity()));
		goto IL_03ef;
	}

IL_00e5:
	{
		String_t* L_20 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_20, _stringLiteral376316188, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_010c;
		}
	}
	{
		__this->set_type_6(2);
		__this->set_n_10((-std::numeric_limits<float>::infinity()));
		goto IL_03ef;
	}

IL_010c:
	{
		String_t* L_22 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_22, _stringLiteral452738781, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0133;
		}
	}
	{
		__this->set_type_6(2);
		__this->set_n_10((std::numeric_limits<float>::quiet_NaN()));
		goto IL_03ef;
	}

IL_0133:
	{
		String_t* L_24 = ___str0;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m4230566705(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0162;
		}
	}
	{
		__this->set_type_6(1);
		String_t* L_26 = ___str0;
		String_t* L_27 = ___str0;
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m1606060069(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_29 = String_Substring_m12482732(L_26, 1, ((int32_t)((int32_t)L_28-(int32_t)2)), /*hidden argument*/NULL);
		__this->set_str_9(L_29);
		goto IL_03ef;
	}

IL_0162:
	{
		V_0 = 1;
		V_1 = 0;
		String_t* L_30 = ___str0;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		Il2CppChar L_32 = String_get_Chars_m4230566705(L_30, L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		Il2CppChar L_33 = V_2;
		if ((((int32_t)L_33) == ((int32_t)((int32_t)123))))
		{
			goto IL_0183;
		}
	}
	{
		Il2CppChar L_34 = V_2;
		if ((((int32_t)L_34) == ((int32_t)((int32_t)91))))
		{
			goto IL_01a5;
		}
	}
	{
		goto IL_01bc;
	}

IL_0183:
	{
		__this->set_type_6(3);
		List_1_t1398341365 * L_35 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_35, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_8(L_35);
		List_1_t1341003379 * L_36 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_36, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		__this->set_list_7(L_36);
		goto IL_0215;
	}

IL_01a5:
	{
		__this->set_type_6(4);
		List_1_t1341003379 * L_37 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_37, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		__this->set_list_7(L_37);
		goto IL_0215;
	}

IL_01bc:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_38 = ___str0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
			float L_39 = Convert_ToSingle_m1977583125(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
			__this->set_n_10(L_39);
			String_t* L_40 = ___str0;
			NullCheck(L_40);
			bool L_41 = String_Contains_m4017059963(L_40, _stringLiteral372029316, /*hidden argument*/NULL);
			if (L_41)
			{
				goto IL_01eb;
			}
		}

IL_01d8:
		{
			String_t* L_42 = ___str0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
			int64_t L_43 = Convert_ToInt64_m3181519185(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
			__this->set_i_12(L_43);
			__this->set_useInt_11((bool)1);
		}

IL_01eb:
		{
			__this->set_type_6(2);
			goto IL_0214;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2948921286_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01f7;
		throw e;
	}

CATCH_01f7:
	{ // begin catch(System.FormatException)
		__this->set_type_6(0);
		String_t* L_44 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral150668183, L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		goto IL_0214;
	} // end catch (depth: 1)

IL_0214:
	{
		return;
	}

IL_0215:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_46;
		V_4 = (bool)0;
		V_5 = (bool)0;
		V_6 = 0;
		goto IL_03df;
	}

IL_0229:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_47 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_5();
		String_t* L_48 = ___str0;
		int32_t L_49 = V_1;
		NullCheck(L_48);
		Il2CppChar L_50 = String_get_Chars_m4230566705(L_48, L_49, /*hidden argument*/NULL);
		int32_t L_51 = Array_IndexOf_TisChar_t3454481338_m3847818391(NULL /*static, unused*/, L_47, L_50, /*hidden argument*/Array_IndexOf_TisChar_t3454481338_m3847818391_MethodInfo_var);
		if ((((int32_t)L_51) <= ((int32_t)(-1))))
		{
			goto IL_0245;
		}
	}
	{
		goto IL_03df;
	}

IL_0245:
	{
		String_t* L_52 = ___str0;
		int32_t L_53 = V_1;
		NullCheck(L_52);
		Il2CppChar L_54 = String_get_Chars_m4230566705(L_52, L_53, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_54) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_025c;
		}
	}
	{
		int32_t L_55 = V_1;
		V_1 = ((int32_t)((int32_t)L_55+(int32_t)1));
		goto IL_03df;
	}

IL_025c:
	{
		String_t* L_56 = ___str0;
		int32_t L_57 = V_1;
		NullCheck(L_56);
		Il2CppChar L_58 = String_get_Chars_m4230566705(L_56, L_57, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_58) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_02ba;
		}
	}
	{
		bool L_59 = V_4;
		if (!L_59)
		{
			goto IL_02a2;
		}
	}
	{
		bool L_60 = V_5;
		if (L_60)
		{
			goto IL_029a;
		}
	}
	{
		int32_t L_61 = V_6;
		if (L_61)
		{
			goto IL_029a;
		}
	}
	{
		int32_t L_62 = __this->get_type_6();
		if ((!(((uint32_t)L_62) == ((uint32_t)3))))
		{
			goto IL_029a;
		}
	}
	{
		String_t* L_63 = ___str0;
		int32_t L_64 = V_0;
		int32_t L_65 = V_1;
		int32_t L_66 = V_0;
		NullCheck(L_63);
		String_t* L_67 = String_Substring_m12482732(L_63, ((int32_t)((int32_t)L_64+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_65-(int32_t)L_66))-(int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_67;
	}

IL_029a:
	{
		V_4 = (bool)0;
		goto IL_02ba;
	}

IL_02a2:
	{
		int32_t L_68 = V_6;
		if (L_68)
		{
			goto IL_02b7;
		}
	}
	{
		int32_t L_69 = __this->get_type_6();
		if ((!(((uint32_t)L_69) == ((uint32_t)3))))
		{
			goto IL_02b7;
		}
	}
	{
		int32_t L_70 = V_1;
		V_0 = L_70;
	}

IL_02b7:
	{
		V_4 = (bool)1;
	}

IL_02ba:
	{
		bool L_71 = V_4;
		if (!L_71)
		{
			goto IL_02c6;
		}
	}
	{
		goto IL_03df;
	}

IL_02c6:
	{
		int32_t L_72 = __this->get_type_6();
		if ((!(((uint32_t)L_72) == ((uint32_t)3))))
		{
			goto IL_02ee;
		}
	}
	{
		int32_t L_73 = V_6;
		if (L_73)
		{
			goto IL_02ee;
		}
	}
	{
		String_t* L_74 = ___str0;
		int32_t L_75 = V_1;
		NullCheck(L_74);
		Il2CppChar L_76 = String_get_Chars_m4230566705(L_74, L_75, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_76) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_02ee;
		}
	}
	{
		int32_t L_77 = V_1;
		V_0 = ((int32_t)((int32_t)L_77+(int32_t)1));
		V_5 = (bool)1;
	}

IL_02ee:
	{
		String_t* L_78 = ___str0;
		int32_t L_79 = V_1;
		NullCheck(L_78);
		Il2CppChar L_80 = String_get_Chars_m4230566705(L_78, L_79, /*hidden argument*/NULL);
		if ((((int32_t)L_80) == ((int32_t)((int32_t)91))))
		{
			goto IL_030a;
		}
	}
	{
		String_t* L_81 = ___str0;
		int32_t L_82 = V_1;
		NullCheck(L_81);
		Il2CppChar L_83 = String_get_Chars_m4230566705(L_81, L_82, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_83) == ((uint32_t)((int32_t)123)))))
		{
			goto IL_0315;
		}
	}

IL_030a:
	{
		int32_t L_84 = V_6;
		V_6 = ((int32_t)((int32_t)L_84+(int32_t)1));
		goto IL_0337;
	}

IL_0315:
	{
		String_t* L_85 = ___str0;
		int32_t L_86 = V_1;
		NullCheck(L_85);
		Il2CppChar L_87 = String_get_Chars_m4230566705(L_85, L_86, /*hidden argument*/NULL);
		if ((((int32_t)L_87) == ((int32_t)((int32_t)93))))
		{
			goto IL_0331;
		}
	}
	{
		String_t* L_88 = ___str0;
		int32_t L_89 = V_1;
		NullCheck(L_88);
		Il2CppChar L_90 = String_get_Chars_m4230566705(L_88, L_89, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_90) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0337;
		}
	}

IL_0331:
	{
		int32_t L_91 = V_6;
		V_6 = ((int32_t)((int32_t)L_91-(int32_t)1));
	}

IL_0337:
	{
		String_t* L_92 = ___str0;
		int32_t L_93 = V_1;
		NullCheck(L_92);
		Il2CppChar L_94 = String_get_Chars_m4230566705(L_92, L_93, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_94) == ((uint32_t)((int32_t)44)))))
		{
			goto IL_034c;
		}
	}
	{
		int32_t L_95 = V_6;
		if (!L_95)
		{
			goto IL_0354;
		}
	}

IL_034c:
	{
		int32_t L_96 = V_6;
		if ((((int32_t)L_96) >= ((int32_t)0)))
		{
			goto IL_03df;
		}
	}

IL_0354:
	{
		V_5 = (bool)0;
		String_t* L_97 = ___str0;
		int32_t L_98 = V_0;
		int32_t L_99 = V_1;
		int32_t L_100 = V_0;
		NullCheck(L_97);
		String_t* L_101 = String_Substring_m12482732(L_97, L_98, ((int32_t)((int32_t)L_99-(int32_t)L_100)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_102 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_5();
		NullCheck(L_101);
		String_t* L_103 = String_Trim_m3982520224(L_101, L_102, /*hidden argument*/NULL);
		V_7 = L_103;
		String_t* L_104 = V_7;
		NullCheck(L_104);
		int32_t L_105 = String_get_Length_m1606060069(L_104, /*hidden argument*/NULL);
		if ((((int32_t)L_105) <= ((int32_t)0)))
		{
			goto IL_03db;
		}
	}
	{
		int32_t L_106 = __this->get_type_6();
		if ((!(((uint32_t)L_106) == ((uint32_t)3))))
		{
			goto IL_0392;
		}
	}
	{
		List_1_t1398341365 * L_107 = __this->get_keys_8();
		String_t* L_108 = V_3;
		NullCheck(L_107);
		List_1_Add_m4061286785(L_107, L_108, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
	}

IL_0392:
	{
		int32_t L_109 = ___maxDepth1;
		if ((((int32_t)L_109) == ((int32_t)(-1))))
		{
			goto IL_03c3;
		}
	}
	{
		List_1_t1341003379 * L_110 = __this->get_list_7();
		String_t* L_111 = V_7;
		int32_t L_112 = ___maxDepth1;
		G_B66_0 = L_111;
		G_B66_1 = L_110;
		if ((((int32_t)L_112) >= ((int32_t)(-1))))
		{
			G_B67_0 = L_111;
			G_B67_1 = L_110;
			goto IL_03af;
		}
	}
	{
		G_B68_0 = ((int32_t)-2);
		G_B68_1 = G_B66_0;
		G_B68_2 = G_B66_1;
		goto IL_03b2;
	}

IL_03af:
	{
		int32_t L_113 = ___maxDepth1;
		G_B68_0 = ((int32_t)((int32_t)L_113-(int32_t)1));
		G_B68_1 = G_B67_0;
		G_B68_2 = G_B67_1;
	}

IL_03b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_114 = JSONObject_Create_m1582109667(NULL /*static, unused*/, G_B68_1, G_B68_0, (bool)0, (bool)0, /*hidden argument*/NULL);
		NullCheck(G_B68_2);
		List_1_Add_m3057035458(G_B68_2, L_114, /*hidden argument*/List_1_Add_m3057035458_MethodInfo_var);
		goto IL_03db;
	}

IL_03c3:
	{
		bool L_115 = ___storeExcessLevels2;
		if (!L_115)
		{
			goto IL_03db;
		}
	}
	{
		List_1_t1341003379 * L_116 = __this->get_list_7();
		String_t* L_117 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_118 = JSONObject_CreateBakedObject_m1414246618(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		NullCheck(L_116);
		List_1_Add_m3057035458(L_116, L_118, /*hidden argument*/List_1_Add_m3057035458_MethodInfo_var);
	}

IL_03db:
	{
		int32_t L_119 = V_1;
		V_0 = ((int32_t)((int32_t)L_119+(int32_t)1));
	}

IL_03df:
	{
		int32_t L_120 = V_1;
		int32_t L_121 = ((int32_t)((int32_t)L_120+(int32_t)1));
		V_1 = L_121;
		String_t* L_122 = ___str0;
		NullCheck(L_122);
		int32_t L_123 = String_get_Length_m1606060069(L_122, /*hidden argument*/NULL);
		if ((((int32_t)L_121) < ((int32_t)L_123)))
		{
			goto IL_0229;
		}
	}

IL_03ef:
	{
		goto IL_03fb;
	}

IL_03f4:
	{
		__this->set_type_6(0);
	}

IL_03fb:
	{
		goto IL_0407;
	}

IL_0400:
	{
		__this->set_type_6(0);
	}

IL_0407:
	{
		return;
	}
}
// System.Boolean JSONObject::get_IsNumber()
extern "C"  bool JSONObject_get_IsNumber_m1211710060 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsNull()
extern "C"  bool JSONObject_get_IsNull_m4292535844 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsString()
extern "C"  bool JSONObject_get_IsString_m1134326012 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsBool()
extern "C"  bool JSONObject_get_IsBool_m631014411 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)5))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsArray()
extern "C"  bool JSONObject_get_IsArray_m3696731856 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)4))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsObject()
extern "C"  bool JSONObject_get_IsObject_m1916105538 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = __this->get_type_6();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)6))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Void JSONObject::Add(System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m4073537166_MetadataUsageId;
extern "C"  void JSONObject_Add_m4073537166 (JSONObject_t1971882247 * __this, bool ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m4073537166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m322727955(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m3618328192(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.Single)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m3120056428_MetadataUsageId;
extern "C"  void JSONObject_Add_m3120056428 (JSONObject_t1971882247 * __this, float ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m3120056428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m4234548109(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m3618328192(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m449104870_MetadataUsageId;
extern "C"  void JSONObject_Add_m449104870 (JSONObject_t1971882247 * __this, int32_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m449104870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m2780992853(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m3618328192(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m930192605_MetadataUsageId;
extern "C"  void JSONObject_Add_m930192605 (JSONObject_t1971882247 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m930192605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_CreateStringObject_m227421806(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m3618328192(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(JSONObject/AddJSONContents)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m3191854140_MetadataUsageId;
extern "C"  void JSONObject_Add_m3191854140 (JSONObject_t1971882247 * __this, AddJSONContents_t3850664647 * ___content0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m3191854140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddJSONContents_t3850664647 * L_0 = ___content0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m2296526109(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m3618328192(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3984043222_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3057035458_MethodInfo_var;
extern const uint32_t JSONObject_Add_m3618328192_MetadataUsageId;
extern "C"  void JSONObject_Add_m3618328192 (JSONObject_t1971882247 * __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m3618328192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_1 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_2 = __this->get_type_6();
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_0034;
		}
	}
	{
		__this->set_type_6(4);
		List_1_t1341003379 * L_3 = __this->get_list_7();
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		List_1_t1341003379 * L_4 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_4, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		__this->set_list_7(L_4);
	}

IL_0034:
	{
		List_1_t1341003379 * L_5 = __this->get_list_7();
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_5);
		List_1_Add_m3057035458(L_5, L_6, /*hidden argument*/List_1_Add_m3057035458_MethodInfo_var);
	}

IL_0040:
	{
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m1448237390_MetadataUsageId;
extern "C"  void JSONObject_AddField_m1448237390 (JSONObject_t1971882247 * __this, String_t* ___name0, bool ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m1448237390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m322727955(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1721386152(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Single)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m2712088548_MetadataUsageId;
extern "C"  void JSONObject_AddField_m2712088548 (JSONObject_t1971882247 * __this, String_t* ___name0, float ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m2712088548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		float L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m4234548109(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1721386152(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m894980766_MetadataUsageId;
extern "C"  void JSONObject_AddField_m894980766 (JSONObject_t1971882247 * __this, String_t* ___name0, int32_t ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m894980766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m2780992853(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1721386152(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Int64)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m1600793269_MetadataUsageId;
extern "C"  void JSONObject_AddField_m1600793269 (JSONObject_t1971882247 * __this, String_t* ___name0, int64_t ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m1600793269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		int64_t L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m1974423762(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1721386152(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,JSONObject/AddJSONContents)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m733507028_MetadataUsageId;
extern "C"  void JSONObject_AddField_m733507028 (JSONObject_t1971882247 * __this, String_t* ___name0, AddJSONContents_t3850664647 * ___content1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m733507028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		AddJSONContents_t3850664647 * L_1 = ___content1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m2296526109(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1721386152(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m757984985_MetadataUsageId;
extern "C"  void JSONObject_AddField_m757984985 (JSONObject_t1971882247 * __this, String_t* ___name0, String_t* ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m757984985_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_CreateStringObject_m227421806(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1721386152(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3984043222_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3057035458_MethodInfo_var;
extern const uint32_t JSONObject_AddField_m1721386152_MetadataUsageId;
extern "C"  void JSONObject_AddField_m1721386152 (JSONObject_t1971882247 * __this, String_t* ___name0, JSONObject_t1971882247 * ___obj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m1721386152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		JSONObject_t1971882247 * L_0 = ___obj1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_1 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_2 = __this->get_type_6();
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0092;
		}
	}
	{
		List_1_t1398341365 * L_3 = __this->get_keys_8();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		List_1_t1398341365 * L_4 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_4, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_8(L_4);
	}

IL_002d:
	{
		int32_t L_5 = __this->get_type_6();
		if ((!(((uint32_t)L_5) == ((uint32_t)4))))
		{
			goto IL_0075;
		}
	}
	{
		V_0 = 0;
		goto IL_005f;
	}

IL_0040:
	{
		List_1_t1398341365 * L_6 = __this->get_keys_8();
		int32_t L_7 = V_0;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_11 = String_Concat_m56707527(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		List_1_Add_m4061286785(L_6, L_11, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_13 = V_0;
		List_1_t1341003379 * L_14 = __this->get_list_7();
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m3485821544(L_14, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0040;
		}
	}
	{
		goto IL_008b;
	}

IL_0075:
	{
		List_1_t1341003379 * L_16 = __this->get_list_7();
		if (L_16)
		{
			goto IL_008b;
		}
	}
	{
		List_1_t1341003379 * L_17 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m3984043222(L_17, /*hidden argument*/List_1__ctor_m3984043222_MethodInfo_var);
		__this->set_list_7(L_17);
	}

IL_008b:
	{
		__this->set_type_6(3);
	}

IL_0092:
	{
		List_1_t1398341365 * L_18 = __this->get_keys_8();
		String_t* L_19 = ___name0;
		NullCheck(L_18);
		List_1_Add_m4061286785(L_18, L_19, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1341003379 * L_20 = __this->get_list_7();
		JSONObject_t1971882247 * L_21 = ___obj1;
		NullCheck(L_20);
		List_1_Add_m3057035458(L_20, L_21, /*hidden argument*/List_1_Add_m3057035458_MethodInfo_var);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m4067629664_MetadataUsageId;
extern "C"  void JSONObject_SetField_m4067629664 (JSONObject_t1971882247 * __this, String_t* ___name0, String_t* ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m4067629664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_CreateStringObject_m227421806(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m1180592005(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m2028330731_MetadataUsageId;
extern "C"  void JSONObject_SetField_m2028330731 (JSONObject_t1971882247 * __this, String_t* ___name0, bool ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m2028330731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m322727955(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m1180592005(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Single)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m484187805_MetadataUsageId;
extern "C"  void JSONObject_SetField_m484187805 (JSONObject_t1971882247 * __this, String_t* ___name0, float ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m484187805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		float L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m4234548109(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m1180592005(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m3929609445_MetadataUsageId;
extern "C"  void JSONObject_SetField_m3929609445 (JSONObject_t1971882247 * __this, String_t* ___name0, int32_t ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m3929609445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m2780992853(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m1180592005(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,JSONObject)
extern const MethodInfo* List_1_Remove_m1073513939_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m3019523498_MethodInfo_var;
extern const uint32_t JSONObject_SetField_m1180592005_MetadataUsageId;
extern "C"  void JSONObject_SetField_m1180592005 (JSONObject_t1971882247 * __this, String_t* ___name0, JSONObject_t1971882247 * ___obj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m1180592005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = JSONObject_HasField_m3030985834(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t1341003379 * L_2 = __this->get_list_7();
		String_t* L_3 = ___name0;
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m2638456294(__this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_Remove_m1073513939(L_2, L_4, /*hidden argument*/List_1_Remove_m1073513939_MethodInfo_var);
		List_1_t1398341365 * L_5 = __this->get_keys_8();
		String_t* L_6 = ___name0;
		NullCheck(L_5);
		List_1_Remove_m3019523498(L_5, L_6, /*hidden argument*/List_1_Remove_m3019523498_MethodInfo_var);
	}

IL_002c:
	{
		String_t* L_7 = ___name0;
		JSONObject_t1971882247 * L_8 = ___obj1;
		JSONObject_AddField_m1721386152(__this, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::RemoveField(System.String)
extern const MethodInfo* List_1_IndexOf_m2492943723_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m2823081968_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m3019523498_MethodInfo_var;
extern const uint32_t JSONObject_RemoveField_m3901079654_MetadataUsageId;
extern "C"  void JSONObject_RemoveField_m3901079654 (JSONObject_t1971882247 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_RemoveField_m3901079654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1398341365 * L_0 = __this->get_keys_8();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		int32_t L_2 = List_1_IndexOf_m2492943723(L_0, L_1, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		if ((((int32_t)L_2) <= ((int32_t)(-1))))
		{
			goto IL_0036;
		}
	}
	{
		List_1_t1341003379 * L_3 = __this->get_list_7();
		List_1_t1398341365 * L_4 = __this->get_keys_8();
		String_t* L_5 = ___name0;
		NullCheck(L_4);
		int32_t L_6 = List_1_IndexOf_m2492943723(L_4, L_5, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		NullCheck(L_3);
		List_1_RemoveAt_m2823081968(L_3, L_6, /*hidden argument*/List_1_RemoveAt_m2823081968_MethodInfo_var);
		List_1_t1398341365 * L_7 = __this->get_keys_8();
		String_t* L_8 = ___name0;
		NullCheck(L_7);
		List_1_Remove_m3019523498(L_7, L_8, /*hidden argument*/List_1_Remove_m3019523498_MethodInfo_var);
	}

IL_0036:
	{
		return;
	}
}
// System.Boolean JSONObject::GetField(System.Boolean&,System.String,System.Boolean)
extern "C"  bool JSONObject_GetField_m749778138 (JSONObject_t1971882247 * __this, bool* ___field0, String_t* ___name1, bool ___fallback2, const MethodInfo* method)
{
	{
		bool* L_0 = ___field0;
		bool L_1 = ___fallback2;
		*((int8_t*)(L_0)) = (int8_t)L_1;
		bool* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m2238877236(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.Boolean&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m2492943723_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m2238877236_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m2238877236 (JSONObject_t1971882247 * __this, bool* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m2238877236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m2492943723(L_1, L_2, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		bool* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m429614411(L_6, L_7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_8);
		bool L_9 = L_8->get_b_13();
		*((int8_t*)(L_5)) = (int8_t)L_9;
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1722936676(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.Single&,System.String,System.Single)
extern "C"  bool JSONObject_GetField_m2835168656 (JSONObject_t1971882247 * __this, float* ___field0, String_t* ___name1, float ___fallback2, const MethodInfo* method)
{
	{
		float* L_0 = ___field0;
		float L_1 = ___fallback2;
		*((float*)(L_0)) = (float)L_1;
		float* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m2524002016(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.Single&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m2492943723_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m2524002016_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m2524002016 (JSONObject_t1971882247 * __this, float* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m2524002016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m2492943723(L_1, L_2, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		float* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m429614411(L_6, L_7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_n_10();
		*((float*)(L_5)) = (float)L_9;
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1722936676(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.Int32&,System.String,System.Int32)
extern "C"  bool JSONObject_GetField_m3075052858 (JSONObject_t1971882247 * __this, int32_t* ___field0, String_t* ___name1, int32_t ___fallback2, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___field0;
		int32_t L_1 = ___fallback2;
		*((int32_t*)(L_0)) = (int32_t)L_1;
		int32_t* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m3356332928(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.Int32&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m2492943723_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m3356332928_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m3356332928 (JSONObject_t1971882247 * __this, int32_t* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m3356332928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1916105538(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m2492943723(L_1, L_2, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		int32_t* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m429614411(L_6, L_7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_n_10();
		*((int32_t*)(L_5)) = (int32_t)(((int32_t)((int32_t)L_9)));
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1722936676(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.Int64&,System.String,System.Int64)
extern "C"  bool JSONObject_GetField_m1835125498 (JSONObject_t1971882247 * __this, int64_t* ___field0, String_t* ___name1, int64_t ___fallback2, const MethodInfo* method)
{
	{
		int64_t* L_0 = ___field0;
		int64_t L_1 = ___fallback2;
		*((int64_t*)(L_0)) = (int64_t)L_1;
		int64_t* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m1787897363(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.Int64&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m2492943723_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m1787897363_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m1787897363 (JSONObject_t1971882247 * __this, int64_t* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m1787897363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1916105538(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m2492943723(L_1, L_2, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		int64_t* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m429614411(L_6, L_7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_n_10();
		*((int64_t*)(L_5)) = (int64_t)(((int64_t)((int64_t)L_9)));
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1722936676(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.UInt32&,System.String,System.UInt32)
extern "C"  bool JSONObject_GetField_m3049188754 (JSONObject_t1971882247 * __this, uint32_t* ___field0, String_t* ___name1, uint32_t ___fallback2, const MethodInfo* method)
{
	{
		uint32_t* L_0 = ___field0;
		uint32_t L_1 = ___fallback2;
		*((int32_t*)(L_0)) = (int32_t)L_1;
		uint32_t* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m2149788417(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.UInt32&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m2492943723_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m2149788417_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m2149788417 (JSONObject_t1971882247 * __this, uint32_t* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m2149788417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1916105538(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m2492943723(L_1, L_2, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		uint32_t* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m429614411(L_6, L_7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_n_10();
		*((int32_t*)(L_5)) = (int32_t)(((int32_t)((uint32_t)L_9)));
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1722936676(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.String&,System.String,System.String)
extern "C"  bool JSONObject_GetField_m3338501930 (JSONObject_t1971882247 * __this, String_t** ___field0, String_t* ___name1, String_t* ___fallback2, const MethodInfo* method)
{
	{
		String_t** L_0 = ___field0;
		String_t* L_1 = ___fallback2;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)L_1;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)L_1);
		String_t** L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m204744839(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.String&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m2492943723_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m204744839_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m204744839 (JSONObject_t1971882247 * __this, String_t** ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m204744839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1916105538(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m2492943723(L_1, L_2, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		String_t** L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m429614411(L_6, L_7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_8);
		String_t* L_9 = L_8->get_str_9();
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)L_9;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)L_9);
		return (bool)1;
	}

IL_0034:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1722936676(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Void JSONObject::GetField(System.String,JSONObject/GetFieldResponse,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m2492943723_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m3040698632_MetadataUsageId;
extern "C"  void JSONObject_GetField_m3040698632 (JSONObject_t1971882247 * __this, String_t* ___name0, GetFieldResponse_t1259369279 * ___response1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m3040698632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GetFieldResponse_t1259369279 * L_0 = ___response1;
		if (!L_0)
		{
			goto IL_0038;
		}
	}
	{
		bool L_1 = JSONObject_get_IsObject_m1916105538(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		List_1_t1398341365 * L_2 = __this->get_keys_8();
		String_t* L_3 = ___name0;
		NullCheck(L_2);
		int32_t L_4 = List_1_IndexOf_m2492943723(L_2, L_3, /*hidden argument*/List_1_IndexOf_m2492943723_MethodInfo_var);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		GetFieldResponse_t1259369279 * L_6 = ___response1;
		List_1_t1341003379 * L_7 = __this->get_list_7();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		JSONObject_t1971882247 * L_9 = List_1_get_Item_m429614411(L_7, L_8, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_6);
		GetFieldResponse_Invoke_m4134964043(L_6, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0038:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0045;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name0;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1722936676(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// JSONObject JSONObject::GetField(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m887332132_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_GetField_m887332132 (JSONObject_t1971882247 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m887332132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1916105538(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003a;
	}

IL_0012:
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		String_t* L_3 = List_1_get_Item_m566484697(L_1, L_2, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		String_t* L_4 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		List_1_t1341003379 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m429614411(L_6, L_7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		return L_8;
	}

IL_0036:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_10 = V_0;
		List_1_t1398341365 * L_11 = __this->get_keys_8();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m780127360(L_11, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}

IL_004b:
	{
		return (JSONObject_t1971882247 *)NULL;
	}
}
// System.Boolean JSONObject::HasFields(System.String[])
extern const MethodInfo* List_1_Contains_m2369280605_MethodInfo_var;
extern const uint32_t JSONObject_HasFields_m1144058745_MetadataUsageId;
extern "C"  bool JSONObject_HasFields_m1144058745 (JSONObject_t1971882247 * __this, StringU5BU5D_t1642385972* ___names0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_HasFields_m1144058745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1916105538(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = 0;
		goto IL_002d;
	}

IL_0014:
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		StringU5BU5D_t1642385972* L_2 = ___names0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		String_t* L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_1);
		bool L_6 = List_1_Contains_m2369280605(L_1, L_5, /*hidden argument*/List_1_Contains_m2369280605_MethodInfo_var);
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_0;
		StringU5BU5D_t1642385972* L_9 = ___names0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean JSONObject::HasField(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const uint32_t JSONObject_HasField_m3030985834_MetadataUsageId;
extern "C"  bool JSONObject_HasField_m3030985834 (JSONObject_t1971882247 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_HasField_m3030985834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1916105538(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = 0;
		goto IL_0031;
	}

IL_0014:
	{
		List_1_t1398341365 * L_1 = __this->get_keys_8();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		String_t* L_3 = List_1_get_Item_m566484697(L_1, L_2, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		String_t* L_4 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		return (bool)1;
	}

IL_002d:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_7 = V_0;
		List_1_t1398341365 * L_8 = __this->get_keys_8();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m780127360(L_8, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void JSONObject::Clear()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2818091657_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const uint32_t JSONObject_Clear_m992277887_MetadataUsageId;
extern "C"  void JSONObject_Clear_m992277887 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Clear_m992277887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_type_6(0);
		List_1_t1341003379 * L_0 = __this->get_list_7();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t1341003379 * L_1 = __this->get_list_7();
		NullCheck(L_1);
		List_1_Clear_m2818091657(L_1, /*hidden argument*/List_1_Clear_m2818091657_MethodInfo_var);
	}

IL_001d:
	{
		List_1_t1398341365 * L_2 = __this->get_keys_8();
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		List_1_t1398341365 * L_3 = __this->get_keys_8();
		NullCheck(L_3);
		List_1_Clear_m2928955906(L_3, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_str_9(L_4);
		__this->set_n_10((0.0f));
		__this->set_b_13((bool)0);
		return;
	}
}
// JSONObject JSONObject::Copy()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Copy_m3864269401_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Copy_m3864269401 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Copy_m3864269401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = JSONObject_Print_m4113013869(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m1582109667(NULL /*static, unused*/, L_0, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::Merge(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Merge_m1969546983_MetadataUsageId;
extern "C"  void JSONObject_Merge_m1969546983 (JSONObject_t1971882247 * __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Merge_m1969546983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m848026001(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::MergeRecur(JSONObject,JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3451532966;
extern const uint32_t JSONObject_MergeRecur_m848026001_MetadataUsageId;
extern "C"  void JSONObject_MergeRecur_m848026001 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___left0, JSONObject_t1971882247 * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_MergeRecur_m848026001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		JSONObject_t1971882247 * L_0 = ___left0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_type_6();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = ___left0;
		JSONObject_t1971882247 * L_3 = ___right1;
		NullCheck(L_2);
		JSONObject_Absorb_m287377368(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0176;
	}

IL_0017:
	{
		JSONObject_t1971882247 * L_4 = ___left0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_type_6();
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_00d2;
		}
	}
	{
		JSONObject_t1971882247 * L_6 = ___right1;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_type_6();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_00d2;
		}
	}
	{
		V_0 = 0;
		goto IL_00bc;
	}

IL_0036:
	{
		JSONObject_t1971882247 * L_8 = ___right1;
		NullCheck(L_8);
		List_1_t1398341365 * L_9 = L_8->get_keys_8();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = List_1_get_Item_m566484697(L_9, L_10, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		V_1 = L_11;
		JSONObject_t1971882247 * L_12 = ___right1;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_14 = JSONObject_get_Item_m481309353(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_15 = JSONObject_get_isContainer_m3175857226(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008b;
		}
	}
	{
		JSONObject_t1971882247 * L_16 = ___left0;
		String_t* L_17 = V_1;
		NullCheck(L_16);
		bool L_18 = JSONObject_HasField_m3030985834(L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0078;
		}
	}
	{
		JSONObject_t1971882247 * L_19 = ___left0;
		String_t* L_20 = V_1;
		NullCheck(L_19);
		JSONObject_t1971882247 * L_21 = JSONObject_get_Item_m2638456294(L_19, L_20, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_22 = ___right1;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		JSONObject_t1971882247 * L_24 = JSONObject_get_Item_m481309353(L_22, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m848026001(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0078:
	{
		JSONObject_t1971882247 * L_25 = ___left0;
		String_t* L_26 = V_1;
		JSONObject_t1971882247 * L_27 = ___right1;
		int32_t L_28 = V_0;
		NullCheck(L_27);
		JSONObject_t1971882247 * L_29 = JSONObject_get_Item_m481309353(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		JSONObject_AddField_m1721386152(L_25, L_26, L_29, /*hidden argument*/NULL);
	}

IL_0086:
	{
		goto IL_00b8;
	}

IL_008b:
	{
		JSONObject_t1971882247 * L_30 = ___left0;
		String_t* L_31 = V_1;
		NullCheck(L_30);
		bool L_32 = JSONObject_HasField_m3030985834(L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00aa;
		}
	}
	{
		JSONObject_t1971882247 * L_33 = ___left0;
		String_t* L_34 = V_1;
		JSONObject_t1971882247 * L_35 = ___right1;
		int32_t L_36 = V_0;
		NullCheck(L_35);
		JSONObject_t1971882247 * L_37 = JSONObject_get_Item_m481309353(L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_33);
		JSONObject_SetField_m1180592005(L_33, L_34, L_37, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_00aa:
	{
		JSONObject_t1971882247 * L_38 = ___left0;
		String_t* L_39 = V_1;
		JSONObject_t1971882247 * L_40 = ___right1;
		int32_t L_41 = V_0;
		NullCheck(L_40);
		JSONObject_t1971882247 * L_42 = JSONObject_get_Item_m481309353(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_38);
		JSONObject_AddField_m1721386152(L_38, L_39, L_42, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		int32_t L_43 = V_0;
		V_0 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_00bc:
	{
		int32_t L_44 = V_0;
		JSONObject_t1971882247 * L_45 = ___right1;
		NullCheck(L_45);
		List_1_t1341003379 * L_46 = L_45->get_list_7();
		NullCheck(L_46);
		int32_t L_47 = List_1_get_Count_m3485821544(L_46, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_44) < ((int32_t)L_47)))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_0176;
	}

IL_00d2:
	{
		JSONObject_t1971882247 * L_48 = ___left0;
		NullCheck(L_48);
		int32_t L_49 = L_48->get_type_6();
		if ((!(((uint32_t)L_49) == ((uint32_t)4))))
		{
			goto IL_0176;
		}
	}
	{
		JSONObject_t1971882247 * L_50 = ___right1;
		NullCheck(L_50);
		int32_t L_51 = L_50->get_type_6();
		if ((!(((uint32_t)L_51) == ((uint32_t)4))))
		{
			goto IL_0176;
		}
	}
	{
		JSONObject_t1971882247 * L_52 = ___right1;
		NullCheck(L_52);
		int32_t L_53 = JSONObject_get_Count_m2165978410(L_52, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_54 = ___left0;
		NullCheck(L_54);
		int32_t L_55 = JSONObject_get_Count_m2165978410(L_54, /*hidden argument*/NULL);
		if ((((int32_t)L_53) <= ((int32_t)L_55)))
		{
			goto IL_0106;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3451532966, /*hidden argument*/NULL);
		return;
	}

IL_0106:
	{
		V_2 = 0;
		goto IL_0165;
	}

IL_010d:
	{
		JSONObject_t1971882247 * L_56 = ___left0;
		int32_t L_57 = V_2;
		NullCheck(L_56);
		JSONObject_t1971882247 * L_58 = JSONObject_get_Item_m481309353(L_56, L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		int32_t L_59 = L_58->get_type_6();
		JSONObject_t1971882247 * L_60 = ___right1;
		int32_t L_61 = V_2;
		NullCheck(L_60);
		JSONObject_t1971882247 * L_62 = JSONObject_get_Item_m481309353(L_60, L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = L_62->get_type_6();
		if ((!(((uint32_t)L_59) == ((uint32_t)L_63))))
		{
			goto IL_0161;
		}
	}
	{
		JSONObject_t1971882247 * L_64 = ___left0;
		int32_t L_65 = V_2;
		NullCheck(L_64);
		JSONObject_t1971882247 * L_66 = JSONObject_get_Item_m481309353(L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		bool L_67 = JSONObject_get_isContainer_m3175857226(L_66, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_0153;
		}
	}
	{
		JSONObject_t1971882247 * L_68 = ___left0;
		int32_t L_69 = V_2;
		NullCheck(L_68);
		JSONObject_t1971882247 * L_70 = JSONObject_get_Item_m481309353(L_68, L_69, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_71 = ___right1;
		int32_t L_72 = V_2;
		NullCheck(L_71);
		JSONObject_t1971882247 * L_73 = JSONObject_get_Item_m481309353(L_71, L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m848026001(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		goto IL_0161;
	}

IL_0153:
	{
		JSONObject_t1971882247 * L_74 = ___left0;
		int32_t L_75 = V_2;
		JSONObject_t1971882247 * L_76 = ___right1;
		int32_t L_77 = V_2;
		NullCheck(L_76);
		JSONObject_t1971882247 * L_78 = JSONObject_get_Item_m481309353(L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_74);
		JSONObject_set_Item_m2332624254(L_74, L_75, L_78, /*hidden argument*/NULL);
	}

IL_0161:
	{
		int32_t L_79 = V_2;
		V_2 = ((int32_t)((int32_t)L_79+(int32_t)1));
	}

IL_0165:
	{
		int32_t L_80 = V_2;
		JSONObject_t1971882247 * L_81 = ___right1;
		NullCheck(L_81);
		List_1_t1341003379 * L_82 = L_81->get_list_7();
		NullCheck(L_82);
		int32_t L_83 = List_1_get_Count_m3485821544(L_82, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_80) < ((int32_t)L_83)))
		{
			goto IL_010d;
		}
	}

IL_0176:
	{
		return;
	}
}
// System.Void JSONObject::Bake()
extern "C"  void JSONObject_Bake_m244276219 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		if ((((int32_t)L_0) == ((int32_t)6)))
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_1 = JSONObject_Print_m4113013869(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_str_9(L_1);
		__this->set_type_6(6);
	}

IL_0020:
	{
		return;
	}
}
// System.Collections.IEnumerable JSONObject::BakeAsync()
extern Il2CppClass* U3CBakeAsyncU3Ec__Iterator0_t1149809410_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_BakeAsync_m2553896557_MetadataUsageId;
extern "C"  Il2CppObject * JSONObject_BakeAsync_m2553896557 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_BakeAsync_m2553896557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CBakeAsyncU3Ec__Iterator0_t1149809410 * V_0 = NULL;
	{
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_0 = (U3CBakeAsyncU3Ec__Iterator0_t1149809410 *)il2cpp_codegen_object_new(U3CBakeAsyncU3Ec__Iterator0_t1149809410_il2cpp_TypeInfo_var);
		U3CBakeAsyncU3Ec__Iterator0__ctor_m2863786865(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_2 = V_0;
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_5(((int32_t)-2));
		return L_3;
	}
}
// System.String JSONObject::Print(System.Boolean)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Print_m4113013869_MetadataUsageId;
extern "C"  String_t* JSONObject_Print_m4113013869 (JSONObject_t1971882247 * __this, bool ___pretty0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Print_m4113013869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		bool L_2 = ___pretty0;
		JSONObject_Stringify_m3496252199(__this, 0, L_1, L_2, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<System.String> JSONObject::PrintAsync(System.Boolean)
extern Il2CppClass* U3CPrintAsyncU3Ec__Iterator1_t716304657_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_PrintAsync_m1157769192_MetadataUsageId;
extern "C"  Il2CppObject* JSONObject_PrintAsync_m1157769192 (JSONObject_t1971882247 * __this, bool ___pretty0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_PrintAsync_m1157769192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPrintAsyncU3Ec__Iterator1_t716304657 * V_0 = NULL;
	{
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_0 = (U3CPrintAsyncU3Ec__Iterator1_t716304657 *)il2cpp_codegen_object_new(U3CPrintAsyncU3Ec__Iterator1_t716304657_il2cpp_TypeInfo_var);
		U3CPrintAsyncU3Ec__Iterator1__ctor_m918627686(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_1 = V_0;
		bool L_2 = ___pretty0;
		NullCheck(L_1);
		L_1->set_pretty_1(L_2);
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_5(__this);
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_4 = V_0;
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_5 = L_4;
		NullCheck(L_5);
		L_5->set_U24PC_8(((int32_t)-2));
		return L_5;
	}
}
// System.Collections.IEnumerable JSONObject::StringifyAsync(System.Int32,System.Text.StringBuilder,System.Boolean)
extern Il2CppClass* U3CStringifyAsyncU3Ec__Iterator2_t4037879552_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_StringifyAsync_m1504868741_MetadataUsageId;
extern "C"  Il2CppObject * JSONObject_StringifyAsync_m1504868741 (JSONObject_t1971882247 * __this, int32_t ___depth0, StringBuilder_t1221177846 * ___builder1, bool ___pretty2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_StringifyAsync_m1504868741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * V_0 = NULL;
	{
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_0 = (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 *)il2cpp_codegen_object_new(U3CStringifyAsyncU3Ec__Iterator2_t4037879552_il2cpp_TypeInfo_var);
		U3CStringifyAsyncU3Ec__Iterator2__ctor_m3369090517(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_1 = V_0;
		int32_t L_2 = ___depth0;
		NullCheck(L_1);
		L_1->set_depth_0(L_2);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_3 = V_0;
		StringBuilder_t1221177846 * L_4 = ___builder1;
		NullCheck(L_3);
		L_3->set_builder_2(L_4);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_5 = V_0;
		bool L_6 = ___pretty2;
		NullCheck(L_5);
		L_5->set_pretty_3(L_6);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_7 = V_0;
		int32_t L_8 = ___depth0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Edepth_17(L_8);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_U24this_14(__this);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_10 = V_0;
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_11 = L_10;
		NullCheck(L_11);
		L_11->set_U24PC_18(((int32_t)-2));
		return L_11;
	}
}
// System.Void JSONObject::Stringify(System.Int32,System.Text.StringBuilder,System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2041690406;
extern Il2CppCodeGenString* _stringLiteral1554931876;
extern Il2CppCodeGenString* _stringLiteral1751346954;
extern Il2CppCodeGenString* _stringLiteral376316188;
extern Il2CppCodeGenString* _stringLiteral452738781;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral372029349;
extern Il2CppCodeGenString* _stringLiteral845265362;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t JSONObject_Stringify_m3496252199_MetadataUsageId;
extern "C"  void JSONObject_Stringify_m3496252199 (JSONObject_t1971882247 * __this, int32_t ___depth0, StringBuilder_t1221177846 * ___builder1, bool ___pretty2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Stringify_m3496252199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	JSONObject_t1971882247 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		int32_t L_0 = ___depth0;
		int32_t L_1 = L_0;
		___depth0 = ((int32_t)((int32_t)L_1+(int32_t)1));
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)100))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2041690406, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_6();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_03cf;
		}
		if (L_3 == 1)
		{
			goto IL_0058;
		}
		if (L_3 == 2)
		{
			goto IL_006f;
		}
		if (L_3 == 3)
		{
			goto IL_0117;
		}
		if (L_3 == 4)
		{
			goto IL_0262;
		}
		if (L_3 == 5)
		{
			goto IL_03a2;
		}
		if (L_3 == 6)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_03e0;
	}

IL_0046:
	{
		StringBuilder_t1221177846 * L_4 = ___builder1;
		String_t* L_5 = __this->get_str_9();
		NullCheck(L_4);
		StringBuilder_Append_m3636508479(L_4, L_5, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_0058:
	{
		StringBuilder_t1221177846 * L_6 = ___builder1;
		String_t* L_7 = __this->get_str_9();
		NullCheck(L_6);
		StringBuilder_AppendFormat_m3265503696(L_6, _stringLiteral1554931876, L_7, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_006f:
	{
		bool L_8 = __this->get_useInt_11();
		if (!L_8)
		{
			goto IL_0097;
		}
	}
	{
		StringBuilder_t1221177846 * L_9 = ___builder1;
		int64_t* L_10 = __this->get_address_of_i_12();
		String_t* L_11 = Int64_ToString_m689375889(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		StringBuilder_Append_m3636508479(L_9, L_11, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_0097:
	{
		float L_12 = __this->get_n_10();
		bool L_13 = Single_IsInfinity_m3331110346(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00b8;
		}
	}
	{
		StringBuilder_t1221177846 * L_14 = ___builder1;
		NullCheck(L_14);
		StringBuilder_Append_m3636508479(L_14, _stringLiteral1751346954, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_00b8:
	{
		float L_15 = __this->get_n_10();
		bool L_16 = Single_IsNegativeInfinity_m2615809279(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00d9;
		}
	}
	{
		StringBuilder_t1221177846 * L_17 = ___builder1;
		NullCheck(L_17);
		StringBuilder_Append_m3636508479(L_17, _stringLiteral376316188, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_00d9:
	{
		float L_18 = __this->get_n_10();
		bool L_19 = Single_IsNaN_m2349591895(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00fa;
		}
	}
	{
		StringBuilder_t1221177846 * L_20 = ___builder1;
		NullCheck(L_20);
		StringBuilder_Append_m3636508479(L_20, _stringLiteral452738781, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_00fa:
	{
		StringBuilder_t1221177846 * L_21 = ___builder1;
		float* L_22 = __this->get_address_of_n_10();
		String_t* L_23 = Single_ToString_m1813392066(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		StringBuilder_Append_m3636508479(L_21, L_23, /*hidden argument*/NULL);
	}

IL_0112:
	{
		goto IL_03e0;
	}

IL_0117:
	{
		StringBuilder_t1221177846 * L_24 = ___builder1;
		NullCheck(L_24);
		StringBuilder_Append_m3636508479(L_24, _stringLiteral372029399, /*hidden argument*/NULL);
		List_1_t1341003379 * L_25 = __this->get_list_7();
		NullCheck(L_25);
		int32_t L_26 = List_1_get_Count_m3485821544(L_25, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_26) <= ((int32_t)0)))
		{
			goto IL_020a;
		}
	}
	{
		bool L_27 = ___pretty2;
		if (!L_27)
		{
			goto IL_0146;
		}
	}
	{
		StringBuilder_t1221177846 * L_28 = ___builder1;
		NullCheck(L_28);
		StringBuilder_Append_m3636508479(L_28, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_0146:
	{
		V_1 = 0;
		goto IL_01d2;
	}

IL_014d:
	{
		List_1_t1398341365 * L_29 = __this->get_keys_8();
		int32_t L_30 = V_1;
		NullCheck(L_29);
		String_t* L_31 = List_1_get_Item_m566484697(L_29, L_30, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		V_2 = L_31;
		List_1_t1341003379 * L_32 = __this->get_list_7();
		int32_t L_33 = V_1;
		NullCheck(L_32);
		JSONObject_t1971882247 * L_34 = List_1_get_Item_m429614411(L_32, L_33, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		V_3 = L_34;
		JSONObject_t1971882247 * L_35 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_36 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01ce;
		}
	}
	{
		bool L_37 = ___pretty2;
		if (!L_37)
		{
			goto IL_019a;
		}
	}
	{
		V_4 = 0;
		goto IL_0192;
	}

IL_0180:
	{
		StringBuilder_t1221177846 * L_38 = ___builder1;
		NullCheck(L_38);
		StringBuilder_Append_m3636508479(L_38, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_39 = V_4;
		V_4 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0192:
	{
		int32_t L_40 = V_4;
		int32_t L_41 = ___depth0;
		if ((((int32_t)L_40) < ((int32_t)L_41)))
		{
			goto IL_0180;
		}
	}

IL_019a:
	{
		StringBuilder_t1221177846 * L_42 = ___builder1;
		String_t* L_43 = V_2;
		NullCheck(L_42);
		StringBuilder_AppendFormat_m3265503696(L_42, _stringLiteral845265362, L_43, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_44 = V_3;
		int32_t L_45 = ___depth0;
		StringBuilder_t1221177846 * L_46 = ___builder1;
		bool L_47 = ___pretty2;
		NullCheck(L_44);
		JSONObject_Stringify_m3496252199(L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_48 = ___builder1;
		NullCheck(L_48);
		StringBuilder_Append_m3636508479(L_48, _stringLiteral372029314, /*hidden argument*/NULL);
		bool L_49 = ___pretty2;
		if (!L_49)
		{
			goto IL_01ce;
		}
	}
	{
		StringBuilder_t1221177846 * L_50 = ___builder1;
		NullCheck(L_50);
		StringBuilder_Append_m3636508479(L_50, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_01ce:
	{
		int32_t L_51 = V_1;
		V_1 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_01d2:
	{
		int32_t L_52 = V_1;
		List_1_t1341003379 * L_53 = __this->get_list_7();
		NullCheck(L_53);
		int32_t L_54 = List_1_get_Count_m3485821544(L_53, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_014d;
		}
	}
	{
		bool L_55 = ___pretty2;
		if (!L_55)
		{
			goto IL_01fc;
		}
	}
	{
		StringBuilder_t1221177846 * L_56 = ___builder1;
		StringBuilder_t1221177846 * L_57 = L_56;
		NullCheck(L_57);
		int32_t L_58 = StringBuilder_get_Length_m1608241323(L_57, /*hidden argument*/NULL);
		NullCheck(L_57);
		StringBuilder_set_Length_m3039225444(L_57, ((int32_t)((int32_t)L_58-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_020a;
	}

IL_01fc:
	{
		StringBuilder_t1221177846 * L_59 = ___builder1;
		StringBuilder_t1221177846 * L_60 = L_59;
		NullCheck(L_60);
		int32_t L_61 = StringBuilder_get_Length_m1608241323(L_60, /*hidden argument*/NULL);
		NullCheck(L_60);
		StringBuilder_set_Length_m3039225444(L_60, ((int32_t)((int32_t)L_61-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_020a:
	{
		bool L_62 = ___pretty2;
		if (!L_62)
		{
			goto IL_0251;
		}
	}
	{
		List_1_t1341003379 * L_63 = __this->get_list_7();
		NullCheck(L_63);
		int32_t L_64 = List_1_get_Count_m3485821544(L_63, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_64) <= ((int32_t)0)))
		{
			goto IL_0251;
		}
	}
	{
		StringBuilder_t1221177846 * L_65 = ___builder1;
		NullCheck(L_65);
		StringBuilder_Append_m3636508479(L_65, _stringLiteral372029352, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_0247;
	}

IL_0235:
	{
		StringBuilder_t1221177846 * L_66 = ___builder1;
		NullCheck(L_66);
		StringBuilder_Append_m3636508479(L_66, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_67 = V_5;
		V_5 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0247:
	{
		int32_t L_68 = V_5;
		int32_t L_69 = ___depth0;
		if ((((int32_t)L_68) < ((int32_t)((int32_t)((int32_t)L_69-(int32_t)1)))))
		{
			goto IL_0235;
		}
	}

IL_0251:
	{
		StringBuilder_t1221177846 * L_70 = ___builder1;
		NullCheck(L_70);
		StringBuilder_Append_m3636508479(L_70, _stringLiteral372029393, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_0262:
	{
		StringBuilder_t1221177846 * L_71 = ___builder1;
		NullCheck(L_71);
		StringBuilder_Append_m3636508479(L_71, _stringLiteral372029431, /*hidden argument*/NULL);
		List_1_t1341003379 * L_72 = __this->get_list_7();
		NullCheck(L_72);
		int32_t L_73 = List_1_get_Count_m3485821544(L_72, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_73) <= ((int32_t)0)))
		{
			goto IL_034a;
		}
	}
	{
		bool L_74 = ___pretty2;
		if (!L_74)
		{
			goto IL_0291;
		}
	}
	{
		StringBuilder_t1221177846 * L_75 = ___builder1;
		NullCheck(L_75);
		StringBuilder_Append_m3636508479(L_75, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_0291:
	{
		V_6 = 0;
		goto IL_0311;
	}

IL_0299:
	{
		List_1_t1341003379 * L_76 = __this->get_list_7();
		int32_t L_77 = V_6;
		NullCheck(L_76);
		JSONObject_t1971882247 * L_78 = List_1_get_Item_m429614411(L_76, L_77, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_79 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_030b;
		}
	}
	{
		bool L_80 = ___pretty2;
		if (!L_80)
		{
			goto IL_02d8;
		}
	}
	{
		V_7 = 0;
		goto IL_02d0;
	}

IL_02be:
	{
		StringBuilder_t1221177846 * L_81 = ___builder1;
		NullCheck(L_81);
		StringBuilder_Append_m3636508479(L_81, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_82 = V_7;
		V_7 = ((int32_t)((int32_t)L_82+(int32_t)1));
	}

IL_02d0:
	{
		int32_t L_83 = V_7;
		int32_t L_84 = ___depth0;
		if ((((int32_t)L_83) < ((int32_t)L_84)))
		{
			goto IL_02be;
		}
	}

IL_02d8:
	{
		List_1_t1341003379 * L_85 = __this->get_list_7();
		int32_t L_86 = V_6;
		NullCheck(L_85);
		JSONObject_t1971882247 * L_87 = List_1_get_Item_m429614411(L_85, L_86, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		int32_t L_88 = ___depth0;
		StringBuilder_t1221177846 * L_89 = ___builder1;
		bool L_90 = ___pretty2;
		NullCheck(L_87);
		JSONObject_Stringify_m3496252199(L_87, L_88, L_89, L_90, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_91 = ___builder1;
		NullCheck(L_91);
		StringBuilder_Append_m3636508479(L_91, _stringLiteral372029314, /*hidden argument*/NULL);
		bool L_92 = ___pretty2;
		if (!L_92)
		{
			goto IL_030b;
		}
	}
	{
		StringBuilder_t1221177846 * L_93 = ___builder1;
		NullCheck(L_93);
		StringBuilder_Append_m3636508479(L_93, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_030b:
	{
		int32_t L_94 = V_6;
		V_6 = ((int32_t)((int32_t)L_94+(int32_t)1));
	}

IL_0311:
	{
		int32_t L_95 = V_6;
		List_1_t1341003379 * L_96 = __this->get_list_7();
		NullCheck(L_96);
		int32_t L_97 = List_1_get_Count_m3485821544(L_96, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_95) < ((int32_t)L_97)))
		{
			goto IL_0299;
		}
	}
	{
		bool L_98 = ___pretty2;
		if (!L_98)
		{
			goto IL_033c;
		}
	}
	{
		StringBuilder_t1221177846 * L_99 = ___builder1;
		StringBuilder_t1221177846 * L_100 = L_99;
		NullCheck(L_100);
		int32_t L_101 = StringBuilder_get_Length_m1608241323(L_100, /*hidden argument*/NULL);
		NullCheck(L_100);
		StringBuilder_set_Length_m3039225444(L_100, ((int32_t)((int32_t)L_101-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_034a;
	}

IL_033c:
	{
		StringBuilder_t1221177846 * L_102 = ___builder1;
		StringBuilder_t1221177846 * L_103 = L_102;
		NullCheck(L_103);
		int32_t L_104 = StringBuilder_get_Length_m1608241323(L_103, /*hidden argument*/NULL);
		NullCheck(L_103);
		StringBuilder_set_Length_m3039225444(L_103, ((int32_t)((int32_t)L_104-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_034a:
	{
		bool L_105 = ___pretty2;
		if (!L_105)
		{
			goto IL_0391;
		}
	}
	{
		List_1_t1341003379 * L_106 = __this->get_list_7();
		NullCheck(L_106);
		int32_t L_107 = List_1_get_Count_m3485821544(L_106, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_107) <= ((int32_t)0)))
		{
			goto IL_0391;
		}
	}
	{
		StringBuilder_t1221177846 * L_108 = ___builder1;
		NullCheck(L_108);
		StringBuilder_Append_m3636508479(L_108, _stringLiteral372029352, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_0387;
	}

IL_0375:
	{
		StringBuilder_t1221177846 * L_109 = ___builder1;
		NullCheck(L_109);
		StringBuilder_Append_m3636508479(L_109, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_110 = V_8;
		V_8 = ((int32_t)((int32_t)L_110+(int32_t)1));
	}

IL_0387:
	{
		int32_t L_111 = V_8;
		int32_t L_112 = ___depth0;
		if ((((int32_t)L_111) < ((int32_t)((int32_t)((int32_t)L_112-(int32_t)1)))))
		{
			goto IL_0375;
		}
	}

IL_0391:
	{
		StringBuilder_t1221177846 * L_113 = ___builder1;
		NullCheck(L_113);
		StringBuilder_Append_m3636508479(L_113, _stringLiteral372029425, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_03a2:
	{
		bool L_114 = __this->get_b_13();
		if (!L_114)
		{
			goto IL_03be;
		}
	}
	{
		StringBuilder_t1221177846 * L_115 = ___builder1;
		NullCheck(L_115);
		StringBuilder_Append_m3636508479(L_115, _stringLiteral3323263070, /*hidden argument*/NULL);
		goto IL_03ca;
	}

IL_03be:
	{
		StringBuilder_t1221177846 * L_116 = ___builder1;
		NullCheck(L_116);
		StringBuilder_Append_m3636508479(L_116, _stringLiteral2609877245, /*hidden argument*/NULL);
	}

IL_03ca:
	{
		goto IL_03e0;
	}

IL_03cf:
	{
		StringBuilder_t1221177846 * L_117 = ___builder1;
		NullCheck(L_117);
		StringBuilder_Append_m3636508479(L_117, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_03e0:
	{
		return;
	}
}
// UnityEngine.WWWForm JSONObject::op_Implicit(JSONObject)
extern Il2CppClass* WWWForm_t3950226929_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern const uint32_t JSONObject_op_Implicit_m146727933_MetadataUsageId;
extern "C"  WWWForm_t3950226929 * JSONObject_op_Implicit_m146727933 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_op_Implicit_m146727933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WWWForm_t3950226929 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		WWWForm_t3950226929 * L_0 = (WWWForm_t3950226929 *)il2cpp_codegen_object_new(WWWForm_t3950226929_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2129424870(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_007d;
	}

IL_000d:
	{
		int32_t L_1 = V_1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_type_6();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0037;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = ___obj0;
		NullCheck(L_8);
		List_1_t1398341365 * L_9 = L_8->get_keys_8();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = List_1_get_Item_m566484697(L_9, L_10, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		V_2 = L_11;
	}

IL_0037:
	{
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		List_1_t1341003379 * L_13 = L_12->get_list_7();
		int32_t L_14 = V_1;
		NullCheck(L_13);
		JSONObject_t1971882247 * L_15 = List_1_get_Item_m429614411(L_13, L_14, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		JSONObject_t1971882247 * L_17 = ___obj0;
		NullCheck(L_17);
		List_1_t1341003379 * L_18 = L_17->get_list_7();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_20 = List_1_get_Item_m429614411(L_18, L_19, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_20);
		int32_t L_21 = L_20->get_type_6();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_22);
		String_t* L_24 = String_Replace_m1941156251(L_22, _stringLiteral372029312, L_23, /*hidden argument*/NULL);
		V_3 = L_24;
	}

IL_0071:
	{
		WWWForm_t3950226929 * L_25 = V_0;
		String_t* L_26 = V_2;
		String_t* L_27 = V_3;
		NullCheck(L_25);
		WWWForm_AddField_m1334606983(L_25, L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_007d:
	{
		int32_t L_29 = V_1;
		JSONObject_t1971882247 * L_30 = ___obj0;
		NullCheck(L_30);
		List_1_t1341003379 * L_31 = L_30->get_list_7();
		NullCheck(L_31);
		int32_t L_32 = List_1_get_Count_m3485821544(L_31, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_29) < ((int32_t)L_32)))
		{
			goto IL_000d;
		}
	}
	{
		WWWForm_t3950226929 * L_33 = V_0;
		return L_33;
	}
}
// JSONObject JSONObject::get_Item(System.Int32)
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const uint32_t JSONObject_get_Item_m481309353_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_get_Item_m481309353 (JSONObject_t1971882247 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_Item_m481309353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1341003379 * L_0 = __this->get_list_7();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3485821544(L_0, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		int32_t L_2 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1341003379 * L_3 = __this->get_list_7();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_5 = List_1_get_Item_m429614411(L_3, L_4, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		return L_5;
	}

IL_001e:
	{
		return (JSONObject_t1971882247 *)NULL;
	}
}
// System.Void JSONObject::set_Item(System.Int32,JSONObject)
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m254334392_MethodInfo_var;
extern const uint32_t JSONObject_set_Item_m2332624254_MetadataUsageId;
extern "C"  void JSONObject_set_Item_m2332624254 (JSONObject_t1971882247 * __this, int32_t ___index0, JSONObject_t1971882247 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_set_Item_m2332624254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1341003379 * L_0 = __this->get_list_7();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3485821544(L_0, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		int32_t L_2 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1341003379 * L_3 = __this->get_list_7();
		int32_t L_4 = ___index0;
		JSONObject_t1971882247 * L_5 = ___value1;
		NullCheck(L_3);
		List_1_set_Item_m254334392(L_3, L_4, L_5, /*hidden argument*/List_1_set_Item_m254334392_MethodInfo_var);
	}

IL_001e:
	{
		return;
	}
}
// JSONObject JSONObject::get_Item(System.String)
extern "C"  JSONObject_t1971882247 * JSONObject_get_Item_m2638456294 (JSONObject_t1971882247 * __this, String_t* ___index0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___index0;
		JSONObject_t1971882247 * L_1 = JSONObject_GetField_m887332132(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::set_Item(System.String,JSONObject)
extern "C"  void JSONObject_set_Item_m2230411265 (JSONObject_t1971882247 * __this, String_t* ___index0, JSONObject_t1971882247 * ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___index0;
		JSONObject_t1971882247 * L_1 = ___value1;
		JSONObject_SetField_m1180592005(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String JSONObject::ToString()
extern "C"  String_t* JSONObject_ToString_m2912340705 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = JSONObject_Print_m4113013869(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String JSONObject::ToString(System.Boolean)
extern "C"  String_t* JSONObject_ToString_m3778538422 (JSONObject_t1971882247 * __this, bool ___pretty0, const MethodInfo* method)
{
	{
		bool L_0 = ___pretty0;
		String_t* L_1 = JSONObject_Print_m4113013869(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> JSONObject::ToDictionary()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3686231158_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral942984848;
extern Il2CppCodeGenString* _stringLiteral688080911;
extern Il2CppCodeGenString* _stringLiteral3109692691;
extern const uint32_t JSONObject_ToDictionary_m1765493061_MetadataUsageId;
extern "C"  Dictionary_2_t3943999495 * JSONObject_ToDictionary_m1765493061 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_ToDictionary_m1765493061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3943999495 * V_0 = NULL;
	int32_t V_1 = 0;
	JSONObject_t1971882247 * V_2 = NULL;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_00ff;
		}
	}
	{
		Dictionary_2_t3943999495 * L_1 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_1, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_00ec;
	}

IL_0019:
	{
		List_1_t1341003379 * L_2 = __this->get_list_7();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		JSONObject_t1971882247 * L_4 = List_1_get_Item_m429614411(L_2, L_3, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		V_2 = L_4;
		JSONObject_t1971882247 * L_5 = V_2;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_type_6();
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 0)
		{
			goto IL_004e;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 1)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 2)
		{
			goto IL_00c3;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 3)
		{
			goto IL_00c3;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 4)
		{
			goto IL_0097;
		}
	}
	{
		goto IL_00c3;
	}

IL_004e:
	{
		Dictionary_2_t3943999495 * L_8 = V_0;
		List_1_t1398341365 * L_9 = __this->get_keys_8();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = List_1_get_Item_m566484697(L_9, L_10, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		JSONObject_t1971882247 * L_12 = V_2;
		NullCheck(L_12);
		String_t* L_13 = L_12->get_str_9();
		NullCheck(L_8);
		Dictionary_2_Add_m3686231158(L_8, L_11, L_13, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		goto IL_00e8;
	}

IL_006b:
	{
		Dictionary_2_t3943999495 * L_14 = V_0;
		List_1_t1398341365 * L_15 = __this->get_keys_8();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		String_t* L_17 = List_1_get_Item_m566484697(L_15, L_16, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		JSONObject_t1971882247 * L_18 = V_2;
		NullCheck(L_18);
		float L_19 = L_18->get_n_10();
		float L_20 = L_19;
		Il2CppObject * L_21 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_23 = String_Concat_m56707527(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_14);
		Dictionary_2_Add_m3686231158(L_14, L_17, L_23, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		goto IL_00e8;
	}

IL_0097:
	{
		Dictionary_2_t3943999495 * L_24 = V_0;
		List_1_t1398341365 * L_25 = __this->get_keys_8();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		String_t* L_27 = List_1_get_Item_m566484697(L_25, L_26, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		JSONObject_t1971882247 * L_28 = V_2;
		NullCheck(L_28);
		bool L_29 = L_28->get_b_13();
		bool L_30 = L_29;
		Il2CppObject * L_31 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_33 = String_Concat_m56707527(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_24);
		Dictionary_2_Add_m3686231158(L_24, L_27, L_33, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		goto IL_00e8;
	}

IL_00c3:
	{
		List_1_t1398341365 * L_34 = __this->get_keys_8();
		int32_t L_35 = V_1;
		NullCheck(L_34);
		String_t* L_36 = List_1_get_Item_m566484697(L_34, L_35, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral942984848, L_36, _stringLiteral688080911, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_00e8:
	{
		int32_t L_38 = V_1;
		V_1 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00ec:
	{
		int32_t L_39 = V_1;
		List_1_t1341003379 * L_40 = __this->get_list_7();
		NullCheck(L_40);
		int32_t L_41 = List_1_get_Count_m3485821544(L_40, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_39) < ((int32_t)L_41)))
		{
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t3943999495 * L_42 = V_0;
		return L_42;
	}

IL_00ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3109692691, /*hidden argument*/NULL);
		return (Dictionary_2_t3943999495 *)NULL;
	}
}
// System.Boolean JSONObject::op_Implicit(JSONObject)
extern "C"  bool JSONObject_op_Implicit_m2214727230 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___o0, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = ___o0;
		return (bool)((((int32_t)((((Il2CppObject*)(JSONObject_t1971882247 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void JSONObject::.cctor()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Stopwatch_t1380178105_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0_FieldInfo_var;
extern const uint32_t JSONObject__cctor_m3703431513_MetadataUsageId;
extern "C"  void JSONObject__cctor_m3703431513 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__cctor_m3703431513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t1328083999* L_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)6));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0_FieldInfo_var), /*hidden argument*/NULL);
		((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->set_WHITESPACE_5(L_0);
		Stopwatch_t1380178105 * L_1 = (Stopwatch_t1380178105 *)il2cpp_codegen_object_new(Stopwatch_t1380178105_il2cpp_TypeInfo_var);
		Stopwatch__ctor_m589309528(L_1, /*hidden argument*/NULL);
		((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->set_printWatch_15(L_1);
		return;
	}
}
// System.Void JSONObject/<BakeAsync>c__Iterator0::.ctor()
extern "C"  void U3CBakeAsyncU3Ec__Iterator0__ctor_m2863786865 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JSONObject/<BakeAsync>c__Iterator0::MoveNext()
extern Il2CppClass* IEnumerable_1_t2321347278_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3799711356_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator0_MoveNext_m913392635_MetadataUsageId;
extern "C"  bool U3CBakeAsyncU3Ec__Iterator0_MoveNext_m913392635 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator0_MoveNext_m913392635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_00f6;
	}

IL_0023:
	{
		JSONObject_t1971882247 * L_2 = __this->get_U24this_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_type_6();
		if ((((int32_t)L_3) == ((int32_t)6)))
		{
			goto IL_00ef;
		}
	}
	{
		JSONObject_t1971882247 * L_4 = __this->get_U24this_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = JSONObject_PrintAsync_m1157769192(L_4, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t2321347278_il2cpp_TypeInfo_var, L_5);
		__this->set_U24locvar0_0(L_6);
		V_0 = ((int32_t)-3);
	}

IL_004e:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_7 = V_0;
			if (((int32_t)((int32_t)L_7-(int32_t)1)) == 0)
			{
				goto IL_009d;
			}
		}

IL_005a:
		{
			goto IL_00b3;
		}

IL_005f:
		{
			Il2CppObject* L_8 = __this->get_U24locvar0_0();
			NullCheck(L_8);
			String_t* L_9 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t3799711356_il2cpp_TypeInfo_var, L_8);
			__this->set_U3CsU3E__0_1(L_9);
			String_t* L_10 = __this->get_U3CsU3E__0_1();
			if (L_10)
			{
				goto IL_00a2;
			}
		}

IL_007b:
		{
			String_t* L_11 = __this->get_U3CsU3E__0_1();
			__this->set_U24current_3(L_11);
			bool L_12 = __this->get_U24disposing_4();
			if (L_12)
			{
				goto IL_0096;
			}
		}

IL_008f:
		{
			__this->set_U24PC_5(1);
		}

IL_0096:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xF8, FINALLY_00c8);
		}

IL_009d:
		{
			goto IL_00b3;
		}

IL_00a2:
		{
			JSONObject_t1971882247 * L_13 = __this->get_U24this_2();
			String_t* L_14 = __this->get_U3CsU3E__0_1();
			NullCheck(L_13);
			L_13->set_str_9(L_14);
		}

IL_00b3:
		{
			Il2CppObject* L_15 = __this->get_U24locvar0_0();
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_005f;
			}
		}

IL_00c3:
		{
			IL2CPP_LEAVE(0xE3, FINALLY_00c8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c8;
	}

FINALLY_00c8:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_00cc;
			}
		}

IL_00cb:
		{
			IL2CPP_END_FINALLY(200)
		}

IL_00cc:
		{
			Il2CppObject* L_18 = __this->get_U24locvar0_0();
			if (!L_18)
			{
				goto IL_00e2;
			}
		}

IL_00d7:
		{
			Il2CppObject* L_19 = __this->get_U24locvar0_0();
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_19);
		}

IL_00e2:
		{
			IL2CPP_END_FINALLY(200)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(200)
	{
		IL2CPP_JUMP_TBL(0xF8, IL_00f8)
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e3:
	{
		JSONObject_t1971882247 * L_20 = __this->get_U24this_2();
		NullCheck(L_20);
		L_20->set_type_6(6);
	}

IL_00ef:
	{
		__this->set_U24PC_5((-1));
	}

IL_00f6:
	{
		return (bool)0;
	}

IL_00f8:
	{
		return (bool)1;
	}
}
// System.Object JSONObject/<BakeAsync>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3567374907 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object JSONObject/<BakeAsync>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3869766659 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void JSONObject/<BakeAsync>c__Iterator0::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator0_Dispose_m3935130108_MetadataUsageId;
extern "C"  void U3CBakeAsyncU3Ec__Iterator0_Dispose_m3935130108 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator0_Dispose_m3935130108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0044;
		}
		if (L_1 == 1)
		{
			goto IL_0028;
		}
	}
	{
		goto IL_0044;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x44, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U24locvar0_0();
			if (!L_2)
			{
				goto IL_0043;
			}
		}

IL_0038:
		{
			Il2CppObject* L_3 = __this->get_U24locvar0_0();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_3);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(45)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0044:
	{
		return;
	}
}
// System.Void JSONObject/<BakeAsync>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator0_Reset_m1787939058_MetadataUsageId;
extern "C"  void U3CBakeAsyncU3Ec__Iterator0_Reset_m1787939058 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator0_Reset_m1787939058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator JSONObject/<BakeAsync>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m3693741580 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3677305720(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<BakeAsync>c__Iterator0::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern Il2CppClass* U3CBakeAsyncU3Ec__Iterator0_t1149809410_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3677305720_MetadataUsageId;
extern "C"  Il2CppObject* U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3677305720 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3677305720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CBakeAsyncU3Ec__Iterator0_t1149809410 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_2 = (U3CBakeAsyncU3Ec__Iterator0_t1149809410 *)il2cpp_codegen_object_new(U3CBakeAsyncU3Ec__Iterator0_t1149809410_il2cpp_TypeInfo_var);
		U3CBakeAsyncU3Ec__Iterator0__ctor_m2863786865(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_3 = V_0;
		JSONObject_t1971882247 * L_4 = __this->get_U24this_2();
		NullCheck(L_3);
		L_3->set_U24this_2(L_4);
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_5 = V_0;
		return L_5;
	}
}
// System.Void JSONObject/<PrintAsync>c__Iterator1::.ctor()
extern "C"  void U3CPrintAsyncU3Ec__Iterator1__ctor_m918627686 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JSONObject/<PrintAsync>c__Iterator1::MoveNext()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator1_MoveNext_m2236606538_MetadataUsageId;
extern "C"  bool U3CPrintAsyncU3Ec__Iterator1_MoveNext_m2236606538 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator1_MoveNext_m2236606538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_006c;
		}
		if (L_1 == 2)
		{
			goto IL_0113;
		}
	}
	{
		goto IL_011a;
	}

IL_0027:
	{
		StringBuilder_t1221177846 * L_2 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_2, /*hidden argument*/NULL);
		__this->set_U3CbuilderU3E__0_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		Stopwatch_t1380178105 * L_3 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_15();
		NullCheck(L_3);
		Stopwatch_Reset_m3196507227(L_3, /*hidden argument*/NULL);
		Stopwatch_t1380178105 * L_4 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_15();
		NullCheck(L_4);
		Stopwatch_Start_m2051791460(L_4, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_5 = __this->get_U24this_5();
		StringBuilder_t1221177846 * L_6 = __this->get_U3CbuilderU3E__0_0();
		bool L_7 = __this->get_pretty_1();
		NullCheck(L_5);
		Il2CppObject * L_8 = JSONObject_StringifyAsync_m1504868741(L_5, 0, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_8);
		__this->set_U24locvar0_2(L_9);
		V_0 = ((int32_t)-3);
	}

IL_006c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_10 = V_0;
			if (((int32_t)((int32_t)L_10-(int32_t)1)) == 0)
			{
				goto IL_00b0;
			}
		}

IL_0078:
		{
			goto IL_00b0;
		}

IL_007d:
		{
			Il2CppObject * L_11 = __this->get_U24locvar0_2();
			NullCheck(L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_11);
			__this->set_U3CeU3E__1_3(((Il2CppObject *)Castclass(L_12, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
			__this->set_U24current_6((String_t*)NULL);
			bool L_13 = __this->get_U24disposing_7();
			if (L_13)
			{
				goto IL_00a9;
			}
		}

IL_00a2:
		{
			__this->set_U24PC_8(1);
		}

IL_00a9:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x11C, FINALLY_00c5);
		}

IL_00b0:
		{
			Il2CppObject * L_14 = __this->get_U24locvar0_2();
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_007d;
			}
		}

IL_00c0:
		{
			IL2CPP_LEAVE(0xEE, FINALLY_00c5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c5;
	}

FINALLY_00c5:
	{ // begin finally (depth: 1)
		{
			bool L_16 = V_1;
			if (!L_16)
			{
				goto IL_00c9;
			}
		}

IL_00c8:
		{
			IL2CPP_END_FINALLY(197)
		}

IL_00c9:
		{
			Il2CppObject * L_17 = __this->get_U24locvar0_2();
			Il2CppObject * L_18 = ((Il2CppObject *)IsInst(L_17, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_2 = L_18;
			__this->set_U24locvar1_4(L_18);
			Il2CppObject * L_19 = V_2;
			if (!L_19)
			{
				goto IL_00ed;
			}
		}

IL_00e2:
		{
			Il2CppObject * L_20 = __this->get_U24locvar1_4();
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_20);
		}

IL_00ed:
		{
			IL2CPP_END_FINALLY(197)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(197)
	{
		IL2CPP_JUMP_TBL(0x11C, IL_011c)
		IL2CPP_JUMP_TBL(0xEE, IL_00ee)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ee:
	{
		StringBuilder_t1221177846 * L_21 = __this->get_U3CbuilderU3E__0_0();
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		__this->set_U24current_6(L_22);
		bool L_23 = __this->get_U24disposing_7();
		if (L_23)
		{
			goto IL_010e;
		}
	}
	{
		__this->set_U24PC_8(2);
	}

IL_010e:
	{
		goto IL_011c;
	}

IL_0113:
	{
		__this->set_U24PC_8((-1));
	}

IL_011a:
	{
		return (bool)0;
	}

IL_011c:
	{
		return (bool)1;
	}
}
// System.String JSONObject/<PrintAsync>c__Iterator1::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m1959409068 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object JSONObject/<PrintAsync>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m281630052 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Void JSONObject/<PrintAsync>c__Iterator1::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator1_Dispose_m655283723_MetadataUsageId;
extern "C"  void U3CPrintAsyncU3Ec__Iterator1_Dispose_m655283723 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator1_Dispose_m655283723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24disposing_7((bool)1);
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0056;
		}
		if (L_1 == 1)
		{
			goto IL_002c;
		}
		if (L_1 == 2)
		{
			goto IL_0056;
		}
	}
	{
		goto IL_0056;
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x56, FINALLY_0031);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_U24locvar0_2();
			Il2CppObject * L_3 = ((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_1 = L_3;
			__this->set_U24locvar1_4(L_3);
			Il2CppObject * L_4 = V_1;
			if (!L_4)
			{
				goto IL_0055;
			}
		}

IL_004a:
		{
			Il2CppObject * L_5 = __this->get_U24locvar1_4();
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_5);
		}

IL_0055:
		{
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0056:
	{
		return;
	}
}
// System.Void JSONObject/<PrintAsync>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator1_Reset_m1797953401_MetadataUsageId;
extern "C"  void U3CPrintAsyncU3Ec__Iterator1_Reset_m1797953401 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator1_Reset_m1797953401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator JSONObject/<PrintAsync>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m648327517 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m157685351(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.String> JSONObject/<PrintAsync>c__Iterator1::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern Il2CppClass* U3CPrintAsyncU3Ec__Iterator1_t716304657_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m157685351_MetadataUsageId;
extern "C"  Il2CppObject* U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m157685351 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m157685351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPrintAsyncU3Ec__Iterator1_t716304657 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_8();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_2 = (U3CPrintAsyncU3Ec__Iterator1_t716304657 *)il2cpp_codegen_object_new(U3CPrintAsyncU3Ec__Iterator1_t716304657_il2cpp_TypeInfo_var);
		U3CPrintAsyncU3Ec__Iterator1__ctor_m918627686(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_3 = V_0;
		JSONObject_t1971882247 * L_4 = __this->get_U24this_5();
		NullCheck(L_3);
		L_3->set_U24this_5(L_4);
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_5 = V_0;
		bool L_6 = __this->get_pretty_1();
		NullCheck(L_5);
		L_5->set_pretty_1(L_6);
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_7 = V_0;
		return L_7;
	}
}
// System.Void JSONObject/<StringifyAsync>c__Iterator2::.ctor()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator2__ctor_m3369090517 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JSONObject/<StringifyAsync>c__Iterator2::MoveNext()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2041690406;
extern Il2CppCodeGenString* _stringLiteral1554931876;
extern Il2CppCodeGenString* _stringLiteral1751346954;
extern Il2CppCodeGenString* _stringLiteral376316188;
extern Il2CppCodeGenString* _stringLiteral452738781;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral372029349;
extern Il2CppCodeGenString* _stringLiteral845265362;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator2_MoveNext_m4115355999_MetadataUsageId;
extern "C"  bool U3CStringifyAsyncU3Ec__Iterator2_MoveNext_m4115355999 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator2_MoveNext_m4115355999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	TimeSpan_t3430258949  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_18();
		V_0 = L_0;
		__this->set_U24PC_18((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0097;
		}
		if (L_1 == 2)
		{
			goto IL_030a;
		}
		if (L_1 == 3)
		{
			goto IL_0576;
		}
	}
	{
		goto IL_075a;
	}

IL_002b:
	{
		int32_t L_2 = __this->get_depth_0();
		int32_t L_3 = L_2;
		V_2 = L_3;
		__this->set_depth_0(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)100))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2041690406, /*hidden argument*/NULL);
		goto IL_075a;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		Stopwatch_t1380178105 * L_5 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_15();
		NullCheck(L_5);
		TimeSpan_t3430258949  L_6 = Stopwatch_get_Elapsed_m3190561196(L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		double L_7 = TimeSpan_get_TotalSeconds_m1295026915((&V_3), /*hidden argument*/NULL);
		if ((!(((double)L_7) > ((double)(0.00800000037997961)))))
		{
			goto IL_00a1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		Stopwatch_t1380178105 * L_8 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_15();
		NullCheck(L_8);
		Stopwatch_Reset_m3196507227(L_8, /*hidden argument*/NULL);
		__this->set_U24current_15(NULL);
		bool L_9 = __this->get_U24disposing_16();
		if (L_9)
		{
			goto IL_0092;
		}
	}
	{
		__this->set_U24PC_18(1);
	}

IL_0092:
	{
		goto IL_075c;
	}

IL_0097:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		Stopwatch_t1380178105 * L_10 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_15();
		NullCheck(L_10);
		Stopwatch_Start_m2051791460(L_10, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		JSONObject_t1971882247 * L_11 = __this->get_U24this_14();
		NullCheck(L_11);
		int32_t L_12 = L_11->get_type_6();
		__this->set_U24locvar0_1(L_12);
		int32_t L_13 = __this->get_U24locvar0_1();
		if (L_13 == 0)
		{
			goto IL_073d;
		}
		if (L_13 == 1)
		{
			goto IL_00fa;
		}
		if (L_13 == 2)
		{
			goto IL_011b;
		}
		if (L_13 == 3)
		{
			goto IL_01fa;
		}
		if (L_13 == 4)
		{
			goto IL_0495;
		}
		if (L_13 == 5)
		{
			goto IL_0701;
		}
		if (L_13 == 6)
		{
			goto IL_00de;
		}
	}
	{
		goto IL_0753;
	}

IL_00de:
	{
		StringBuilder_t1221177846 * L_14 = __this->get_builder_2();
		JSONObject_t1971882247 * L_15 = __this->get_U24this_14();
		NullCheck(L_15);
		String_t* L_16 = L_15->get_str_9();
		NullCheck(L_14);
		StringBuilder_Append_m3636508479(L_14, L_16, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_00fa:
	{
		StringBuilder_t1221177846 * L_17 = __this->get_builder_2();
		JSONObject_t1971882247 * L_18 = __this->get_U24this_14();
		NullCheck(L_18);
		String_t* L_19 = L_18->get_str_9();
		NullCheck(L_17);
		StringBuilder_AppendFormat_m3265503696(L_17, _stringLiteral1554931876, L_19, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_011b:
	{
		JSONObject_t1971882247 * L_20 = __this->get_U24this_14();
		NullCheck(L_20);
		bool L_21 = L_20->get_useInt_11();
		if (!L_21)
		{
			goto IL_0152;
		}
	}
	{
		StringBuilder_t1221177846 * L_22 = __this->get_builder_2();
		JSONObject_t1971882247 * L_23 = __this->get_U24this_14();
		NullCheck(L_23);
		int64_t* L_24 = L_23->get_address_of_i_12();
		String_t* L_25 = Int64_ToString_m689375889(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		StringBuilder_Append_m3636508479(L_22, L_25, /*hidden argument*/NULL);
		goto IL_01f5;
	}

IL_0152:
	{
		JSONObject_t1971882247 * L_26 = __this->get_U24this_14();
		NullCheck(L_26);
		float L_27 = L_26->get_n_10();
		bool L_28 = Single_IsInfinity_m3331110346(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_017d;
		}
	}
	{
		StringBuilder_t1221177846 * L_29 = __this->get_builder_2();
		NullCheck(L_29);
		StringBuilder_Append_m3636508479(L_29, _stringLiteral1751346954, /*hidden argument*/NULL);
		goto IL_01f5;
	}

IL_017d:
	{
		JSONObject_t1971882247 * L_30 = __this->get_U24this_14();
		NullCheck(L_30);
		float L_31 = L_30->get_n_10();
		bool L_32 = Single_IsNegativeInfinity_m2615809279(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_01a8;
		}
	}
	{
		StringBuilder_t1221177846 * L_33 = __this->get_builder_2();
		NullCheck(L_33);
		StringBuilder_Append_m3636508479(L_33, _stringLiteral376316188, /*hidden argument*/NULL);
		goto IL_01f5;
	}

IL_01a8:
	{
		JSONObject_t1971882247 * L_34 = __this->get_U24this_14();
		NullCheck(L_34);
		float L_35 = L_34->get_n_10();
		bool L_36 = Single_IsNaN_m2349591895(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01d3;
		}
	}
	{
		StringBuilder_t1221177846 * L_37 = __this->get_builder_2();
		NullCheck(L_37);
		StringBuilder_Append_m3636508479(L_37, _stringLiteral452738781, /*hidden argument*/NULL);
		goto IL_01f5;
	}

IL_01d3:
	{
		StringBuilder_t1221177846 * L_38 = __this->get_builder_2();
		JSONObject_t1971882247 * L_39 = __this->get_U24this_14();
		NullCheck(L_39);
		float* L_40 = L_39->get_address_of_n_10();
		String_t* L_41 = Single_ToString_m1813392066(L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		StringBuilder_Append_m3636508479(L_38, L_41, /*hidden argument*/NULL);
	}

IL_01f5:
	{
		goto IL_0753;
	}

IL_01fa:
	{
		StringBuilder_t1221177846 * L_42 = __this->get_builder_2();
		NullCheck(L_42);
		StringBuilder_Append_m3636508479(L_42, _stringLiteral372029399, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_43 = __this->get_U24this_14();
		NullCheck(L_43);
		List_1_t1341003379 * L_44 = L_43->get_list_7();
		NullCheck(L_44);
		int32_t L_45 = List_1_get_Count_m3485821544(L_44, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_45) <= ((int32_t)0)))
		{
			goto IL_041f;
		}
	}
	{
		bool L_46 = __this->get_pretty_3();
		if (!L_46)
		{
			goto IL_023d;
		}
	}
	{
		StringBuilder_t1221177846 * L_47 = __this->get_builder_2();
		NullCheck(L_47);
		StringBuilder_Append_m3636508479(L_47, _stringLiteral2162321587, /*hidden argument*/NULL);
	}

IL_023d:
	{
		__this->set_U3CiU3E__0_4(0);
		goto IL_03ce;
	}

IL_0249:
	{
		JSONObject_t1971882247 * L_48 = __this->get_U24this_14();
		NullCheck(L_48);
		List_1_t1398341365 * L_49 = L_48->get_keys_8();
		int32_t L_50 = __this->get_U3CiU3E__0_4();
		NullCheck(L_49);
		String_t* L_51 = List_1_get_Item_m566484697(L_49, L_50, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		__this->set_U3CkeyU3E__1_5(L_51);
		JSONObject_t1971882247 * L_52 = __this->get_U24this_14();
		NullCheck(L_52);
		List_1_t1341003379 * L_53 = L_52->get_list_7();
		int32_t L_54 = __this->get_U3CiU3E__0_4();
		NullCheck(L_53);
		JSONObject_t1971882247 * L_55 = List_1_get_Item_m429614411(L_53, L_54, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		__this->set_U3CobjU3E__2_6(L_55);
		JSONObject_t1971882247 * L_56 = __this->get_U3CobjU3E__2_6();
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_57 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_03c0;
		}
	}
	{
		bool L_58 = __this->get_pretty_3();
		if (!L_58)
		{
			goto IL_02c8;
		}
	}
	{
		V_4 = 0;
		goto IL_02bb;
	}

IL_02a4:
	{
		StringBuilder_t1221177846 * L_59 = __this->get_builder_2();
		NullCheck(L_59);
		StringBuilder_Append_m3636508479(L_59, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_60 = V_4;
		V_4 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_02bb:
	{
		int32_t L_61 = V_4;
		int32_t L_62 = __this->get_depth_0();
		if ((((int32_t)L_61) < ((int32_t)L_62)))
		{
			goto IL_02a4;
		}
	}

IL_02c8:
	{
		StringBuilder_t1221177846 * L_63 = __this->get_builder_2();
		String_t* L_64 = __this->get_U3CkeyU3E__1_5();
		NullCheck(L_63);
		StringBuilder_AppendFormat_m3265503696(L_63, _stringLiteral845265362, L_64, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_65 = __this->get_U3CobjU3E__2_6();
		int32_t L_66 = __this->get_depth_0();
		StringBuilder_t1221177846 * L_67 = __this->get_builder_2();
		bool L_68 = __this->get_pretty_3();
		NullCheck(L_65);
		Il2CppObject * L_69 = JSONObject_StringifyAsync_m1504868741(L_65, L_66, L_67, L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		Il2CppObject * L_70 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_69);
		__this->set_U24locvar1_7(L_70);
		V_0 = ((int32_t)-3);
	}

IL_030a:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_71 = V_0;
			if (((int32_t)((int32_t)L_71-(int32_t)2)) == 0)
			{
				goto IL_0353;
			}
		}

IL_0316:
		{
			goto IL_0353;
		}

IL_031b:
		{
			Il2CppObject * L_72 = __this->get_U24locvar1_7();
			NullCheck(L_72);
			Il2CppObject * L_73 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_72);
			__this->set_U3CeU3E__3_8(((Il2CppObject *)Castclass(L_73, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
			Il2CppObject * L_74 = __this->get_U3CeU3E__3_8();
			__this->set_U24current_15(L_74);
			bool L_75 = __this->get_U24disposing_16();
			if (L_75)
			{
				goto IL_034c;
			}
		}

IL_0345:
		{
			__this->set_U24PC_18(2);
		}

IL_034c:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x75C, FINALLY_0368);
		}

IL_0353:
		{
			Il2CppObject * L_76 = __this->get_U24locvar1_7();
			NullCheck(L_76);
			bool L_77 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_76);
			if (L_77)
			{
				goto IL_031b;
			}
		}

IL_0363:
		{
			IL2CPP_LEAVE(0x393, FINALLY_0368);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0368;
	}

FINALLY_0368:
	{ // begin finally (depth: 1)
		{
			bool L_78 = V_1;
			if (!L_78)
			{
				goto IL_036c;
			}
		}

IL_036b:
		{
			IL2CPP_END_FINALLY(872)
		}

IL_036c:
		{
			Il2CppObject * L_79 = __this->get_U24locvar1_7();
			Il2CppObject * L_80 = ((Il2CppObject *)IsInst(L_79, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_5 = L_80;
			__this->set_U24locvar2_9(L_80);
			Il2CppObject * L_81 = V_5;
			if (!L_81)
			{
				goto IL_0392;
			}
		}

IL_0387:
		{
			Il2CppObject * L_82 = __this->get_U24locvar2_9();
			NullCheck(L_82);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_82);
		}

IL_0392:
		{
			IL2CPP_END_FINALLY(872)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(872)
	{
		IL2CPP_JUMP_TBL(0x75C, IL_075c)
		IL2CPP_JUMP_TBL(0x393, IL_0393)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0393:
	{
		StringBuilder_t1221177846 * L_83 = __this->get_builder_2();
		NullCheck(L_83);
		StringBuilder_Append_m3636508479(L_83, _stringLiteral372029314, /*hidden argument*/NULL);
		bool L_84 = __this->get_pretty_3();
		if (!L_84)
		{
			goto IL_03c0;
		}
	}
	{
		StringBuilder_t1221177846 * L_85 = __this->get_builder_2();
		NullCheck(L_85);
		StringBuilder_Append_m3636508479(L_85, _stringLiteral2162321587, /*hidden argument*/NULL);
	}

IL_03c0:
	{
		int32_t L_86 = __this->get_U3CiU3E__0_4();
		__this->set_U3CiU3E__0_4(((int32_t)((int32_t)L_86+(int32_t)1)));
	}

IL_03ce:
	{
		int32_t L_87 = __this->get_U3CiU3E__0_4();
		JSONObject_t1971882247 * L_88 = __this->get_U24this_14();
		NullCheck(L_88);
		List_1_t1341003379 * L_89 = L_88->get_list_7();
		NullCheck(L_89);
		int32_t L_90 = List_1_get_Count_m3485821544(L_89, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_87) < ((int32_t)L_90)))
		{
			goto IL_0249;
		}
	}
	{
		bool L_91 = __this->get_pretty_3();
		if (!L_91)
		{
			goto IL_040c;
		}
	}
	{
		StringBuilder_t1221177846 * L_92 = __this->get_builder_2();
		StringBuilder_t1221177846 * L_93 = L_92;
		NullCheck(L_93);
		int32_t L_94 = StringBuilder_get_Length_m1608241323(L_93, /*hidden argument*/NULL);
		NullCheck(L_93);
		StringBuilder_set_Length_m3039225444(L_93, ((int32_t)((int32_t)L_94-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_041f;
	}

IL_040c:
	{
		StringBuilder_t1221177846 * L_95 = __this->get_builder_2();
		StringBuilder_t1221177846 * L_96 = L_95;
		NullCheck(L_96);
		int32_t L_97 = StringBuilder_get_Length_m1608241323(L_96, /*hidden argument*/NULL);
		NullCheck(L_96);
		StringBuilder_set_Length_m3039225444(L_96, ((int32_t)((int32_t)L_97-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_041f:
	{
		bool L_98 = __this->get_pretty_3();
		if (!L_98)
		{
			goto IL_047f;
		}
	}
	{
		JSONObject_t1971882247 * L_99 = __this->get_U24this_14();
		NullCheck(L_99);
		List_1_t1341003379 * L_100 = L_99->get_list_7();
		NullCheck(L_100);
		int32_t L_101 = List_1_get_Count_m3485821544(L_100, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_101) <= ((int32_t)0)))
		{
			goto IL_047f;
		}
	}
	{
		StringBuilder_t1221177846 * L_102 = __this->get_builder_2();
		NullCheck(L_102);
		StringBuilder_Append_m3636508479(L_102, _stringLiteral2162321587, /*hidden argument*/NULL);
		V_6 = 0;
		goto IL_0470;
	}

IL_0459:
	{
		StringBuilder_t1221177846 * L_103 = __this->get_builder_2();
		NullCheck(L_103);
		StringBuilder_Append_m3636508479(L_103, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_104 = V_6;
		V_6 = ((int32_t)((int32_t)L_104+(int32_t)1));
	}

IL_0470:
	{
		int32_t L_105 = V_6;
		int32_t L_106 = __this->get_depth_0();
		if ((((int32_t)L_105) < ((int32_t)((int32_t)((int32_t)L_106-(int32_t)1)))))
		{
			goto IL_0459;
		}
	}

IL_047f:
	{
		StringBuilder_t1221177846 * L_107 = __this->get_builder_2();
		NullCheck(L_107);
		StringBuilder_Append_m3636508479(L_107, _stringLiteral372029393, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_0495:
	{
		StringBuilder_t1221177846 * L_108 = __this->get_builder_2();
		NullCheck(L_108);
		StringBuilder_Append_m3636508479(L_108, _stringLiteral372029431, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_109 = __this->get_U24this_14();
		NullCheck(L_109);
		List_1_t1341003379 * L_110 = L_109->get_list_7();
		NullCheck(L_110);
		int32_t L_111 = List_1_get_Count_m3485821544(L_110, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_111) <= ((int32_t)0)))
		{
			goto IL_068b;
		}
	}
	{
		bool L_112 = __this->get_pretty_3();
		if (!L_112)
		{
			goto IL_04d8;
		}
	}
	{
		StringBuilder_t1221177846 * L_113 = __this->get_builder_2();
		NullCheck(L_113);
		StringBuilder_Append_m3636508479(L_113, _stringLiteral2162321587, /*hidden argument*/NULL);
	}

IL_04d8:
	{
		__this->set_U3CiU3E__4_10(0);
		goto IL_063a;
	}

IL_04e4:
	{
		JSONObject_t1971882247 * L_114 = __this->get_U24this_14();
		NullCheck(L_114);
		List_1_t1341003379 * L_115 = L_114->get_list_7();
		int32_t L_116 = __this->get_U3CiU3E__4_10();
		NullCheck(L_115);
		JSONObject_t1971882247 * L_117 = List_1_get_Item_m429614411(L_115, L_116, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_118 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		if (!L_118)
		{
			goto IL_062c;
		}
	}
	{
		bool L_119 = __this->get_pretty_3();
		if (!L_119)
		{
			goto IL_053b;
		}
	}
	{
		V_7 = 0;
		goto IL_052e;
	}

IL_0517:
	{
		StringBuilder_t1221177846 * L_120 = __this->get_builder_2();
		NullCheck(L_120);
		StringBuilder_Append_m3636508479(L_120, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_121 = V_7;
		V_7 = ((int32_t)((int32_t)L_121+(int32_t)1));
	}

IL_052e:
	{
		int32_t L_122 = V_7;
		int32_t L_123 = __this->get_depth_0();
		if ((((int32_t)L_122) < ((int32_t)L_123)))
		{
			goto IL_0517;
		}
	}

IL_053b:
	{
		JSONObject_t1971882247 * L_124 = __this->get_U24this_14();
		NullCheck(L_124);
		List_1_t1341003379 * L_125 = L_124->get_list_7();
		int32_t L_126 = __this->get_U3CiU3E__4_10();
		NullCheck(L_125);
		JSONObject_t1971882247 * L_127 = List_1_get_Item_m429614411(L_125, L_126, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		int32_t L_128 = __this->get_depth_0();
		StringBuilder_t1221177846 * L_129 = __this->get_builder_2();
		bool L_130 = __this->get_pretty_3();
		NullCheck(L_127);
		Il2CppObject * L_131 = JSONObject_StringifyAsync_m1504868741(L_127, L_128, L_129, L_130, /*hidden argument*/NULL);
		NullCheck(L_131);
		Il2CppObject * L_132 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_131);
		__this->set_U24locvar3_11(L_132);
		V_0 = ((int32_t)-3);
	}

IL_0576:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_133 = V_0;
			if (((int32_t)((int32_t)L_133-(int32_t)3)) == 0)
			{
				goto IL_05bf;
			}
		}

IL_0582:
		{
			goto IL_05bf;
		}

IL_0587:
		{
			Il2CppObject * L_134 = __this->get_U24locvar3_11();
			NullCheck(L_134);
			Il2CppObject * L_135 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_134);
			__this->set_U3CeU3E__5_12(((Il2CppObject *)Castclass(L_135, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
			Il2CppObject * L_136 = __this->get_U3CeU3E__5_12();
			__this->set_U24current_15(L_136);
			bool L_137 = __this->get_U24disposing_16();
			if (L_137)
			{
				goto IL_05b8;
			}
		}

IL_05b1:
		{
			__this->set_U24PC_18(3);
		}

IL_05b8:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x75C, FINALLY_05d4);
		}

IL_05bf:
		{
			Il2CppObject * L_138 = __this->get_U24locvar3_11();
			NullCheck(L_138);
			bool L_139 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_138);
			if (L_139)
			{
				goto IL_0587;
			}
		}

IL_05cf:
		{
			IL2CPP_LEAVE(0x5FF, FINALLY_05d4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_05d4;
	}

FINALLY_05d4:
	{ // begin finally (depth: 1)
		{
			bool L_140 = V_1;
			if (!L_140)
			{
				goto IL_05d8;
			}
		}

IL_05d7:
		{
			IL2CPP_END_FINALLY(1492)
		}

IL_05d8:
		{
			Il2CppObject * L_141 = __this->get_U24locvar3_11();
			Il2CppObject * L_142 = ((Il2CppObject *)IsInst(L_141, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_5 = L_142;
			__this->set_U24locvar4_13(L_142);
			Il2CppObject * L_143 = V_5;
			if (!L_143)
			{
				goto IL_05fe;
			}
		}

IL_05f3:
		{
			Il2CppObject * L_144 = __this->get_U24locvar4_13();
			NullCheck(L_144);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_144);
		}

IL_05fe:
		{
			IL2CPP_END_FINALLY(1492)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1492)
	{
		IL2CPP_JUMP_TBL(0x75C, IL_075c)
		IL2CPP_JUMP_TBL(0x5FF, IL_05ff)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_05ff:
	{
		StringBuilder_t1221177846 * L_145 = __this->get_builder_2();
		NullCheck(L_145);
		StringBuilder_Append_m3636508479(L_145, _stringLiteral372029314, /*hidden argument*/NULL);
		bool L_146 = __this->get_pretty_3();
		if (!L_146)
		{
			goto IL_062c;
		}
	}
	{
		StringBuilder_t1221177846 * L_147 = __this->get_builder_2();
		NullCheck(L_147);
		StringBuilder_Append_m3636508479(L_147, _stringLiteral2162321587, /*hidden argument*/NULL);
	}

IL_062c:
	{
		int32_t L_148 = __this->get_U3CiU3E__4_10();
		__this->set_U3CiU3E__4_10(((int32_t)((int32_t)L_148+(int32_t)1)));
	}

IL_063a:
	{
		int32_t L_149 = __this->get_U3CiU3E__4_10();
		JSONObject_t1971882247 * L_150 = __this->get_U24this_14();
		NullCheck(L_150);
		List_1_t1341003379 * L_151 = L_150->get_list_7();
		NullCheck(L_151);
		int32_t L_152 = List_1_get_Count_m3485821544(L_151, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_149) < ((int32_t)L_152)))
		{
			goto IL_04e4;
		}
	}
	{
		bool L_153 = __this->get_pretty_3();
		if (!L_153)
		{
			goto IL_0678;
		}
	}
	{
		StringBuilder_t1221177846 * L_154 = __this->get_builder_2();
		StringBuilder_t1221177846 * L_155 = L_154;
		NullCheck(L_155);
		int32_t L_156 = StringBuilder_get_Length_m1608241323(L_155, /*hidden argument*/NULL);
		NullCheck(L_155);
		StringBuilder_set_Length_m3039225444(L_155, ((int32_t)((int32_t)L_156-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_068b;
	}

IL_0678:
	{
		StringBuilder_t1221177846 * L_157 = __this->get_builder_2();
		StringBuilder_t1221177846 * L_158 = L_157;
		NullCheck(L_158);
		int32_t L_159 = StringBuilder_get_Length_m1608241323(L_158, /*hidden argument*/NULL);
		NullCheck(L_158);
		StringBuilder_set_Length_m3039225444(L_158, ((int32_t)((int32_t)L_159-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_068b:
	{
		bool L_160 = __this->get_pretty_3();
		if (!L_160)
		{
			goto IL_06eb;
		}
	}
	{
		JSONObject_t1971882247 * L_161 = __this->get_U24this_14();
		NullCheck(L_161);
		List_1_t1341003379 * L_162 = L_161->get_list_7();
		NullCheck(L_162);
		int32_t L_163 = List_1_get_Count_m3485821544(L_162, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_163) <= ((int32_t)0)))
		{
			goto IL_06eb;
		}
	}
	{
		StringBuilder_t1221177846 * L_164 = __this->get_builder_2();
		NullCheck(L_164);
		StringBuilder_Append_m3636508479(L_164, _stringLiteral2162321587, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_06dc;
	}

IL_06c5:
	{
		StringBuilder_t1221177846 * L_165 = __this->get_builder_2();
		NullCheck(L_165);
		StringBuilder_Append_m3636508479(L_165, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_166 = V_8;
		V_8 = ((int32_t)((int32_t)L_166+(int32_t)1));
	}

IL_06dc:
	{
		int32_t L_167 = V_8;
		int32_t L_168 = __this->get_depth_0();
		if ((((int32_t)L_167) < ((int32_t)((int32_t)((int32_t)L_168-(int32_t)1)))))
		{
			goto IL_06c5;
		}
	}

IL_06eb:
	{
		StringBuilder_t1221177846 * L_169 = __this->get_builder_2();
		NullCheck(L_169);
		StringBuilder_Append_m3636508479(L_169, _stringLiteral372029425, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_0701:
	{
		JSONObject_t1971882247 * L_170 = __this->get_U24this_14();
		NullCheck(L_170);
		bool L_171 = L_170->get_b_13();
		if (!L_171)
		{
			goto IL_0727;
		}
	}
	{
		StringBuilder_t1221177846 * L_172 = __this->get_builder_2();
		NullCheck(L_172);
		StringBuilder_Append_m3636508479(L_172, _stringLiteral3323263070, /*hidden argument*/NULL);
		goto IL_0738;
	}

IL_0727:
	{
		StringBuilder_t1221177846 * L_173 = __this->get_builder_2();
		NullCheck(L_173);
		StringBuilder_Append_m3636508479(L_173, _stringLiteral2609877245, /*hidden argument*/NULL);
	}

IL_0738:
	{
		goto IL_0753;
	}

IL_073d:
	{
		StringBuilder_t1221177846 * L_174 = __this->get_builder_2();
		NullCheck(L_174);
		StringBuilder_Append_m3636508479(L_174, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_0753:
	{
		__this->set_U24PC_18((-1));
	}

IL_075a:
	{
		return (bool)0;
	}

IL_075c:
	{
		return (bool)1;
	}
}
// System.Object JSONObject/<StringifyAsync>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1690338427 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_15();
		return L_0;
	}
}
// System.Object JSONObject/<StringifyAsync>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3671563155 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_15();
		return L_0;
	}
}
// System.Void JSONObject/<StringifyAsync>c__Iterator2::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator2_Dispose_m640875482_MetadataUsageId;
extern "C"  void U3CStringifyAsyncU3Ec__Iterator2_Dispose_m640875482 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator2_Dispose_m640875482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_18();
		V_0 = L_0;
		__this->set_U24disposing_16((bool)1);
		__this->set_U24PC_18((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0089;
		}
		if (L_1 == 1)
		{
			goto IL_0089;
		}
		if (L_1 == 2)
		{
			goto IL_0030;
		}
		if (L_1 == 3)
		{
			goto IL_005f;
		}
	}
	{
		goto IL_0089;
	}

IL_0030:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x5A, FINALLY_0035);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_U24locvar1_7();
			Il2CppObject * L_3 = ((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_1 = L_3;
			__this->set_U24locvar2_9(L_3);
			Il2CppObject * L_4 = V_1;
			if (!L_4)
			{
				goto IL_0059;
			}
		}

IL_004e:
		{
			Il2CppObject * L_5 = __this->get_U24locvar2_9();
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_5);
		}

IL_0059:
		{
			IL2CPP_END_FINALLY(53)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005a:
	{
		goto IL_0089;
	}

IL_005f:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x89, FINALLY_0064);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_6 = __this->get_U24locvar3_11();
			Il2CppObject * L_7 = ((Il2CppObject *)IsInst(L_6, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_1 = L_7;
			__this->set_U24locvar4_13(L_7);
			Il2CppObject * L_8 = V_1;
			if (!L_8)
			{
				goto IL_0088;
			}
		}

IL_007d:
		{
			Il2CppObject * L_9 = __this->get_U24locvar4_13();
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_9);
		}

IL_0088:
		{
			IL2CPP_END_FINALLY(100)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0089:
	{
		return;
	}
}
// System.Void JSONObject/<StringifyAsync>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator2_Reset_m3967791656_MetadataUsageId;
extern "C"  void U3CStringifyAsyncU3Ec__Iterator2_Reset_m3967791656 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator2_Reset_m3967791656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator2::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m4201252242 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3780376918(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<StringifyAsync>c__Iterator2::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern Il2CppClass* U3CStringifyAsyncU3Ec__Iterator2_t4037879552_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3780376918_MetadataUsageId;
extern "C"  Il2CppObject* U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3780376918 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3780376918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_18();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_2 = (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 *)il2cpp_codegen_object_new(U3CStringifyAsyncU3Ec__Iterator2_t4037879552_il2cpp_TypeInfo_var);
		U3CStringifyAsyncU3Ec__Iterator2__ctor_m3369090517(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_3 = V_0;
		JSONObject_t1971882247 * L_4 = __this->get_U24this_14();
		NullCheck(L_3);
		L_3->set_U24this_14(L_4);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_5 = V_0;
		int32_t L_6 = __this->get_U3CU24U3Edepth_17();
		NullCheck(L_5);
		L_5->set_depth_0(L_6);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_7 = V_0;
		StringBuilder_t1221177846 * L_8 = __this->get_builder_2();
		NullCheck(L_7);
		L_7->set_builder_2(L_8);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_9 = V_0;
		bool L_10 = __this->get_pretty_3();
		NullCheck(L_9);
		L_9->set_pretty_3(L_10);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_11 = V_0;
		return L_11;
	}
}
// System.Void JSONObject/AddJSONContents::.ctor(System.Object,System.IntPtr)
extern "C"  void AddJSONContents__ctor_m937288960 (AddJSONContents_t3850664647 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void JSONObject/AddJSONContents::Invoke(JSONObject)
extern "C"  void AddJSONContents_Invoke_m1742311611 (AddJSONContents_t3850664647 * __this, JSONObject_t1971882247 * ___self0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AddJSONContents_Invoke_m1742311611((AddJSONContents_t3850664647 *)__this->get_prev_9(),___self0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JSONObject_t1971882247 * ___self0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___self0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JSONObject_t1971882247 * ___self0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___self0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___self0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult JSONObject/AddJSONContents::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddJSONContents_BeginInvoke_m4227897068 (AddJSONContents_t3850664647 * __this, JSONObject_t1971882247 * ___self0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___self0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void JSONObject/AddJSONContents::EndInvoke(System.IAsyncResult)
extern "C"  void AddJSONContents_EndInvoke_m2419029234 (AddJSONContents_t3850664647 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void JSONObject/FieldNotFound::.ctor(System.Object,System.IntPtr)
extern "C"  void FieldNotFound__ctor_m3637701308 (FieldNotFound_t865402053 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void JSONObject/FieldNotFound::Invoke(System.String)
extern "C"  void FieldNotFound_Invoke_m1722936676 (FieldNotFound_t865402053 * __this, String_t* ___name0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FieldNotFound_Invoke_m1722936676((FieldNotFound_t865402053 *)__this->get_prev_9(),___name0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___name0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___name0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___name0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___name0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___name0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FieldNotFound_t865402053 (FieldNotFound_t865402053 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Native function invocation
	il2cppPInvokeFunc(____name0_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

}
// System.IAsyncResult JSONObject/FieldNotFound::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FieldNotFound_BeginInvoke_m622233301 (FieldNotFound_t865402053 * __this, String_t* ___name0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___name0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void JSONObject/FieldNotFound::EndInvoke(System.IAsyncResult)
extern "C"  void FieldNotFound_EndInvoke_m4171153578 (FieldNotFound_t865402053 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void JSONObject/GetFieldResponse::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFieldResponse__ctor_m1655757532 (GetFieldResponse_t1259369279 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void JSONObject/GetFieldResponse::Invoke(JSONObject)
extern "C"  void GetFieldResponse_Invoke_m4134964043 (GetFieldResponse_t1259369279 * __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetFieldResponse_Invoke_m4134964043((GetFieldResponse_t1259369279 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult JSONObject/GetFieldResponse::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFieldResponse_BeginInvoke_m804213744 (GetFieldResponse_t1259369279 * __this, JSONObject_t1971882247 * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void JSONObject/GetFieldResponse::EndInvoke(System.IAsyncResult)
extern "C"  void GetFieldResponse_EndInvoke_m4147177482 (GetFieldResponse_t1259369279 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// JSONObject JSONTemplates::TOJSON(System.Object)
extern const Il2CppType* JSONTemplates_t3006274921_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* JSONTemplates_t3006274921_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_Add_m225222789_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2967033390;
extern Il2CppCodeGenString* _stringLiteral2122037725;
extern Il2CppCodeGenString* _stringLiteral1264830171;
extern Il2CppCodeGenString* _stringLiteral2539188669;
extern Il2CppCodeGenString* _stringLiteral1638831994;
extern const uint32_t JSONTemplates_TOJSON_m2221174251_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_TOJSON_m2221174251 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_TOJSON_m2221174251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	FieldInfoU5BU5D_t125053523* V_1 = NULL;
	FieldInfo_t * V_2 = NULL;
	FieldInfoU5BU5D_t125053523* V_3 = NULL;
	int32_t V_4 = 0;
	JSONObject_t1971882247 * V_5 = NULL;
	MethodInfo_t * V_6 = NULL;
	ObjectU5BU5D_t3614634134* V_7 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_8 = NULL;
	PropertyInfo_t * V_9 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_10 = NULL;
	int32_t V_11 = 0;
	JSONObject_t1971882247 * V_12 = NULL;
	MethodInfo_t * V_13 = NULL;
	ObjectU5BU5D_t3614634134* V_14 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONTemplates_t3006274921_il2cpp_TypeInfo_var);
		HashSet_1_t1022910149 * L_0 = ((JSONTemplates_t3006274921_StaticFields*)JSONTemplates_t3006274921_il2cpp_TypeInfo_var->static_fields)->get_touched_0();
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_0);
		bool L_2 = HashSet_1_Add_m225222789(L_0, L_1, /*hidden argument*/HashSet_1_Add_m225222789_MethodInfo_var);
		if (!L_2)
		{
			goto IL_02d7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_3 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_4);
		Type_t * L_5 = Object_GetType_m191970594(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FieldInfoU5BU5D_t125053523* L_6 = Type_GetFields_m445058499(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		FieldInfoU5BU5D_t125053523* L_7 = V_1;
		V_3 = L_7;
		V_4 = 0;
		goto IL_0162;
	}

IL_002c:
	{
		FieldInfoU5BU5D_t125053523* L_8 = V_3;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		FieldInfo_t * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_2 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_12 = JSONObject_get_nullJO_m3951718059(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_12;
		FieldInfo_t * L_13 = V_2;
		Il2CppObject * L_14 = ___obj0;
		NullCheck(L_13);
		Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_13, L_14);
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_15, NULL);
		if (L_16)
		{
			goto IL_00e4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JSONTemplates_t3006274921_0_0_0_var), /*hidden argument*/NULL);
		FieldInfo_t * L_18 = V_2;
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_18);
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2967033390, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		MethodInfo_t * L_22 = Type_GetMethod_m1197504218(L_17, L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		MethodInfo_t * L_23 = V_6;
		if (!L_23)
		{
			goto IL_00a0;
		}
	}
	{
		V_7 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t3614634134* L_24 = V_7;
		FieldInfo_t * L_25 = V_2;
		Il2CppObject * L_26 = ___obj0;
		NullCheck(L_25);
		Il2CppObject * L_27 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_25, L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_27);
		MethodInfo_t * L_28 = V_6;
		ObjectU5BU5D_t3614634134* L_29 = V_7;
		NullCheck(L_28);
		Il2CppObject * L_30 = MethodBase_Invoke_m1075809207(L_28, NULL, L_29, /*hidden argument*/NULL);
		V_5 = ((JSONObject_t1971882247 *)CastclassClass(L_30, JSONObject_t1971882247_il2cpp_TypeInfo_var));
		goto IL_00e4;
	}

IL_00a0:
	{
		FieldInfo_t * L_31 = V_2;
		NullCheck(L_31);
		Type_t * L_32 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_31);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_32) == ((Il2CppObject*)(Type_t *)L_33))))
		{
			goto IL_00cd;
		}
	}
	{
		FieldInfo_t * L_34 = V_2;
		Il2CppObject * L_35 = ___obj0;
		NullCheck(L_34);
		Il2CppObject * L_36 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_34, L_35);
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_36);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_38 = JSONObject_CreateStringObject_m227421806(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		V_5 = L_38;
		goto IL_00e4;
	}

IL_00cd:
	{
		FieldInfo_t * L_39 = V_2;
		Il2CppObject * L_40 = ___obj0;
		NullCheck(L_39);
		Il2CppObject * L_41 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_39, L_40);
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_41);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_43 = JSONObject_Create_m1582109667(NULL /*static, unused*/, L_42, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		V_5 = L_43;
	}

IL_00e4:
	{
		JSONObject_t1971882247 * L_44 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_45 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_015c;
		}
	}
	{
		JSONObject_t1971882247 * L_46 = V_5;
		NullCheck(L_46);
		int32_t L_47 = L_46->get_type_6();
		if (!L_47)
		{
			goto IL_010f;
		}
	}
	{
		JSONObject_t1971882247 * L_48 = V_0;
		FieldInfo_t * L_49 = V_2;
		NullCheck(L_49);
		String_t* L_50 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_49);
		JSONObject_t1971882247 * L_51 = V_5;
		NullCheck(L_48);
		JSONObject_AddField_m1721386152(L_48, L_50, L_51, /*hidden argument*/NULL);
		goto IL_015c;
	}

IL_010f:
	{
		StringU5BU5D_t1642385972* L_52 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, _stringLiteral2122037725);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2122037725);
		StringU5BU5D_t1642385972* L_53 = L_52;
		FieldInfo_t * L_54 = V_2;
		NullCheck(L_54);
		String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_54);
		NullCheck(L_53);
		ArrayElementTypeCheck (L_53, L_55);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_55);
		StringU5BU5D_t1642385972* L_56 = L_53;
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, _stringLiteral1264830171);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1264830171);
		StringU5BU5D_t1642385972* L_57 = L_56;
		Il2CppObject * L_58 = ___obj0;
		NullCheck(L_58);
		Type_t * L_59 = Object_GetType_m191970594(L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_59);
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, L_60);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_60);
		StringU5BU5D_t1642385972* L_61 = L_57;
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, _stringLiteral2539188669);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2539188669);
		StringU5BU5D_t1642385972* L_62 = L_61;
		FieldInfo_t * L_63 = V_2;
		NullCheck(L_63);
		Type_t * L_64 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_63);
		NullCheck(L_64);
		String_t* L_65 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_64);
		NullCheck(L_62);
		ArrayElementTypeCheck (L_62, L_65);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = String_Concat_m626692867(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
	}

IL_015c:
	{
		int32_t L_67 = V_4;
		V_4 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0162:
	{
		int32_t L_68 = V_4;
		FieldInfoU5BU5D_t125053523* L_69 = V_3;
		NullCheck(L_69);
		if ((((int32_t)L_68) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_69)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_70 = ___obj0;
		NullCheck(L_70);
		Type_t * L_71 = Object_GetType_m191970594(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		PropertyInfoU5BU5D_t1736152084* L_72 = Type_GetProperties_m2803026104(L_71, /*hidden argument*/NULL);
		V_8 = L_72;
		PropertyInfoU5BU5D_t1736152084* L_73 = V_8;
		V_10 = L_73;
		V_11 = 0;
		goto IL_02ca;
	}

IL_0185:
	{
		PropertyInfoU5BU5D_t1736152084* L_74 = V_10;
		int32_t L_75 = V_11;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		PropertyInfo_t * L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		V_9 = L_77;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_78 = JSONObject_get_nullJO_m3951718059(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_12 = L_78;
		PropertyInfo_t * L_79 = V_9;
		Il2CppObject * L_80 = ___obj0;
		NullCheck(L_79);
		Il2CppObject * L_81 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_79, L_80, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		NullCheck(L_81);
		bool L_82 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_81, NULL);
		if (L_82)
		{
			goto IL_0249;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_83 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JSONTemplates_t3006274921_0_0_0_var), /*hidden argument*/NULL);
		PropertyInfo_t * L_84 = V_9;
		NullCheck(L_84);
		Type_t * L_85 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_84);
		NullCheck(L_85);
		String_t* L_86 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_85);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2967033390, L_86, /*hidden argument*/NULL);
		NullCheck(L_83);
		MethodInfo_t * L_88 = Type_GetMethod_m1197504218(L_83, L_87, /*hidden argument*/NULL);
		V_13 = L_88;
		MethodInfo_t * L_89 = V_13;
		if (!L_89)
		{
			goto IL_0200;
		}
	}
	{
		V_14 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t3614634134* L_90 = V_14;
		PropertyInfo_t * L_91 = V_9;
		Il2CppObject * L_92 = ___obj0;
		NullCheck(L_91);
		Il2CppObject * L_93 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_91, L_92, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		NullCheck(L_90);
		ArrayElementTypeCheck (L_90, L_93);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_93);
		MethodInfo_t * L_94 = V_13;
		ObjectU5BU5D_t3614634134* L_95 = V_14;
		NullCheck(L_94);
		Il2CppObject * L_96 = MethodBase_Invoke_m1075809207(L_94, NULL, L_95, /*hidden argument*/NULL);
		V_12 = ((JSONObject_t1971882247 *)CastclassClass(L_96, JSONObject_t1971882247_il2cpp_TypeInfo_var));
		goto IL_0249;
	}

IL_0200:
	{
		PropertyInfo_t * L_97 = V_9;
		NullCheck(L_97);
		Type_t * L_98 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_97);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_99 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_98) == ((Il2CppObject*)(Type_t *)L_99))))
		{
			goto IL_0230;
		}
	}
	{
		PropertyInfo_t * L_100 = V_9;
		Il2CppObject * L_101 = ___obj0;
		NullCheck(L_100);
		Il2CppObject * L_102 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_100, L_101, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		NullCheck(L_102);
		String_t* L_103 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_102);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_104 = JSONObject_CreateStringObject_m227421806(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		V_12 = L_104;
		goto IL_0249;
	}

IL_0230:
	{
		PropertyInfo_t * L_105 = V_9;
		Il2CppObject * L_106 = ___obj0;
		NullCheck(L_105);
		Il2CppObject * L_107 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_105, L_106, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		NullCheck(L_107);
		String_t* L_108 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_107);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_109 = JSONObject_Create_m1582109667(NULL /*static, unused*/, L_108, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		V_12 = L_109;
	}

IL_0249:
	{
		JSONObject_t1971882247 * L_110 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_111 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_110, /*hidden argument*/NULL);
		if (!L_111)
		{
			goto IL_02c4;
		}
	}
	{
		JSONObject_t1971882247 * L_112 = V_12;
		NullCheck(L_112);
		int32_t L_113 = L_112->get_type_6();
		if (!L_113)
		{
			goto IL_0275;
		}
	}
	{
		JSONObject_t1971882247 * L_114 = V_0;
		PropertyInfo_t * L_115 = V_9;
		NullCheck(L_115);
		String_t* L_116 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_115);
		JSONObject_t1971882247 * L_117 = V_12;
		NullCheck(L_114);
		JSONObject_AddField_m1721386152(L_114, L_116, L_117, /*hidden argument*/NULL);
		goto IL_02c4;
	}

IL_0275:
	{
		StringU5BU5D_t1642385972* L_118 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_118);
		ArrayElementTypeCheck (L_118, _stringLiteral2122037725);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2122037725);
		StringU5BU5D_t1642385972* L_119 = L_118;
		PropertyInfo_t * L_120 = V_9;
		NullCheck(L_120);
		String_t* L_121 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_120);
		NullCheck(L_119);
		ArrayElementTypeCheck (L_119, L_121);
		(L_119)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_121);
		StringU5BU5D_t1642385972* L_122 = L_119;
		NullCheck(L_122);
		ArrayElementTypeCheck (L_122, _stringLiteral1264830171);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1264830171);
		StringU5BU5D_t1642385972* L_123 = L_122;
		Il2CppObject * L_124 = ___obj0;
		NullCheck(L_124);
		Type_t * L_125 = Object_GetType_m191970594(L_124, /*hidden argument*/NULL);
		NullCheck(L_125);
		String_t* L_126 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_125);
		NullCheck(L_123);
		ArrayElementTypeCheck (L_123, L_126);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_126);
		StringU5BU5D_t1642385972* L_127 = L_123;
		NullCheck(L_127);
		ArrayElementTypeCheck (L_127, _stringLiteral2539188669);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2539188669);
		StringU5BU5D_t1642385972* L_128 = L_127;
		PropertyInfo_t * L_129 = V_9;
		NullCheck(L_129);
		Type_t * L_130 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_129);
		NullCheck(L_130);
		String_t* L_131 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_130);
		NullCheck(L_128);
		ArrayElementTypeCheck (L_128, L_131);
		(L_128)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_131);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_132 = String_Concat_m626692867(NULL /*static, unused*/, L_128, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
	}

IL_02c4:
	{
		int32_t L_133 = V_11;
		V_11 = ((int32_t)((int32_t)L_133+(int32_t)1));
	}

IL_02ca:
	{
		int32_t L_134 = V_11;
		PropertyInfoU5BU5D_t1736152084* L_135 = V_10;
		NullCheck(L_135);
		if ((((int32_t)L_134) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_135)->max_length)))))))
		{
			goto IL_0185;
		}
	}
	{
		JSONObject_t1971882247 * L_136 = V_0;
		return L_136;
	}

IL_02d7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1638831994, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_137 = JSONObject_get_nullJO_m3951718059(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_137;
	}
}
// UnityEngine.Vector2 JSONTemplates::ToVector2(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern const uint32_t JSONTemplates_ToVector2_m4255645740_MetadataUsageId;
extern "C"  Vector2_t2243707579  JSONTemplates_ToVector2_m4255645740 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToVector2_m4255645740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m2638456294(L_0, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m2638456294(L_3, _stringLiteral372029398, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m1158093019(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002f;
	}

IL_002a:
	{
		G_B3_0 = (0.0f);
	}

IL_002f:
	{
		V_0 = G_B3_0;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m2638456294(L_6, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m2638456294(L_9, _stringLiteral372029397, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m1158093019(L_10, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		goto IL_005f;
	}

IL_005a:
	{
		G_B6_0 = (0.0f);
	}

IL_005f:
	{
		V_1 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_1;
		Vector2_t2243707579  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m3067419446(&L_14, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// JSONObject JSONTemplates::FromVector2(UnityEngine.Vector2)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern const uint32_t JSONTemplates_FromVector2_m1084394455_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromVector2_m1084394455 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromVector2_m1084394455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___v0)->get_x_0();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___v0)->get_x_0();
		NullCheck(L_2);
		JSONObject_AddField_m2712088548(L_2, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___v0)->get_y_1();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___v0)->get_y_1();
		NullCheck(L_5);
		JSONObject_AddField_m2712088548(L_5, _stringLiteral372029397, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		JSONObject_t1971882247 * L_7 = V_0;
		return L_7;
	}
}
// JSONObject JSONTemplates::FromVector3(UnityEngine.Vector3)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern const uint32_t JSONTemplates_FromVector3_m3510317143_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromVector3_m3510317143 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromVector3_m3510317143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___v0)->get_x_1();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___v0)->get_x_1();
		NullCheck(L_2);
		JSONObject_AddField_m2712088548(L_2, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___v0)->get_y_2();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___v0)->get_y_2();
		NullCheck(L_5);
		JSONObject_AddField_m2712088548(L_5, _stringLiteral372029397, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___v0)->get_z_3();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___v0)->get_z_3();
		NullCheck(L_8);
		JSONObject_AddField_m2712088548(L_8, _stringLiteral372029400, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		JSONObject_t1971882247 * L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.Vector3 JSONTemplates::ToVector3(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern const uint32_t JSONTemplates_ToVector3_m4142347206_MetadataUsageId;
extern "C"  Vector3_t2243707580  JSONTemplates_ToVector3_m4142347206 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToVector3_m4142347206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m2638456294(L_0, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m2638456294(L_3, _stringLiteral372029398, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m1158093019(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002f;
	}

IL_002a:
	{
		G_B3_0 = (0.0f);
	}

IL_002f:
	{
		V_0 = G_B3_0;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m2638456294(L_6, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m2638456294(L_9, _stringLiteral372029397, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m1158093019(L_10, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		goto IL_005f;
	}

IL_005a:
	{
		G_B6_0 = (0.0f);
	}

IL_005f:
	{
		V_1 = G_B6_0;
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_get_Item_m2638456294(L_12, _stringLiteral372029400, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_14 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008a;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_16 = JSONObject_get_Item_m2638456294(L_15, _stringLiteral372029400, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = JSONObject_get_f_m1158093019(L_16, /*hidden argument*/NULL);
		G_B9_0 = L_17;
		goto IL_008f;
	}

IL_008a:
	{
		G_B9_0 = (0.0f);
	}

IL_008f:
	{
		V_2 = G_B9_0;
		float L_18 = V_0;
		float L_19 = V_1;
		float L_20 = V_2;
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322(&L_21, L_18, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// JSONObject JSONTemplates::FromVector4(UnityEngine.Vector4)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern const uint32_t JSONTemplates_FromVector4_m3216565463_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromVector4_m3216565463 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromVector4_m3216565463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___v0)->get_x_1();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___v0)->get_x_1();
		NullCheck(L_2);
		JSONObject_AddField_m2712088548(L_2, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___v0)->get_y_2();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___v0)->get_y_2();
		NullCheck(L_5);
		JSONObject_AddField_m2712088548(L_5, _stringLiteral372029397, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___v0)->get_z_3();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___v0)->get_z_3();
		NullCheck(L_8);
		JSONObject_AddField_m2712088548(L_8, _stringLiteral372029400, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = (&___v0)->get_w_4();
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = (&___v0)->get_w_4();
		NullCheck(L_11);
		JSONObject_AddField_m2712088548(L_11, _stringLiteral372029387, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONObject_t1971882247 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Vector4 JSONTemplates::ToVector4(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern const uint32_t JSONTemplates_ToVector4_m362392788_MetadataUsageId;
extern "C"  Vector4_t2243707581  JSONTemplates_ToVector4_m362392788 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToVector4_m362392788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B12_0 = 0.0f;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m2638456294(L_0, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m2638456294(L_3, _stringLiteral372029398, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m1158093019(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002f;
	}

IL_002a:
	{
		G_B3_0 = (0.0f);
	}

IL_002f:
	{
		V_0 = G_B3_0;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m2638456294(L_6, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m2638456294(L_9, _stringLiteral372029397, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m1158093019(L_10, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		goto IL_005f;
	}

IL_005a:
	{
		G_B6_0 = (0.0f);
	}

IL_005f:
	{
		V_1 = G_B6_0;
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_get_Item_m2638456294(L_12, _stringLiteral372029400, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_14 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008a;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_16 = JSONObject_get_Item_m2638456294(L_15, _stringLiteral372029400, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = JSONObject_get_f_m1158093019(L_16, /*hidden argument*/NULL);
		G_B9_0 = L_17;
		goto IL_008f;
	}

IL_008a:
	{
		G_B9_0 = (0.0f);
	}

IL_008f:
	{
		V_2 = G_B9_0;
		JSONObject_t1971882247 * L_18 = ___obj0;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m2638456294(L_18, _stringLiteral372029387, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_20 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ba;
		}
	}
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_22 = JSONObject_get_Item_m2638456294(L_21, _stringLiteral372029387, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = JSONObject_get_f_m1158093019(L_22, /*hidden argument*/NULL);
		G_B12_0 = L_23;
		goto IL_00bf;
	}

IL_00ba:
	{
		G_B12_0 = (0.0f);
	}

IL_00bf:
	{
		V_3 = G_B12_0;
		float L_24 = V_0;
		float L_25 = V_1;
		float L_26 = V_2;
		float L_27 = V_3;
		Vector4_t2243707581  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector4__ctor_m1222289168(&L_28, L_24, L_25, L_26, L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// JSONObject JSONTemplates::FromMatrix4x4(UnityEngine.Matrix4x4)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104528321;
extern Il2CppCodeGenString* _stringLiteral104528322;
extern Il2CppCodeGenString* _stringLiteral104528323;
extern Il2CppCodeGenString* _stringLiteral104528324;
extern Il2CppCodeGenString* _stringLiteral2833411676;
extern Il2CppCodeGenString* _stringLiteral2833411677;
extern Il2CppCodeGenString* _stringLiteral2833411678;
extern Il2CppCodeGenString* _stringLiteral2833411679;
extern Il2CppCodeGenString* _stringLiteral3236696203;
extern Il2CppCodeGenString* _stringLiteral3236696204;
extern Il2CppCodeGenString* _stringLiteral3236696205;
extern Il2CppCodeGenString* _stringLiteral3236696206;
extern Il2CppCodeGenString* _stringLiteral1670612262;
extern Il2CppCodeGenString* _stringLiteral1670612263;
extern Il2CppCodeGenString* _stringLiteral1670612264;
extern Il2CppCodeGenString* _stringLiteral1670612265;
extern const uint32_t JSONTemplates_FromMatrix4x4_m3949774999_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromMatrix4x4_m3949774999 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromMatrix4x4_m3949774999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___m0)->get_m00_0();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___m0)->get_m00_0();
		NullCheck(L_2);
		JSONObject_AddField_m2712088548(L_2, _stringLiteral104528321, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___m0)->get_m01_4();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___m0)->get_m01_4();
		NullCheck(L_5);
		JSONObject_AddField_m2712088548(L_5, _stringLiteral104528322, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___m0)->get_m02_8();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___m0)->get_m02_8();
		NullCheck(L_8);
		JSONObject_AddField_m2712088548(L_8, _stringLiteral104528323, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = (&___m0)->get_m03_12();
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = (&___m0)->get_m03_12();
		NullCheck(L_11);
		JSONObject_AddField_m2712088548(L_11, _stringLiteral104528324, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		float L_13 = (&___m0)->get_m10_1();
		if ((((float)L_13) == ((float)(0.0f))))
		{
			goto IL_00b5;
		}
	}
	{
		JSONObject_t1971882247 * L_14 = V_0;
		float L_15 = (&___m0)->get_m10_1();
		NullCheck(L_14);
		JSONObject_AddField_m2712088548(L_14, _stringLiteral2833411676, L_15, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		float L_16 = (&___m0)->get_m11_5();
		if ((((float)L_16) == ((float)(0.0f))))
		{
			goto IL_00d8;
		}
	}
	{
		JSONObject_t1971882247 * L_17 = V_0;
		float L_18 = (&___m0)->get_m11_5();
		NullCheck(L_17);
		JSONObject_AddField_m2712088548(L_17, _stringLiteral2833411677, L_18, /*hidden argument*/NULL);
	}

IL_00d8:
	{
		float L_19 = (&___m0)->get_m12_9();
		if ((((float)L_19) == ((float)(0.0f))))
		{
			goto IL_00fb;
		}
	}
	{
		JSONObject_t1971882247 * L_20 = V_0;
		float L_21 = (&___m0)->get_m12_9();
		NullCheck(L_20);
		JSONObject_AddField_m2712088548(L_20, _stringLiteral2833411678, L_21, /*hidden argument*/NULL);
	}

IL_00fb:
	{
		float L_22 = (&___m0)->get_m13_13();
		if ((((float)L_22) == ((float)(0.0f))))
		{
			goto IL_011e;
		}
	}
	{
		JSONObject_t1971882247 * L_23 = V_0;
		float L_24 = (&___m0)->get_m13_13();
		NullCheck(L_23);
		JSONObject_AddField_m2712088548(L_23, _stringLiteral2833411679, L_24, /*hidden argument*/NULL);
	}

IL_011e:
	{
		float L_25 = (&___m0)->get_m20_2();
		if ((((float)L_25) == ((float)(0.0f))))
		{
			goto IL_0141;
		}
	}
	{
		JSONObject_t1971882247 * L_26 = V_0;
		float L_27 = (&___m0)->get_m20_2();
		NullCheck(L_26);
		JSONObject_AddField_m2712088548(L_26, _stringLiteral3236696203, L_27, /*hidden argument*/NULL);
	}

IL_0141:
	{
		float L_28 = (&___m0)->get_m21_6();
		if ((((float)L_28) == ((float)(0.0f))))
		{
			goto IL_0164;
		}
	}
	{
		JSONObject_t1971882247 * L_29 = V_0;
		float L_30 = (&___m0)->get_m21_6();
		NullCheck(L_29);
		JSONObject_AddField_m2712088548(L_29, _stringLiteral3236696204, L_30, /*hidden argument*/NULL);
	}

IL_0164:
	{
		float L_31 = (&___m0)->get_m22_10();
		if ((((float)L_31) == ((float)(0.0f))))
		{
			goto IL_0187;
		}
	}
	{
		JSONObject_t1971882247 * L_32 = V_0;
		float L_33 = (&___m0)->get_m22_10();
		NullCheck(L_32);
		JSONObject_AddField_m2712088548(L_32, _stringLiteral3236696205, L_33, /*hidden argument*/NULL);
	}

IL_0187:
	{
		float L_34 = (&___m0)->get_m23_14();
		if ((((float)L_34) == ((float)(0.0f))))
		{
			goto IL_01aa;
		}
	}
	{
		JSONObject_t1971882247 * L_35 = V_0;
		float L_36 = (&___m0)->get_m23_14();
		NullCheck(L_35);
		JSONObject_AddField_m2712088548(L_35, _stringLiteral3236696206, L_36, /*hidden argument*/NULL);
	}

IL_01aa:
	{
		float L_37 = (&___m0)->get_m30_3();
		if ((((float)L_37) == ((float)(0.0f))))
		{
			goto IL_01cd;
		}
	}
	{
		JSONObject_t1971882247 * L_38 = V_0;
		float L_39 = (&___m0)->get_m30_3();
		NullCheck(L_38);
		JSONObject_AddField_m2712088548(L_38, _stringLiteral1670612262, L_39, /*hidden argument*/NULL);
	}

IL_01cd:
	{
		float L_40 = (&___m0)->get_m31_7();
		if ((((float)L_40) == ((float)(0.0f))))
		{
			goto IL_01f0;
		}
	}
	{
		JSONObject_t1971882247 * L_41 = V_0;
		float L_42 = (&___m0)->get_m31_7();
		NullCheck(L_41);
		JSONObject_AddField_m2712088548(L_41, _stringLiteral1670612263, L_42, /*hidden argument*/NULL);
	}

IL_01f0:
	{
		float L_43 = (&___m0)->get_m32_11();
		if ((((float)L_43) == ((float)(0.0f))))
		{
			goto IL_0213;
		}
	}
	{
		JSONObject_t1971882247 * L_44 = V_0;
		float L_45 = (&___m0)->get_m32_11();
		NullCheck(L_44);
		JSONObject_AddField_m2712088548(L_44, _stringLiteral1670612264, L_45, /*hidden argument*/NULL);
	}

IL_0213:
	{
		float L_46 = (&___m0)->get_m33_15();
		if ((((float)L_46) == ((float)(0.0f))))
		{
			goto IL_0236;
		}
	}
	{
		JSONObject_t1971882247 * L_47 = V_0;
		float L_48 = (&___m0)->get_m33_15();
		NullCheck(L_47);
		JSONObject_AddField_m2712088548(L_47, _stringLiteral1670612265, L_48, /*hidden argument*/NULL);
	}

IL_0236:
	{
		JSONObject_t1971882247 * L_49 = V_0;
		return L_49;
	}
}
// UnityEngine.Matrix4x4 JSONTemplates::ToMatrix4x4(JSONObject)
extern Il2CppClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104528321;
extern Il2CppCodeGenString* _stringLiteral104528322;
extern Il2CppCodeGenString* _stringLiteral104528323;
extern Il2CppCodeGenString* _stringLiteral104528324;
extern Il2CppCodeGenString* _stringLiteral2833411676;
extern Il2CppCodeGenString* _stringLiteral2833411677;
extern Il2CppCodeGenString* _stringLiteral2833411678;
extern Il2CppCodeGenString* _stringLiteral2833411679;
extern Il2CppCodeGenString* _stringLiteral3236696203;
extern Il2CppCodeGenString* _stringLiteral3236696204;
extern Il2CppCodeGenString* _stringLiteral3236696205;
extern Il2CppCodeGenString* _stringLiteral3236696206;
extern Il2CppCodeGenString* _stringLiteral1670612262;
extern Il2CppCodeGenString* _stringLiteral1670612263;
extern Il2CppCodeGenString* _stringLiteral1670612264;
extern Il2CppCodeGenString* _stringLiteral1670612265;
extern const uint32_t JSONTemplates_ToMatrix4x4_m3802232620_MetadataUsageId;
extern "C"  Matrix4x4_t2933234003  JSONTemplates_ToMatrix4x4_m3802232620 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToMatrix4x4_m3802232620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_0));
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m2638456294(L_0, _stringLiteral104528321, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m2638456294(L_3, _stringLiteral104528321, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m1158093019(L_4, /*hidden argument*/NULL);
		(&V_0)->set_m00_0(L_5);
	}

IL_0034:
	{
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m2638456294(L_6, _stringLiteral104528322, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0060;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m2638456294(L_9, _stringLiteral104528322, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m1158093019(L_10, /*hidden argument*/NULL);
		(&V_0)->set_m01_4(L_11);
	}

IL_0060:
	{
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_get_Item_m2638456294(L_12, _stringLiteral104528323, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_14 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008c;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_16 = JSONObject_get_Item_m2638456294(L_15, _stringLiteral104528323, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = JSONObject_get_f_m1158093019(L_16, /*hidden argument*/NULL);
		(&V_0)->set_m02_8(L_17);
	}

IL_008c:
	{
		JSONObject_t1971882247 * L_18 = ___obj0;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m2638456294(L_18, _stringLiteral104528324, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_20 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_22 = JSONObject_get_Item_m2638456294(L_21, _stringLiteral104528324, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = JSONObject_get_f_m1158093019(L_22, /*hidden argument*/NULL);
		(&V_0)->set_m03_12(L_23);
	}

IL_00b8:
	{
		JSONObject_t1971882247 * L_24 = ___obj0;
		NullCheck(L_24);
		JSONObject_t1971882247 * L_25 = JSONObject_get_Item_m2638456294(L_24, _stringLiteral2833411676, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_26 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00e4;
		}
	}
	{
		JSONObject_t1971882247 * L_27 = ___obj0;
		NullCheck(L_27);
		JSONObject_t1971882247 * L_28 = JSONObject_get_Item_m2638456294(L_27, _stringLiteral2833411676, /*hidden argument*/NULL);
		NullCheck(L_28);
		float L_29 = JSONObject_get_f_m1158093019(L_28, /*hidden argument*/NULL);
		(&V_0)->set_m10_1(L_29);
	}

IL_00e4:
	{
		JSONObject_t1971882247 * L_30 = ___obj0;
		NullCheck(L_30);
		JSONObject_t1971882247 * L_31 = JSONObject_get_Item_m2638456294(L_30, _stringLiteral2833411677, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_32 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0110;
		}
	}
	{
		JSONObject_t1971882247 * L_33 = ___obj0;
		NullCheck(L_33);
		JSONObject_t1971882247 * L_34 = JSONObject_get_Item_m2638456294(L_33, _stringLiteral2833411677, /*hidden argument*/NULL);
		NullCheck(L_34);
		float L_35 = JSONObject_get_f_m1158093019(L_34, /*hidden argument*/NULL);
		(&V_0)->set_m11_5(L_35);
	}

IL_0110:
	{
		JSONObject_t1971882247 * L_36 = ___obj0;
		NullCheck(L_36);
		JSONObject_t1971882247 * L_37 = JSONObject_get_Item_m2638456294(L_36, _stringLiteral2833411678, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_38 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_013c;
		}
	}
	{
		JSONObject_t1971882247 * L_39 = ___obj0;
		NullCheck(L_39);
		JSONObject_t1971882247 * L_40 = JSONObject_get_Item_m2638456294(L_39, _stringLiteral2833411678, /*hidden argument*/NULL);
		NullCheck(L_40);
		float L_41 = JSONObject_get_f_m1158093019(L_40, /*hidden argument*/NULL);
		(&V_0)->set_m12_9(L_41);
	}

IL_013c:
	{
		JSONObject_t1971882247 * L_42 = ___obj0;
		NullCheck(L_42);
		JSONObject_t1971882247 * L_43 = JSONObject_get_Item_m2638456294(L_42, _stringLiteral2833411679, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_44 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0168;
		}
	}
	{
		JSONObject_t1971882247 * L_45 = ___obj0;
		NullCheck(L_45);
		JSONObject_t1971882247 * L_46 = JSONObject_get_Item_m2638456294(L_45, _stringLiteral2833411679, /*hidden argument*/NULL);
		NullCheck(L_46);
		float L_47 = JSONObject_get_f_m1158093019(L_46, /*hidden argument*/NULL);
		(&V_0)->set_m13_13(L_47);
	}

IL_0168:
	{
		JSONObject_t1971882247 * L_48 = ___obj0;
		NullCheck(L_48);
		JSONObject_t1971882247 * L_49 = JSONObject_get_Item_m2638456294(L_48, _stringLiteral3236696203, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_50 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0194;
		}
	}
	{
		JSONObject_t1971882247 * L_51 = ___obj0;
		NullCheck(L_51);
		JSONObject_t1971882247 * L_52 = JSONObject_get_Item_m2638456294(L_51, _stringLiteral3236696203, /*hidden argument*/NULL);
		NullCheck(L_52);
		float L_53 = JSONObject_get_f_m1158093019(L_52, /*hidden argument*/NULL);
		(&V_0)->set_m20_2(L_53);
	}

IL_0194:
	{
		JSONObject_t1971882247 * L_54 = ___obj0;
		NullCheck(L_54);
		JSONObject_t1971882247 * L_55 = JSONObject_get_Item_m2638456294(L_54, _stringLiteral3236696204, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_56 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01c0;
		}
	}
	{
		JSONObject_t1971882247 * L_57 = ___obj0;
		NullCheck(L_57);
		JSONObject_t1971882247 * L_58 = JSONObject_get_Item_m2638456294(L_57, _stringLiteral3236696204, /*hidden argument*/NULL);
		NullCheck(L_58);
		float L_59 = JSONObject_get_f_m1158093019(L_58, /*hidden argument*/NULL);
		(&V_0)->set_m21_6(L_59);
	}

IL_01c0:
	{
		JSONObject_t1971882247 * L_60 = ___obj0;
		NullCheck(L_60);
		JSONObject_t1971882247 * L_61 = JSONObject_get_Item_m2638456294(L_60, _stringLiteral3236696205, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_62 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_01ec;
		}
	}
	{
		JSONObject_t1971882247 * L_63 = ___obj0;
		NullCheck(L_63);
		JSONObject_t1971882247 * L_64 = JSONObject_get_Item_m2638456294(L_63, _stringLiteral3236696205, /*hidden argument*/NULL);
		NullCheck(L_64);
		float L_65 = JSONObject_get_f_m1158093019(L_64, /*hidden argument*/NULL);
		(&V_0)->set_m22_10(L_65);
	}

IL_01ec:
	{
		JSONObject_t1971882247 * L_66 = ___obj0;
		NullCheck(L_66);
		JSONObject_t1971882247 * L_67 = JSONObject_get_Item_m2638456294(L_66, _stringLiteral3236696206, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_68 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_0218;
		}
	}
	{
		JSONObject_t1971882247 * L_69 = ___obj0;
		NullCheck(L_69);
		JSONObject_t1971882247 * L_70 = JSONObject_get_Item_m2638456294(L_69, _stringLiteral3236696206, /*hidden argument*/NULL);
		NullCheck(L_70);
		float L_71 = JSONObject_get_f_m1158093019(L_70, /*hidden argument*/NULL);
		(&V_0)->set_m23_14(L_71);
	}

IL_0218:
	{
		JSONObject_t1971882247 * L_72 = ___obj0;
		NullCheck(L_72);
		JSONObject_t1971882247 * L_73 = JSONObject_get_Item_m2638456294(L_72, _stringLiteral1670612262, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_74 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0244;
		}
	}
	{
		JSONObject_t1971882247 * L_75 = ___obj0;
		NullCheck(L_75);
		JSONObject_t1971882247 * L_76 = JSONObject_get_Item_m2638456294(L_75, _stringLiteral1670612262, /*hidden argument*/NULL);
		NullCheck(L_76);
		float L_77 = JSONObject_get_f_m1158093019(L_76, /*hidden argument*/NULL);
		(&V_0)->set_m30_3(L_77);
	}

IL_0244:
	{
		JSONObject_t1971882247 * L_78 = ___obj0;
		NullCheck(L_78);
		JSONObject_t1971882247 * L_79 = JSONObject_get_Item_m2638456294(L_78, _stringLiteral1670612263, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_80 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_0270;
		}
	}
	{
		JSONObject_t1971882247 * L_81 = ___obj0;
		NullCheck(L_81);
		JSONObject_t1971882247 * L_82 = JSONObject_get_Item_m2638456294(L_81, _stringLiteral1670612263, /*hidden argument*/NULL);
		NullCheck(L_82);
		float L_83 = JSONObject_get_f_m1158093019(L_82, /*hidden argument*/NULL);
		(&V_0)->set_m31_7(L_83);
	}

IL_0270:
	{
		JSONObject_t1971882247 * L_84 = ___obj0;
		NullCheck(L_84);
		JSONObject_t1971882247 * L_85 = JSONObject_get_Item_m2638456294(L_84, _stringLiteral1670612264, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_86 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_029c;
		}
	}
	{
		JSONObject_t1971882247 * L_87 = ___obj0;
		NullCheck(L_87);
		JSONObject_t1971882247 * L_88 = JSONObject_get_Item_m2638456294(L_87, _stringLiteral1670612264, /*hidden argument*/NULL);
		NullCheck(L_88);
		float L_89 = JSONObject_get_f_m1158093019(L_88, /*hidden argument*/NULL);
		(&V_0)->set_m32_11(L_89);
	}

IL_029c:
	{
		JSONObject_t1971882247 * L_90 = ___obj0;
		NullCheck(L_90);
		JSONObject_t1971882247 * L_91 = JSONObject_get_Item_m2638456294(L_90, _stringLiteral1670612265, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_92 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_02c8;
		}
	}
	{
		JSONObject_t1971882247 * L_93 = ___obj0;
		NullCheck(L_93);
		JSONObject_t1971882247 * L_94 = JSONObject_get_Item_m2638456294(L_93, _stringLiteral1670612265, /*hidden argument*/NULL);
		NullCheck(L_94);
		float L_95 = JSONObject_get_f_m1158093019(L_94, /*hidden argument*/NULL);
		(&V_0)->set_m33_15(L_95);
	}

IL_02c8:
	{
		Matrix4x4_t2933234003  L_96 = V_0;
		return L_96;
	}
}
// JSONObject JSONTemplates::FromQuaternion(UnityEngine.Quaternion)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern const uint32_t JSONTemplates_FromQuaternion_m2649516947_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromQuaternion_m2649516947 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromQuaternion_m2649516947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___q0)->get_w_3();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___q0)->get_w_3();
		NullCheck(L_2);
		JSONObject_AddField_m2712088548(L_2, _stringLiteral372029387, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___q0)->get_x_0();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___q0)->get_x_0();
		NullCheck(L_5);
		JSONObject_AddField_m2712088548(L_5, _stringLiteral372029398, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___q0)->get_y_1();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___q0)->get_y_1();
		NullCheck(L_8);
		JSONObject_AddField_m2712088548(L_8, _stringLiteral372029397, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = (&___q0)->get_z_2();
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = (&___q0)->get_z_2();
		NullCheck(L_11);
		JSONObject_AddField_m2712088548(L_11, _stringLiteral372029400, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONObject_t1971882247 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Quaternion JSONTemplates::ToQuaternion(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern const uint32_t JSONTemplates_ToQuaternion_m3034868414_MetadataUsageId;
extern "C"  Quaternion_t4030073918  JSONTemplates_ToQuaternion_m3034868414 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToQuaternion_m3034868414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B12_0 = 0.0f;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m2638456294(L_0, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m2638456294(L_3, _stringLiteral372029398, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m1158093019(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002f;
	}

IL_002a:
	{
		G_B3_0 = (0.0f);
	}

IL_002f:
	{
		V_0 = G_B3_0;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m2638456294(L_6, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m2638456294(L_9, _stringLiteral372029397, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m1158093019(L_10, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		goto IL_005f;
	}

IL_005a:
	{
		G_B6_0 = (0.0f);
	}

IL_005f:
	{
		V_1 = G_B6_0;
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_get_Item_m2638456294(L_12, _stringLiteral372029400, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_14 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008a;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_16 = JSONObject_get_Item_m2638456294(L_15, _stringLiteral372029400, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = JSONObject_get_f_m1158093019(L_16, /*hidden argument*/NULL);
		G_B9_0 = L_17;
		goto IL_008f;
	}

IL_008a:
	{
		G_B9_0 = (0.0f);
	}

IL_008f:
	{
		V_2 = G_B9_0;
		JSONObject_t1971882247 * L_18 = ___obj0;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m2638456294(L_18, _stringLiteral372029387, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_20 = JSONObject_op_Implicit_m2214727230(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ba;
		}
	}
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_22 = JSONObject_get_Item_m2638456294(L_21, _stringLiteral372029387, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = JSONObject_get_f_m1158093019(L_22, /*hidden argument*/NULL);
		G_B12_0 = L_23;
		goto IL_00bf;
	}

IL_00ba:
	{
		G_B12_0 = (0.0f);
	}

IL_00bf:
	{
		V_3 = G_B12_0;
		float L_24 = V_0;
		float L_25 = V_1;
		float L_26 = V_2;
		float L_27 = V_3;
		Quaternion_t4030073918  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Quaternion__ctor_m3196903881(&L_28, L_24, L_25, L_26, L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// JSONObject JSONTemplates::FromColor(UnityEngine.Color)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern Il2CppCodeGenString* _stringLiteral372029376;
extern Il2CppCodeGenString* _stringLiteral372029373;
extern const uint32_t JSONTemplates_FromColor_m773919959_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromColor_m773919959 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromColor_m773919959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___c0)->get_r_0();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___c0)->get_r_0();
		NullCheck(L_2);
		JSONObject_AddField_m2712088548(L_2, _stringLiteral372029392, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___c0)->get_g_1();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___c0)->get_g_1();
		NullCheck(L_5);
		JSONObject_AddField_m2712088548(L_5, _stringLiteral372029371, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___c0)->get_b_2();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___c0)->get_b_2();
		NullCheck(L_8);
		JSONObject_AddField_m2712088548(L_8, _stringLiteral372029376, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = (&___c0)->get_a_3();
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = (&___c0)->get_a_3();
		NullCheck(L_11);
		JSONObject_AddField_m2712088548(L_11, _stringLiteral372029373, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONObject_t1971882247 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Color JSONTemplates::ToColor(JSONObject)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern Il2CppCodeGenString* _stringLiteral372029376;
extern Il2CppCodeGenString* _stringLiteral372029373;
extern const uint32_t JSONTemplates_ToColor_m1289362228_MetadataUsageId;
extern "C"  Color_t2020392075  JSONTemplates_ToColor_m1289362228 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToColor_m1289362228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		Initobj (Color_t2020392075_il2cpp_TypeInfo_var, (&V_0));
		V_1 = 0;
		goto IL_00cb;
	}

IL_000f:
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		List_1_t1398341365 * L_1 = L_0->get_keys_8();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		String_t* L_3 = List_1_get_Item_m566484697(L_1, L_2, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		V_2 = L_3;
		String_t* L_4 = V_2;
		if (!L_4)
		{
			goto IL_00c7;
		}
	}
	{
		String_t* L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral372029392, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, _stringLiteral372029371, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral372029376, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0097;
		}
	}
	{
		String_t* L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral372029373, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00af;
		}
	}
	{
		goto IL_00c7;
	}

IL_0067:
	{
		JSONObject_t1971882247 * L_13 = ___obj0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		JSONObject_t1971882247 * L_15 = JSONObject_get_Item_m481309353(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		float L_16 = JSONObject_get_f_m1158093019(L_15, /*hidden argument*/NULL);
		(&V_0)->set_r_0(L_16);
		goto IL_00c7;
	}

IL_007f:
	{
		JSONObject_t1971882247 * L_17 = ___obj0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m481309353(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		float L_20 = JSONObject_get_f_m1158093019(L_19, /*hidden argument*/NULL);
		(&V_0)->set_g_1(L_20);
		goto IL_00c7;
	}

IL_0097:
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_23 = JSONObject_get_Item_m481309353(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		float L_24 = JSONObject_get_f_m1158093019(L_23, /*hidden argument*/NULL);
		(&V_0)->set_b_2(L_24);
		goto IL_00c7;
	}

IL_00af:
	{
		JSONObject_t1971882247 * L_25 = ___obj0;
		int32_t L_26 = V_1;
		NullCheck(L_25);
		JSONObject_t1971882247 * L_27 = JSONObject_get_Item_m481309353(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		float L_28 = JSONObject_get_f_m1158093019(L_27, /*hidden argument*/NULL);
		(&V_0)->set_a_3(L_28);
		goto IL_00c7;
	}

IL_00c7:
	{
		int32_t L_29 = V_1;
		V_1 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00cb:
	{
		int32_t L_30 = V_1;
		JSONObject_t1971882247 * L_31 = ___obj0;
		NullCheck(L_31);
		int32_t L_32 = JSONObject_get_Count_m2165978410(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_000f;
		}
	}
	{
		Color_t2020392075  L_33 = V_0;
		return L_33;
	}
}
// JSONObject JSONTemplates::FromLayerMask(UnityEngine.LayerMask)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t JSONTemplates_FromLayerMask_m1188911255_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromLayerMask_m1188911255 (Il2CppObject * __this /* static, unused */, LayerMask_t3188175821  ___l0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromLayerMask_m1188911255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		int32_t L_2 = LayerMask_get_value_m251765876((&___l0), /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_AddField_m894980766(L_1, _stringLiteral1803325615, L_2, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.LayerMask JSONTemplates::ToLayerMask(JSONObject)
extern Il2CppClass* LayerMask_t3188175821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t JSONTemplates_ToLayerMask_m1877598044_MetadataUsageId;
extern "C"  LayerMask_t3188175821  JSONTemplates_ToLayerMask_m1877598044 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToLayerMask_m1877598044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LayerMask_t3188175821  V_0;
	memset(&V_0, 0, sizeof(V_0));
	LayerMask_t3188175821  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (LayerMask_t3188175821_il2cpp_TypeInfo_var, (&V_1));
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m2638456294(L_0, _stringLiteral1803325615, /*hidden argument*/NULL);
		NullCheck(L_1);
		float L_2 = L_1->get_n_10();
		LayerMask_set_value_m3659587097((&V_1), (((int32_t)((int32_t)L_2))), /*hidden argument*/NULL);
		LayerMask_t3188175821  L_3 = V_1;
		V_0 = L_3;
		LayerMask_t3188175821  L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONTemplates::FromRect(UnityEngine.Rect)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral1113197259;
extern Il2CppCodeGenString* _stringLiteral1366542454;
extern const uint32_t JSONTemplates_FromRect_m2670430419_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromRect_m2670430419 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromRect_m2670430419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Rect_get_x_m1393582490((&___r0), /*hidden argument*/NULL);
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = Rect_get_x_m1393582490((&___r0), /*hidden argument*/NULL);
		NullCheck(L_2);
		JSONObject_AddField_m2712088548(L_2, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = Rect_get_y_m1393582395((&___r0), /*hidden argument*/NULL);
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = Rect_get_y_m1393582395((&___r0), /*hidden argument*/NULL);
		NullCheck(L_5);
		JSONObject_AddField_m2712088548(L_5, _stringLiteral372029397, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = Rect_get_height_m3128694305((&___r0), /*hidden argument*/NULL);
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = Rect_get_height_m3128694305((&___r0), /*hidden argument*/NULL);
		NullCheck(L_8);
		JSONObject_AddField_m2712088548(L_8, _stringLiteral1113197259, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = Rect_get_width_m1138015702((&___r0), /*hidden argument*/NULL);
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = Rect_get_width_m1138015702((&___r0), /*hidden argument*/NULL);
		NullCheck(L_11);
		JSONObject_AddField_m2712088548(L_11, _stringLiteral1366542454, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONObject_t1971882247 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Rect JSONTemplates::ToRect(JSONObject)
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral1113197259;
extern Il2CppCodeGenString* _stringLiteral1366542454;
extern const uint32_t JSONTemplates_ToRect_m4037111550_MetadataUsageId;
extern "C"  Rect_t3681755626  JSONTemplates_ToRect_m4037111550 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToRect_m4037111550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		Initobj (Rect_t3681755626_il2cpp_TypeInfo_var, (&V_0));
		V_1 = 0;
		goto IL_00cb;
	}

IL_000f:
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		List_1_t1398341365 * L_1 = L_0->get_keys_8();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		String_t* L_3 = List_1_get_Item_m566484697(L_1, L_2, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		V_2 = L_3;
		String_t* L_4 = V_2;
		if (!L_4)
		{
			goto IL_00c7;
		}
	}
	{
		String_t* L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral372029398, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, _stringLiteral372029397, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral1113197259, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0097;
		}
	}
	{
		String_t* L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral1366542454, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00af;
		}
	}
	{
		goto IL_00c7;
	}

IL_0067:
	{
		JSONObject_t1971882247 * L_13 = ___obj0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		JSONObject_t1971882247 * L_15 = JSONObject_get_Item_m481309353(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		float L_16 = JSONObject_get_f_m1158093019(L_15, /*hidden argument*/NULL);
		Rect_set_x_m3783700513((&V_0), L_16, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_007f:
	{
		JSONObject_t1971882247 * L_17 = ___obj0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m481309353(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		float L_20 = JSONObject_get_f_m1158093019(L_19, /*hidden argument*/NULL);
		Rect_set_y_m4294916608((&V_0), L_20, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_0097:
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_23 = JSONObject_get_Item_m481309353(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		float L_24 = JSONObject_get_f_m1158093019(L_23, /*hidden argument*/NULL);
		Rect_set_height_m2019122814((&V_0), L_24, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00af:
	{
		JSONObject_t1971882247 * L_25 = ___obj0;
		int32_t L_26 = V_1;
		NullCheck(L_25);
		JSONObject_t1971882247 * L_27 = JSONObject_get_Item_m481309353(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		float L_28 = JSONObject_get_f_m1158093019(L_27, /*hidden argument*/NULL);
		Rect_set_width_m1921257731((&V_0), L_28, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00c7:
	{
		int32_t L_29 = V_1;
		V_1 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00cb:
	{
		int32_t L_30 = V_1;
		JSONObject_t1971882247 * L_31 = ___obj0;
		NullCheck(L_31);
		int32_t L_32 = JSONObject_get_Count_m2165978410(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_000f;
		}
	}
	{
		Rect_t3681755626  L_33 = V_0;
		return L_33;
	}
}
// JSONObject JSONTemplates::FromRectOffset(UnityEngine.RectOffset)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral70907419;
extern Il2CppCodeGenString* _stringLiteral3423761043;
extern Il2CppCodeGenString* _stringLiteral109637592;
extern Il2CppCodeGenString* _stringLiteral1502598707;
extern const uint32_t JSONTemplates_FromRectOffset_m2102731279_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromRectOffset_m2102731279 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromRectOffset_m2102731279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		RectOffset_t3387826427 * L_1 = ___r0;
		NullCheck(L_1);
		int32_t L_2 = RectOffset_get_bottom_m4112328858(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = V_0;
		RectOffset_t3387826427 * L_4 = ___r0;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_bottom_m4112328858(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		JSONObject_AddField_m894980766(L_3, _stringLiteral70907419, L_5, /*hidden argument*/NULL);
	}

IL_0022:
	{
		RectOffset_t3387826427 * L_6 = ___r0;
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m439065308(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		RectOffset_t3387826427 * L_9 = ___r0;
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m439065308(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		JSONObject_AddField_m894980766(L_8, _stringLiteral3423761043, L_10, /*hidden argument*/NULL);
	}

IL_003e:
	{
		RectOffset_t3387826427 * L_11 = ___r0;
		NullCheck(L_11);
		int32_t L_12 = RectOffset_get_right_m281378687(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_13 = V_0;
		RectOffset_t3387826427 * L_14 = ___r0;
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_right_m281378687(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		JSONObject_AddField_m894980766(L_13, _stringLiteral109637592, L_15, /*hidden argument*/NULL);
	}

IL_005a:
	{
		RectOffset_t3387826427 * L_16 = ___r0;
		NullCheck(L_16);
		int32_t L_17 = RectOffset_get_top_m3629049358(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0076;
		}
	}
	{
		JSONObject_t1971882247 * L_18 = V_0;
		RectOffset_t3387826427 * L_19 = ___r0;
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m3629049358(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		JSONObject_AddField_m894980766(L_18, _stringLiteral1502598707, L_20, /*hidden argument*/NULL);
	}

IL_0076:
	{
		JSONObject_t1971882247 * L_21 = V_0;
		return L_21;
	}
}
// UnityEngine.RectOffset JSONTemplates::ToRectOffset(JSONObject)
extern Il2CppClass* RectOffset_t3387826427_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral70907419;
extern Il2CppCodeGenString* _stringLiteral3423761043;
extern Il2CppCodeGenString* _stringLiteral109637592;
extern Il2CppCodeGenString* _stringLiteral1502598707;
extern const uint32_t JSONTemplates_ToRectOffset_m75654846_MetadataUsageId;
extern "C"  RectOffset_t3387826427 * JSONTemplates_ToRectOffset_m75654846 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToRectOffset_m75654846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectOffset_t3387826427 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		RectOffset_t3387826427 * L_0 = (RectOffset_t3387826427 *)il2cpp_codegen_object_new(RectOffset_t3387826427_il2cpp_TypeInfo_var);
		RectOffset__ctor_m2227510254(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_00c9;
	}

IL_000d:
	{
		JSONObject_t1971882247 * L_1 = ___obj0;
		NullCheck(L_1);
		List_1_t1398341365 * L_2 = L_1->get_keys_8();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		String_t* L_4 = List_1_get_Item_m566484697(L_2, L_3, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		V_2 = L_4;
		String_t* L_5 = V_2;
		if (!L_5)
		{
			goto IL_00c5;
		}
	}
	{
		String_t* L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral70907419, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral3423761043, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_007d;
		}
	}
	{
		String_t* L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral109637592, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0095;
		}
	}
	{
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, _stringLiteral1502598707, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_00ad;
		}
	}
	{
		goto IL_00c5;
	}

IL_0065:
	{
		RectOffset_t3387826427 * L_14 = V_0;
		JSONObject_t1971882247 * L_15 = ___obj0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_17 = JSONObject_get_Item_m481309353(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		float L_18 = L_17->get_n_10();
		NullCheck(L_14);
		RectOffset_set_bottom_m4065521443(L_14, (((int32_t)((int32_t)L_18))), /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_007d:
	{
		RectOffset_t3387826427 * L_19 = V_0;
		JSONObject_t1971882247 * L_20 = ___obj0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		JSONObject_t1971882247 * L_22 = JSONObject_get_Item_m481309353(L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = L_22->get_n_10();
		NullCheck(L_19);
		RectOffset_set_left_m620681523(L_19, (((int32_t)((int32_t)L_23))), /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_0095:
	{
		RectOffset_t3387826427 * L_24 = V_0;
		JSONObject_t1971882247 * L_25 = ___obj0;
		int32_t L_26 = V_1;
		NullCheck(L_25);
		JSONObject_t1971882247 * L_27 = JSONObject_get_Item_m481309353(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		float L_28 = L_27->get_n_10();
		NullCheck(L_24);
		RectOffset_set_right_m1671272302(L_24, (((int32_t)((int32_t)L_28))), /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_00ad:
	{
		RectOffset_t3387826427 * L_29 = V_0;
		JSONObject_t1971882247 * L_30 = ___obj0;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		JSONObject_t1971882247 * L_32 = JSONObject_get_Item_m481309353(L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		float L_33 = L_32->get_n_10();
		NullCheck(L_29);
		RectOffset_set_top_m3579196427(L_29, (((int32_t)((int32_t)L_33))), /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_00c5:
	{
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00c9:
	{
		int32_t L_35 = V_1;
		JSONObject_t1971882247 * L_36 = ___obj0;
		NullCheck(L_36);
		int32_t L_37 = JSONObject_get_Count_m2165978410(L_36, /*hidden argument*/NULL);
		if ((((int32_t)L_35) < ((int32_t)L_37)))
		{
			goto IL_000d;
		}
	}
	{
		RectOffset_t3387826427 * L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.AnimationCurve JSONTemplates::ToAnimationCurve(JSONObject)
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONTemplates_t3006274921_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1857677462;
extern Il2CppCodeGenString* _stringLiteral4185425568;
extern Il2CppCodeGenString* _stringLiteral10864839;
extern const uint32_t JSONTemplates_ToAnimationCurve_m2654279102_MetadataUsageId;
extern "C"  AnimationCurve_t3306541151 * JSONTemplates_ToAnimationCurve_m2654279102 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToAnimationCurve_m2654279102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimationCurve_t3306541151 * V_0 = NULL;
	JSONObject_t1971882247 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		AnimationCurve_t3306541151 * L_0 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m3707994114(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = ___obj0;
		NullCheck(L_1);
		bool L_2 = JSONObject_HasField_m3030985834(L_1, _stringLiteral1857677462, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_GetField_m887332132(L_3, _stringLiteral1857677462, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0040;
	}

IL_0029:
	{
		AnimationCurve_t3306541151 * L_5 = V_0;
		JSONObject_t1971882247 * L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = JSONObject_get_Item_m481309353(L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONTemplates_t3006274921_il2cpp_TypeInfo_var);
		Keyframe_t1449471340  L_9 = JSONTemplates_ToKeyframe_m1491004926(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		AnimationCurve_AddKey_m1034249733(L_5, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_2;
		JSONObject_t1971882247 * L_12 = V_1;
		NullCheck(L_12);
		List_1_t1341003379 * L_13 = L_12->get_list_7();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m3485821544(L_13, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_0029;
		}
	}

IL_0051:
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		bool L_16 = JSONObject_HasField_m3030985834(L_15, _stringLiteral4185425568, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0078;
		}
	}
	{
		AnimationCurve_t3306541151 * L_17 = V_0;
		JSONObject_t1971882247 * L_18 = ___obj0;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_19 = JSONObject_GetField_m887332132(L_18, _stringLiteral4185425568, /*hidden argument*/NULL);
		NullCheck(L_19);
		float L_20 = L_19->get_n_10();
		NullCheck(L_17);
		AnimationCurve_set_preWrapMode_m2999148321(L_17, (((int32_t)((int32_t)L_20))), /*hidden argument*/NULL);
	}

IL_0078:
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		NullCheck(L_21);
		bool L_22 = JSONObject_HasField_m3030985834(L_21, _stringLiteral10864839, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009f;
		}
	}
	{
		AnimationCurve_t3306541151 * L_23 = V_0;
		JSONObject_t1971882247 * L_24 = ___obj0;
		NullCheck(L_24);
		JSONObject_t1971882247 * L_25 = JSONObject_GetField_m887332132(L_24, _stringLiteral10864839, /*hidden argument*/NULL);
		NullCheck(L_25);
		float L_26 = L_25->get_n_10();
		NullCheck(L_23);
		AnimationCurve_set_postWrapMode_m262982620(L_23, (((int32_t)((int32_t)L_26))), /*hidden argument*/NULL);
	}

IL_009f:
	{
		AnimationCurve_t3306541151 * L_27 = V_0;
		return L_27;
	}
}
// JSONObject JSONTemplates::FromAnimationCurve(UnityEngine.AnimationCurve)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapMode_t255797857_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONTemplates_t3006274921_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4185425568;
extern Il2CppCodeGenString* _stringLiteral10864839;
extern Il2CppCodeGenString* _stringLiteral1857677462;
extern const uint32_t JSONTemplates_FromAnimationCurve_m448168407_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromAnimationCurve_m448168407 (Il2CppObject * __this /* static, unused */, AnimationCurve_t3306541151 * ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromAnimationCurve_m448168407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	JSONObject_t1971882247 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		AnimationCurve_t3306541151 * L_2 = ___a0;
		NullCheck(L_2);
		int32_t L_3 = AnimationCurve_get_preWrapMode_m1489197552(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppObject * L_4 = Box(WrapMode_t255797857_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_1);
		JSONObject_AddField_m757984985(L_1, _stringLiteral4185425568, L_5, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_6 = V_0;
		AnimationCurve_t3306541151 * L_7 = ___a0;
		NullCheck(L_7);
		int32_t L_8 = AnimationCurve_get_postWrapMode_m1346218857(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Il2CppObject * L_9 = Box(WrapMode_t255797857_il2cpp_TypeInfo_var, (&V_2));
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		NullCheck(L_6);
		JSONObject_AddField_m757984985(L_6, _stringLiteral10864839, L_10, /*hidden argument*/NULL);
		AnimationCurve_t3306541151 * L_11 = ___a0;
		NullCheck(L_11);
		KeyframeU5BU5D_t449065829* L_12 = AnimationCurve_get_keys_m162753017(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_009e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_13 = JSONObject_Create_m134949822(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_13;
		V_4 = 0;
		goto IL_0083;
	}

IL_0060:
	{
		JSONObject_t1971882247 * L_14 = V_3;
		AnimationCurve_t3306541151 * L_15 = ___a0;
		NullCheck(L_15);
		KeyframeU5BU5D_t449065829* L_16 = AnimationCurve_get_keys_m162753017(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_4;
		NullCheck(L_16);
		IL2CPP_RUNTIME_CLASS_INIT(JSONTemplates_t3006274921_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_18 = JSONTemplates_FromKeyframe_m1523648219(NULL /*static, unused*/, (*(Keyframe_t1449471340 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))), /*hidden argument*/NULL);
		NullCheck(L_14);
		JSONObject_Add_m3618328192(L_14, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_4;
		V_4 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_4;
		AnimationCurve_t3306541151 * L_21 = ___a0;
		NullCheck(L_21);
		KeyframeU5BU5D_t449065829* L_22 = AnimationCurve_get_keys_m162753017(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_0060;
		}
	}
	{
		JSONObject_t1971882247 * L_23 = V_0;
		JSONObject_t1971882247 * L_24 = V_3;
		NullCheck(L_23);
		JSONObject_AddField_m1721386152(L_23, _stringLiteral1857677462, L_24, /*hidden argument*/NULL);
	}

IL_009e:
	{
		JSONObject_t1971882247 * L_25 = V_0;
		return L_25;
	}
}
// UnityEngine.Keyframe JSONTemplates::ToKeyframe(JSONObject)
extern Il2CppCodeGenString* _stringLiteral3457519049;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern Il2CppCodeGenString* _stringLiteral2674127958;
extern Il2CppCodeGenString* _stringLiteral3634190493;
extern Il2CppCodeGenString* _stringLiteral602706040;
extern const uint32_t JSONTemplates_ToKeyframe_m1491004926_MetadataUsageId;
extern "C"  Keyframe_t1449471340  JSONTemplates_ToKeyframe_m1491004926 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToKeyframe_m1491004926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Keyframe_t1449471340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Keyframe_t1449471340 * G_B2_0 = NULL;
	Keyframe_t1449471340 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Keyframe_t1449471340 * G_B3_1 = NULL;
	float G_B5_0 = 0.0f;
	Keyframe_t1449471340 * G_B5_1 = NULL;
	float G_B4_0 = 0.0f;
	Keyframe_t1449471340 * G_B4_1 = NULL;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	Keyframe_t1449471340 * G_B6_2 = NULL;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		bool L_1 = JSONObject_HasField_m3030985834(L_0, _stringLiteral3457519049, /*hidden argument*/NULL);
		G_B1_0 = (&V_0);
		if (!L_1)
		{
			G_B2_0 = (&V_0);
			goto IL_0027;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = ___obj0;
		NullCheck(L_2);
		JSONObject_t1971882247 * L_3 = JSONObject_GetField_m887332132(L_2, _stringLiteral3457519049, /*hidden argument*/NULL);
		NullCheck(L_3);
		float L_4 = L_3->get_n_10();
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_0027:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		JSONObject_t1971882247 * L_5 = ___obj0;
		NullCheck(L_5);
		bool L_6 = JSONObject_HasField_m3030985834(L_5, _stringLiteral1803325615, /*hidden argument*/NULL);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		if (!L_6)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			goto IL_0051;
		}
	}
	{
		JSONObject_t1971882247 * L_7 = ___obj0;
		NullCheck(L_7);
		JSONObject_t1971882247 * L_8 = JSONObject_GetField_m887332132(L_7, _stringLiteral1803325615, /*hidden argument*/NULL);
		NullCheck(L_8);
		float L_9 = L_8->get_n_10();
		G_B6_0 = L_9;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0056;
	}

IL_0051:
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0056:
	{
		Keyframe__ctor_m2042404667(G_B6_2, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_10 = ___obj0;
		NullCheck(L_10);
		bool L_11 = JSONObject_HasField_m3030985834(L_10, _stringLiteral2674127958, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0082;
		}
	}
	{
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_GetField_m887332132(L_12, _stringLiteral2674127958, /*hidden argument*/NULL);
		NullCheck(L_13);
		float L_14 = L_13->get_n_10();
		Keyframe_set_inTangent_m4280114775((&V_0), L_14, /*hidden argument*/NULL);
	}

IL_0082:
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		bool L_16 = JSONObject_HasField_m3030985834(L_15, _stringLiteral3634190493, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a9;
		}
	}
	{
		JSONObject_t1971882247 * L_17 = ___obj0;
		NullCheck(L_17);
		JSONObject_t1971882247 * L_18 = JSONObject_GetField_m887332132(L_17, _stringLiteral3634190493, /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_19 = L_18->get_n_10();
		Keyframe_set_outTangent_m1054927214((&V_0), L_19, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		JSONObject_t1971882247 * L_20 = ___obj0;
		NullCheck(L_20);
		bool L_21 = JSONObject_HasField_m3030985834(L_20, _stringLiteral602706040, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00d1;
		}
	}
	{
		JSONObject_t1971882247 * L_22 = ___obj0;
		NullCheck(L_22);
		JSONObject_t1971882247 * L_23 = JSONObject_GetField_m887332132(L_22, _stringLiteral602706040, /*hidden argument*/NULL);
		NullCheck(L_23);
		float L_24 = L_23->get_n_10();
		Keyframe_set_tangentMode_m1073266123((&V_0), (((int32_t)((int32_t)L_24))), /*hidden argument*/NULL);
	}

IL_00d1:
	{
		Keyframe_t1449471340  L_25 = V_0;
		return L_25;
	}
}
// JSONObject JSONTemplates::FromKeyframe(UnityEngine.Keyframe)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2674127958;
extern Il2CppCodeGenString* _stringLiteral3634190493;
extern Il2CppCodeGenString* _stringLiteral602706040;
extern Il2CppCodeGenString* _stringLiteral3457519049;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t JSONTemplates_FromKeyframe_m1523648219_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromKeyframe_m1523648219 (Il2CppObject * __this /* static, unused */, Keyframe_t1449471340  ___k0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromKeyframe_m1523648219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m3201114506(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Keyframe_get_inTangent_m3256944616((&___k0), /*hidden argument*/NULL);
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = Keyframe_get_inTangent_m3256944616((&___k0), /*hidden argument*/NULL);
		NullCheck(L_2);
		JSONObject_AddField_m2712088548(L_2, _stringLiteral2674127958, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = Keyframe_get_outTangent_m1894374085((&___k0), /*hidden argument*/NULL);
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = Keyframe_get_outTangent_m1894374085((&___k0), /*hidden argument*/NULL);
		NullCheck(L_5);
		JSONObject_AddField_m2712088548(L_5, _stringLiteral3634190493, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		int32_t L_7 = Keyframe_get_tangentMode_m1869200796((&___k0), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006a;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		int32_t L_9 = Keyframe_get_tangentMode_m1869200796((&___k0), /*hidden argument*/NULL);
		NullCheck(L_8);
		JSONObject_AddField_m894980766(L_8, _stringLiteral602706040, L_9, /*hidden argument*/NULL);
	}

IL_006a:
	{
		float L_10 = Keyframe_get_time_m2226372497((&___k0), /*hidden argument*/NULL);
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_008d;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = Keyframe_get_time_m2226372497((&___k0), /*hidden argument*/NULL);
		NullCheck(L_11);
		JSONObject_AddField_m2712088548(L_11, _stringLiteral3457519049, L_12, /*hidden argument*/NULL);
	}

IL_008d:
	{
		float L_13 = Keyframe_get_value_m979894315((&___k0), /*hidden argument*/NULL);
		if ((((float)L_13) == ((float)(0.0f))))
		{
			goto IL_00b0;
		}
	}
	{
		JSONObject_t1971882247 * L_14 = V_0;
		float L_15 = Keyframe_get_value_m979894315((&___k0), /*hidden argument*/NULL);
		NullCheck(L_14);
		JSONObject_AddField_m2712088548(L_14, _stringLiteral1803325615, L_15, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		JSONObject_t1971882247 * L_16 = V_0;
		return L_16;
	}
}
// System.Void JSONTemplates::.cctor()
extern Il2CppClass* HashSet_1_t1022910149_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONTemplates_t3006274921_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m3326270949_MethodInfo_var;
extern const uint32_t JSONTemplates__cctor_m1639877595_MetadataUsageId;
extern "C"  void JSONTemplates__cctor_m1639877595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates__cctor_m1639877595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t1022910149 * L_0 = (HashSet_1_t1022910149 *)il2cpp_codegen_object_new(HashSet_1_t1022910149_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3326270949(L_0, /*hidden argument*/HashSet_1__ctor_m3326270949_MethodInfo_var);
		((JSONTemplates_t3006274921_StaticFields*)JSONTemplates_t3006274921_il2cpp_TypeInfo_var->static_fields)->set_touched_0(L_0);
		return;
	}
}
// System.Void ObjGenerator::.ctor()
extern "C"  void ObjGenerator__ctor_m1985018875 (ObjGenerator_t2287658554 * __this, const MethodInfo* method)
{
	{
		Info_t2064034856 * L_0 = Info_GetInstance_m196706287(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_info_7(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ObjGenerator::Start()
extern Il2CppClass* U3CStartU3Ec__Iterator0_t2867974660_il2cpp_TypeInfo_var;
extern const uint32_t ObjGenerator_Start_m2678281225_MetadataUsageId;
extern "C"  Il2CppObject * ObjGenerator_Start_m2678281225 (ObjGenerator_t2287658554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjGenerator_Start_m2678281225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartU3Ec__Iterator0_t2867974660 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t2867974660 * L_0 = (U3CStartU3Ec__Iterator0_t2867974660 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t2867974660_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m3369458555(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t2867974660 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_8(__this);
		U3CStartU3Ec__Iterator0_t2867974660 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ObjGenerator::geterateObj(System.Int32,System.String,System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisObjVO_t1115677294_m333022829_MethodInfo_var;
extern const uint32_t ObjGenerator_geterateObj_m2488201922_MetadataUsageId;
extern "C"  void ObjGenerator_geterateObj_m2488201922 (ObjGenerator_t2287658554 * __this, int32_t ___idx0, String_t* ___title1, String_t* ___content2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjGenerator_geterateObj_m2488201922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)-10), ((int32_t)10), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)-10), ((int32_t)10), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		Vector3__ctor_m2638739322((&V_2), (((float)((float)L_2))), (-3.0f), (((float)((float)L_3))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = V_2;
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		List_1_t1125654279 * L_8 = __this->get_obj_8();
		GameObject_t1756533147 * L_9 = __this->get_prefab_2();
		Vector3_t2243707580  L_10 = V_2;
		Quaternion_t4030073918  L_11 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_12 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		NullCheck(L_8);
		List_1_Add_m3441471442(L_8, L_12, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		List_1_t1125654279 * L_13 = __this->get_obj_8();
		int32_t L_14 = ___idx0;
		NullCheck(L_13);
		GameObject_t1756533147 * L_15 = List_1_get_Item_m939767277(L_13, L_14, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_15);
		ObjVO_t1115677294 * L_16 = GameObject_GetComponent_TisObjVO_t1115677294_m333022829(L_15, /*hidden argument*/GameObject_GetComponent_TisObjVO_t1115677294_m333022829_MethodInfo_var);
		String_t* L_17 = ___title1;
		NullCheck(L_16);
		ObjVO_setTitle_m3418548101(L_16, L_17, /*hidden argument*/NULL);
		List_1_t1125654279 * L_18 = __this->get_obj_8();
		int32_t L_19 = ___idx0;
		NullCheck(L_18);
		GameObject_t1756533147 * L_20 = List_1_get_Item_m939767277(L_18, L_19, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_20);
		ObjVO_t1115677294 * L_21 = GameObject_GetComponent_TisObjVO_t1115677294_m333022829(L_20, /*hidden argument*/GameObject_GetComponent_TisObjVO_t1115677294_m333022829_MethodInfo_var);
		String_t* L_22 = ___content2;
		NullCheck(L_21);
		ObjVO_setContent_m4129050350(L_21, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjGenerator::resetBtn()
extern "C"  void ObjGenerator_resetBtn_m881138824 (ObjGenerator_t2287658554 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isRunning_6();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ObjGenerator_Start_m2678281225(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void ObjGenerator/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3369458555 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ObjGenerator/<Start>c__Iterator0::MoveNext()
extern Il2CppClass* WWWForm_t3950226929_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4244870320_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m4030601119_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3485821544_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429614411_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral696029425;
extern Il2CppCodeGenString* _stringLiteral3068682405;
extern Il2CppCodeGenString* _stringLiteral4165124290;
extern Il2CppCodeGenString* _stringLiteral1391431453;
extern Il2CppCodeGenString* _stringLiteral3320057068;
extern Il2CppCodeGenString* _stringLiteral2225486074;
extern Il2CppCodeGenString* _stringLiteral502065907;
extern Il2CppCodeGenString* _stringLiteral2409670919;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m2711278237_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2711278237 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m2711278237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int32_t L_0 = __this->get_U24PC_11();
		V_0 = L_0;
		__this->set_U24PC_11((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_013d;
		}
	}
	{
		goto IL_033e;
	}

IL_0021:
	{
		ObjGenerator_t2287658554 * L_2 = __this->get_U24this_8();
		NullCheck(L_2);
		L_2->set_isRunning_6((bool)1);
		WWWForm_t3950226929 * L_3 = (WWWForm_t3950226929 *)il2cpp_codegen_object_new(WWWForm_t3950226929_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2129424870(L_3, /*hidden argument*/NULL);
		__this->set_U3CformU3E__0_0(L_3);
		WWWForm_t3950226929 * L_4 = __this->get_U3CformU3E__0_0();
		ObjGenerator_t2287658554 * L_5 = __this->get_U24this_8();
		NullCheck(L_5);
		Info_t2064034856 * L_6 = L_5->get_info_7();
		NullCheck(L_6);
		String_t* L_7 = Info_getUserId_m1498575430(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		WWWForm_AddField_m1334606983(L_4, _stringLiteral287061489, L_7, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_8 = __this->get_U3CformU3E__0_0();
		ObjGenerator_t2287658554 * L_9 = __this->get_U24this_8();
		NullCheck(L_9);
		Info_t2064034856 * L_10 = L_9->get_info_7();
		NullCheck(L_10);
		float L_11 = Info_getLatitude_m3586550869(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		String_t* L_12 = Single_ToString_m1813392066((&V_1), /*hidden argument*/NULL);
		NullCheck(L_8);
		WWWForm_AddField_m1334606983(L_8, _stringLiteral696029425, L_12, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_13 = __this->get_U3CformU3E__0_0();
		ObjGenerator_t2287658554 * L_14 = __this->get_U24this_8();
		NullCheck(L_14);
		Info_t2064034856 * L_15 = L_14->get_info_7();
		NullCheck(L_15);
		float L_16 = Info_getLongitude_m4220800764(L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		String_t* L_17 = Single_ToString_m1813392066((&V_2), /*hidden argument*/NULL);
		NullCheck(L_13);
		WWWForm_AddField_m1334606983(L_13, _stringLiteral3068682405, L_17, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_18 = __this->get_U3CformU3E__0_0();
		NullCheck(L_18);
		Dictionary_2_t3943999495 * L_19 = WWWForm_get_headers_m3744493569(L_18, /*hidden argument*/NULL);
		__this->set_U3CheadersU3E__1_1(L_19);
		Dictionary_2_t3943999495 * L_20 = __this->get_U3CheadersU3E__1_1();
		NullCheck(L_20);
		Dictionary_2_set_Item_m4244870320(L_20, _stringLiteral4165124290, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		WWWForm_t3950226929 * L_21 = __this->get_U3CformU3E__0_0();
		NullCheck(L_21);
		ByteU5BU5D_t3397334013* L_22 = WWWForm_get_data_m1788094649(L_21, /*hidden argument*/NULL);
		__this->set_U3CrawDataU3E__2_2(L_22);
		__this->set_U3CurlU3E__3_3(_stringLiteral3320057068);
		ObjGenerator_t2287658554 * L_23 = __this->get_U24this_8();
		String_t* L_24 = __this->get_U3CurlU3E__3_3();
		ByteU5BU5D_t3397334013* L_25 = __this->get_U3CrawDataU3E__2_2();
		Dictionary_2_t3943999495 * L_26 = __this->get_U3CheadersU3E__1_1();
		WWW_t2919945039 * L_27 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m1577105574(L_27, L_24, L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_23);
		L_23->set_itemsData_5(L_27);
		ObjGenerator_t2287658554 * L_28 = __this->get_U24this_8();
		NullCheck(L_28);
		WWW_t2919945039 * L_29 = L_28->get_itemsData_5();
		__this->set_U24current_9(L_29);
		bool L_30 = __this->get_U24disposing_10();
		if (L_30)
		{
			goto IL_0138;
		}
	}
	{
		__this->set_U24PC_11(1);
	}

IL_0138:
	{
		goto IL_0340;
	}

IL_013d:
	{
		ObjGenerator_t2287658554 * L_31 = __this->get_U24this_8();
		NullCheck(L_31);
		L_31->set_isRunning_6((bool)0);
		ObjGenerator_t2287658554 * L_32 = __this->get_U24this_8();
		NullCheck(L_32);
		List_1_t1125654279 * L_33 = L_32->get_obj_8();
		if (!L_33)
		{
			goto IL_01b6;
		}
	}
	{
		ObjGenerator_t2287658554 * L_34 = __this->get_U24this_8();
		NullCheck(L_34);
		List_1_t1125654279 * L_35 = L_34->get_obj_8();
		NullCheck(L_35);
		int32_t L_36 = List_1_get_Count_m2764296230(L_35, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		if ((((int32_t)L_36) <= ((int32_t)0)))
		{
			goto IL_01b6;
		}
	}
	{
		V_3 = 0;
		goto IL_0190;
	}

IL_0176:
	{
		ObjGenerator_t2287658554 * L_37 = __this->get_U24this_8();
		NullCheck(L_37);
		List_1_t1125654279 * L_38 = L_37->get_obj_8();
		int32_t L_39 = V_3;
		NullCheck(L_38);
		GameObject_t1756533147 * L_40 = List_1_get_Item_m939767277(L_38, L_39, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		int32_t L_41 = V_3;
		V_3 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_0190:
	{
		int32_t L_42 = V_3;
		ObjGenerator_t2287658554 * L_43 = __this->get_U24this_8();
		NullCheck(L_43);
		List_1_t1125654279 * L_44 = L_43->get_obj_8();
		NullCheck(L_44);
		int32_t L_45 = List_1_get_Count_m2764296230(L_44, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		if ((((int32_t)L_42) < ((int32_t)L_45)))
		{
			goto IL_0176;
		}
	}
	{
		ObjGenerator_t2287658554 * L_46 = __this->get_U24this_8();
		NullCheck(L_46);
		List_1_t1125654279 * L_47 = L_46->get_obj_8();
		NullCheck(L_47);
		List_1_Clear_m4030601119(L_47, /*hidden argument*/List_1_Clear_m4030601119_MethodInfo_var);
	}

IL_01b6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_48 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3Cutf8U3E__4_4(L_48);
		Encoding_t663144255 * L_49 = __this->get_U3Cutf8U3E__4_4();
		ObjGenerator_t2287658554 * L_50 = __this->get_U24this_8();
		NullCheck(L_50);
		WWW_t2919945039 * L_51 = L_50->get_itemsData_5();
		NullCheck(L_51);
		String_t* L_52 = WWW_get_text_m1558985139(L_51, /*hidden argument*/NULL);
		NullCheck(L_49);
		ByteU5BU5D_t3397334013* L_53 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_49, L_52);
		__this->set_U3Cutf8byteU3E__5_5(L_53);
		Encoding_t663144255 * L_54 = __this->get_U3Cutf8U3E__4_4();
		ByteU5BU5D_t3397334013* L_55 = __this->get_U3Cutf8byteU3E__5_5();
		NullCheck(L_54);
		String_t* L_56 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_54, L_55);
		__this->set_U3Cutf8stringU3E__6_6(L_56);
		String_t* L_57 = __this->get_U3Cutf8stringU3E__6_6();
		JSONObject_t1971882247 * L_58 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m1387305321(L_58, L_57, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__7_7(L_58);
		JSONObject_t1971882247 * L_59 = __this->get_U3CjsonU3E__7_7();
		if (!L_59)
		{
			goto IL_032d;
		}
	}
	{
		JSONObject_t1971882247 * L_60 = __this->get_U3CjsonU3E__7_7();
		NullCheck(L_60);
		List_1_t1341003379 * L_61 = L_60->get_list_7();
		NullCheck(L_61);
		int32_t L_62 = List_1_get_Count_m3485821544(L_61, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		int32_t L_63 = L_62;
		Il2CppObject * L_64 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_63);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_65 = __this->get_U3CjsonU3E__7_7();
		NullCheck(L_65);
		List_1_t1341003379 * L_66 = L_65->get_list_7();
		NullCheck(L_66);
		int32_t L_67 = List_1_get_Count_m3485821544(L_66, /*hidden argument*/List_1_get_Count_m3485821544_MethodInfo_var);
		V_4 = L_67;
		ObjGenerator_t2287658554 * L_68 = __this->get_U24this_8();
		ObjGenerator_t2287658554 * L_69 = __this->get_U24this_8();
		NullCheck(L_69);
		Info_t2064034856 * L_70 = L_69->get_info_7();
		NullCheck(L_70);
		List_1_t1125654279 * L_71 = Info_getObjList_m3675622311(L_70, /*hidden argument*/NULL);
		NullCheck(L_68);
		L_68->set_obj_8(L_71);
		V_5 = 0;
		goto IL_031f;
	}

IL_0268:
	{
		ObjGenerator_t2287658554 * L_72 = __this->get_U24this_8();
		JSONObject_t1971882247 * L_73 = __this->get_U3CjsonU3E__7_7();
		NullCheck(L_73);
		List_1_t1341003379 * L_74 = L_73->get_list_7();
		int32_t L_75 = V_5;
		NullCheck(L_74);
		JSONObject_t1971882247 * L_76 = List_1_get_Item_m429614411(L_74, L_75, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_76);
		List_1_t1341003379 * L_77 = L_76->get_list_7();
		NullCheck(L_77);
		JSONObject_t1971882247 * L_78 = List_1_get_Item_m429614411(L_77, 6, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_78);
		String_t* L_79 = L_78->get_str_9();
		NullCheck(L_72);
		L_72->set_title_3(L_79);
		ObjGenerator_t2287658554 * L_80 = __this->get_U24this_8();
		JSONObject_t1971882247 * L_81 = __this->get_U3CjsonU3E__7_7();
		NullCheck(L_81);
		List_1_t1341003379 * L_82 = L_81->get_list_7();
		int32_t L_83 = V_5;
		NullCheck(L_82);
		JSONObject_t1971882247 * L_84 = List_1_get_Item_m429614411(L_82, L_83, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_84);
		List_1_t1341003379 * L_85 = L_84->get_list_7();
		NullCheck(L_85);
		JSONObject_t1971882247 * L_86 = List_1_get_Item_m429614411(L_85, 7, /*hidden argument*/List_1_get_Item_m429614411_MethodInfo_var);
		NullCheck(L_86);
		String_t* L_87 = L_86->get_str_9();
		NullCheck(L_80);
		L_80->set_content_4(L_87);
		ObjGenerator_t2287658554 * L_88 = __this->get_U24this_8();
		NullCheck(L_88);
		String_t* L_89 = L_88->get_title_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_90 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2225486074, L_89, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
		ObjGenerator_t2287658554 * L_91 = __this->get_U24this_8();
		NullCheck(L_91);
		String_t* L_92 = L_91->get_content_4();
		String_t* L_93 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral502065907, L_92, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		ObjGenerator_t2287658554 * L_94 = __this->get_U24this_8();
		int32_t L_95 = V_5;
		ObjGenerator_t2287658554 * L_96 = __this->get_U24this_8();
		NullCheck(L_96);
		String_t* L_97 = L_96->get_title_3();
		ObjGenerator_t2287658554 * L_98 = __this->get_U24this_8();
		NullCheck(L_98);
		String_t* L_99 = L_98->get_content_4();
		NullCheck(L_94);
		ObjGenerator_geterateObj_m2488201922(L_94, L_95, L_97, L_99, /*hidden argument*/NULL);
		int32_t L_100 = V_5;
		V_5 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_031f:
	{
		int32_t L_101 = V_5;
		int32_t L_102 = V_4;
		if ((((int32_t)L_101) < ((int32_t)L_102)))
		{
			goto IL_0268;
		}
	}
	{
		goto IL_0337;
	}

IL_032d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2409670919, /*hidden argument*/NULL);
	}

IL_0337:
	{
		__this->set_U24PC_11((-1));
	}

IL_033e:
	{
		return (bool)0;
	}

IL_0340:
	{
		return (bool)1;
	}
}
// System.Object ObjGenerator/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4243978025 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Object ObjGenerator/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1485435745 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Void ObjGenerator/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3539632498 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_10((bool)1);
		__this->set_U24PC_11((-1));
		return;
	}
}
// System.Void ObjGenerator/<Start>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m1921155132_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1921155132 (U3CStartU3Ec__Iterator0_t2867974660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m1921155132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ObjVO::.ctor()
extern "C"  void ObjVO__ctor_m4169190289 (ObjVO_t1115677294 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjVO::setTitle(System.String)
extern "C"  void ObjVO_setTitle_m3418548101 (ObjVO_t1115677294 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title0;
		__this->set_title_2(L_0);
		return;
	}
}
// System.Void ObjVO::setContent(System.String)
extern "C"  void ObjVO_setContent_m4129050350 (ObjVO_t1115677294 * __this, String_t* ___content0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___content0;
		__this->set_content_3(L_0);
		return;
	}
}
// System.String ObjVO::getTitle()
extern "C"  String_t* ObjVO_getTitle_m627832088 (ObjVO_t1115677294 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_title_2();
		return L_0;
	}
}
// System.String ObjVO::getContent()
extern "C"  String_t* ObjVO_getContent_m1391192527 (ObjVO_t1115677294 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_content_3();
		return L_0;
	}
}
// System.Void OnClickEvent::.ctor()
extern "C"  void OnClickEvent__ctor_m4243796372 (OnClickEvent_t1249826957 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnClickEvent::Start()
extern const MethodInfo* GameObject_GetComponent_TisObjVO_t1115677294_m333022829_MethodInfo_var;
extern const uint32_t OnClickEvent_Start_m2260616076_MetadataUsageId;
extern "C"  void OnClickEvent_Start_m2260616076 (OnClickEvent_t1249826957 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnClickEvent_Start_m2260616076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjVO_t1115677294 * L_1 = GameObject_GetComponent_TisObjVO_t1115677294_m333022829(L_0, /*hidden argument*/GameObject_GetComponent_TisObjVO_t1115677294_m333022829_MethodInfo_var);
		__this->set_obj_6(L_1);
		return;
	}
}
// System.Void OnClickEvent::OnMouseDown()
extern "C"  void OnClickEvent_OnMouseDown_m1568766256 (OnClickEvent_t1249826957 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_title_4();
		ObjVO_t1115677294 * L_1 = __this->get_obj_6();
		NullCheck(L_1);
		String_t* L_2 = ObjVO_getTitle_m627832088(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		Text_t356221433 * L_3 = __this->get_content_5();
		ObjVO_t1115677294 * L_4 = __this->get_obj_6();
		NullCheck(L_4);
		String_t* L_5 = ObjVO_getContent_m1391192527(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_5);
		__this->set_panelOpen_3((bool)1);
		Canvas_t209405766 * L_6 = __this->get_panelCanvas_2();
		NullCheck(L_6);
		Behaviour_set_enabled_m1796096907(L_6, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReadPanelController::.ctor()
extern "C"  void ReadPanelController__ctor_m282136683 (ReadPanelController_t2513243910 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReadPanelController::Start()
extern const MethodInfo* GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var;
extern const uint32_t ReadPanelController_Start_m3624722871_MetadataUsageId;
extern "C"  void ReadPanelController_Start_m3624722871 (ReadPanelController_t2513243910 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadPanelController_Start_m3624722871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Canvas_t209405766 * L_1 = GameObject_GetComponent_TisCanvas_t209405766_m195193039(L_0, /*hidden argument*/GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var);
		__this->set_panelCanvas_2(L_1);
		return;
	}
}
// System.Void ReadPanelController::CloseBtn()
extern "C"  void ReadPanelController_CloseBtn_m2624603461 (ReadPanelController_t2513243910 * __this, const MethodInfo* method)
{
	{
		Canvas_t209405766 * L_0 = __this->get_panelCanvas_2();
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThrowNote::.ctor()
extern "C"  void ThrowNote__ctor_m3644619261 (ThrowNote_t1571054592 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThrowNote::OnClick()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596708495;
extern const uint32_t ThrowNote_OnClick_m665322552_MetadataUsageId;
extern "C"  void ThrowNote_OnClick_m665322552 (ThrowNote_t1571054592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThrowNote_OnClick_m665322552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1596708495, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TreeScript::.ctor()
extern "C"  void TreeScript__ctor_m3481356894 (TreeScript_t1745883499 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TreeScript::OnMouseDown()
extern "C"  void TreeScript_OnMouseDown_m1610342334 (TreeScript_t1745883499 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_panelOpen_7();
		if (L_0)
		{
			goto IL_0045;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_editTitle_5();
		String_t* L_2 = __this->get_title_2();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		Text_t356221433 * L_3 = __this->get_editContent_6();
		String_t* L_4 = __this->get_content_3();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
		__this->set_panelOpen_7((bool)1);
		Canvas_t209405766 * L_5 = __this->get_panelCanvas_4();
		NullCheck(L_5);
		Behaviour_set_enabled_m1796096907(L_5, (bool)1, /*hidden argument*/NULL);
		goto IL_0058;
	}

IL_0045:
	{
		__this->set_panelOpen_7((bool)0);
		Canvas_t209405766 * L_6 = __this->get_panelCanvas_4();
		NullCheck(L_6);
		Behaviour_set_enabled_m1796096907(L_6, (bool)0, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void TreeScript::setTitle(System.String)
extern "C"  void TreeScript_setTitle_m3285513596 (TreeScript_t1745883499 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title0;
		__this->set_title_2(L_0);
		return;
	}
}
// System.Void TreeScript::setContent(System.String)
extern "C"  void TreeScript_setContent_m3995843423 (TreeScript_t1745883499 * __this, String_t* ___content0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___content0;
		__this->set_content_3(L_0);
		return;
	}
}
// System.String TreeScript::getTitle()
extern "C"  String_t* TreeScript_getTitle_m226220833 (TreeScript_t1745883499 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_title_2();
		return L_0;
	}
}
// System.String TreeScript::getContent()
extern "C"  String_t* TreeScript_getContent_m2641054676 (TreeScript_t1745883499 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_content_3();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
