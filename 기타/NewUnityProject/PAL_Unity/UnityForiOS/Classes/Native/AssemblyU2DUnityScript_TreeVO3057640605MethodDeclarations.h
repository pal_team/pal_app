﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TreeVO
struct TreeVO_t3057640605;

#include "codegen/il2cpp-codegen.h"

// System.Void TreeVO::.ctor()
extern "C"  void TreeVO__ctor_m3544254977 (TreeVO_t3057640605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeVO::OnMouseDown()
extern "C"  void TreeVO_OnMouseDown_m1568564863 (TreeVO_t3057640605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreeVO::Main()
extern "C"  void TreeVO_Main_m4235660574 (TreeVO_t3057640605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
