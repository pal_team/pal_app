﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThrowNote
struct ThrowNote_t1571054592;

#include "codegen/il2cpp-codegen.h"

// System.Void ThrowNote::.ctor()
extern "C"  void ThrowNote__ctor_m3644619261 (ThrowNote_t1571054592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThrowNote::OnClick()
extern "C"  void ThrowNote_OnClick_m665322552 (ThrowNote_t1571054592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
