﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas
struct Canvas_t209405766;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreeVO
struct  TreeVO_t3057640605  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Canvas TreeVO::panelCanvas
	Canvas_t209405766 * ___panelCanvas_2;
	// System.Boolean TreeVO::panelOpen
	bool ___panelOpen_3;

public:
	inline static int32_t get_offset_of_panelCanvas_2() { return static_cast<int32_t>(offsetof(TreeVO_t3057640605, ___panelCanvas_2)); }
	inline Canvas_t209405766 * get_panelCanvas_2() const { return ___panelCanvas_2; }
	inline Canvas_t209405766 ** get_address_of_panelCanvas_2() { return &___panelCanvas_2; }
	inline void set_panelCanvas_2(Canvas_t209405766 * value)
	{
		___panelCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___panelCanvas_2, value);
	}

	inline static int32_t get_offset_of_panelOpen_3() { return static_cast<int32_t>(offsetof(TreeVO_t3057640605, ___panelOpen_3)); }
	inline bool get_panelOpen_3() const { return ___panelOpen_3; }
	inline bool* get_address_of_panelOpen_3() { return &___panelOpen_3; }
	inline void set_panelOpen_3(bool value)
	{
		___panelOpen_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
