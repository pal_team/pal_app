﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ObjGenerator
struct ObjGenerator_t2287658554;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ObjGenerator::.ctor()
extern "C"  void ObjGenerator__ctor_m1985018875 (ObjGenerator_t2287658554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ObjGenerator::Start()
extern "C"  Il2CppObject * ObjGenerator_Start_m2678281225 (ObjGenerator_t2287658554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjGenerator::geterateObj(System.Int32,System.String,System.String)
extern "C"  void ObjGenerator_geterateObj_m2488201922 (ObjGenerator_t2287658554 * __this, int32_t ___idx0, String_t* ___title1, String_t* ___content2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjGenerator::resetBtn()
extern "C"  void ObjGenerator_resetBtn_m881138824 (ObjGenerator_t2287658554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
