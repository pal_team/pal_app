﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t663144255;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t1971882247;
// Generate
struct Generate_t664530809;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Generate/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3787076049  : public Il2CppObject
{
public:
	// System.Text.Encoding Generate/<Start>c__Iterator0::<utf8>__0
	Encoding_t663144255 * ___U3Cutf8U3E__0_0;
	// System.Byte[] Generate/<Start>c__Iterator0::<utf8byte>__1
	ByteU5BU5D_t3397334013* ___U3Cutf8byteU3E__1_1;
	// System.String Generate/<Start>c__Iterator0::<utf8string>__2
	String_t* ___U3Cutf8stringU3E__2_2;
	// JSONObject Generate/<Start>c__Iterator0::<json>__3
	JSONObject_t1971882247 * ___U3CjsonU3E__3_3;
	// Generate Generate/<Start>c__Iterator0::$this
	Generate_t664530809 * ___U24this_4;
	// System.Object Generate/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean Generate/<Start>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Generate/<Start>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3Cutf8U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3787076049, ___U3Cutf8U3E__0_0)); }
	inline Encoding_t663144255 * get_U3Cutf8U3E__0_0() const { return ___U3Cutf8U3E__0_0; }
	inline Encoding_t663144255 ** get_address_of_U3Cutf8U3E__0_0() { return &___U3Cutf8U3E__0_0; }
	inline void set_U3Cutf8U3E__0_0(Encoding_t663144255 * value)
	{
		___U3Cutf8U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cutf8U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3Cutf8byteU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3787076049, ___U3Cutf8byteU3E__1_1)); }
	inline ByteU5BU5D_t3397334013* get_U3Cutf8byteU3E__1_1() const { return ___U3Cutf8byteU3E__1_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3Cutf8byteU3E__1_1() { return &___U3Cutf8byteU3E__1_1; }
	inline void set_U3Cutf8byteU3E__1_1(ByteU5BU5D_t3397334013* value)
	{
		___U3Cutf8byteU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cutf8byteU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3Cutf8stringU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3787076049, ___U3Cutf8stringU3E__2_2)); }
	inline String_t* get_U3Cutf8stringU3E__2_2() const { return ___U3Cutf8stringU3E__2_2; }
	inline String_t** get_address_of_U3Cutf8stringU3E__2_2() { return &___U3Cutf8stringU3E__2_2; }
	inline void set_U3Cutf8stringU3E__2_2(String_t* value)
	{
		___U3Cutf8stringU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cutf8stringU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__3_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3787076049, ___U3CjsonU3E__3_3)); }
	inline JSONObject_t1971882247 * get_U3CjsonU3E__3_3() const { return ___U3CjsonU3E__3_3; }
	inline JSONObject_t1971882247 ** get_address_of_U3CjsonU3E__3_3() { return &___U3CjsonU3E__3_3; }
	inline void set_U3CjsonU3E__3_3(JSONObject_t1971882247 * value)
	{
		___U3CjsonU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3787076049, ___U24this_4)); }
	inline Generate_t664530809 * get_U24this_4() const { return ___U24this_4; }
	inline Generate_t664530809 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Generate_t664530809 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3787076049, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3787076049, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3787076049, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
