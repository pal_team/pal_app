﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using ASRoundedRect;

[CustomEditor(typeof(RoundedRect))]
public class RoundedRectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        RoundedRect roundedRect = target as RoundedRect;

        roundedRect.color = EditorGUILayout.ColorField("Color", roundedRect.color);
        EditorGUI.BeginChangeCheck();
        roundedRect.Radius = EditorGUILayout.DelayedIntField("Corner Radius", roundedRect.Radius);
		EditorGUILayout.HelpBox("Minimum value: "+ASRoundedRect.RoundedRect.MIN_RADIUS.ToString()+System.Environment.NewLine+"Maximum value: "+ASRoundedRect.RoundedRect.MAX_RADIUS.ToString(), MessageType.Info);

        if (EditorGUI.EndChangeCheck())
        {
			roundedRect.Radius = Mathf.Max(ASRoundedRect.RoundedRect.MIN_RADIUS, Mathf.Min(ASRoundedRect.RoundedRect.MAX_RADIUS, roundedRect.Radius));
            roundedRect.UpdateSprite();
        }

        EditorUtility.SetDirty(target);
    }

	[MenuItem("GameObject/UI/Rounded Rect")]
	public static void CreateRoundedRect()
	{
		GameObject go = new GameObject("Rounded Rect");
		go.AddComponent<RoundedRect>();

		GameObject selectedObj = Selection.activeGameObject;
		if (selectedObj != null)
		{
			go.transform.SetParent(selectedObj.transform, false);
		}
		Selection.activeGameObject = go;
	}

	[MenuItem("Component/UI/Rounded Rect")]
	public static void AppendRoundedRectComponent()
	{
		if (Selection.activeGameObject != null)
			Selection.activeGameObject.AddComponent<RoundedRect>();
	}

	[MenuItem ("Component/UI/Rounded Rect", true)]
    public static bool ValidateAppendRoundedRectComponent () {
        return Selection.activeTransform != null;
    }
}
#endif
