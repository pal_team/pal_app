﻿using UnityEngine;
using UnityEngine.UI;

namespace ASRoundedRect
{
    public class RoundedRect : Image
    {
        public static int MIN_RADIUS = 4;
        public static int MAX_RADIUS = 1000;
        public static int DEFAULT_RADIUS = 20;

        [SerializeField]
        private int radius = DEFAULT_RADIUS;
        public int Radius
        {
            get
            {
                return radius;
            }
            set
            {
                radius = value;
            }
        }

        protected override void Start()
        {
            UpdateSprite();
        }

        protected override void OnDestroy()
        {
            ASRoundedRect.TextureCache.ClearAllCachedDataByInstanceId(gameObject.GetInstanceID());
        }

        public void UpdateSprite()
        {
            string textureName = "ASRoundedRect_" + radius.ToString();
            Texture2D texture = ASRoundedRect.TextureCache.GetTexture2DFromCache(textureName, gameObject.GetInstanceID());
            if (texture == null)
            {
#if UNITY_EDITOR
                texture = ASRoundedRect.Graphics.CreateCircleTexture(radius);
                ASRoundedRect.TextureCache.SaveTexture2DToCache(textureName, texture, gameObject.GetInstanceID());
                this.sprite = ASRoundedRect.Graphics.CreateCircleSpriteFromTexture2D(texture);
                this.type = Image.Type.Sliced;
#endif
            }
            else
            {
                this.sprite = ASRoundedRect.Graphics.CreateCircleSpriteFromTexture2D(texture);
                this.type = Image.Type.Sliced;
            }
        }


    }
}


