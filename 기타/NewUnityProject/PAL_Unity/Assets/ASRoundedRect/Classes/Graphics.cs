﻿using UnityEngine;

namespace ASRoundedRect
{
    public class Graphics
    {
        public int radius;

        public static Texture2D CreateCircleTexture(int radius)
        {
            Texture2D texture = new Texture2D(radius * 2, radius * 2, TextureFormat.ARGB32, false);
            DrawCircle(texture, radius, radius, radius, Color.white);
            return texture;
        }

        public static Sprite CreateCircleSpriteFromTexture2D(Texture2D texture)
        {
            return Sprite.Create(
                texture,
                new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5f, 0.5f),
                100,
                0,
                SpriteMeshType.Tight,
                new Vector4(texture.width / 2f, texture.width / 2f, texture.width / 2f, texture.width / 2f)
            );
        }

        private static void DrawCircle(Texture2D tex, int centerX, int centerY, int rad, Color col)
        {
            int x, y, px, nx, py, ny, d;

            for (x = 0; x <= rad; x++)
            {
                d = (int)Mathf.Ceil(Mathf.Sqrt(rad * rad - x * x));

                for (y = d; y <= rad; y++)
                {
                    px = centerX + x;
                    nx = centerX - x;
                    py = centerY + y;
                    ny = centerY - y;

                    tex.SetPixel(px, py, Color.clear);
                    tex.SetPixel(nx, py, Color.clear);

                    tex.SetPixel(px, ny, Color.clear);
                    tex.SetPixel(nx, ny, Color.clear);
                }

                for (y = 0; y <= d; y++)
                {
                    Color _col = col;
                    if (y > d - 3)
                    {
                        if (d - y > 0)
                        {
                            _col.a = 1f - 1f / (d - y);
                            _col.a = _col.a * _col.a;
                        }
                        else
                        {
                            _col.a = 0f;
                        }
                    }

                    px = centerX + x;
                    nx = centerX - x;
                    py = centerY + y;
                    ny = centerY - y;

                    tex.SetPixel(px, py, _col);
                    tex.SetPixel(nx, py, _col);

                    tex.SetPixel(px, ny, _col);
                    tex.SetPixel(nx, ny, _col);
                }
            }
            tex.Apply();
        }
    }
}