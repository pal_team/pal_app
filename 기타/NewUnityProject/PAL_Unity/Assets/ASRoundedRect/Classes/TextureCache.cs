﻿using System.IO;
using UnityEngine;

namespace ASRoundedRect
{
    public class TextureCache
    {
        public static Texture2D GetTexture2DFromCache(string textureName, int instanceId)
        {
#if UNITY_EDITOR
            string path = Path.Combine(Application.dataPath, "Resources/ASRoundedRect/" + instanceId.ToString() + "/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
#endif
            return Resources.Load<Texture2D>("ASRoundedRect/" + instanceId.ToString() + "/" + textureName);
        }

        public static void SaveTexture2DToCache(string textureName, Texture2D texture, int instanceId)
        {
            ClearUnusedCacheWithInstanceId(instanceId, textureName);
            string path = Path.Combine(Application.dataPath, "Resources/ASRoundedRect/" + instanceId.ToString() + "/" + textureName);
            byte[] bytes = texture.EncodeToPNG();
            File.WriteAllBytes(path, bytes);
        }

        public static void ClearUnusedCacheWithInstanceId(int id, string currentTexture)
        {
            string path = Path.Combine(Application.dataPath, "Resources/ASRoundedRect/" + id.ToString() + "/");
            DirectoryInfo di = new DirectoryInfo(path);
            foreach (FileInfo file in di.GetFiles())
            {
                if (!file.Name.Equals(currentTexture))
                    file.Delete();
            }
        }

        public static void ClearAllCachedDataByInstanceId(int id)
        {
            string path = Path.Combine(Application.dataPath, "Resources/ASRoundedRect/" + id.ToString());
            if (Directory.Exists(path + "/"))
            {
                Directory.Delete(path + "/", true);
            }
            if (File.Exists(path + ".meta"))
            {
                File.Delete(path + ".meta");
            }

        }
    }
}
