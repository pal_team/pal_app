﻿//Singleton pattern Class
using UnityEngine;
using System.Collections.Generic;
public class Info:MonoBehaviour{
	//Singleton Pattern Generate Code
	public static Info instance;

	void Awake(){
		instance = this;
	}



	private List<GameObject> objList = new List<GameObject>();
	private string UserId = "user";
	private string latitude = "37.49466";
	private string longitude = "127.03765";
	//lat = 37.494661
	//lon = 127.027657

	public List<GameObject> getObjList(){
		return objList;
	}

	public string getLatitude(){
		return this.latitude;
	}
	public string getLongitude(){
		return this.longitude;
	}
	public string getUserId(){
		return this.UserId;
	}
	public void setUserId(string id){
		this.UserId = id;
	}
	public void setLatitude(string lat){
		this.latitude = lat;
		Debug.Log ("setLatitude Call : " + this.latitude);
	}
	public void setLongitude(string lon){
		this.longitude = lon;
		Debug.Log ("setLongitude Call : " + this.longitude);
	}
}
