﻿using UnityEngine;
public class ObjVO : MonoBehaviour{
	private string title;
	private string content;

	public void setTitle (string title){
		this.title = title;
	}
	public void setContent(string content){
		this.content = content;
	}

	public string getTitle(){
		return this.title;
	}
	public string getContent(){
		return this.content;
	}
}
