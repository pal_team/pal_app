﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;

public class CreateNote : MonoBehaviour {
	
	public GameObject prefab;

	private bool isRunning;
	public Canvas writeForm;
	public Text title;
	public Text content;
	public Transform worlObj;

//	private Info info = Info.instance;
	private List<GameObject> obj;

	public IEnumerator createNote(){
		obj = Info.instance.getObjList();
		if (getTextForm()) {
			isRunning = true;
			WWWForm form = new WWWForm ();
			form.AddField ("id", Info.instance.getUserId ());
			form.AddField ("lat", Info.instance.getLatitude ());
			form.AddField ("lng", Info.instance.getLongitude ());
			form.AddField ("title", title.text);
			form.AddField ("content", content.text);
			Dictionary<string, string> headers = form.headers;
			headers ["content-type"] = "application/json";

			byte[] rawData = form.data;

			string url = "http://52.79.150.65:9000/createNote";
//			string url = "http://localhost:9000/createNote";
			WWW www = new WWW (url, rawData, headers);
			yield return www;



			Encoding utf8 = Encoding.UTF8;
			//		string itemsDataString = itemsData.text;
			byte[] utf8byte = utf8.GetBytes(www.text);
			string utf8string = utf8.GetString(utf8byte);

			JSONObject json = new JSONObject (utf8string);
			if(json != null && json.list [0].str.Equals ("success")){
				float x = worlObj.eulerAngles.x;
//				float z = worlObj.eulerAngles.z;
			
				Vector3 position = new Vector3 (x, -2, 10);
				position += transform.position;

				obj.Add(Instantiate(prefab, position, Quaternion.identity));


				obj[obj.Count-1].GetComponent<ObjVO> ().setTitle (title.text);
				obj[obj.Count-1].GetComponent<ObjVO> ().setContent (content.text);
			}

			isRunning = false;
			writeForm.enabled = false;
		}

	}
	private bool getTextForm(){
//		if (title.text.Length < 3) {
//			title.text = "Title will be at least 3 letter";
//			return false;
//		}
//		if (content.text.Length < 5) {
//			title.text = "Content will be at least 5 letter";
//			return false;
//		}
		return true;
	}

	public void toggleBtn(){
		writeForm.enabled = !writeForm.enabled;
	}

	public void createBtn(){
		if (!isRunning) {
			StartCoroutine (createNote());
		}
	}
}
