﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.UI;

public class ObjGenerator : MonoBehaviour {
	public GameObject prefab;

	private string title;
	private string content;
	private WWW itemsData;
	private bool isRunning;
//	private Info info = Info.instance;
	private List<GameObject> obj;

	// Use this for initialization

	private IEnumerator Start(){
		isRunning = true;

		WWWForm form = new WWWForm ();
		form.AddField ("id", Info.instance.getUserId ());
		form.AddField ("lat", Info.instance.getLatitude ());
		form.AddField ("lng", Info.instance.getLongitude ());
		Dictionary<string, string> headers = form.headers;
		headers ["content-type"] = "application/json";

		byte[] rawData = form.data;
		string url = "http://52.79.150.65:9000/searchNote";
//		string url = "http://localhost:9000/searchNote";

		itemsData = new WWW (url, rawData, headers);
		yield return itemsData;
		isRunning = false;
	
		if (obj != null && obj.Count > 0) {
			for (int idx = 0; idx < obj.Count; idx++) {
				Destroy (obj [idx]);
			}
			obj.Clear ();
		}

		Encoding utf8 = Encoding.UTF8;
		//		string itemsDataString = itemsData.text;
		byte[] utf8byte = utf8.GetBytes(itemsData.text);
		string utf8string = utf8.GetString(utf8byte);

		JSONObject json = new JSONObject (utf8string);
		if (json != null) {
			Debug.Log (json.list.Count);
			int num = json.list.Count;
			obj = Info.instance.getObjList();
			for (int i = 0; i < num; i++) {
				title = json.list [i].list [6].str;
				content = json.list [i].list [7].str;

				Debug.Log ("Title : " + title);
				Debug.Log ("Content : " + content);
				geterateObj (i, title, content);
			}
			//			indicator.enabled = false;
		} else {
			Debug.Log ("Note doesn't exist here.");
		}

	}
	private void geterateObj(int idx, string title, string content){
		int x = Random.Range (-10, 10);
		int z = Random.Range (-10, 10);
		Vector3 position = new Vector3 (x, -3, z);
		position += transform.position;
		obj.Add(Instantiate(prefab, position, Quaternion.identity));

		obj [idx].GetComponent<ObjVO> ().setTitle (title);
		obj [idx].GetComponent<ObjVO> ().setContent (content);
	}

	public void resetBtn(){
		if (!isRunning) {
			StartCoroutine (Start());
		}
	}
}