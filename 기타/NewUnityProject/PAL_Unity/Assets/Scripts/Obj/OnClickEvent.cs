﻿using UnityEngine;
using UnityEngine.UI;

public class OnClickEvent : MonoBehaviour {
	public Canvas panelCanvas;
	public bool panelOpen;
	public Text title;
	public Text content;

	private ObjVO obj;

	void Start(){
		obj = gameObject.GetComponent<ObjVO> ();
	}

	private void OnMouseDown(){
//		title = GameObject.FindGameObjectWithTag ("ReadModalTitle").GetComponent<Text>();
//		content = GameObject.FindGameObjectWithTag ("ReadModalContent").GetComponent<Text>();

		title.text = obj.getTitle ();
		content.text = obj.getContent ();

		panelOpen = true;
		panelCanvas.enabled = true;
	}
}
