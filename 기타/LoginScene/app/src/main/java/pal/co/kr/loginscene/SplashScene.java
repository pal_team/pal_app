package pal.co.kr.loginscene;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.HashMap;

public class SplashScene extends AppCompatActivity {
    private final String LOGIN_URL = "http://ec2-52-78-104-204.ap-northeast-2.compute.amazonaws.com:9000/authUser";
    private final String TAG = this.getClass().getName();
    private HashMap<String, String> loginInfo;
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_scene);


        loginInfo = new HashMap<>();
        pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        editor = pref.edit();
        String checkId = pref.getString("id", "");
        String checkPassword = pref.getString("password", "");
        Log.d(TAG, checkId);
        Log.d(TAG, checkPassword);

        loginInfo.put("id", checkId);
        loginInfo.put("password", checkPassword);

        PostResponseAsyncTask task = new PostResponseAsyncTask(SplashScene.this,
                loginInfo,
                new AsyncResponse() {
                    @Override
                    public void processFinish(String s) {
                        Intent intent;
                        if (s.contains("footer"))
                        //Login success (html페이지에 footer가 있으면 성공)
                        {
                            intent = new Intent(SplashScene.this, MainService.class);
                        } else {

                            intent = new Intent(SplashScene.this, LoginActivity.class);
                        }
                        Log.d(TAG, s);
                        startActivity(intent);
                        finish();
                    }
                });
        task.execute(LOGIN_URL);

    }
}
