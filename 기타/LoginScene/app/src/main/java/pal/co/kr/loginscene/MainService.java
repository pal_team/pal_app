package pal.co.kr.loginscene;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainService extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_service);
        Button logOut = (Button) findViewById(R.id.logout);

        pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        Log.d(TAG,pref.getString("id",""));
        Log.d(TAG,pref.getString("password",""));
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor = pref.edit();
                editor.clear();
                editor.commit();
                Log.d(TAG,pref.getString("id",""));
                Log.d(TAG,pref.getString("password",""));
                Intent intent = new Intent(MainService.this,LoginActivity.class);
                startActivity(intent);
                finish();
//                pref.edit().clear().commit();

            }
        });
    }
}
