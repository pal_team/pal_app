package pal.co.kr.loginscene;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener{
    private final String LOGIN_URL = "http://ec2-52-78-104-204.ap-northeast-2.compute.amazonaws.com:9000/authUser";
    private final String TAG = this.getClass().getName();
    private EditText id;
    private EditText password;
    private CheckBox remeber;
    private Button btnLogin;
    private HashMap<String, String> loginInfo;
    private boolean checkFlag;
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;


    @Override
    protected void onResume(){
        super.onResume();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginInfo = new HashMap<>();
        id = (EditText) findViewById(R.id.id);
        password = (EditText) findViewById(R.id.password);

        //Remember CheckBox
        remeber = (CheckBox) findViewById(R.id.remember);
        remeber.setOnCheckedChangeListener(this);
        checkFlag = remeber.isChecked();


        //Login Button
        btnLogin = (Button) findViewById(R.id.btnLogin);
        pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        editor = pref.edit();


//        String checkId = pref.getString("id","");
//        String checkPassword = pref.getString("password","");
//        Log.d(TAG,checkId);
//        Log.d(TAG,checkPassword);
//
//        loginInfo.put("id",checkId);
//        loginInfo.put("password",checkPassword);
//        if(!(checkId.toString().equals("")) && !(checkPassword.toString().equals(""))){
//            PostResponseAsyncTask task = new PostResponseAsyncTask(LoginActivity.this,
//                    loginInfo,
//                    new AsyncResponse() {
//                        @Override
//                        public void processFinish(String s) {
//                            if(s.contains(id.getText().toString()))
//                            //Login success (html페이지에 id가 있으면 성공)
//                            {
//                                Intent intent = new Intent(LoginActivity.this, MainService.class);
//                                startActivity(intent);
//                                finish();
//                            }
//                        }
//                    });
//            task.execute(LOGIN_URL);
//        }


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkInfo()) {
                    loginInfo.put("id",id.getText().toString());
                    loginInfo.put("password",password.getText().toString());

                    PostResponseAsyncTask task = new PostResponseAsyncTask(LoginActivity.this,
                            loginInfo,
                            new AsyncResponse() {
                        @Override
                        public void processFinish(String s) {
                            if(s.contains("footer"))
                                //Login success (html페이지에 id가 있으면 성공)
                            {
                                if (checkFlag) {
                                    editor.putString("id", id.getText().toString());
                                    editor.putString("password", password.getText().toString());
                                    editor.apply();
                                }
                                Intent intent = new Intent(getBaseContext(), MainService.class);
                                startActivity(intent);
                                finish();
                            }
                            else{
                                Toast.makeText(LoginActivity.this, "Fail to login", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    task.execute(LOGIN_URL);
                }
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        checkFlag = isChecked;
    }

    private boolean checkInfo(){
        if(id.getText().toString().trim().equals("")){
            Toast.makeText(this, "Please type id", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password.getText().toString().trim().equals("")){
            Toast.makeText(this, "Please type password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password.getText().toString().toString().length() < 8){
            Toast.makeText(this, "Password must over 8 word", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }
}
