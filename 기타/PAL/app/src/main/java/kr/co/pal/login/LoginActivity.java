package kr.co.pal.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import kr.co.pal.SplashActivity;
import kr.co.pal.util.BackPressCloseHandler;
import kr.co.pal.R;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.NetworkUtil;

public class LoginActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private final String LOGIN_URL = "http://52.79.150.65:9000/mauthUser";
//    private final String LOGIN_URL = "http://192.168.1.193:9000/mauthUser";
    private final String TAG = this.getClass().getName();
    private EditText id;
    private EditText password;
    private CheckBox remeber;
    private Button btnLogin;
    private HashMap<String, String> loginInfo;
//
    private HashMap<String, String> userInfo;
//
    private boolean checkFlag;
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;
    private BackPressCloseHandler backPressCloseHandler;

    private JSONArray json;
    private UserVO user = UserVO.getInstance();
    private Map<String,String> userVO = user.getUserVO();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        backPressCloseHandler = new BackPressCloseHandler(this);
        loginInfo = new HashMap<>();
//
        userInfo = new HashMap<>();
//
        id = (EditText) findViewById(R.id.id);
        password = (EditText) findViewById(R.id.password);

        //Remember CheckBox
        remeber = (CheckBox) findViewById(R.id.remember);
        remeber.setOnCheckedChangeListener(this);
        checkFlag = remeber.isChecked();


        //Login Button
        btnLogin = (Button) findViewById(R.id.btnLogin);
        pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        editor = pref.edit();


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkUtil.TYPE_NOT_CONNECTED == NetworkUtil.getConnectivityStatus(getBaseContext())){
                    Toast.makeText(getBaseContext(), "로그인을 하시려면\n네트워크에 연결되어있어야 합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(checkInfo()) {
                    loginInfo.put("id",id.getText().toString());
                    loginInfo.put("password",password.getText().toString());
                    Log.d(TAG,id.getText().toString());
                    Log.d(TAG,password.getText().toString());

                    PostResponseAsyncTask task = new PostResponseAsyncTask(LoginActivity.this,
                            loginInfo,
                            new AsyncResponse() {
                                @Override
                                public void processFinish(String res) {
                                    try {
                                        json = new JSONArray(res);
                                        JSONObject jsonObject = json.getJSONObject(0);
                                        Intent intent;
                                        Iterator<?> keys = jsonObject.keys();
                                        Log.d(TAG,jsonObject.toString());
                                        while( keys.hasNext() ) {
                                            String key = (String)keys.next();
                                            String value = jsonObject.get(key).toString();
                                            if(value.equals("error")){
                                                Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                                                userVO.clear();
                                                break;
                                            }else{
                                                userVO.put(key,value);
                                            }
                                        }

                                        if(userVO.size() > 0){
                                            if(checkFlag){
                                                editor.putString("id",userVO.get("id"));
                                                editor.putString("password",userVO.get("password"));
                                                editor.apply();
                                            }

                                            intent = new Intent(LoginActivity.this, ServiceActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    task.execute(LOGIN_URL);
                }
            }
        });
    }

    @Override
    public void onBackPressed(){
//        super.onBackPressed();
        backPressCloseHandler.onBackPressed();
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        checkFlag = isChecked;
    }

    private boolean checkInfo(){
        if(id.getText().toString().trim().equals("")){
            Toast.makeText(this, "Please type id", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password.getText().toString().trim().equals("")){
            Toast.makeText(this, "Please type password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password.getText().toString().toString().length() < 8){
            Toast.makeText(this, "Password must over 8 word", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }
}
