package kr.co.pal.main.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import kr.co.pal.R;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.util.LocationHelper;

import static java.lang.Thread.sleep;

public class GoogleMap extends Fragment implements OnMapReadyCallback {
    private final String TAG = this.getClass().getName();
    private LatLng latLng = new LatLng(37.494605, 127.027724);
    private static View rootView;
    private static com.google.android.gms.maps.GoogleMap googleMap;
    private static MapView mMapView;
    private CameraUpdate center;
    private Marker marker;


//    public LocationHelper.LocationResult locationResult;
    public LocationHelper locationHelper;

    public static GoogleMap newInstance(int sectionNumber) {
        GoogleMap fragment = new GoogleMap();
        Bundle args = new Bundle();
        args.putInt(ServiceActivity.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.e(TAG,"Call onCreate");
        super.onCreate(savedInstanceState);
    }
    public LocationHelper.LocationResult locationResult = new LocationHelper.LocationResult() {
        @Override
        public void gotLocation(Location location) {
            if (location != null) {
//                double latitude = location.getLatitude();
//                double longitude = location.getLongitude();
                latLng = new LatLng(location.getLatitude(),location.getLongitude());
                center = CameraUpdateFactory.newLatLng(latLng);
                googleMap.moveCamera(center);

//                CameraUpdate zoom=CameraUpdateFactory.zoomTo(18);
//                googleMap.animateCamera(zoom);
                if(marker != null)
                    marker.remove();
                marker = googleMap.addMarker(new MarkerOptions()
                                .position(latLng));
                Log.e(TAG, "lat: " + location.getLatitude() + ", long: " + location.getLongitude());
            } else {
                Log.e(TAG, "Location is null.");
            }
        }
    };

    @Override
    public void onResume(){
        super.onResume();
        locationHelper = new LocationHelper();
        locationHelper.getLocation(getActivity(), this.locationResult);
    }

    @Override
    public void onPause(){
        locationHelper.stopGettingLocationUpdates();
        super.onPause();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG,"Call onCreateView");
        try {
            rootView = inflater.inflate(R.layout.fragment_google_map, container, false);
            MapsInitializer.initialize(this.getActivity());
            mMapView = (MapView) rootView.findViewById(R.id.map);
            mMapView.onCreate(savedInstanceState);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }

    public void onDestroyView() {
        Log.e(TAG,"Call onDestroyView");
        super.onDestroyView();
        if (mMapView != null)
            mMapView.onDestroy();
    }


    @Override
    public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
//        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.mapstyle_grayscale);
        googleMap.setMapStyle(style);
    }

}
