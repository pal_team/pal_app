package kr.co.pal.main;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Map;

import kr.co.pal.R;
import kr.co.pal.main.fragments.GoogleMap;
import kr.co.pal.main.fragments.Settings;
import kr.co.pal.main.fragments.UnityScene;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.BackPressCloseHandler;
import kr.co.pal.util.LocationHelper;

public class ServiceActivity extends AppCompatActivity {
    public static final String ARG_SECTION_NUMBER = "section_number";
    private final String TAG = this.getClass().getName();
    private final int FIRST_PAGE = 0;
    private final int SECOND_PAGE = 1;
    private final int THIRD_PAGE = 2;

    private UnityScene unityScene;
    private GoogleMap googleMap;
    private Settings settings;

    public static Context mContext;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;



    private BackPressCloseHandler backPressCloseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        UserVO userVO = UserVO.getInstance();
        Map<String, String> user = userVO.getUserVO();

        backPressCloseHandler = new BackPressCloseHandler(this);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(SECOND_PAGE);
        mViewPager.setOnPageChangeListener(pageChangeListener);

        mContext = this;
    }


    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if(position == 0){
                Log.e(TAG,"Unity Scene selected");
                googleMap.locationHelper.stopGettingLocationUpdates();
            }

            if(position == 1){
                Log.e(TAG,"GoogleMap Scene selected");
                googleMap.locationHelper.getLocation(getBaseContext(), googleMap.locationResult);
            }

            if(position == 2){
                Log.e(TAG,"Setting Scene selected");
                googleMap.locationHelper.stopGettingLocationUpdates();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        backPressCloseHandler.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    unityScene = UnityScene.newInstance(position + 1);
                    return unityScene;
                case 1:
                    googleMap = GoogleMap.newInstance(position + 1);
                    return googleMap;
                case 2:
                    settings = Settings.newInstance(position + 1);
                    return settings;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}