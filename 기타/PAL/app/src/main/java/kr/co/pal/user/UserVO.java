package kr.co.pal.user;

import android.location.Location;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by JiHoon on 2017. 1. 6..
 */
public class UserVO {


    private Map<String, String> userVO;

    private static UserVO ourInstance = new UserVO();

    public static UserVO getInstance() {
        return ourInstance;
    }

    private UserVO() {
        userVO = new HashMap<>();
    }

    public Map<String, String> getUserVO() {
        return userVO;
    }

    public void setUserVO(Map<String, String> userVO) {
        this.userVO = userVO;
    }


}
