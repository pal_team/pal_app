package kr.co.pal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import kr.co.pal.login.LoginActivity;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.NetworkChangeReceiver;
import kr.co.pal.util.NetworkUtil;

public class SplashActivity extends AppCompatActivity {

    //Splash screen delay 1.3 seconds
    private final int SPLASH_DISPLAY_LENGTH = 1500;

    private final String LOGIN_URL = "http://52.79.150.65:9000/mauthUser";
//    private final String LOGIN_URL = "http://192.168.1.193:9000/mauthUser";
    private final String TAG = this.getClass().getName();
    private HashMap<String, String> loginInfo;
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;


    private JSONArray json;
    private UserVO user = UserVO.getInstance();
    private Map<String,String> userVO = user.getUserVO();

    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        /* SPLASH_DISPLAY_LENGTH 뒤에 메뉴 액티비티를 실행시키고 종료한다.*/
        new Handler().postDelayed(new Runnable() {
            Intent intent;
            @Override
            public void run() {

                //네트워크 연결 상태를 확인하고, 연결이 되어있지 않으면 서버에 요청하지 않고 바로 로드인 Activity로 이동한다.
                if(NetworkUtil.TYPE_NOT_CONNECTED == NetworkUtil.getConnectivityStatus(getBaseContext())){
                    Toast.makeText(SplashActivity.this, "네트워크 연결이 되어있지 않습니다.", Toast.LENGTH_SHORT).show();
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }


                /* 메뉴액티비티를 실행하고 로딩화면을 죽인다.*/
                //Login.conf 파일을 확인하고, 로그인 정보가 있으면 바로 ServiceActivity로 이동
                // 로그인 정보가 없다면 LoginActivity로 이동한다.
                loginInfo = new HashMap<>();
                pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
                editor = pref.edit();
                String checkId = pref.getString("id", "");
                String checkPassword = pref.getString("password", "");
                Log.d(TAG, checkId);
                Log.d(TAG, checkPassword);

                loginInfo.put("id", checkId);
                loginInfo.put("password", checkPassword);
                PostResponseAsyncTask task = new PostResponseAsyncTask(SplashActivity.this,
                        loginInfo,
                        new AsyncResponse() {
                            @Override
                            public void processFinish(String res) {
                                try {
                                    json = new JSONArray(res);
                                    JSONObject jsonObject = json.getJSONObject(0);
                                    Iterator<?> keys = jsonObject.keys();
                                    Log.d(TAG,jsonObject.toString());
                                    while( keys.hasNext() ) {
                                        String key = (String)keys.next();
                                        String value = jsonObject.get(key).toString();
                                        if(value.equals("error")){
                                            userVO.clear();
                                            intent = new Intent(SplashActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }else{
                                            userVO.put(key,value);
                                        }
                                    }

                                    if(userVO.size() > 0){
                                        intent = new Intent(SplashActivity.this, ServiceActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                task.execute(LOGIN_URL);
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
