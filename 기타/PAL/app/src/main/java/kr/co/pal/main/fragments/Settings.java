package kr.co.pal.main.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Map;

import kr.co.pal.R;
import kr.co.pal.login.LoginActivity;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.user.UserVO;

public class Settings extends Fragment {
    private final String TAG = this.getClass().getName();
    private View rootView;

    private TextView idText;
    private TextView nameText;
    private TextView emailText;
    private Map<String, String> userVO = UserVO.getInstance().getUserVO();

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;


    public static Settings newInstance(int sectionNumber) {
        Settings fragment = new Settings();
        Bundle args = new Bundle();
        args.putInt(ServiceActivity.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        idText = (TextView) rootView.findViewById(R.id.idText);
        nameText = (TextView) rootView.findViewById(R.id.nameText);
        emailText = (TextView) rootView.findViewById(R.id.emailText);

        idText.setText(userVO.get("id").toString());
        nameText.setText(userVO.get("nickname").toString());
        emailText.setText(userVO.get("email").toString());


        Button logOut = (Button) rootView.findViewById(R.id.logout);
        pref = getActivity().getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        Log.d(TAG,pref.getString("id",""));
        Log.d(TAG,pref.getString("password",""));
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor = pref.edit();
                editor.clear();
                editor.commit();
                Log.d(TAG,pref.getString("id",""));
                Log.d(TAG,pref.getString("password",""));
                Intent intent = new Intent(getActivity(),LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return rootView;
    }

    public void onDestroyView() {
        super.onDestroyView();
    }
}

