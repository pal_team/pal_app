package kr.co.pal.main.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.pal.R;
import kr.co.pal.main.ServiceActivity;

/**
 * Created by JiHoon on 2017. 1. 1..
 */

public class UnityScene extends Fragment {

    private final String TAG = this.getClass().getName();
    private View rootView;

    public static UnityScene newInstance(int sectionNumber) {
        UnityScene fragment = new UnityScene();
        Bundle args = new Bundle();
        args.putInt(ServiceActivity.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_unity, container, false);
        return rootView;
    }

    public void onDestroyView() {
        super.onDestroyView();
    }
}
