package kr.co.pal.listview.googlemap;

import android.graphics.drawable.Drawable;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by JiHoon on 2017. 1. 19..
 */

public class ListViewParent {
    private Drawable image;
    private String title;
    private String subtitle;
    private int number;
    private ArrayList<ListViewChild> children = new ArrayList<>();

    public static final Comparator<ListViewParent> ALPHA_COMPARATOR= new Comparator<ListViewParent>() {
        private final Collator sColltor = Collator.getInstance();
        @Override
        public int compare(ListViewParent o1, ListViewParent o2) {
            return sColltor.compare(o1.title,o2.title);
        }
    };

    @Override
    public String toString() {
        return "ProfileItems{" +
                "image=" + image +
                ", title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", number=" + number +
                ", children=" + children +
                '}';
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ArrayList<ListViewChild> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<ListViewChild> children) {
        this.children = children;
    }
}
