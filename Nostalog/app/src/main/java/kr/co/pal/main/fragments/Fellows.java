package kr.co.pal.main.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import kr.co.pal.R;
import kr.co.pal.login.LoginActivity;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.main.SettingsActivity;
import kr.co.pal.main.fragments.fellows.Followers;
import kr.co.pal.main.fragments.fellows.Follows;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.JsonHandler;

/**
 * Created by JiHoon on 2017. 1. 20..
 */

public class Fellows extends Fragment {
    private final String TAG = this.getClass().getName();
//    private final String url = "http://192.168.1.37:9000/";
    private final String url = "http://13.124.11.80:9000/";
    private final String GET_FELLOW = url + "getfollow";

    private UserVO user = UserVO.getInstance();
    private Map<String, String> userVO = user.getUserVO();
    private View rootView;
    private FragmentTabHost tabHost;
    public static Followers followers;
    public static Follows follows;

    public static Fellows newInstance(int sectionNumber) {
        Fellows fragment = new Fellows();
        Bundle args = new Bundle();
        args.putInt(ServiceActivity.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getFollow(getContext(), (HashMap<String, String>) userVO);
        tabHost = new FragmentTabHost(getActivity());
        tabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_fellows);

        follows = Follows.newInstance("Tab1", "Follow");
        followers = Followers.newInstance("Tab2", "Followers");

//        Bundle arg1 = new Bundle();
//        arg1.putInt("Arg for Frag1", 1);
//        tabHost.addTab(tabHost.newTabSpec("Tab1").setIndicator("팔로우"),
//                follows.getClass(), arg1);
//
//        Bundle arg2 = new Bundle();
//        arg2.putInt("Arg for Frag2", 2);
//        tabHost.addTab(tabHost.newTabSpec("Tab2").setIndicator("팔로워"),
//                followers.getClass(), arg2);

        return tabHost;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        tabHost = null;
    }

    public void getFollow(Context context, HashMap<String, String> info) {
        PostResponseAsyncTask getFollow = new PostResponseAsyncTask(context, info, false,
                new AsyncResponse() {
                    @Override
                    public void processFinish(String res) {
                        try {
                            JSONObject jsonObject = new JSONObject(res);
                            JSONArray jsonArray = jsonObject.getJSONArray("fellow");
                            JsonHandler jsonHandler = new JsonHandler();
                            List<Map<String, String>> dummyList = new ArrayList<>();
                            jsonHandler.jsonArr2List(jsonArray, dummyList);

                            user.getFollow().clear();
                            user.getFollower().clear();

                            for (int idx = 0; idx < dummyList.size(); idx++) {
                                if (dummyList.get(idx).get("follower") != null) {
                                    if (dummyList.get(idx).get("follower").equals(userVO.get("id"))) {
                                        user.getFollow().add(dummyList.get(idx));
                                        fileDown(dummyList.get(idx).get("follow"));
                                    } else {
                                        user.getFollower().add(dummyList.get(idx));
                                        fileDown(dummyList.get(idx).get("follower"));
                                    }
                                }
                                Log.e("follower",dummyList.get(idx).toString());
                            }
                            for(int idx = 0; idx < user.getFollow().size(); idx++){
                                String topic = user.getFollow().get(idx).get("follow");
                                Log.e(TAG,topic);
                                FirebaseMessaging.getInstance().subscribeToTopic(topic);
                            }

                            Bundle arg1 = new Bundle();
                            arg1.putInt("Arg for Frag1", 1);
                            tabHost.addTab(tabHost.newTabSpec("Tab1").setIndicator("팔로잉"),
                                    follows.getClass(), arg1);

                            Bundle arg2 = new Bundle();
                            arg2.putInt("Arg for Frag2", 2);
                            tabHost.addTab(tabHost.newTabSpec("Tab2").setIndicator("팔로워"),
                                    followers.getClass(), arg2);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        getFollow.execute(GET_FELLOW);
//        getFollow.execute("http://192.168.1.37:9000/getfollow");
    }

    public void fileDown(String id){
        String firebaseStorage = "gs://wattagam-160905.appspot.com";
        final FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReferenceFromUrl(firebaseStorage);
        final StorageReference userReference = storageReference.child("userImg/"+id+".png");
        final String fileName = id;
        try {
            final File localFile = File.createTempFile("images", "jpg");
            userReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    userReference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                        @Override
                        public void onSuccess(StorageMetadata storageMetadata) {
                            Log.e("MetaData",storageMetadata.getUpdatedTimeMillis()+"");
                        }
                    });
                    String filePath = localFile.getPath();

                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inSampleSize = 4;
                    Bitmap src = BitmapFactory.decodeFile(filePath, options);
                    Bitmap bitmap = Bitmap.createScaledBitmap(src, 100, 100, true);

                    storeImage(bitmap, fileName);

//                    src.recycle();
//                    src = null;
//
//                    bitmap.recycle();
//                    bitmap = null;
                }
            });
        } catch (Exception e) {
            Log.e(TAG,id+"님은 사진을 등록하지 않았습니다.");
        }
    }
    private  File getOutputMediaFile(String fileName){
        try {
            File mediaStorageDir = new File(getActivity().getFilesDir()+"/userImg");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            File mediaFile;
            mediaFile = new File(mediaStorageDir.getPath() +"/"+fileName+ ".png");
            return mediaFile;
        }catch (NullPointerException e){
            e.printStackTrace();
            return null;
        }
    }

    private void storeImage(Bitmap image, String fileName) {
        File pictureFile = getOutputMediaFile(fileName);
        if (pictureFile == null) {
            Log.d(TAG,"Error creating media file, check storage permissions: ");
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }
}