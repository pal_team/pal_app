package kr.co.pal.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import kr.co.pal.R;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.main.SettingsActivity;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.BackPressCloseHandler;
import kr.co.pal.util.JsonHandler;
import kr.co.pal.util.NetworkUtil;


public class LoginActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

//    private final String url = "http://52.79.150.65:9000/";
//    private final String url = "http://192.168.1.33:9000/";
    @BindString(R.string.url) String url;
    @Bind(R.id.btnJoin) Button joinUs;
    private String loginUrl;
//    private final String url = "http://192.168.0.54:9000/";
//    private final String LOGIN_URL = url+"mauthUser";

    private final String TAG = this.getClass().getName();
    private EditText id;
    private EditText password;
    private CheckBox remeber;
    private Button btnLogin;
    private HashMap<String, String> loginInfo;
    //
    private HashMap<String, String> userInfo;
    //
    private boolean checkFlag;
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;
    private BackPressCloseHandler backPressCloseHandler;
    private Intent intent;

    private UserVO user = UserVO.getInstance();
    private Map<String,String> userVO = user.getUserVO();


    private CallbackManager callbackManager;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginUrl = url+"mauthUser";

        callbackManager = CallbackManager.Factory.create();


        backPressCloseHandler = new BackPressCloseHandler(this);
        loginInfo = new HashMap<>();
        userInfo = new HashMap<>();
        id = (EditText) findViewById(R.id.id);
        password = (EditText) findViewById(R.id.password);

        //Remember CheckBox
        remeber = (CheckBox) findViewById(R.id.remember);
        remeber.setOnCheckedChangeListener(this);
        checkFlag = remeber.isChecked();

        btnLogin = (Button) findViewById(R.id.btnLogin);

        pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        editor = pref.edit();

        joinUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(LoginActivity.this, JoinUsActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.TYPE_NOT_CONNECTED == NetworkUtil.getConnectivityStatus(getBaseContext())) {
                    Toast.makeText(getBaseContext(), "로그인을 하시려면\n네트워크에 연결되어있어야 합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (checkInfo()) {
                    loginInfo.put("id", id.getText().toString());
                    loginInfo.put("password", password.getText().toString());
                    loginInfo.put("firebaseToken", FirebaseInstanceId.getInstance().getToken());
                    Log.d(TAG, id.getText().toString());
                    Log.d(TAG, password.getText().toString());

                    PostResponseAsyncTask task = new PostResponseAsyncTask(LoginActivity.this,
                            loginInfo,
                            new AsyncResponse() {
                                @Override
                                public void processFinish(String res) {
                                    try {
                                        JSONObject json = new JSONObject(res);
                                        String state = json.getString("state");
                                        if (state.equals("success")) {
                                            JSONObject userInfo =
                                                    json.getJSONArray("userInfo").getJSONObject(0);

                                            JsonHandler jsonHandler = new JsonHandler();
                                            jsonHandler.jsonObj2Map(userInfo,userVO);
                                            Log.e(TAG,userVO.toString());

                                            if(checkFlag) {
                                                editor.putString("id", userVO.get("id"));
                                                editor.putString("password", userVO.get("password"));
                                                editor.apply();
                                            }
                                            intent = new Intent(LoginActivity.this, ServiceActivity.class);
                                            finish();
                                            startActivity(intent);

                                        } else if(state.equals("invalid")){
                                            AlertDialog.Builder ab = new AlertDialog.Builder(LoginActivity.this);
                                            ab.setMessage("Nostalog의 운영정책에 위배되는 행위를 하여 일시정지된 계정입니다.\n고객센터로 문의주세요");
                                            ab.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            ab.show();

                                        } else{
                                            Toast.makeText(LoginActivity.this,
                                                    "아이디와 비밀번호를 확인해주세요.",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    task.execute(loginUrl);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        backPressCloseHandler.onBackPressed();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        checkFlag = isChecked;
    }

    private boolean checkInfo() {
        if (id.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Please type id", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Please type password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.getText().toString().toString().length() < 8) {
            Toast.makeText(this, "Password must over 8 word", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }
}
