package kr.co.pal.main.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.unity3d.player.UnityPlayer;

import kr.co.pal.R;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.user.UserVO;

/**
 * Created by JiHoon on 2017. 1. 1..
 */

public class UnityScene extends Fragment {

    private final String TAG = this.getClass().getName();
    private View rootView;
    public static UnityPlayer unity;
    private LinearLayout linearLayout;
    private UserVO userVO = UserVO.getInstance();

    public static UnityScene newInstance(int sectionNumber) {
        UnityScene fragment = new UnityScene();
        Bundle args = new Bundle();
        args.putInt(ServiceActivity.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        Log.e(TAG,"Unity onDestroy");
        unity.quit();
        super.onDestroy();
    }

    // Pause Unity
    @Override
    public void onPause() {
        Log.e(TAG,"Unity onPause");
        super.onPause();
        unity.pause();
    }

    // Resume Unity
    @Override
    public void onResume() {
        Log.e(TAG,"Unity onResume");
        super.onResume();
        unity.resume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        unity = new UnityPlayer(getActivity());
        Log.e(TAG,"Unity onCreate");

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Clear low profile flags to apply non-fullscreen mode before splash screen
        showSystemUi();
        addUiVisibilityChangeListener();

        UnityPlayer.UnitySendMessage("UserInfo","setUserId", userVO.getUserVO().get("id"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_unity, container,false);

        linearLayout = (LinearLayout)rootView.findViewById(R.id.unityLayout);
        linearLayout.addView(unity.getView());
//        rootView = unity.getView();

        Log.e(TAG,"Unity onCreateView");
        return rootView;
    }


    public void onDestroyView() {
        linearLayout.removeAllViews();
        super.onDestroyView();
        Log.e(TAG,"Unity onDestroyView");
    }






    private static int getLowProfileFlag()
    {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                ?
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_FULLSCREEN
                :
                View.SYSTEM_UI_FLAG_LOW_PROFILE;
    }

    private void showSystemUi()
    {
        // Works from API level 11
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            return;

        unity.setSystemUiVisibility(unity.getSystemUiVisibility() & ~getLowProfileFlag());
    }

    private void addUiVisibilityChangeListener()
    {
        // Works from API level 11
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            return;

        unity.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
        {
            @Override
            public void onSystemUiVisibilityChange(final int visibility)
            {
                // Whatever changes - force status/nav bar to be visible
                showSystemUi();
            }
        });
    }
}
