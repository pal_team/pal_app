package kr.co.pal.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.File;
import java.net.URLDecoder;

import kr.co.pal.R;
import kr.co.pal.listview.googlemap.ListViewAdapter;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.main.fragments.GoogleMap;

/**
 * @author ton1n8o - antoniocarlos.dev@gmail.com on 6/13/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyGcmListenerService";

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }
    final Handler newFollow = new Handler() {
        public void handleMessage(Message msg) {
            GoogleMap fragment = ServiceActivity.googleMap;
            if(fragment != null) {
            }
        }
    };
    final Handler newNote = new Handler() {
        public void handleMessage(Message msg) {
            Bundle args = msg.getData();
            GoogleMap fragment = ServiceActivity.googleMap;
            String title = args.getString("title");
            title = title.split(" ")[0];
            String id = title.substring(0,title.length()-2);
            String addr = args.getString("body");

            if(fragment != null) {
                ListViewAdapter adapter = fragment.getAdapter();
                int currentNum = adapter.getGroup(0).getNumber();
                adapter.getGroup(0).setNumber(++currentNum);
                adapter.getGroup(0).setSubtitle(
                        "최근 "+ id +"님이 " +
                        addr.split(" ")[2] +
                        "에 쪽지를 남기셨습니다.");

                String imgPath = getFilesDir().getPath() + "/userImg/" + id + ".png";
                File file = new File(imgPath);
                Bitmap bitmap = null;

                if (file.exists()) {
                    bitmap= BitmapFactory.decodeFile(file.getPath());
                }
                else {
                    bitmap = getBitmap(R.drawable.person);
                }
                adapter.addChild(0, bitmap, id+"님의 "+addr +" 새쪽지", "" , getDrawable(R.drawable.ic_check_24dp));
                adapter.dataChange();
            }
        }
    };
    @Override
    public void onMessageReceived(RemoteMessage message) {
//        FollowerAdapter followerAdapter = new FollowerAdapter(ServiceActivity.mContext);
//        followerAdapter.addItem(ServiceActivity.mContext.getDrawable(R.drawable.minkyung),
//                message.getNotification().getBody().split(" ")[0]);
//        followerAdapter.dataChange();
        Log.e(TAG,"Message Recive");

        String image = message.getNotification().getIcon();
        final String title = message.getNotification().getTitle();
        final String text = message.getNotification().getBody();
        String sound = message.getNotification().getSound();

        if(title.contains("팔로우")){
            new Thread() {
                public void run() {
                    Message msg = newFollow.obtainMessage();
                    newFollow.sendMessage(msg);
                }
            }.start();
        }
        if(title.contains("쪽지")){
            new Thread() {
                public void run() {
                    Bundle args = new Bundle();
                    args.putString("title", title);
                    args.putString("body", text);
                    Message msg = newNote.obtainMessage();
                    msg.setData(args);
                    newNote.sendMessage(msg);
                }
            }.start();
        }

        int id = 0;
        Object obj = message.getData().get("id");
        if (obj != null) {
            id = Integer.valueOf(obj.toString());
        }
        if(title == null)
            this.sendNotification(text);
        else
            this.sendNotification(new NotificationData(image, id, title, text, sound));
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, ServiceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.noti_icon)
                .setColor(getResources().getColor(R.color.colorPrimaryLight))
                .setContentTitle("Nostalog")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void sendNotification(NotificationData notificationData) {

        Intent intent = new Intent(this, ServiceActivity.class);
        intent.putExtra(NotificationData.TEXT, notificationData.getTextMessage());

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = null;
        try {

            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.noti_icon)
                    .setColor(getResources().getColor(R.color.colorPrimaryLight))
                    .setContentTitle(URLDecoder.decode(notificationData.getTitle(), "UTF-8"))
                    .setContentText(URLDecoder.decode(notificationData.getTextMessage(), "UTF-8"))
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (notificationBuilder != null) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificationData.getId(), notificationBuilder.build());
        } else {
            Log.d(TAG, "Não foi possível criar objeto notificationBuilder");
        }
    }
}
