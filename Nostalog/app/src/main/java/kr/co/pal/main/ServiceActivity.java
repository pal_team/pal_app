package kr.co.pal.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.unity3d.player.UnityPlayer;

import java.io.File;
import java.util.List;
import java.util.Map;

import kr.co.pal.R;
import kr.co.pal.httprequest.AsyncRequest;
import kr.co.pal.main.fragments.Dummy;
import kr.co.pal.main.fragments.Fellows;
import kr.co.pal.main.fragments.GoogleMap;
import kr.co.pal.main.fragments.UnityScene;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.BackPressCloseHandler;
import kr.co.pal.util.ForceKillDetector;
import kr.co.pal.util.ImageMapper;

public class ServiceActivity extends AppCompatActivity {
    public static final String ARG_SECTION_NUMBER = "section_number";
    private final String TAG = this.getClass().getName();
    private final String[] PAGE_TITLE = {"Unity", "Map", "Fellows"};
    private final int FIRST_PAGE = 0;
    private final int SECOND_PAGE = 1;
    private final int THIRD_PAGE = 2;

    public static UnityScene unityScene;
    public static GoogleMap googleMap;
    public static Fellows fellows;

    private Dummy dummy;

    public static Context mContext;

    private UserVO user = UserVO.getInstance();
    private Map<String, String> userVO = user.getUserVO();
    private SectionsPagerAdapter mSectionsPagerAdapter;
    public ViewPager mViewPager;
    private SharedPreferences pref;

    private BackPressCloseHandler backPressCloseHandler;

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(this, ForceKillDetector.class));
        setContentView(R.layout.activity_service);

        String imgPath = getApplication().getFilesDir().getPath() + "/userImg";

        File file = new File(imgPath);
        File[] images = file.listFiles();
        if (images != null) {
            Log.e(TAG, "Images : " + images.length);
            for (int idx = 0; idx < images.length; idx++) {
                ImageMapper mapper = new ImageMapper();
                String fileName = images[idx].getName().split("\\.")[0];
                Bitmap bitmap = BitmapFactory.decodeFile(images[idx].getPath());

                mapper.setId(fileName);
                mapper.setImage(bitmap);
                user.getImageMap().add(mapper);
            }
        }

        file = new File(imgPath + "/" + userVO.get("id") + ".png");
        if (file.exists()) {
            user.setProfileImg(BitmapFactory.decodeFile(file.getPath()));
        } else {
            user.setProfileImg(getBitmap(R.drawable.person));
        }


        //Permission을 확인함
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS}, 1);
            }
        }

        backPressCloseHandler = new BackPressCloseHandler(this);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(SECOND_PAGE);
        mViewPager.setOnPageChangeListener(pageChangeListener);
        Log.e(TAG, FirebaseInstanceId.getInstance().getToken());
        mContext = this;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        unityScene.unity.configurationChanged(newConfig);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        unityScene.unity.windowFocusChanged(hasFocus);
    }


    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }


        @Override
        public void onPageSelected(int position) {
            if (position == 0) {
                Log.e(TAG, "Unity Scene selected");
                if (googleMap.locationHelper != null)
                    googleMap.locationHelper.stopLocationUpdates();
//                    googleMap.locationHelper.stopLocationUpdates();
                UnityPlayer.UnitySendMessage("Object Generator", "resetBtn", "");
            } else if (position == 1) {
                Log.e(TAG, "GoogleMap Scene selected");
                googleMap.locationHelper.startLocationUpdates(googleMap.locationResult);
//                googleMap.locationHelper.startLocationUpdates(googleMap.locationResult);
            } else if (position == 2) {
                Log.e(TAG, "Setting Scene selected");
                if (googleMap.locationHelper != null)
                    googleMap.locationHelper.stopLocationUpdates();
//                    googleMap.locationHelper.stopLocationUpdates();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        backPressCloseHandler.onBackPressed();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        String id = pref.getString("id", "");
        System.out.println("User id : " + id);
        if (id.equals("")) {
            AsyncRequest request = new AsyncRequest();
            request.removeToken(this, UserVO.getInstance().getUserVO());

            List<Map<String, String>> follow = UserVO.getInstance().getFollow();
            for (int idx = 0; idx < follow.size(); idx++) {
                String topic = follow.get(idx).get("follow");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
            }
        }

        super.onDestroy();
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    unityScene = UnityScene.newInstance(position + 1);
                    return unityScene;
//                    dummy = dummy.newInstance(position + 1);
//                    return dummy;
                case 1:
                    googleMap = GoogleMap.newInstance(position + 1);
                    return googleMap;
                case 2:
                    fellows = Fellows.newInstance(position + 1);
                    return fellows;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return PAGE_TITLE[0];
                case 1:
                    return PAGE_TITLE[1];
                case 2:
                    return PAGE_TITLE[2];
            }
            return null;
        }
    }
}