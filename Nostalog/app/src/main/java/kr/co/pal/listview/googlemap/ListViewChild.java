package kr.co.pal.listview.googlemap;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by JiHoon on 2017. 1. 19..
 */

public class ListViewChild {
    private Bitmap profileImg;
    private String title;
    private String subtitle;
    private Drawable stateImg;


    @Override
    public String toString() {
        return "ListViewChild{" +
                "profileImg=" + profileImg +
                ", title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", stateImg=" + stateImg +
                '}';
    }

    public Bitmap getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(Bitmap profileImg) {
        this.profileImg = profileImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Drawable getStateImg() {
        return stateImg;
    }

    public void setStateImg(Drawable stateImg) {
        this.stateImg = stateImg;
    }
}
