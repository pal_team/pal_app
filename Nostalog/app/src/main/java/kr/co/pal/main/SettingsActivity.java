package kr.co.pal.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.BindColor;
import butterknife.BindString;
import butterknife.ButterKnife;
import kr.co.pal.R;
import kr.co.pal.login.JoinUsActivity;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.ImagePicker;

public class SettingsActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getName();
    private final int TAKE_GALLERY = 21;
    private static final int MAX_IMAGE_SIZE = 172;

    @BindColor(R.color.warning)
    int warning;
    @BindColor(R.color.alert)
    int alert;
    @BindColor(R.color.green)
    int green;
    @BindString(R.string.url)
    String url;


    @Bind(R.id.userImg) ImageView userImg;
    @Bind(R.id.logout) Button logOut;
    @Bind(R.id.idText) TextView idText;
    @Bind(R.id.nameText) TextView nameText;
    @Bind(R.id.nickNameText) EditText nickNameText;
    @Bind(R.id.password) EditText password;
    @Bind(R.id.password2) EditText password2;
    @Bind(R.id.checkPassword) TextView checkPassword;
    @Bind(R.id.emailText) EditText emailText;
    @Bind(R.id.submit) Button submit;
    private Activity activity;


    @BindString(R.string.storage) String firebaseStorage;

    private UserVO user = UserVO.getInstance();
    private Map<String, String> userVO = user.getUserVO();

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Bitmap bmp;
    private boolean passwordConfirm;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX
            = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static final Pattern VALID_PASSWORD_REGEX_ALPHA_NUM
            = Pattern.compile("^[a-zA-Z0-9!@.#$%^&*?_~]{8,16}$"); // 4자리 ~ 16자리까지 가능


    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean validatePassword(String pwStr) {
        Matcher matcher = VALID_PASSWORD_REGEX_ALPHA_NUM.matcher(pwStr);
        return matcher.matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_GALLERY) {
            if (data != null) {
                bmp = ImagePicker.getImageFromResult(this, resultCode, data);
                ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream) ;
                byte[] byteArray = stream.toByteArray() ;

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageReference = storage.getReferenceFromUrl(firebaseStorage);
                final StorageReference userReference = storageReference.child("userImg/"+userVO.get("id")+".png");

                UploadTask uploadTask = userReference.putBytes(byteArray);
//                UploadTask uploadTask = userReference.putFile(data.getData());
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Uploading");
                progressDialog.show();

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(SettingsActivity.this, "사진이 정상적으로 업로드되지 않았습니다.", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(SettingsActivity.this, "사진이 정상적으로 업로드되었습니다.", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        userReference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                            @Override
                            public void onSuccess(StorageMetadata storageMetadata) {
                                Log.e(TAG,String.valueOf(storageMetadata.getUpdatedTimeMillis()));
                            }
                        });
                        try {
//                            Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), photo);
                            storeImage(bmp);
                            String imgPath = getApplication().getFilesDir().getPath() + "/userImg/" + userVO.get("id")+".png";
                            user.setProfileImg(BitmapFactory.decodeFile(imgPath));
                            userImg.setImageBitmap(user.getProfileImg());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        System.out.println("Upload is " + progress + "% done");
                    }
                });
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

//    private  File getOutputMediaFile(){
//        File mediaStorageDir = new File(getApplication().getFilesDir(),"userImg");
//
//        if (! mediaStorageDir.exists()){
//            if (! mediaStorageDir.mkdirs()){
//                return null;
//            }
//        }
//        Log.e(TAG,mediaStorageDir.getPath().toString());
//        File mediaFile;
//        mediaFile = new File(mediaStorageDir.getPath()+userVO.get("id")+".png");
//        return mediaFile;
//    }

    private  File getOutputMediaFile(){
        try {
            File mediaStorageDir = new File(getApplication().getFilesDir()+"/userImg");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            File mediaFile;
            mediaFile = new File(mediaStorageDir.getPath() +"/"+userVO.get("id")+ ".png");
            return mediaFile;
        }catch (NullPointerException e){
            e.printStackTrace();
            return null;
        }
    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,"Error creating media file, check storage permissions: ");
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            Log.e(TAG,pictureFile.getPath());
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        activity = this;
        userImg.setImageBitmap(user.getProfileImg());

        mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInAnonymously:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInAnonymously", task.getException());
                            Toast.makeText(SettingsActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        Set<String> keySet = userVO.keySet();
        Iterator<?> keys = keySet.iterator();
        String name = "";
        String firstName="";
        String lastName="";
        Log.e(TAG,userVO.toString());
        while(keys.hasNext()){
            String key = (String)keys.next();

            if(key.contains("id")){
                idText.setText(userVO.get(key).toString());
            }else if(key.contains("nickname")){
                nickNameText.setText(userVO.get(key).toString());
            }else if(key.contains("email")){
                emailText.setText(userVO.get(key).toString());
            }
            if(key.contains("password")){
                password.setText(userVO.get(key).toString());
            }
            if(key.contains("first")){
                firstName = userVO.get(key).toString();
            }
            if(key.contains("last")){
                lastName = userVO.get(key).toString();
            }
            name = firstName + lastName;
        }
        nameText.setText(name);

        userImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, TAKE_GALLERY);
            }
        });

        pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder ab = new AlertDialog.Builder(SettingsActivity.this);
                ab.setMessage("자동로그인 정보를 삭제하시겠습니까?");
                ab.setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor = pref.edit();
                        editor.clear();
                        editor.commit();
                        dialog.dismiss();
                        Toast.makeText(SettingsActivity.this, "자동로그인 정보를 삭제하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
                ab.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                ab.show();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nickName = String.valueOf(nickNameText.getText());
                String email = String.valueOf(emailText.getText());
                String pw = String.valueOf(password.getText());
                if (pw.equals("")) {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(alert);
                    checkPassword.setText("비밀번호를 입력하세요.");
                    passwordConfirm = false;
                }
                if(nickName.equals("")){
                    Toast.makeText(activity, "별명을 입력해 주세요.",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!validateEmail(email)) {
                    Toast.makeText(activity, "이메일주소가 올바르지 않습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (passwordConfirm) {
                    Map param = new HashMap<>();
                    param.clear();
                    param.put("id",userVO.get("id"));
                    param.put("password", pw);
                    param.put("nickname",nickName);
                    param.put("email", email);

                    Log.e(TAG,userVO.toString());

                    edit(getBaseContext(), (HashMap<String, String>) param);
                }
            }
        });
        password.addTextChangedListener(watcher1);
        password2.addTextChangedListener(watcher2);
    }

    TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String txt = String.valueOf(s);
            String pw2 = String.valueOf(password2.getText());
            if (txt.length() < 8) {
                checkPassword.setVisibility(View.VISIBLE);
                checkPassword.setTextColor(alert);
                checkPassword.setText("비밀번호가 너무 짧습니다.");
                passwordConfirm = false;
            } else if (!txt.equals(pw2)) {
                checkPassword.setVisibility(View.VISIBLE);
                checkPassword.setTextColor(alert);
                checkPassword.setText("비밀번호 재확인을 확인해주세요.");
                passwordConfirm = false;
            } else {
                if (validatePassword(txt)) {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(green);
                    checkPassword.setText("사용할 수 있는 비밀번호 입니다.");
                    passwordConfirm = true;
                } else {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(alert);
                    checkPassword.setText("비밀번호 형식이 올바르지 않습니다.");
                    passwordConfirm = false;
                }
            }

        }
    };
    TextWatcher watcher2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String pw = String.valueOf(password.getText());
            String txt = String.valueOf(s);
            if (pw.length() < 8) {
                checkPassword.setVisibility(View.VISIBLE);
                checkPassword.setTextColor(alert);
                checkPassword.setText("비밀번호가 너무 짧습니다.");
                passwordConfirm = false;
            } else if (!txt.equals(pw)) {
                checkPassword.setVisibility(View.VISIBLE);
                checkPassword.setTextColor(alert);
                checkPassword.setText("비밀번호 재확인을 확인해주세요.");
                passwordConfirm = false;
            } else {
                if (validatePassword(pw)) {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(green);
                    checkPassword.setText("사용할 수 있는 비밀번호 입니다.");
                    passwordConfirm = true;
                } else {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(alert);
                    checkPassword.setText("비밀번호 형식이 올바르지 않습니다.");
                    passwordConfirm = false;
                }
            }
        }
    };

    public void edit(final Context context, HashMap<String, String> info) {
        PostResponseAsyncTask editUser = new PostResponseAsyncTask(context,
                info, false, new AsyncResponse() {
            @Override
            public void processFinish(String res) {
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    String state = jsonObject.getString("state");
                    if (state.equals("success")) {
                        String nickName = String.valueOf(nickNameText.getText());
                        String email = String.valueOf(emailText.getText());
                        String pw = String.valueOf(password.getText());

                        userVO.put("nickname",nickName);
                        userVO.put("password",pw);
                        userVO.put("email",email);
                        AlertDialog.Builder ab = new AlertDialog.Builder(SettingsActivity.this);
                        ab.setMessage("회원정보가 성공적으로 수정되었습니다!");
                        ab.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        ab.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        editUser.execute(url + "editUser");
//        join.execute("http://192.168.1.37:9000/joinUser");
    }

}

