package kr.co.pal.httprequest;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.Toast;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import kr.co.pal.R;
import kr.co.pal.listview.googlemap.ListViewAdapter;
import kr.co.pal.listview.googlemap.ListViewParent;
import kr.co.pal.login.LoginActivity;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.JsonHandler;

import static android.content.ContentValues.TAG;

/**
 * Created by JiHoon on 2017. 1. 31..
 */


public class AsyncRequest {

    private final String url = "http://13.124.11.80:9000/";
//    private final String url = "http://192.168.1.37:9000/";

    private final String LOGIN_URL = url+"mauthUser";
    private final String GET_FOLLOW = url+"getfollow";
    private final String GET_FOLLOWER = url+"getfollower";
    private final String GET_MYNOTE = url+"getmynote";
    private final String REMOVE_TOKEN = url+"applogout";
    private final String REMOVE_FOLLOW = url+"removefollow";
    private final String ADD_FOLLOW = url+"addfollow";

    private JSONArray json;
    private UserVO user = UserVO.getInstance();
    private Map<String,String> userVO = user.getUserVO();

    private Map<String,String> followVO;
    private List<Map<String,String>> follow = user.getFollow();

    private Map<String,String> followerVO;
    private List<Map<String,String>> follower = user.getFollower();

    private Map<String,String> myNoteVO;
    private List<Map<String,String>> myNote = user.getMyNote();



    public void getFollower(Context context, HashMap<String, String> info){
        PostResponseAsyncTask getFollower = new PostResponseAsyncTask(context,
                info,false, new AsyncResponse() {
                    @Override
                    public void processFinish(String res) {
                        try {
                            follower.clear();
                            json = new JSONArray(res);
                            for(int idx = 0; idx < json.length(); idx++){
                                JSONObject jsonObject = json.getJSONObject(idx);
                                Iterator<?> keys = jsonObject.keys();
                                followerVO = new HashMap<>();
                                while (keys.hasNext()) {
                                    String key = (String) keys.next();
                                    String value = jsonObject.get(key).toString();
                                    followerVO.put(key,value);
                                }
                                follower.add(followerVO);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        getFollower.execute(GET_FOLLOWER);
    }


    public void addFollow(Context context, HashMap<String, String> info) {
        PostResponseAsyncTask addFollow = new PostResponseAsyncTask(context, info, false,
                new AsyncResponse() {
                    @Override
                    public void processFinish(String res) {
                    }
                });
        addFollow.execute(ADD_FOLLOW);
    }

    public void removeFollow(Context context, HashMap<String, String> info) {
        PostResponseAsyncTask removeFollow = new PostResponseAsyncTask(context, info, false,
                new AsyncResponse() {
                    @Override
                    public void processFinish(String res) {
                    }
                });
        removeFollow.execute(REMOVE_FOLLOW);
    }

    public void removeToken(Context context, Map<String, String> info){
        PostResponseAsyncTask removeToken = new PostResponseAsyncTask(context,
                (HashMap<String, String>) info, false,
                new AsyncResponse() {
                    @Override
                    public void processFinish(String res){
                    }
                });
        removeToken.execute(REMOVE_TOKEN);
    }
}
