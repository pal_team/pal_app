package kr.co.pal.main.fragments.fellows;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.File;
import java.util.List;
import java.util.Map;

import kr.co.pal.R;
import kr.co.pal.listview.fellow.follower.FollowerAdapter;
import kr.co.pal.user.UserVO;


public class Followers extends Fragment {
    private final String TAG = this.getClass().getName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private View rootView;
    private ListView myList;
    private FollowerAdapter adapter;
    private UserVO userVO = UserVO.getInstance();
    private List<Map<String, String>> follower;
    private Bitmap bitmap = null;

//    private OnFragmentInteractionListener mListener;


    public FollowerAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(FollowerAdapter adapter) {
        this.adapter = adapter;
    }

    public Followers() {
    }

    public static Followers newInstance(String param1, String param2) {
        Followers fragment = new Followers();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        follower = userVO.getFollower();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(bitmap != null){
            bitmap.recycle();
            bitmap = null;
        }
    }

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fellows_item_list, container, false);
        myList = (ListView) rootView.findViewById(R.id.listView);
        adapter = new FollowerAdapter(rootView.getContext());
        for (int idx = 0; idx < follower.size(); idx++) {
            String imgPath = getActivity().getFilesDir().getPath() + "/userImg/" + follower.get(idx).get("follower") + ".png";
            File file = new File(imgPath);
            if (file.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap src = BitmapFactory.decodeFile(file.getPath(), options);
                bitmap = Bitmap.createScaledBitmap(src, 100, 100, true);
            }
            else {
                Bitmap bmp = getBitmap(R.drawable.person);
                bitmap = Bitmap.createScaledBitmap(bmp, 100, 100, true);
            }
            adapter.addItem(bitmap,
                    follower.get(idx).get("follower"));
        }
        myList.setAdapter(adapter);

        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(view.getId() == R.id.followBtn)
                    Log.e(TAG, position + "");
            }
        });

        return rootView;
    }

//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }
//
//    public interface OnFragmentInteractionListener {
//        void onFragmentInteraction(Uri uri);
//    }
}
