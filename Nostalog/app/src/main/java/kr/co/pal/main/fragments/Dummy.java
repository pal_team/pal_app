package kr.co.pal.main.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.pal.R;
import kr.co.pal.main.ServiceActivity;


public class Dummy extends Fragment {

    public Dummy() {
        // Required empty public constructor
    }

    public static Dummy newInstance(int sectionNumber) {
        Dummy fragment = new Dummy();
        Bundle args = new Bundle();
        args.putInt(ServiceActivity.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dummy, container, false);
    }

}
