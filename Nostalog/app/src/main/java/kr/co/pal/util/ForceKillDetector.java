package kr.co.pal.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;
import java.util.Map;

import kr.co.pal.httprequest.AsyncRequest;
import kr.co.pal.user.UserVO;

/**
 * Created by JiHoon on 2017. 2. 28..
 */

public class ForceKillDetector extends Service {
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("ForceKillDetector","onCreate");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("ForceKillDetector","killed");

        SharedPreferences pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
        String id = pref.getString("id", "");
        if (id.equals("")) {
            AsyncRequest request = new AsyncRequest();
            request.removeToken(getBaseContext(), UserVO.getInstance().getUserVO());

            List<Map<String, String>> follow = UserVO.getInstance().getFollow();
            for (int idx = 0; idx < follow.size(); idx++) {
                String topic = follow.get(idx).get("follow");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
            }
        }

        stopSelf();
    }
}
