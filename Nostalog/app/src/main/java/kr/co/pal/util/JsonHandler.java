package kr.co.pal.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by JiHoon on 2017. 2. 6..
 */

public class JsonHandler {
    public void jsonArr2List(JSONArray jsonArray, List<Map<String,String>> list) throws JSONException {
        for(int idx = 0 ; idx < jsonArray.length(); idx++){
            JSONObject jsonObject = jsonArray.getJSONObject(idx);
            Map<String, String> temp = new HashMap<>();
            jsonObj2Map(jsonObject,temp);
            list.add(temp);
        }
    }

    public void jsonObj2Map(JSONObject jsonObject, Map<String,String> map) throws JSONException {
        Iterator<?> keys = jsonObject.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            String value = jsonObject.get(key).toString();
            map.put(key, value);
        }
    }
}
