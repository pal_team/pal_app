package kr.co.pal.listview.googlemap;

import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.pal.R;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.main.fragments.GoogleMap;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by JiHoon on 2017. 1. 19..
 */

public class ListViewAdapter extends BaseExpandableListAdapter {
    private ArrayList<ListViewParent> parents = new ArrayList<>();
    private static ListViewAdapter mAdapter;
    private Context mContext;
    public ListViewAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getGroupCount() {
        return parents.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return parents.get(groupPosition).getChildren().size();
    }

    @Override
    public ListViewParent getGroup(int groupPosition) {
        return parents.get(groupPosition);
    }

    @Override
    public ListViewChild getChild(int groupPosition, int childPosition) {
        return parents.get(groupPosition).getChildren().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public void addItem(Drawable icon, String title, String subtitle, int number) {
        ListViewParent data = new ListViewParent();
        data.setImage(icon);
        data.setTitle(title);
        data.setSubtitle(subtitle);
        data.setNumber(number);
        parents.add(data);

    }
    public void addChild(int position, Bitmap img, String title, String subtitle, Drawable icon){
        ListViewChild data = new ListViewChild();
        data.setProfileImg(img);
        data.setTitle(title);
        data.setSubtitle(subtitle);
        data.setStateImg(icon);
        parents.get(position).getChildren().add(0,data);
    }


    public void sort() {
        Collections.sort(parents, ListViewParent.ALPHA_COMPARATOR);
        dataChange();
    }

    public void removeChild(int parent, int position){
        parents.get(parent).getChildren().remove(position);
        dataChange();
    }

    public void remove(int position) {
        parents.remove(position);
        dataChange();
    }

    public void dataChange() {
        this.notifyDataSetChanged();
    }
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ParentViewHolder holder;
        if (convertView == null) {
            holder = new ParentViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customlist_parent_items,null);
            holder.pIcon = (ImageView) convertView.findViewById(R.id.list_item_img);
            holder.pTitle = (TextView) convertView.findViewById(R.id.list_item_title);
            holder.pSubtitle = (TextView) convertView.findViewById(R.id.list_item_subtitle);
            holder.pNumber = (TextView) convertView.findViewById(R.id.list_item_number);
            convertView.setTag(holder);
        } else {
            holder = (ParentViewHolder) convertView.getTag();
        }
        ListViewParent data = parents.get(groupPosition);
        holder.pIcon.setImageDrawable(data.getImage());
        holder.pTitle.setText(data.getTitle());
        holder.pSubtitle.setText(data.getSubtitle());
        holder.pNumber.setText(data.getNumber() + "");

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder holder;
        if (convertView == null) {
            holder = new ChildViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customlist_child_items, null);
            holder.cProfileImg = (CircleImageView) convertView.findViewById(R.id.child_item_profile_img);
            holder.cTitle = (TextView) convertView.findViewById(R.id.child_item_title);
//            holder.cSubtitle = (TextView) convertView.findViewById(R.id.child_item_subtitle);
            holder.cStateImg = (ImageView) convertView.findViewById(R.id.child_item_state);
            convertView.setTag(holder);
        } else {
            holder = (ChildViewHolder) convertView.getTag();
        }
        ListViewChild data = parents.get(groupPosition).getChildren().get(childPosition);
        holder.cProfileImg.setImageBitmap(data.getProfileImg());
        holder.cTitle.setText(data.getTitle());
//        holder.cSubtitle.setText(data.getSubtitle());
        holder.cStateImg.setImageDrawable(data.getStateImg());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private class ParentViewHolder {
        private ImageView pIcon;
        private TextView pTitle;
        private TextView pSubtitle;
        private TextView pNumber;
    }

    private class ChildViewHolder {
        private CircleImageView cProfileImg;
        private TextView cTitle;
        private TextView cSubtitle;
        private ImageView cStateImg;
    }
}
