package kr.co.pal.main.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;
import com.unity3d.player.UnityPlayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.pal.R;
import kr.co.pal.listview.googlemap.ListViewAdapter;
import kr.co.pal.listview.googlemap.ListViewChild;
import kr.co.pal.listview.googlemap.ListViewParent;
import kr.co.pal.main.ProfileActivity;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.main.SettingsActivity;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.GPSUtil;
import kr.co.pal.util.JsonHandler;
import kr.co.pal.util.LocationUtil;

public class GoogleMap extends Fragment implements OnMapReadyCallback {
//        private final String GET_INFO = "http://192.168.1.37:9000/getinfoList";
    private final String GET_INFO = "http://13.124.11.80:9000/getinfoList";
//    @BindString(R.string.url) String url;

    private final String TAG = this.getClass().getName();
    private final int[] ICON_SET = {
            R.drawable.ic_face_24dp,
            R.drawable.ic_history_24dp,
            R.drawable.ic_offline_pin_24dp};
    private LatLng latLng = new LatLng(37.49445447462366, 127.02763259410858);
    public static View rootView;
    private com.google.android.gms.maps.GoogleMap googleMap;
    private MapView mMapView;
    private CameraUpdate center;
    private Marker marker;
    private Geocoder geocoder;
    private ListViewAdapter mAdapter;
    private ExpandableListView mListView;
    private TextView currentAddr;
    //    public LocationHelper locationHelper;
    public LocationUtil locationHelper;
    private TextView profileName;
    private UserVO user = UserVO.getInstance();
    private Map<String, String> userVO = user.getUserVO();
    private List<Map<String, String>> myNote = user.getMyNote();
    private List<Map<String, String>> myNews = user.getNews();
    private CircleImageView myLocation;

    private ImageButton switchPage;
    private ImageButton settingBtn;
    private CircleImageView profileImg;
    private boolean firstGet;
    private LatLng recentNoteLocation;
    private Marker recentNoteMarker;
    private Bitmap bitmap = null;

    public static GoogleMap newInstance(int sectionNumber) {
        GoogleMap fragment = new GoogleMap();
        Bundle args = new Bundle();
        args.putInt(ServiceActivity.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isGooglePlayServicesAvailable()) {
            Log.e(TAG,"google play service error");
        }
        locationHelper = new LocationUtil(getActivity(),locationResult);
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

    }

    public LocationUtil.LocationResult locationResult = new LocationUtil.LocationResult() {
        @Override
        public void gotLocation(Location location) {
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
                if(!firstGet) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                    firstGet = true;
                }
//                center = CameraUpdateFactory.newLatLng(latLng);
//                googleMap.moveCamera(center);

                String addr = findAddress(latitude, longitude);
                currentAddr.setText(addr);

                if (marker != null)
                    marker.remove();
                marker = googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("flag_icon", 100, 100))));
                Log.e(TAG, "lat: " + location.getLatitude() + ", long: " + location.getLongitude());
                UnityPlayer.UnitySendMessage("UserInfo", "setLatitude", String.valueOf(latitude));
                UnityPlayer.UnitySendMessage("UserInfo", "setLongitude", String.valueOf(longitude));
                UnityPlayer.UnitySendMessage("UserInfo", "setLocation", addr);

            } else {
                Log.e(TAG, "Location is null.");
            }
        }
    };


    public Bitmap resizeMapIcons(String iconName, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(iconName, "drawable", getActivity().getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

    private String findAddress(double lat, double lng) {
        String currentLocationAddress = null;
        List<Address> address;
        try {
            if (geocoder != null) {
                address = geocoder.getFromLocation(lat, lng, 1);
                if (address != null && address.size() > 0) {
                    currentLocationAddress = address.get(0).getAddressLine(0).toString();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return currentLocationAddress;
    }

    @Override
    public void onStart() {
        super.onStart();
        locationHelper.googleApiConnection();
    }

    @Override
    public void onStop() {
        super.onStop();
        locationHelper.googleApiDisconnection();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "GoogleMap onResume");
        if (locationHelper.isConnected()) {
            locationHelper.startLocationUpdates(locationResult);
        }
//        locationHelper.startLocationService(locationResult);
        try {
            profileImg.setImageBitmap(user.getProfileImg());
            Log.e(TAG, "Picture change success");
        } catch (NullPointerException e) {
            Log.e(TAG, getContext().getFilesDir().getPath() + "/" + userVO.get("id"));
        }

        String nickName = userVO.get("nickname");
        Log.e(TAG,nickName);
        if(nickName.equals("null")){
            Log.e(TAG,"hello world");
        }
        profileName.setText("@" + userVO.get("nickname"));
    }

    @Override
    public void onPause() {

        super.onPause();
        if(locationHelper.isConnected())
            locationHelper.stopLocationUpdates();
//        locationHelper.stopLocationService();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "Call onCreateView");
        rootView = inflater.inflate(R.layout.fragment_google_map, container, false);
        try {

            MapsInitializer.initialize(this.getActivity());
            mMapView = (MapView) rootView.findViewById(R.id.map);
            mMapView.onCreate(savedInstanceState);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
/////////////////////////////////////get xml items..///////////////////////////////////

        currentAddr = (TextView) rootView.findViewById(R.id.current_addr);
        profileName = (TextView) rootView.findViewById(R.id.profile_name);
        myLocation = (CircleImageView) rootView.findViewById(R.id.myLocation);
        switchPage = (ImageButton) rootView.findViewById(R.id.switchPage);
        settingBtn = (ImageButton) rootView.findViewById(R.id.settingBtn);
        profileImg = (CircleImageView) rootView.findViewById(R.id.profile_img);

///////////////////////////////////////////////////////////////////////////////////////////

        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }
        });

        mListView = (ExpandableListView) rootView.findViewById(R.id.bottomList);

        mAdapter = new ListViewAdapter(rootView.getContext());

        mAdapter.addItem(
                getActivity().getDrawable(ICON_SET[0]),
                "알림",
                "최근 팔로우가 남긴 쪽지가 없습니다.",
                0
        );

        mAdapter.addItem(
                getActivity().getDrawable(ICON_SET[1]),
                "나의 쪽지",
                "최근 남긴 쪽지가 없습니다.",
                0
        );

        mAdapter.addItem(
                getActivity().getDrawable(ICON_SET[2]),
                "이벤트",
                "최근 이벤트가 없습니다.",
                0
        );
        getInfo(getActivity(), userVO);

        mListView.setAdapter(mAdapter);

        mListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });
        mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                switch (groupPosition) {
                    case 0:
                        ListViewChild child = mAdapter.getChild(groupPosition, childPosition);
                        String str = child.getTitle();
                        str = str.split(" ")[0];
                        String idStr = str.substring(0, str.length() - 2);
                        Intent intent = new Intent(getActivity(), ProfileActivity.class);
                        intent.putExtra("id", idStr);
                        parent.collapseGroup(groupPosition);
                        startActivity(intent);
                        break;
                    case 1:
                        String lat = myNote.get(myNote.size()-1-childPosition).get("lat");
                        String lng = myNote.get(myNote.size()-1-childPosition).get("lng");
                        recentNoteLocation = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                        recentNoteMarker = googleMap.addMarker(new MarkerOptions()
                                .position(recentNoteLocation));
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(recentNoteLocation, 17));
                        parent.collapseGroup(groupPosition);
                        break;
                }
                return false;
            }
        });

        switchPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);

            }
        });

        profileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra("id", userVO.get("id"));
                startActivity(intent);

            }
        });


        return rootView;
    }

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    public void onDestroyView() {
        Log.e(TAG, "Call onDestroyView");
        super.onDestroyView();

        if (mMapView != null) {
            mMapView.onDestroy();
            mMapView = null;
        }

        if (googleMap != null)
            googleMap.clear();

        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
    }


    @Override
    public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
//        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.mapstyle_grayscale);
        googleMap.setMapStyle(style);
    }

    public ListViewAdapter getAdapter() {
        return mAdapter;
    }

    public void getInfo(Context context, Map<String, String> info) {
        PostResponseAsyncTask getInfoList = new PostResponseAsyncTask(context, (HashMap<String, String>) info,
                false, new AsyncResponse() {
            @Override
            public void processFinish(String res) {

                try {
                    JSONObject jsonObject = new JSONObject(res);
                    if (!jsonObject.getString("news_state").equals("error") &&
                            !jsonObject.getString("note_state").equals("error")) {
                        JsonHandler jsonHandler = new JsonHandler();

                        ListViewParent firstItem = mAdapter.getGroup(0);
                        ListViewParent secondItem = mAdapter.getGroup(1);

                        if (!jsonObject.getString("news_state").equals("null")) {
                            JSONArray news = jsonObject.getJSONArray("news");
                            myNews.clear();
                            jsonHandler.jsonArr2List(news, myNews);
                            if (myNews.size() != 0) {
                                firstItem.setNumber(myNews.size());
                                firstItem.setSubtitle(
                                        "최근 " + myNews.get(myNews.size() - 1).get("id") + "님이 " +
                                                myNews.get(myNews.size() - 1).get("location_name").split(" ")[2] +
                                                "에 쪽지를 남기셨습니다."

                                );
                            }

                            for (int idx = 0; idx < myNews.size(); idx++) {
                                String id = myNews.get(idx).get("id");
                                String imgPath = getActivity().getFilesDir().getPath() + "/userImg/" + id + ".png";
                                File file = new File(imgPath);
                                if (file.exists()) {
                                    BitmapFactory.Options options = new BitmapFactory.Options();
//                                    options.inSampleSize = 4;
                                    Bitmap src = BitmapFactory.decodeFile(file.getPath(), options);
                                    bitmap = Bitmap.createScaledBitmap(src, 100, 100, true);
                                } else {
                                    Bitmap bmp = getBitmap(R.drawable.person);
                                    bitmap = Bitmap.createScaledBitmap(bmp, 100, 100, true);
                                }
                                mAdapter.addChild(
                                        0,
                                        bitmap,
                                        id + "님의 " +
                                                myNews.get(idx).get("location_name") + " 새쪽지",
                                        "",
                                        getActivity().getDrawable(R.drawable.ic_check_24dp)
                                );
                            }
                        }

                        if (!jsonObject.getString("note_state").equals("null")) {
                            JSONArray note = jsonObject.getJSONArray("note");
                            myNote.clear();
                            jsonHandler.jsonArr2List(note, myNote);
                            if (myNote.size() != 0) {
                                secondItem.setNumber(myNote.size());
                                secondItem.setSubtitle(
                                        "최근 " +
                                                myNote.get(myNote.size() - 1).get("location_name").split(" ")[2] +
                                                "에 쪽지를 남기셨습니다."
                                );
                            }
                            Bitmap bitmap = getBitmap(R.drawable.ic_person_pin_24dp);
                            for (int idx = 0; idx < myNote.size(); idx++) {
                                mAdapter.addChild(
                                        1,
                                        bitmap,
                                        myNote.get(idx).get("location_name"),
                                        null, null
                                );
                            }
                        }

                        mAdapter.dataChange();
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        getInfoList.execute(GET_INFO);
    }
}


// 셈플
//mAdapter.addChild(
//        0,
//        getActivity().getDrawable(R.drawable.minkyung),
//        "minkyung님의 서울특별시 서초구 서초동 74길 33 새쪽지",
//        "",
//        getActivity().getDrawable(R.drawable.ic_favorite_24dp)
//        );