package kr.co.pal.listview.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import kr.co.pal.R;
import kr.co.pal.httprequest.AsyncRequest;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by JiHoon on 2017. 1. 19..
 */

public class ProfileAdapter extends BaseAdapter {

    private Context mContext;
    private AsyncRequest request;
    private List<ProfileItems> items = new ArrayList<>();

    public ProfileAdapter(Context mContext) {
        this.mContext = mContext;
        request = new AsyncRequest();
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ProfileItems getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addItem(String title, String date, LatLng latLng){
        ProfileItems data = new ProfileItems();
        data.setTitle(title);
        data.setDate(date);
        data.setLatLng(latLng);
        items.add(0,data);
    }
    public void dataChange(){
        this.notifyDataSetChanged();
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customlist_profile_items,null);
            holder.title = (TextView) convertView.findViewById(R.id.profile_item_title);
            holder.date = (TextView) convertView.findViewById(R.id.profile_item_date);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        ProfileItems data = items.get(position);
        holder.title.setText(data.getTitle());
        holder.date.setText(data.getDate());
        return convertView;
    }

    private class Holder{
        private TextView title;
        private TextView date;
    }
}
