package kr.co.pal.listview.fellow.follow;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.co.pal.R;
import kr.co.pal.httprequest.AsyncRequest;
import kr.co.pal.listview.profile.ProfileAdapter;
import kr.co.pal.main.ProfileActivity;
import kr.co.pal.user.UserVO;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by JiHoon on 2017. 1. 19..
 */

public class FollowAdapter extends BaseAdapter {

    private Context mContext;
    private List<FollowItems> items = new ArrayList<>();
    private List<Map<String, String>> follow = UserVO.getInstance().getFollow();
    private HashMap<String,String> requestParam = new HashMap<>();
    private AsyncRequest request;

    public FollowAdapter(Context mContext) {
        this.mContext = mContext;
        request = new AsyncRequest();
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void remove(int position){
        String whom = items.get(position).getName();
        Toast.makeText(mContext, whom+"님을 더 이상 팔로우 하지 않습니다", Toast.LENGTH_SHORT).show();
        requestParam.put("id",UserVO.getInstance().getUserVO().get("id"));
        requestParam.put("follow",whom);
        request.removeFollow(mContext,requestParam);
        follow.remove(position);
        items.remove(position);
        dataChange();
    }

    public void addItem(Bitmap icon, String name){
        FollowItems data = new FollowItems();
        data.setImage(icon);
        data.setName(name);
        items.add(data);
    }
    public void dataChange(){
        this.notifyDataSetChanged();
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customlist_fellow_items,null);
            holder.icon = (ImageView) convertView.findViewById(R.id.profile_img);
            holder.name = (TextView) convertView.findViewById(R.id.profile_name);
            holder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProfileActivity.class);
                    intent.putExtra("id",holder.name.getText());
                    mContext.startActivity(intent);
                }
            });
            holder.delete = (Button) convertView.findViewById(R.id.followBtn);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    remove(position);
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        FollowItems data = items.get(position);
        holder.icon.setImageBitmap(data.getImage());
        holder.name.setText(data.getName());
        holder.delete.setText(data.getBtnText());

        return convertView;
    }

    private class Holder{
        private ImageView icon;
        private TextView name;
        private Button delete;
    }
}
