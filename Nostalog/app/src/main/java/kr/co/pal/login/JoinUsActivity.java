package kr.co.pal.login;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.BindColor;
import butterknife.BindString;
import butterknife.ButterKnife;
import kr.co.pal.R;
import kr.co.pal.util.GPSUtil;
import kr.co.pal.util.LocationUtil;

public class JoinUsActivity extends AppCompatActivity {


    private final String TAG = this.getClass().getName();
    @BindString(R.string.url)
    String url;
    @BindColor(R.color.warning)
    int warning;
    @BindColor(R.color.alert)
    int alert;
    @BindColor(R.color.green)
    int green;


    @Bind(R.id.idText) EditText idText;
    @Bind(R.id.searchId) Button searchId;
    @Bind(R.id.checkId) TextView checkId;
    @Bind(R.id.password) EditText password;
    @Bind(R.id.password2) EditText password2;
    @Bind(R.id.checkPassword) TextView checkPassword;
    @Bind(R.id.firstName) EditText firstNameText;
    @Bind(R.id.lastName) EditText lastNameText;
    @Bind(R.id.age) EditText ageText;
    @Bind(R.id.emailText) EditText emailText;
    @Bind(R.id.nickName) EditText nickNameText;
    @Bind(R.id.male) TextView male;
    @Bind(R.id.female) TextView female;
    @Bind(R.id.locale) TextView locale;
    @Bind(R.id.submit) Button submitBtn;

    private String type = "male";
    private Geocoder geocoder;
    private LocationUtil locationHelper;
    private Map<String, String> param;
    private boolean idConfirm;
    private boolean passwordConfirm;
    private Activity activity;


    public static final Pattern VALID_ID_REGEX
            = Pattern.compile("^[a-zA-Z0-9!@.^*?_~]{2,16}$");

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX
            = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static final Pattern VALID_PASSWORD_REGEX_ALPHA_NUM
            = Pattern.compile("^[a-zA-Z0-9!@.#$%^&*?_~]{8,16}$"); // 4자리 ~ 16자리까지 가능

    public static boolean validateId(String idStr) {
        Matcher matcher = VALID_ID_REGEX.matcher(idStr);
        return matcher.find();
    }

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean validatePassword(String pwStr) {
        Matcher matcher = VALID_PASSWORD_REGEX_ALPHA_NUM.matcher(pwStr);
        return matcher.matches();
    }

    public LocationUtil.LocationResult locationResult = new LocationUtil.LocationResult() {
        @Override
        public void gotLocation(Location location) {
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                String addr = findAddress(latitude, longitude);
                locale.setText(addr);
                Log.e("Locale : ", latitude + ", " + longitude + "\n" + addr);
            }
        }
    };


    private String findAddress(double lat, double lng) {
        String currentLocationAddress = null;
        List<Address> address;
        try {
            if (geocoder != null) {
                address = geocoder.getFromLocation(lat, lng, 1);
                if (address != null && address.size() > 0) {
                    currentLocationAddress = address.get(0).getAddressLine(0).toString();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return currentLocationAddress;
    }
    @Override
    public void onResume(){
        super.onResume();
//        locationHelper.startLocationUpdates(locationResult);
        if (locationHelper.isConnected()) {
            locationHelper.startLocationUpdates(locationResult);
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        locationHelper.googleApiConnection();
    }
    @Override
    public void onStop() {
        super.onStop();
        locationHelper.googleApiDisconnection();
    }
    @Override
    public void onPause() {
        if(locationHelper.isConnected()){
            locationHelper.stopLocationUpdates();
        }
//        locationHelper.stopLocationService();
        super.onPause();
    }

    protected void onDestroy() {
        super.onDestroy();

    }
    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joinus);
        ButterKnife.bind(this);
        activity = this;
        param = new HashMap<>();
        if (!isGooglePlayServicesAvailable()) {
            Log.e(TAG,"google play service error");
        }

        geocoder = new Geocoder(this, Locale.getDefault());
        locationHelper = new LocationUtil(getBaseContext(),locationResult);
        searchId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = String.valueOf(idText.getText());
                if (id.equals("")) {
                    checkId.setText("아이디를 입력하세요.");
                    checkId.setTextColor(alert);
                    idConfirm = false;
                } else {
                    param.put("id", id);
                    checkId(getBaseContext(), (HashMap<String, String>) param);
                }
            }
        });

        idText.addTextChangedListener(idWatcher);
        password.addTextChangedListener(watcher1);
        password2.addTextChangedListener(watcher2);

        submitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String id = String.valueOf(idText.getText());
                String pw = String.valueOf(password.getText());
                String firstName = String.valueOf(firstNameText.getText());
                String lastName = String.valueOf(lastNameText.getText());
                String age = String.valueOf(ageText.getText());
                String nickName = String.valueOf(nickNameText.getText());
                String sex = type;
                String email = String.valueOf(emailText.getText());
                String region = String.valueOf(locale.getText());
                String lan = Locale.getDefault().getDisplayLanguage();

                if (!validateId(id)) {
                    Toast.makeText(activity, "아이디 형식이 올바르지 않습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pw.equals("")) {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(alert);
                    checkPassword.setText("비밀번호를 입력하세요.");
                    passwordConfirm = false;
                }
                if (!validateEmail(email)) {
                    Toast.makeText(activity, "이메일주소가 올바르지 않습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(nickName.equals("")){
                    Toast.makeText(activity, "별명을 입력해 주세요.",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(region.contains("GPS")){
                    Toast.makeText(activity, "GPS 정보를 받아오고 있습니다.\nGPS 모듈은 실외에서 더 잘 동작합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (idConfirm && passwordConfirm) {
                    param.clear();
                    param.put("id", id);
                    param.put("password", pw);
                    param.put("firstName", firstName);
                    param.put("lastName", lastName);
                    param.put("age", age);
                    param.put("nickname",nickName);
                    param.put("sex", sex);
                    param.put("email", email);
                    param.put("location", region);
                    param.put("language", lan);
                    join(getBaseContext(), (HashMap<String, String>) param);
                }
            }
        });
    }

    public void switchSex(View view) {
        switch (view.getId()) {
            case R.id.male:
                type = "M";
                male.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                female.setBackgroundColor(getResources().getColor(R.color.colorBackground));
                break;
            case R.id.female:
                type = "F";
                female.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                male.setBackgroundColor(getResources().getColor(R.color.colorBackground));
                break;
        }
    }

    TextWatcher idWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String id = String.valueOf(s);
            if (s.length() < 3) {
                checkId.setText("아이디는 최소한 3글자 이상의 문자열이어야 합니다.");
                checkId.setTextColor(alert);
                idConfirm = false;
            } else if (!validateId(id)) {
                checkId.setText("아이디에 부적합한 문자가 포함되어 있습니다.");
                checkId.setTextColor(alert);
                idConfirm = false;
            } else {
                checkId.setText("ID검색 버튼을 눌러 중복확인을 해주세요.");
                checkId.setTextColor(warning);
                idConfirm = false;
            }
        }
    };

    TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String txt = String.valueOf(s);
            String pw2 = String.valueOf(password2.getText());
            if (txt.length() < 8) {
                checkPassword.setVisibility(View.VISIBLE);
                checkPassword.setTextColor(alert);
                checkPassword.setText("비밀번호가 너무 짧습니다.");
                passwordConfirm = false;
            } else if (!txt.equals(pw2)) {
                checkPassword.setVisibility(View.VISIBLE);
                checkPassword.setTextColor(alert);
                checkPassword.setText("비밀번호 재확인을 확인해주세요.");
                passwordConfirm = false;
            } else {
                if (validatePassword(txt)) {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(green);
                    checkPassword.setText("사용할 수 있는 비밀번호 입니다.");
                    passwordConfirm = true;
                } else {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(alert);
                    checkPassword.setText("비밀번호 형식이 올바르지 않습니다.");
                    passwordConfirm = false;
                }
            }

        }
    };
    TextWatcher watcher2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String pw = String.valueOf(password.getText());
            String txt = String.valueOf(s);
            if (pw.length() < 8) {
                checkPassword.setVisibility(View.VISIBLE);
                checkPassword.setTextColor(alert);
                checkPassword.setText("비밀번호가 너무 짧습니다.");
                passwordConfirm = false;
            } else if (!txt.equals(pw)) {
                checkPassword.setVisibility(View.VISIBLE);
                checkPassword.setTextColor(alert);
                checkPassword.setText("비밀번호 재확인을 확인해주세요.");
                passwordConfirm = false;
            } else {
                if (validatePassword(pw)) {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(green);
                    checkPassword.setText("사용할 수 있는 비밀번호 입니다.");
                    passwordConfirm = true;
                } else {
                    checkPassword.setVisibility(View.VISIBLE);
                    checkPassword.setTextColor(alert);
                    checkPassword.setText("비밀번호 형식이 올바르지 않습니다.");
                    passwordConfirm = false;
                }
            }
        }
    };


    public void join(final Context context, HashMap<String, String> info) {
        PostResponseAsyncTask join = new PostResponseAsyncTask(context,
                info, false, new AsyncResponse() {
            @Override
            public void processFinish(String res) {
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    String state = jsonObject.getString("state");
                    if (state.equals("success")) {
                        AlertDialog.Builder ab = new AlertDialog.Builder(JoinUsActivity.this);
                        ab.setMessage("회원가입이 성공적으로\n완료되었습니다!!");
                        ab.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                                dialog.dismiss();
                            }
                        });
                        ab.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        join.execute(url + "joinUser");
//        join.execute("http://192.168.1.37:9000/joinUser");
    }

    public void checkId(Context context, HashMap<String, String> info) {
        PostResponseAsyncTask idCheck = new PostResponseAsyncTask(context,
                info, false, new AsyncResponse() {
            @Override
            public void processFinish(String res) {
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    String state = jsonObject.getString("state");
                    if (state.equals("exist")) {
                        checkId.setText("이미 존재하는 아이디입니다.");
                        checkId.setTextColor(alert);
                        idConfirm = false;

                    } else {
                        checkId.setText("사용하실 수 있는 아이디입니다.");
                        checkId.setTextColor(green);
                        idConfirm = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        idCheck.execute(url + "checkId");
//        idCheck.execute("http://192.168.1.37:9000/checkId");
    }
}
