package kr.co.pal.user;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.co.pal.util.ImageMapper;

/**
 * Created by JiHoon on 2017. 1. 6..
 */
public class UserVO {

    private Map<String, String> userVO;
    private Bitmap profileImg;
    private List<Map<String, String>> follow;
    private List<Map<String, String>> follower;
    private List<Map<String,String>> myNote;
    private List<Map<String, String>> news;
    private List<ImageMapper> imageMap;

    public Map<String, String> getUserVO() {
        return userVO;
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "userVO=" + userVO +
                ", follow=" + follow +
                ", follower=" + follower +
                ", myNote=" + myNote +
                '}';
    }

    public Bitmap getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(Bitmap profileImg) {
        this.profileImg = profileImg;
    }

    public List<Map<String, String>> getNews() {
        return news;
    }

    public void setNews(List<Map<String, String>> news) {
        this.news = news;
    }

    public void setUserVO(Map<String, String> userVO) {
        this.userVO = userVO;
    }

    public List<Map<String, String>> getFollow() {
        return follow;
    }

    public void setFollow(List<Map<String, String>> follow) {
        this.follow = follow;
    }

    public List<Map<String, String>> getFollower() {
        return follower;
    }

    public void setFollower(List<Map<String, String>> follower) {
        this.follower = follower;
    }

    public List<Map<String, String>> getMyNote() {
        return myNote;
    }

    public void setMyNote(List<Map<String, String>> myNote) {
        this.myNote = myNote;
    }

    private static UserVO ourInstance = new UserVO();

    public static UserVO getInstance() {
        return ourInstance;
    }


    public List<ImageMapper> getImageMap() {
        return imageMap;
    }

    public void setImageMap(List<ImageMapper> imageMap) {
        this.imageMap = imageMap;
    }

    private UserVO() {
        userVO = new HashMap<>();
        follow = new ArrayList<>();
        follower = new ArrayList<>();
        myNote = new ArrayList<>();
        news = new ArrayList<>();
        imageMap = new ArrayList<>();
    }
}
