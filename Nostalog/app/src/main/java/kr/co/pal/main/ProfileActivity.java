package kr.co.pal.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.pal.R;
import kr.co.pal.listview.profile.ProfileAdapter;
import kr.co.pal.listview.profile.ProfileItems;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.JsonHandler;

public class ProfileActivity extends AppCompatActivity implements OnMapReadyCallback{
    private final String TAG = this.getClass().getName();
    @BindString(R.string.url) String url;
    @Bind(R.id.googleMap) MapView mMapView;
    @Bind(R.id.mylistView) ListView mListView;
    @Bind(R.id.idText) TextView idText;
    @Bind(R.id.numOfNote) TextView numOfNote;
    @Bind(R.id.profile_img) CircleImageView profileImg;

    private String id;
    private Map<String ,String> requestParam;
    private List<Map<String,String>> noteList;
    private String getMyNoteUrl;

    private ProfileAdapter mAdapter;

    private com.google.android.gms.maps.GoogleMap googleMap;
    private Marker marker;
    private LatLng latLng = new LatLng(37.494605, 127.027724);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        getMyNoteUrl = url+"getmynote";
        noteList = new ArrayList<>();
        mAdapter = new ProfileAdapter(this);

        Intent intent = getIntent();
        if(intent != null){
            requestParam = new HashMap<>();
            id = intent.getStringExtra("id");
            requestParam.put("id",id);
        }
        if(requestParam != null) {
            getMyNote(this, (HashMap<String, String>) requestParam);
        }

        idText.setText(id);
        String imgPath = getFilesDir().getPath() + "/userImg/" + id + ".png";
        File file = new File(imgPath);
        Bitmap bitmap = null;
        if (file.exists()) {
            bitmap= BitmapFactory.decodeFile(file.getPath());
        }
        else {
            bitmap = getBitmap(R.drawable.person);
        }
        profileImg.setImageBitmap(bitmap);

        try {
            MapsInitializer.initialize(this);
            mMapView.onCreate(savedInstanceState);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProfileItems item = mAdapter.getItem(position);
                LatLng latLng = item.getLatLng();
                if(marker != null)
                    marker.remove();

                marker = googleMap.addMarker(new MarkerOptions()
                        .position(latLng));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));

            }
        });
    }

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_grayscale);
        googleMap.setMapStyle(style);
    }

    public void getMyNote(Context context, HashMap<String, String> info){
        PostResponseAsyncTask getMyNote = new PostResponseAsyncTask(context,
                info,false, new AsyncResponse() {
            @Override
            public void processFinish(String res) {
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray note = jsonObject.getJSONArray("note");
                    noteList.clear();
                    JsonHandler jsonHandler = new JsonHandler();
                    jsonHandler.jsonArr2List(note, noteList);

                    for (int idx = 0; idx < noteList.size(); idx++) {
                        Map<String,String> data = noteList.get(idx);
                        double lat = Double.parseDouble(data.get("lat"));
                        double lng = Double.parseDouble(data.get("lng"));
                        LatLng latLng = new LatLng(lat,lng);
                        mAdapter.addItem(
                                data.get("location_name"),
                                data.get("created_date"),
                                latLng
                        );
                    }
                    mAdapter.dataChange();
                    numOfNote.setText("총 "+noteList.size()+"개의 쪽지");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        getMyNote.execute(getMyNoteUrl);
    }
}
