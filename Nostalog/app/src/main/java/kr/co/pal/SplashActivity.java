package kr.co.pal;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindString;
import butterknife.ButterKnife;
import kr.co.pal.login.LoginActivity;
import kr.co.pal.main.ServiceActivity;
import kr.co.pal.httprequest.AsyncRequest;
import kr.co.pal.user.UserVO;
import kr.co.pal.util.JsonHandler;
import kr.co.pal.util.NetworkUtil;

public class SplashActivity extends AppCompatActivity {

    //Splash screen delay 1.3 seconds
    private final int SPLASH_DISPLAY_LENGTH = 1500;

//        private final String url = "http://52.79.150.65:9000/";
//    private final String url = "http://192.168.1.33:9000/";
    @BindString(R.string.url) String url;
//    private final String url = "http://192.168.0.54:9000/";
    private String loginUrl;

    private final String TAG = this.getClass().getName();

    private HashMap<String, String> loginInfo;
    private HashMap<String, String> userInfo;

    private SharedPreferences pref;

    private UserVO user = UserVO.getInstance();
    private Map<String, String> userVO = user.getUserVO();

    public SplashActivity() {
    }


    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        loginUrl = url + "mauthUser";

        //Permission을 확인함
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS}, 1);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }


        /* SPLASH_DISPLAY_LENGTH 뒤에 메뉴 액티비티를 실행시키고 종료한다.*/
        new Handler().postDelayed(new Runnable() {
            Intent intent;

            @Override
            public void run() {

                //네트워크 연결 상태를 확인하고, 연결이 되어있지 않으면 서버에 요청하지 않고 바로 로드인 Activity로 이동한다.
                if (NetworkUtil.TYPE_NOT_CONNECTED == NetworkUtil.getConnectivityStatus(getBaseContext())) {
                    Toast.makeText(SplashActivity.this, "네트워크 연결이 되어있지 않습니다.", Toast.LENGTH_SHORT).show();
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }

                /* 메뉴액티비티를 실행하고 로딩화면을 죽인다.*/
                //Login.conf 파일을 확인하고, 로그인 정보가 있으면 바로 ServiceActivity로 이동
                // 로그인 정보가 없다면 LoginActivity로 이동한다.
                loginInfo = new HashMap<>();
                userInfo = new HashMap<>();
                pref = getSharedPreferences("Login.conf", Context.MODE_PRIVATE);
                String checkId = pref.getString("id", "");
                String checkPassword = pref.getString("password", "");
                if(checkId.equals("")){
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
                Log.d(TAG, checkId);
                Log.d(TAG, checkPassword);

                loginInfo.put("id", checkId);
                loginInfo.put("password", checkPassword);
                loginInfo.put("firebaseToken", FirebaseInstanceId.getInstance().getToken());

                PostResponseAsyncTask task = new PostResponseAsyncTask(SplashActivity.this,
                        loginInfo,
                        new AsyncResponse() {
                            @Override
                            public void processFinish(String res) {
                                try {
                                    JSONObject json = new JSONObject(res);
                                    if (json.getString("state").equals("success")) {

                                        JSONObject userInfo =
                                                json.getJSONArray("userInfo").getJSONObject(0);
                                        JsonHandler jsonHandler = new JsonHandler();
                                        jsonHandler.jsonObj2Map(userInfo,userVO);

                                        intent = new Intent(SplashActivity.this, ServiceActivity.class);
                                        finish();
                                        startActivity(intent);
                                    }else{
                                        intent = new Intent(SplashActivity.this, LoginActivity.class);
                                        finish();
                                        startActivity(intent);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(SplashActivity.this, "서버에 접속할 수 없습니다.", Toast.LENGTH_SHORT).show();
                                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                                    finish();
                                    startActivity(intent);
                                }
                            }
                        });
                task.execute(loginUrl);
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
