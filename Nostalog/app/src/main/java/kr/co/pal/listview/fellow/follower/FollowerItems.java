package kr.co.pal.listview.fellow.follower;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by JiHoon on 2017. 1. 19..
 */

public class FollowerItems {
    private Bitmap image;
    private String name;
    private String btnText="팔로우";


    @Override
    public String toString() {
        return "FollowerItems{" +
                "image=" + image +
                ", name='" + name + '\'' +
                '}';
    }

    public String getBtnText() {
        return btnText;
    }

    public void setBtnText(String btnText) {
        this.btnText = btnText;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
