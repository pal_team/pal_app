package kr.co.pal.util;

import android.graphics.Bitmap;

import java.util.Map;

/**
 * Created by JiHoon on 2017. 2. 10..
 */

public class ImageMapper {
    private long updateTime;
    private String id;
    private Bitmap image;

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
