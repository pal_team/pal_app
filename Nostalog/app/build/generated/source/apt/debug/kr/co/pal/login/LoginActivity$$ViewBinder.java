// Generated code from Butter Knife. Do not modify!
package kr.co.pal.login;

import android.content.res.Resources;
import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LoginActivity$$ViewBinder<T extends kr.co.pal.login.LoginActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689692, "field 'joinUs'");
    target.joinUs = finder.castView(view, 2131689692, "field 'joinUs'");
    Resources res = finder.getContext(source).getResources();
    target.url = res.getString(2131296428);
  }

  @Override public void unbind(T target) {
    target.joinUs = null;
  }
}
