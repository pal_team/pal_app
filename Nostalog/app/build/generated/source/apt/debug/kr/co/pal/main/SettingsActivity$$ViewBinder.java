// Generated code from Butter Knife. Do not modify!
package kr.co.pal.main;

import android.content.res.Resources;
import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SettingsActivity$$ViewBinder<T extends kr.co.pal.main.SettingsActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689704, "field 'userImg'");
    target.userImg = finder.castView(view, 2131689704, "field 'userImg'");
    view = finder.findRequiredView(source, 2131689705, "field 'logOut'");
    target.logOut = finder.castView(view, 2131689705, "field 'logOut'");
    view = finder.findRequiredView(source, 2131689673, "field 'idText'");
    target.idText = finder.castView(view, 2131689673, "field 'idText'");
    view = finder.findRequiredView(source, 2131689706, "field 'nameText'");
    target.nameText = finder.castView(view, 2131689706, "field 'nameText'");
    view = finder.findRequiredView(source, 2131689707, "field 'nickNameText'");
    target.nickNameText = finder.castView(view, 2131689707, "field 'nickNameText'");
    view = finder.findRequiredView(source, 2131689676, "field 'password'");
    target.password = finder.castView(view, 2131689676, "field 'password'");
    view = finder.findRequiredView(source, 2131689677, "field 'password2'");
    target.password2 = finder.castView(view, 2131689677, "field 'password2'");
    view = finder.findRequiredView(source, 2131689678, "field 'checkPassword'");
    target.checkPassword = finder.castView(view, 2131689678, "field 'checkPassword'");
    view = finder.findRequiredView(source, 2131689684, "field 'emailText'");
    target.emailText = finder.castView(view, 2131689684, "field 'emailText'");
    view = finder.findRequiredView(source, 2131689687, "field 'submit'");
    target.submit = finder.castView(view, 2131689687, "field 'submit'");
    Resources res = finder.getContext(source).getResources();
    target.warning = res.getColor(2131558409);
    target.alert = res.getColor(2131558400);
    target.green = res.getColor(2131558407);
    target.url = res.getString(2131296428);
    target.firebaseStorage = res.getString(2131296421);
  }

  @Override public void unbind(T target) {
    target.userImg = null;
    target.logOut = null;
    target.idText = null;
    target.nameText = null;
    target.nickNameText = null;
    target.password = null;
    target.password2 = null;
    target.checkPassword = null;
    target.emailText = null;
    target.submit = null;
  }
}
