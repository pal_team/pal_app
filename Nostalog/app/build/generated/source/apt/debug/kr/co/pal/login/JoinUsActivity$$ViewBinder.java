// Generated code from Butter Knife. Do not modify!
package kr.co.pal.login;

import android.content.res.Resources;
import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class JoinUsActivity$$ViewBinder<T extends kr.co.pal.login.JoinUsActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689673, "field 'idText'");
    target.idText = finder.castView(view, 2131689673, "field 'idText'");
    view = finder.findRequiredView(source, 2131689674, "field 'searchId'");
    target.searchId = finder.castView(view, 2131689674, "field 'searchId'");
    view = finder.findRequiredView(source, 2131689675, "field 'checkId'");
    target.checkId = finder.castView(view, 2131689675, "field 'checkId'");
    view = finder.findRequiredView(source, 2131689676, "field 'password'");
    target.password = finder.castView(view, 2131689676, "field 'password'");
    view = finder.findRequiredView(source, 2131689677, "field 'password2'");
    target.password2 = finder.castView(view, 2131689677, "field 'password2'");
    view = finder.findRequiredView(source, 2131689678, "field 'checkPassword'");
    target.checkPassword = finder.castView(view, 2131689678, "field 'checkPassword'");
    view = finder.findRequiredView(source, 2131689679, "field 'firstNameText'");
    target.firstNameText = finder.castView(view, 2131689679, "field 'firstNameText'");
    view = finder.findRequiredView(source, 2131689680, "field 'lastNameText'");
    target.lastNameText = finder.castView(view, 2131689680, "field 'lastNameText'");
    view = finder.findRequiredView(source, 2131689681, "field 'ageText'");
    target.ageText = finder.castView(view, 2131689681, "field 'ageText'");
    view = finder.findRequiredView(source, 2131689684, "field 'emailText'");
    target.emailText = finder.castView(view, 2131689684, "field 'emailText'");
    view = finder.findRequiredView(source, 2131689685, "field 'nickNameText'");
    target.nickNameText = finder.castView(view, 2131689685, "field 'nickNameText'");
    view = finder.findRequiredView(source, 2131689682, "field 'male'");
    target.male = finder.castView(view, 2131689682, "field 'male'");
    view = finder.findRequiredView(source, 2131689683, "field 'female'");
    target.female = finder.castView(view, 2131689683, "field 'female'");
    view = finder.findRequiredView(source, 2131689686, "field 'locale'");
    target.locale = finder.castView(view, 2131689686, "field 'locale'");
    view = finder.findRequiredView(source, 2131689687, "field 'submitBtn'");
    target.submitBtn = finder.castView(view, 2131689687, "field 'submitBtn'");
    Resources res = finder.getContext(source).getResources();
    target.warning = res.getColor(2131558409);
    target.alert = res.getColor(2131558400);
    target.green = res.getColor(2131558407);
    target.url = res.getString(2131296428);
  }

  @Override public void unbind(T target) {
    target.idText = null;
    target.searchId = null;
    target.checkId = null;
    target.password = null;
    target.password2 = null;
    target.checkPassword = null;
    target.firstNameText = null;
    target.lastNameText = null;
    target.ageText = null;
    target.emailText = null;
    target.nickNameText = null;
    target.male = null;
    target.female = null;
    target.locale = null;
    target.submitBtn = null;
  }
}
