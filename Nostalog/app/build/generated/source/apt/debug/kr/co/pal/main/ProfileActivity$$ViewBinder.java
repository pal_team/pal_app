// Generated code from Butter Knife. Do not modify!
package kr.co.pal.main;

import android.content.res.Resources;
import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProfileActivity$$ViewBinder<T extends kr.co.pal.main.ProfileActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689694, "field 'mMapView'");
    target.mMapView = finder.castView(view, 2131689694, "field 'mMapView'");
    view = finder.findRequiredView(source, 2131689699, "field 'mListView'");
    target.mListView = finder.castView(view, 2131689699, "field 'mListView'");
    view = finder.findRequiredView(source, 2131689673, "field 'idText'");
    target.idText = finder.castView(view, 2131689673, "field 'idText'");
    view = finder.findRequiredView(source, 2131689698, "field 'numOfNote'");
    target.numOfNote = finder.castView(view, 2131689698, "field 'numOfNote'");
    view = finder.findRequiredView(source, 2131689697, "field 'profileImg'");
    target.profileImg = finder.castView(view, 2131689697, "field 'profileImg'");
    Resources res = finder.getContext(source).getResources();
    target.url = res.getString(2131296428);
  }

  @Override public void unbind(T target) {
    target.mMapView = null;
    target.mListView = null;
    target.idText = null;
    target.numOfNote = null;
    target.profileImg = null;
  }
}
