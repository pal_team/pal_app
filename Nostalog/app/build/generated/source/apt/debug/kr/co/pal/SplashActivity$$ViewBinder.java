// Generated code from Butter Knife. Do not modify!
package kr.co.pal;

import android.content.res.Resources;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SplashActivity$$ViewBinder<T extends kr.co.pal.SplashActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    Resources res = finder.getContext(source).getResources();
    target.url = res.getString(2131296428);
  }

  @Override public void unbind(T target) {
  }
}
